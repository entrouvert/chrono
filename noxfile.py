import shlex
from pathlib import Path

import nox


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def get_lasso3(session):
    src_dir = Path('/usr/lib/python3/dist-packages/')
    venv_dir = Path(session.virtualenv.location)
    for dst_dir in venv_dir.glob('lib/**/site-packages'):
        files_to_link = [src_dir / 'lasso.py'] + list(src_dir.glob('_lasso.cpython-*.so'))

        for src_file in files_to_link:
            dst_file = dst_dir / src_file.name
            if dst_file.exists():
                dst_file.unlink()
            session.log('%s => %s', dst_file, src_file)
            dst_file.symlink_to(src_file)


def setup_venv(session, *packages, django_version='>=4.2,<4.3'):
    packages = [
        f'django{django_version}',
        'WebTest',
        'django-mellon>=1.6.1',
        'django-webtest',
        'git+https://git.entrouvert.org/entrouvert/publik-django-templatetags.git',
        'httmock',
        'pytest',
        'pytest-freezer',
        'pytz',
        'pyquery',
        'responses',
        'git+https://git.entrouvert.org/entrouvert/debian-django-ckeditor.git',
        'git+https://git.entrouvert.org/entrouvert/gadjo.git',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)
    get_lasso3(session)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session()
@nox.parametrize('django,drf', [('>=4.2,<4.3', '>=3.14,<3.15')])
def tests(session, django, drf):
    setup_venv(
        session,
        'pytest-cov',
        'pytest-django',
        'pytest-xdist',
        'mock<4',
        'psycopg2-binary',
        'django-filter>=2.4,<23.2',
        'weasyprint<52',
        f'djangorestframework{drf}',  # matching debian bullseye,
        django_version=django,
    )

    session.run('python3', 'setup.py', 'compile_translations')

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov-context',
            'test',
            '--cov=chrono/',
            '--cov-config',
            '.coveragerc',
            '-v',
            f'--junitxml=junit-coverage.django-{django}.xml',
        ]

    if not session.interactive:
        args += ['-v', '--numprocesses', '6']

    args += session.posargs + ['tests/']

    hookable_run(
        session,
        *args,
        env={
            'DJANGO_SETTINGS_MODULE': 'chrono.settings',
            'CHRONO_SETTINGS_FILE': 'tests/settings.py',
            'DB_ENGINE': 'django.db.backends.postgresql_psycopg2',
        },
    )


@nox.session
def pylint(session):
    setup_venv(session, 'pylint', 'pylint-django', 'nox')
    pylint_command = ['pylint', '--jobs', '6', '-f', 'parseable', '--rcfile', 'pylint.rc']

    if not session.posargs:
        pylint_command += ['chrono/', 'tests/', 'noxfile.py']
    else:
        pylint_command += session.posargs

    if not session.interactive:
        session.run(
            'bash',
            '-c',
            f'{shlex.join(pylint_command)} | tee pylint.out ; test $PIPESTATUS -eq 0',
            external=True,
        )
    else:
        session.run(*pylint_command)


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
        'chrono/manager/static/css/*.css',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
