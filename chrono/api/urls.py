# chrono - agendas system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, path, re_path

from . import views

urlpatterns = [
    path('agenda/', views.agendas),
    path('agendas/datetimes/', views.agendas_datetimes, name='api-agendas-datetimes'),
    path('agendas/recurring-events/', views.recurring_events_list, name='api-agenda-recurring-events'),
    path('agendas/recurring-events/fillslots/', views.recurring_fillslots, name='api-recurring-fillslots'),
    path(
        'agendas/recurring-events/fillslots-by-day/',
        views.recurring_fillslots_by_day,
        name='api-recurring-fillslots-by-day',
    ),
    path(
        'agendas/events/',
        views.agendas_events,
        name='api-agendas-events',
    ),
    path(
        'agendas/events/fillslots/',
        views.agendas_events_fillslots,
        name='api-agendas-events-fillslots',
    ),
    path(
        'agendas/events/fillslots/<uuid:request_uuid>/revert/',
        views.agendas_events_fillslots_revert,
        name='api-agendas-events-fillslots-revert',
    ),
    path(
        'agendas/events/check-status/',
        views.agendas_events_check_status,
        name='api-agendas-events-check-status',
    ),
    path(
        'agendas/events/check-lock/',
        views.agendas_events_check_lock,
        name='api-agendas-events-check-lock',
    ),
    path(
        'agendas/events/invoiced/',
        views.agendas_events_invoiced,
        name='api-agendas-events-invoiced',
    ),
    re_path(r'^agenda/(?P<agenda_identifier>[\w-]+)/$', views.agenda),
    re_path(r'^agenda/(?P<agenda_identifier>[\w-]+)/duplicate/$', views.duplicate),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/datetimes/$', views.datetimes, name='api-agenda-datetimes'
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/fillslot/(?P<event_identifier>[\w:-]+)/$',
        views.fillslot,
        name='api-fillslot',
    ),
    path(
        'agenda/<slug:agenda_identifier>/free-range/fillslot/',
        views.partial_bookings_meeting_fillslot,
        name='api-agenda-partial-bookings-meeting-fillslot',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/events/fillslots/$',
        views.events_fillslots,
        name='api-agenda-events-fillslots',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/event/$',
        views.events,
        name='api-events',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/event/(?P<event_identifier>[\w:-]+)/$',
        views.event,
        name='api-event',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/status/(?P<event_identifier>[\w:-]+)/$',
        views.event_status,
        name='api-event-status',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/bookings/(?P<event_identifier>[\w:-]+)/$',
        views.event_bookings,
        name='api-event-bookings',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/check/(?P<event_identifier>[\w:-]+)/$',
        views.event_check,
        name='api-event-check',
    ),
    re_path(
        r'^agenda/meetings/(?P<meeting_identifier>[\w-]+)/datetimes/$',
        views.meeting_datetimes,
        name='api-agenda-meeting-datetimes-legacy',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/meetings/$', views.meeting_list, name='api-agenda-meetings'
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/meetings/(?P<meeting_identifier>[\w-]+)/$',
        views.meeting_info,
        name='api-agenda-meetings',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/shared-resources/$',
        views.agenda_shared_resource_list,
        name='api-agenda-shared-resources',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/resources/$',
        views.agenda_resource_list,
        name='api-agenda-resources',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/desks/$', views.agenda_desk_list, name='api-agenda-desks'
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/meetings/(?P<meeting_identifier>[\w-]+)/datetimes/$',
        views.meeting_datetimes,
        name='api-agenda-meeting-datetimes',
    ),
    path(
        'agenda/<slug:agenda_identifier>/free-range/datetimes/',
        views.partial_bookings_meeting_datetimes,
        name='api-agenda-partial-bookings-meeting-datetimes',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/subscription/$',
        views.subscriptions,
        name='api-agenda-subscriptions',
    ),
    re_path(
        r'^agenda/(?P<agenda_identifier>[\w-]+)/subscription/(?P<subscription_pk>\d+)/$',
        views.subscription,
        name='api-agenda-subscription',
    ),
    path(
        'agenda/<slug:agenda_identifier>/partial-bookings-check/',
        views.partial_bookings_check,
        name='api-partial-bookings-check',
    ),
    path('bookings/', views.bookings, name='api-bookings'),
    path('bookings/ics/', views.bookings_ics, name='api-bookings-ics'),
    path('booking/<int:booking_pk>/', views.booking, name='api-booking'),
    path('booking/<int:booking_pk>/cancel/', views.cancel_booking, name='api-cancel-booking'),
    path('booking/<int:booking_pk>/accept/', views.accept_booking, name='api-accept-booking'),
    path('booking/<int:booking_pk>/anonymize/', views.anonymize_booking, name='api-anonymize-booking'),
    path('booking/<int:booking_pk>/suspend/', views.suspend_booking, name='api-suspend-booking'),
    path('booking/<int:booking_pk>/resize/', views.resize_booking, name='api-resize-booking'),
    path('booking/<int:booking_pk>/ics/', views.booking_ics, name='api-booking-ics'),
    path('shared-custody/', views.shared_custody_agendas, name='api-shared-custody-agendas'),
    path(
        'shared-custody/<int:agenda_pk>/',
        views.shared_custody_agenda,
        name='api-shared-custody-agenda',
    ),
    path('statistics/', views.statistics_list, name='api-statistics-list'),
    path('statistics/bookings/', views.bookings_statistics, name='api-statistics-bookings'),
    path(
        'statistics/opening-and-booking-time/',
        views.opening_and_booking_statistics,
        name='api-statistics-opening-and-booking-time',
    ),
    path('statistics/fill/', views.fill_statistics, name='api-statistics-fill'),
    path('ants/', include('chrono.apps.ants_hub.api_urls')),
    path('user-preferences/', include('chrono.apps.user_preferences.api_urls')),
]
