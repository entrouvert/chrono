import collections
import datetime
import re

from django.contrib.auth.models import Group
from django.db import models, transaction
from django.db.models import ExpressionWrapper, F, Q
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from chrono.agendas.models import (
    Agenda,
    Booking,
    BookingColor,
    Category,
    Desk,
    Event,
    EventsType,
    Person,
    SharedCustodyAgenda,
    SharedCustodyHolidayRule,
    SharedCustodyRule,
    Subscription,
    TimePeriodExceptionGroup,
)
from chrono.utils.lingo import get_agenda_check_types


def get_objects_from_slugs(slugs, qs):
    slugs = set(slugs)
    objects = qs.filter(slug__in=slugs)
    if len(objects) != len(slugs):
        unknown_slugs = sorted(slugs - {obj.slug for obj in objects})
        unknown_slugs = ', '.join(unknown_slugs)
        raise ValidationError(('invalid slugs: %s') % unknown_slugs)
    return objects


class StringOrListField(serializers.ListField):
    def to_internal_value(self, data):
        if isinstance(data, str):
            data = [s.strip() for s in data.split(',') if s.strip()]
        return super().to_internal_value(data)


class PhoneNumbersStringOrListField(serializers.ListField):
    def to_internal_value(self, data):
        if isinstance(data, str):
            data = [s.strip() for s in data.split(',') if s.strip()]
        # strip white spaces and dots
        data = [re.sub(r'[\s\.]', '', x) for x in data]
        return super().to_internal_value(data)


class CommaSeparatedStringField(serializers.ListField):
    def get_value(self, dictionary):
        return super(serializers.ListField, self).get_value(dictionary)

    def to_internal_value(self, data):
        data = [s.strip() for s in data.split(',') if s.strip()]
        return super().to_internal_value(data)


class DateRangeMixin(metaclass=serializers.SerializerMetaclass):
    datetime_formats = ['%Y-%m-%d', '%Y-%m-%d %H:%M', 'iso-8601']

    date_start = serializers.DateTimeField(required=False, input_formats=datetime_formats)
    date_end = serializers.DateTimeField(required=False, input_formats=datetime_formats)


class AgendaSlugsMixin(metaclass=serializers.SerializerMetaclass):
    agendas = CommaSeparatedStringField(
        required=False, child=serializers.SlugField(max_length=160, allow_blank=False)
    )

    def get_agenda_qs(self):
        return Agenda.objects.filter(kind='events').select_related('events_type')


class FillSlotSerializer(serializers.Serializer):
    label = serializers.CharField(max_length=250, allow_blank=True)
    user_external_id = serializers.CharField(max_length=250, allow_blank=True)
    user_name = serializers.CharField(max_length=250, allow_blank=True)  # compatibility
    user_first_name = serializers.CharField(max_length=250, allow_blank=True)
    user_last_name = serializers.CharField(max_length=250, allow_blank=True)
    user_display_label = serializers.CharField(max_length=250, allow_blank=True)
    user_email = serializers.CharField(max_length=250, allow_blank=True)
    user_phone_number = serializers.CharField(max_length=30, allow_blank=True)
    exclude_user = serializers.BooleanField(default=False)
    events = serializers.CharField(max_length=16, allow_blank=True)
    bypass_delays = serializers.BooleanField(default=False)
    form_url = serializers.CharField(max_length=500, allow_blank=True)
    backoffice_url = serializers.URLField(allow_blank=True, max_length=500)
    cancel_callback_url = serializers.URLField(allow_blank=True, max_length=500)
    presence_callback_url = serializers.URLField(allow_blank=True, max_length=500)
    absence_callback_url = serializers.URLField(allow_blank=True, max_length=500)
    count = serializers.IntegerField(min_value=1)
    cancel_booking_id = serializers.CharField(max_length=250, allow_blank=True, allow_null=True)
    force_waiting_list = serializers.BooleanField(default=False)
    use_color_for = serializers.CharField(max_length=250, allow_blank=True)
    extra_emails = StringOrListField(
        required=False, child=serializers.EmailField(max_length=250, allow_blank=False)
    )
    extra_phone_numbers = PhoneNumbersStringOrListField(
        required=False, child=serializers.CharField(max_length=16, allow_blank=False)
    )
    check_overlaps = serializers.BooleanField(default=False)
    start_time = serializers.TimeField(required=False, allow_null=True)
    end_time = serializers.TimeField(required=False, allow_null=True)
    lock_code = serializers.CharField(max_length=64, required=False, allow_blank=False, trim_whitespace=True)
    confirm_after_lock = serializers.BooleanField(default=False)
    sms_counter = serializers.CharField(max_length=250, required=False, allow_blank=True)


class EventsFillSlotSerializer(FillSlotSerializer):
    def validate(self, attrs):
        super().validate(attrs)
        use_partial_bookings = any(agenda.partial_bookings for agenda in self.context.get('agendas', []))
        if use_partial_bookings:
            if not attrs.get('start_time') or not attrs.get('end_time'):
                raise ValidationError(_('must include start_time and end_time for partial bookings agenda'))
            if attrs['start_time'] > attrs['end_time']:
                raise ValidationError(_('start_time must be before end_time'))
        return attrs


class PartialBookingsMeetingFillSlotSerializer(FillSlotSerializer):
    start_datetime = serializers.DateTimeField(input_formats=['%Y-%m-%d %H:%M', 'iso-8601'])
    end_datetime = serializers.DateTimeField(input_formats=['%Y-%m-%d %H:%M', 'iso-8601'])
    resource = serializers.SlugRelatedField(queryset=Desk.objects.none(), slug_field='slug')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['resource'].queryset = Desk.objects.filter(agenda=self.context['agenda'])

    def validate(self, attrs):
        super().validate(attrs)
        agenda = self.context['agenda']

        start_datetime = attrs.get('start_datetime')
        end_datetime = attrs.get('end_datetime')
        if not start_datetime or not end_datetime:
            raise ValidationError(_('Missing start_datetime and/or end_datetime parameters.'))

        if start_datetime > end_datetime:
            raise ValidationError(_('start_datetime must be before end_datetime.'))

        if start_datetime.date() != end_datetime.date():
            raise ValidationError(_('start_datetime and end_datetime must be the same day.'))

        duration = (end_datetime - start_datetime).seconds // 60
        if agenda.minimal_booking_duration and duration < agenda.minimal_booking_duration:
            raise ValidationError(_('Booking duration is too short.'))

        if agenda.maximal_booking_duration and duration > agenda.maximal_booking_duration:
            raise ValidationError(_('Booking duration is too long.'))

        return attrs


class SlotsSerializer(serializers.Serializer):
    slots = StringOrListField(required=True, child=serializers.CharField(max_length=160, allow_blank=False))

    def validate(self, attrs):
        super().validate(attrs)
        if not attrs.get('slots'):
            raise serializers.ValidationError({'slots': _('This field is required.')})
        return attrs


class FillSlotsSerializer(FillSlotSerializer, SlotsSerializer):
    pass


class EventsFillSlotsSerializer(EventsFillSlotSerializer):
    slots = StringOrListField(required=True, child=serializers.CharField(max_length=160))

    def validate(self, attrs):
        super().validate(attrs)
        if 'slots' not in attrs:
            raise serializers.ValidationError({'slots': _('This field is required.')})
        if not attrs.get('user_external_id'):
            raise serializers.ValidationError({'user_external_id': _('This field is required.')})
        if 'exclude_user' in attrs:
            raise serializers.ValidationError({'exclude_user': _('This parameter is not supported.')})
        return attrs


class MultipleAgendasEventsFillSlotsSerializer(EventsFillSlotsSerializer):
    def validate_slots(self, value):
        allowed_agenda_slugs = self.context['allowed_agenda_slugs']
        slots_agenda_slugs = set()
        for slot in value:
            try:
                agenda_slug, event_slug = slot.split('@')
            except ValueError:
                raise ValidationError(_('Invalid format for slot %s') % slot)
            if not agenda_slug:
                raise ValidationError(_('Missing agenda slug in slot %s') % slot)
            if not event_slug:
                raise ValidationError(_('Missing event slug in slot %s') % slot)
            slots_agenda_slugs.add(agenda_slug)

        extra_agendas = slots_agenda_slugs - set(allowed_agenda_slugs)
        if extra_agendas:
            extra_agendas = ', '.join(sorted(extra_agendas))
            raise ValidationError(_('Events from the following agendas cannot be booked: %s') % extra_agendas)
        return value


class MultipleAgendasEventsCheckStatusSerializer(AgendaSlugsMixin, DateRangeMixin, serializers.Serializer):
    user_external_id = serializers.CharField(required=False, max_length=250, allow_blank=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in ['agendas', 'user_external_id', 'date_start', 'date_end']:
            self.fields[field].required = True

    def validate_agendas(self, value):
        return get_objects_from_slugs(value, qs=self.get_agenda_qs())


class MultipleAgendasEventsCheckLockSerializer(AgendaSlugsMixin, DateRangeMixin, serializers.Serializer):
    check_locked = serializers.BooleanField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in ['agendas', 'date_start', 'date_end', 'check_locked']:
            self.fields[field].required = True

    def validate_agendas(self, value):
        return get_objects_from_slugs(value, qs=self.get_agenda_qs())


class MultipleAgendasEventsInvoicedSerializer(AgendaSlugsMixin, DateRangeMixin, serializers.Serializer):
    invoiced = serializers.BooleanField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in ['agendas', 'date_start', 'date_end', 'invoiced']:
            self.fields[field].required = True

    def validate_agendas(self, value):
        return get_objects_from_slugs(value, qs=self.get_agenda_qs())


class RecurringFillslotsSerializer(MultipleAgendasEventsFillSlotsSerializer):
    include_booked_events_detail = serializers.BooleanField(default=False)
    check_overlaps = CommaSeparatedStringField(
        required=False, child=serializers.SlugField(max_length=160, allow_blank=False)
    )

    def validate_slots(self, value):
        super().validate_slots(value)
        self.initial_slots = value
        open_event_slugs = collections.defaultdict(set)
        for agenda in self.context['agendas']:
            for event in agenda.get_open_recurring_events():
                open_event_slugs[agenda.slug].add(event.slug)

        slots = collections.defaultdict(lambda: collections.defaultdict(list))
        for slot in value:
            try:
                slugs, day = slot.split(':')
                day = int(day)
            except ValueError:
                raise ValidationError(_('invalid slot: %s') % slot)

            agenda_slug, event_slug = slugs.split('@')
            if event_slug not in open_event_slugs[agenda_slug]:
                raise ValidationError(
                    _('event %(event_slug)s of agenda %(agenda_slug)s is not bookable')
                    % {'event_slug': event_slug, 'agenda_slug': agenda_slug}
                )

            slots[agenda_slug][event_slug].append(day)

        return slots


class RecurringFillslotsByDaySerializer(FillSlotSerializer):
    include_booked_events_detail = serializers.BooleanField(default=False)
    weekdays = {
        'monday': 1,
        'tuesday': 2,
        'wednesday': 3,
        'thursday': 4,
        'friday': 5,
        'saturday': 6,
        'sunday': 7,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for weekday in self.weekdays:
            self.fields[weekday] = CommaSeparatedStringField(
                child=serializers.TimeField(), required=False, min_length=2, max_length=2, allow_null=True
            )
            setattr(self, 'validate_%s' % weekday, self.validate_hour_range)

    def validate_hour_range(self, value):
        if not value:
            return None

        start_time, end_time = value
        if start_time >= end_time:
            raise ValidationError(_('Start hour must be before end hour.'))
        return value

    def validate(self, attrs):
        agendas = self.context['agendas']
        if len(agendas) > 1:
            raise ValidationError('Multiple agendas are not supported.')
        agenda = agendas[0]

        if not agenda.partial_bookings:
            raise ValidationError('Agenda kind must be partial bookings.')

        attrs['hours_by_days'] = hours_by_days = {}
        for weekday, weekday_index in self.weekdays.items():
            if attrs.get(weekday):
                hours_by_days[weekday_index] = attrs[weekday]

        days_by_event = collections.defaultdict(list)
        for event in agenda.get_open_recurring_events():
            for day in event.recurrence_days:
                if day in hours_by_days:
                    days_by_event[event.slug].append(day)
        attrs['slots'] = {agenda.slug: days_by_event}

        return attrs

    def to_internal_value(self, data):
        allowed_blank_fields = list(self.weekdays) + ['start_time', 'end_time']
        for field in allowed_blank_fields:
            if data.get(field) == '':
                data[field] = None

        return super().to_internal_value(data)


class BookingSerializer(serializers.ModelSerializer):
    user_was_present = serializers.BooleanField(required=False, allow_null=True)
    user_absence_reason = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    user_presence_reason = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    use_color_for = serializers.CharField(required=False, allow_blank=True, allow_null=True, source='color')

    class Meta:
        model = Booking
        fields = [
            'id',
            'in_waiting_list',
            'user_first_name',
            'user_last_name',
            'user_email',
            'user_phone_number',
            'user_was_present',
            'user_absence_reason',
            'user_presence_reason',
            'use_color_for',
            'extra_data',
            'creation_datetime',
            'cancellation_datetime',
            'label',
        ]
        read_only_fields = [
            'id',
            'in_waiting_list',
            'extra_data',
            'creation_datetime',
            'cancellation_datetime',
        ]

    def __init__(self, *args, **kwargs):
        self.user_check = kwargs.pop('user_check', None)
        super().__init__(*args, **kwargs)

    def to_internal_value(self, data):
        if 'color' in data:
            # legacy
            data['use_color_for'] = data['color']
            del data['color']
        return super().to_internal_value(data)

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        if self.instance.event.agenda.kind != 'events':
            ret['desk'] = {
                'slug': self.instance.event.desk.slug,
                'label': self.instance.event.desk.label,
            }
            ret.pop('user_was_present', None)
            ret.pop('user_absence_reason', None)
            ret.pop('user_presence_reason', None)
        else:
            user_was_present = self.user_check.presence if self.user_check else None
            ret['user_was_present'] = user_was_present
            ret['user_absence_reason'] = self.user_check.type_slug if user_was_present is False else None
            ret['user_presence_reason'] = self.user_check.type_slug if user_was_present is True else None
        if self.instance.event.agenda.kind == 'events' and self.instance.event.agenda.partial_bookings:
            self.instance.user_check_start_time = self.user_check.start_time if self.user_check else None
            self.instance.user_check_end_time = self.user_check.end_time if self.user_check else None
            self.instance.computed_start_time = (
                self.user_check.computed_start_time if self.user_check else None
            )
            self.instance.computed_end_time = self.user_check.computed_end_time if self.user_check else None
            # adjust start_time (in case of multi checks)
            self.instance.adjusted_start_time = self.instance.start_time
            if (
                self.instance.start_time
                and self.instance.computed_start_time
                and self.instance.start_time < self.instance.computed_start_time
            ):
                self.instance.adjusted_start_time = self.instance.computed_start_time
            # and end_time
            self.instance.adjusted_end_time = self.instance.end_time
            if (
                self.instance.end_time
                and self.instance.computed_end_time
                and self.instance.end_time > self.instance.computed_end_time
            ):
                self.instance.adjusted_end_time = self.instance.computed_end_time
            for key in ['', 'user_check_', 'computed_', 'adjusted_']:
                start_key, end_key, minutes_key = (
                    '%sstart_time' % key,
                    '%send_time' % key,
                    '%sduration' % key,
                )
                ret[start_key] = getattr(self.instance, start_key)
                ret[end_key] = getattr(self.instance, end_key)
                ret[minutes_key] = None
                if (
                    getattr(self.instance, start_key) is not None
                    and getattr(self.instance, end_key) is not None
                ):
                    start_minutes = (
                        getattr(self.instance, start_key).hour * 60 + getattr(self.instance, start_key).minute
                    )
                    end_minutes = (
                        getattr(self.instance, end_key).hour * 60 + getattr(self.instance, end_key).minute
                    )
                    ret[minutes_key] = end_minutes - start_minutes
        return ret

    def _validate_check_type(self, kind, value):
        if not value:
            return None

        error_messages = {
            'absence': _('unknown absence reason'),
            'presence': _('unknown presence reason'),
        }

        check_types = get_agenda_check_types(self.instance.event.agenda)
        for check_type in check_types:
            if check_type.kind != kind:
                continue
            if value in [check_type.slug, check_type.label]:
                return check_type
        raise serializers.ValidationError(error_messages[kind])

    def validate_user_absence_reason(self, value):
        return self._validate_check_type('absence', value)

    def validate_user_presence_reason(self, value):
        return self._validate_check_type('presence', value)

    def validate_use_color_for(self, value):
        if value:
            return BookingColor.objects.get_or_create(label=value)[0]

    def validate(self, attrs):
        super().validate(attrs)
        if 'user_absence_reason' in attrs and 'user_presence_reason' in attrs:
            raise ValidationError(
                {'user_absence_reason': _('can not set user_absence_reason and user_presence_reason')}
            )
        if 'user_absence_reason' in attrs:
            check_type = attrs['user_absence_reason']
            attrs['user_check_type_slug'] = check_type.slug if check_type else None
            attrs['user_check_type_label'] = check_type.label if check_type else None
            del attrs['user_absence_reason']
        elif 'user_presence_reason' in attrs:
            check_type = attrs['user_presence_reason']
            attrs['user_check_type_slug'] = check_type.slug if check_type else None
            attrs['user_check_type_label'] = check_type.label if check_type else None
            del attrs['user_presence_reason']
        return attrs


class ResizeSerializer(serializers.Serializer):
    count = serializers.IntegerField(min_value=1)


class PartialBookingsCheckSerializer(serializers.Serializer):
    user_external_id = serializers.CharField(max_length=250, allow_blank=True)
    timestamp = serializers.DateTimeField(input_formats=['iso-8601', '%Y-%m-%d'])


class StatisticsFiltersSerializer(serializers.Serializer):
    time_interval = serializers.ChoiceField(choices=('day', _('Day')), default='day')
    start = serializers.DateTimeField(required=False, input_formats=['iso-8601', '%Y-%m-%d'])
    end = serializers.DateTimeField(required=False, input_formats=['iso-8601', '%Y-%m-%d'])
    agenda = serializers.CharField(required=False, allow_blank=False, max_length=256)
    group_by = serializers.ListField(
        required=False, child=serializers.SlugField(allow_blank=False, max_length=256)
    )


class FillStatisticsFiltersSerializer(serializers.Serializer):
    time_interval = serializers.ChoiceField(choices=('month', _('Month')), default='month')
    start = serializers.DateTimeField(required=False, input_formats=['iso-8601', '%Y-%m-%d'])
    end = serializers.DateTimeField(required=False, input_formats=['iso-8601', '%Y-%m-%d'])
    agenda = serializers.CharField(required=True, allow_blank=False, max_length=256)


class DateRangeSerializer(DateRangeMixin, serializers.Serializer):
    pass


class DatetimesSerializer(DateRangeSerializer):
    min_places = serializers.IntegerField(min_value=1, default=1)
    max_places = serializers.IntegerField(min_value=1, default=None)
    user_external_id = serializers.CharField(required=False, max_length=250, allow_blank=True)
    exclude_user_external_id = serializers.CharField(required=False, max_length=250, allow_blank=True)
    events = serializers.CharField(required=False, max_length=32, allow_blank=True)
    hide_disabled = serializers.BooleanField(default=False)
    bypass_delays = serializers.BooleanField(default=False)
    lock_code = serializers.CharField(max_length=64, required=False, allow_blank=False, trim_whitespace=True)

    def validate(self, attrs):
        super().validate(attrs)
        if (
            'user_external_id' in attrs
            and 'exclude_user_external_id' in attrs
            and attrs['user_external_id'] != attrs['exclude_user_external_id']
        ):
            raise ValidationError(
                {'user_external_id': _('user_external_id and exclude_user_external_id have different values')}
            )
        return attrs


class AgendaOrSubscribedSlugsMixin(AgendaSlugsMixin):
    subscribed = CommaSeparatedStringField(
        required=False, child=serializers.SlugField(max_length=160, allow_blank=False)
    )
    user_external_id = serializers.CharField(required=False, max_length=250, allow_blank=False)
    guardian_external_id = serializers.CharField(required=False, max_length=250, allow_blank=True)

    def validate(self, attrs):
        super().validate(attrs)
        if 'agendas' not in attrs and 'subscribed' not in attrs:
            raise ValidationError(_('Either "agendas" or "subscribed" parameter is required.'))
        if 'agendas' in attrs and 'subscribed' in attrs:
            raise ValidationError(_('"agendas" and "subscribed" parameters are mutually exclusive.'))
        user_external_id = attrs.get('user_external_id', self.context.get('user_external_id'))
        if 'subscribed' in attrs and not user_external_id:
            raise ValidationError(
                {'user_external_id': _('This field is required when using "subscribed" parameter.')}
            )
        if attrs.get('guardian_external_id') and not user_external_id:
            raise serializers.ValidationError(
                {'user_external_id': _('This field is required when using "guardian_external_id" parameter.')}
            )

        if 'subscribed' in attrs:
            lookups = {'subscriptions__user_external_id': user_external_id}
            if 'date_start' in attrs:
                # subscription must end after requested date_start
                lookups['subscriptions__date_end__gt'] = attrs['date_start']
            if 'date_end' in attrs:
                # subscription must start before requested date end
                lookups['subscriptions__date_start__lte'] = attrs['date_end']
            agendas = self.get_agenda_qs().filter(**lookups).distinct().select_related('category')

            if attrs['subscribed'] != ['all']:
                agendas = agendas.filter(category__slug__in=attrs['subscribed'])

            attrs['agendas'] = agendas
            attrs['agenda_slugs'] = [agenda.slug for agenda in agendas]
        else:
            attrs['agenda_slugs'] = self.agenda_slugs

        if any(
            agenda.partial_bookings != attrs['agendas'][0].partial_bookings for agenda in attrs['agendas']
        ):
            raise serializers.ValidationError(
                {'agendas': _('Cannot mix partial bookings agendas with other kinds.')}
            )

        return attrs

    def validate_agendas(self, value):
        self.agenda_slugs = value
        return get_objects_from_slugs(value, qs=self.get_agenda_qs())


class MultipleAgendasDatetimesSerializer(AgendaOrSubscribedSlugsMixin, DatetimesSerializer):
    show_past_events = serializers.BooleanField(default=False)
    with_status = serializers.BooleanField(default=False)
    check_overlaps = serializers.BooleanField(default=False)
    extra_data_keys = CommaSeparatedStringField(
        required=False, child=serializers.CharField(max_length=160, allow_blank=False)
    )

    def validate(self, attrs):
        super().validate(attrs)
        user_external_id = attrs.get('user_external_id')
        if attrs.get('with_status') and not user_external_id:
            raise ValidationError(
                {'user_external_id': _('This field is required when using "with_status" parameter.')}
            )
        return attrs


class AgendaOrSubscribedSlugsSerializer(AgendaOrSubscribedSlugsMixin, DateRangeMixin, serializers.Serializer):
    pass


class RecurringFillslotsQueryStringSerializer(AgendaOrSubscribedSlugsSerializer):
    action = serializers.ChoiceField(required=True, choices=['update', 'update-from-date', 'book', 'unbook'])


class RecurringEventsListSerializer(AgendaOrSubscribedSlugsSerializer):
    sort = serializers.ChoiceField(required=False, choices=['day'])
    check_overlaps = CommaSeparatedStringField(
        required=False, child=serializers.SlugField(max_length=160, allow_blank=False)
    )


class EventSerializer(serializers.ModelSerializer):
    recurrence_days = StringOrListField(
        required=False, child=serializers.IntegerField(min_value=0, max_value=6)
    )
    primary_event = serializers.SlugRelatedField(read_only=True, slug_field='slug')
    agenda = serializers.SlugRelatedField(read_only=True, slug_field='slug')

    class Meta:
        model = Event
        fields = [
            'start_datetime',
            'recurrence_days',
            'recurrence_week_interval',
            'recurrence_end_date',
            'duration',
            'publication_datetime',
            'places',
            'waiting_list_places',
            'label',
            'slug',
            'description',
            'pricing',
            'url',
            'primary_event',
            'agenda',
            'checked',
            'check_locked',
            'invoiced',
        ]
        read_only_fields = ['slug', 'checked', 'check_locked', 'invoiced']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if not self.instance.agenda.events_type:
            return
        field_classes = {
            'text': serializers.CharField,
            'textarea': serializers.CharField,
            'bool': serializers.BooleanField,
            'date': serializers.DateField,
        }
        field_options = {
            'text': {'allow_blank': True},
            'textarea': {'allow_blank': True},
            'bool': {'allow_null': True},
            'date': {'allow_null': True},
        }
        for custom_field in self.instance.agenda.events_type.get_custom_fields():
            field_class = field_classes[custom_field['field_type']]
            field_name = 'custom_field_%s' % custom_field['varname']
            self.fields[field_name] = field_class(
                required=False,
                read_only=self.instance.primary_event is not None,
                **(field_options.get(custom_field['field_type']) or {}),
            )

    def validate_recurrence_days(self, value):
        # keep stable weekday numbering after switch to ISO in db
        return [i + 1 for i in value]

    def validate(self, attrs):
        if not self.instance.agenda.events_type:
            return attrs
        if self.instance.primary_event:
            return attrs
        defaults = {
            'text': '',
            'textarea': '',
            'bool': None,
            'date': None,
        }
        custom_fields = self.instance.custom_fields
        for custom_field in self.instance.agenda.events_type.get_custom_fields():
            varname = custom_field['varname']
            field_name = 'custom_field_%s' % varname
            if varname not in custom_fields:
                # set default
                custom_fields[varname] = defaults[custom_field['field_type']]
            if field_name in attrs:
                custom_fields[varname] = attrs[field_name]
                del attrs[field_name]
        attrs['custom_fields'] = custom_fields
        return attrs

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        if ret.get('recurrence_days'):
            # keep stable weekday numbering after switch to ISO in db
            ret['recurrence_days'] = [i - 1 for i in ret['recurrence_days']]
        if not self.instance.agenda.events_type:
            return ret
        defaults = {
            'text': '',
            'textarea': '',
            'bool': None,
            'date': None,
        }
        custom_fields = self.instance.custom_fields
        for custom_field in self.instance.agenda.events_type.get_custom_fields():
            varname = custom_field['varname']
            field_name = 'custom_field_%s' % varname
            value = defaults[custom_field['field_type']]
            if varname in custom_fields:
                value = custom_fields[varname]
            ret[field_name] = value
        return ret


class AgendaSerializer(serializers.ModelSerializer):
    admin_role = serializers.CharField(required=False, max_length=150)
    edit_role = serializers.CharField(required=False, max_length=150)
    view_role = serializers.CharField(required=False, max_length=150)
    slug = serializers.SlugField(required=True, max_length=160)
    category = serializers.SlugField(required=False, max_length=160)
    events_type = serializers.SlugField(required=False, max_length=160)

    class Meta:
        model = Agenda
        fields = [
            'slug',
            'label',
            'kind',
            'partial_bookings',
            'minimal_booking_delay',
            'minimal_booking_delay_in_working_days',
            'maximal_booking_delay',
            'minimal_booking_time',
            'anonymize_delay',
            'admin_role',
            'edit_role',
            'view_role',
            'category',
            'booking_form_url',
            'mark_event_checked_auto',
            'disable_check_update',
            'booking_check_filters',
            'events_type',
            'event_display_template',
        ]

    def get_role(self, value):
        try:
            return Group.objects.get(name=value)
        except Group.DoesNotExist:
            raise serializers.ValidationError(_('unknown role: %s' % value))

    def validate_admin_role(self, value):
        return self.get_role(value)

    def validate_edit_role(self, value):
        return self.get_role(value)

    def validate_view_role(self, value):
        return self.get_role(value)

    def validate_category(self, value):
        try:
            return Category.objects.get(slug=value)
        except Category.DoesNotExist:
            raise serializers.ValidationError(_('unknown category: %s' % value))

    def validate_events_type(self, value):
        try:
            return EventsType.objects.get(slug=value)
        except EventsType.DoesNotExist:
            raise serializers.ValidationError(_('unknown events type: %s' % value))

    def validate(self, attrs):
        super().validate(attrs)
        if attrs.get('minimal_booking_delay_in_working_days') and attrs.get('kind', 'events') != 'events':
            raise ValidationError(
                {
                    'minimal_booking_delay_in_working_days': _('Option not available on %s agenda')
                    % attrs['kind']
                }
            )
        if attrs.get('events_type') and attrs.get('kind', 'events') != 'events':
            raise ValidationError({'events_type': _('Option not available on %s agenda') % attrs['kind']})
        if attrs.get('partial_bookings') and attrs.get('kind', 'events') != 'events':
            raise ValidationError(
                {'partial_bookings': _('Option not available on %s agenda') % attrs['kind']}
            )
        return attrs


class SubscriptionSerializer(serializers.ModelSerializer):
    check_out_of_period_bookings = serializers.BooleanField(initial=False, default=False)

    class Meta:
        model = Subscription
        fields = [
            'id',
            'user_external_id',
            'user_first_name',
            'user_last_name',
            'user_email',
            'user_phone_number',
            'date_start',
            'date_end',
            'extra_data',
            'check_out_of_period_bookings',
        ]
        read_only_fields = ['id', 'extra_data', 'check_out_of_period_bookings']

    def validate(self, attrs):
        super().validate(attrs)
        if attrs.get('date_start') and attrs.get('date_end') and attrs['date_start'] > attrs['date_end']:
            raise ValidationError(_('start_datetime must be before end_datetime'))
        return attrs

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret.pop('check_out_of_period_bookings', None)
        return ret


class SharedCustodyAgendaMixin:
    def validate(self, attrs):
        date_start = attrs['date_start']
        date_end = attrs.get('date_end')

        if date_end and date_start >= date_end:
            raise ValidationError(_('date_start must be before date_end'))

        child_id = self.get_child_id(attrs)
        overlapping_agendas = SharedCustodyAgenda.objects.filter(
            Q(date_end__gte=date_start) | Q(date_end__isnull=True), child__user_external_id=child_id
        )
        if date_end:
            overlapping_agendas = overlapping_agendas.filter(date_start__lte=date_end)
        if self.instance:
            overlapping_agendas = overlapping_agendas.exclude(pk=self.instance.pk)

        if overlapping_agendas:
            request = self.context.get('request')
            raise ValidationError(
                _('Invalid date_start/date_end, agenda would overlap with %s.')
                % request.build_absolute_uri(overlapping_agendas[0].get_absolute_url())
            )

        return attrs


class SharedCustodyAgendaCreateSerializer(SharedCustodyAgendaMixin, serializers.Serializer):
    period_mirrors = {
        'even': 'odd',
        'odd': 'even',
        'first-half': 'second-half',
        'second-half': 'first-half',
        'first-and-third-quarters': 'second-and-fourth-quarters',
        'second-and-fourth-quarters': 'first-and-third-quarters',
    }

    guardian_first_name = serializers.CharField(max_length=250)
    guardian_last_name = serializers.CharField(max_length=250)
    guardian_id = serializers.CharField(max_length=250)
    other_guardian_first_name = serializers.CharField(max_length=250)
    other_guardian_last_name = serializers.CharField(max_length=250)
    other_guardian_id = serializers.CharField(max_length=250)
    child_first_name = serializers.CharField(max_length=250)
    child_last_name = serializers.CharField(max_length=250)
    child_id = serializers.CharField(max_length=250)
    weeks = serializers.ChoiceField(required=False, choices=['', 'even', 'odd'])
    date_start = serializers.DateField(required=True)
    date_end = serializers.DateField(required=False)

    settings_url = serializers.SerializerMethodField()

    def get_child_id(self, attrs):
        return attrs['child_id']

    def validate(self, attrs):
        super().validate(attrs)

        attrs['holidays'] = collections.defaultdict(dict)
        for key, value in self.initial_data.items():
            if key in attrs or ':' not in key:
                continue

            holiday_slug, field = key.split(':')
            if field not in ('periodicity', 'years'):
                raise ValidationError(
                    _('Unknown parameter for holiday %(holiday)s: %(param)s')
                    % {'holiday': holiday_slug, 'param': field}
                )
            attrs['holidays'][holiday_slug][field] = value

        for holiday_slug in attrs['holidays'].copy():
            try:
                holiday = TimePeriodExceptionGroup.objects.get(slug=holiday_slug)
            except TimePeriodExceptionGroup.DoesNotExist:
                raise ValidationError(_('Unknown holiday: %s') % holiday_slug)

            field_values = attrs['holidays'].pop(holiday_slug)
            if 'periodicity' not in field_values:
                raise ValidationError(_('Missing periodicity for holiday: %s') % holiday_slug)

            holidays = holiday.exceptions.annotate(
                delta=ExpressionWrapper(
                    F('end_datetime') - F('start_datetime'), output_field=models.DurationField()
                )
            )
            is_short_holiday = holidays.filter(delta__lt=datetime.timedelta(days=28)).exists()
            if 'quarter' in field_values['periodicity'] and is_short_holiday:
                raise ValidationError(_('Short holidays cannot be cut into quarters.'))

            attrs['holidays'][holiday] = field_values

        return attrs

    @transaction.atomic
    def create(self, validated_data):
        guardian, dummy = Person.objects.get_or_create(
            user_external_id=validated_data['guardian_id'],
            defaults={
                'first_name': validated_data['guardian_first_name'],
                'last_name': validated_data['guardian_last_name'],
            },
        )
        other_guardian, dummy = Person.objects.get_or_create(
            user_external_id=validated_data['other_guardian_id'],
            defaults={
                'first_name': validated_data['other_guardian_first_name'],
                'last_name': validated_data['other_guardian_last_name'],
            },
        )
        child, dummy = Person.objects.get_or_create(
            user_external_id=validated_data['child_id'],
            defaults={
                'first_name': validated_data['child_first_name'],
                'last_name': validated_data['child_last_name'],
            },
        )

        self.agenda = SharedCustodyAgenda.objects.create(
            first_guardian=guardian,
            second_guardian=other_guardian,
            child=child,
            date_start=validated_data['date_start'],
            date_end=validated_data.get('date_end'),
        )

        if validated_data.get('weeks'):
            self.create_custody_rules(guardian, validated_data['weeks'], create_mirror_for=other_guardian)

        for holiday, params in validated_data.get('holidays', {}).items():
            self.create_holiday_rules(
                holiday,
                guardian,
                create_mirror_for=other_guardian,
                periodicity=params['periodicity'],
                years=params.get('years', ''),
            )

        return self.agenda

    def create_custody_rules(self, guardian, weeks, create_mirror_for=None):
        SharedCustodyRule.objects.create(
            agenda=self.agenda, days=list(range(1, 8)), weeks=weeks, guardian=guardian
        )

        if create_mirror_for:
            self.create_custody_rules(create_mirror_for, self.period_mirrors[weeks])

    def create_holiday_rules(self, holiday, guardian, years, periodicity, create_mirror_for=None):
        rule = SharedCustodyHolidayRule.objects.create(
            agenda=self.agenda, holiday=holiday, guardian=guardian, years=years, periodicity=periodicity
        )
        rule.update_or_create_periods()

        if years:
            rule = SharedCustodyHolidayRule.objects.create(
                agenda=self.agenda,
                holiday=holiday,
                guardian=guardian,
                years=self.period_mirrors[years],
                periodicity=self.period_mirrors[periodicity],
            )
            rule.update_or_create_periods()

        if create_mirror_for:
            self.create_holiday_rules(holiday, create_mirror_for, years, self.period_mirrors[periodicity])

    def get_settings_url(self, obj):
        request = self.context.get('request')
        return request.build_absolute_uri(obj.get_settings_url())


class SharedCustodyAgendaSerializer(SharedCustodyAgendaMixin, serializers.ModelSerializer):
    class Meta:
        model = SharedCustodyAgenda
        fields = ['date_start', 'date_end']

    def get_child_id(self, attrs):
        return self.instance.child.user_external_id


class AgendaDuplicateSerializer(serializers.Serializer):
    label = serializers.CharField(max_length=150, allow_blank=True, default=None)
    slug = serializers.SlugField(max_length=160, allow_blank=True, default=None)
    include_events = serializers.BooleanField(initial=True, default=True)


class MinutesSerializer(serializers.Serializer):
    minutes = CommaSeparatedStringField(
        required=False, child=serializers.IntegerField(min_value=0, max_value=59)
    )
