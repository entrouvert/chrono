# chrono - agendas system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Django settings file; it loads the default settings, and local settings
(from a local_settings.py file, or a configuration file set in the
COMBO_SETTINGS_FILE environment variable).

The local settings file should exist, at least to set a suitable SECRET_KEY,
and to disable DEBUG mode in production.
"""

import os

from django.conf.global_settings import STATICFILES_FINDERS

N_ = lambda s: s

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '1am-@xw1d%#+1f+4$ws4e3*k9+z&f4f9i#di4pt4@_%829(%bl'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'gadjo',
    'rest_framework',
    'django_filters',
    'sorl.thumbnail',
    'chrono.agendas',
    'chrono.api',
    'chrono.manager',
    'chrono.apps.ants_hub',
    'chrono.apps.export_import',
    'chrono.apps.journal',
    'chrono.apps.snapshot',
    'chrono.apps.user_preferences',
)

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# Serve xstatic files, required for gadjo
STATICFILES_FINDERS = tuple(STATICFILES_FINDERS) + ('gadjo.finders.XStaticFinder',)

ROOT_URLCONF = 'chrono.urls'

WSGI_APPLICATION = 'chrono.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
    }
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'UTC'

USE_I18N = True


USE_TZ = True

LOCALE_PATHS = (os.path.join(BASE_DIR, 'chrono', 'locale'),)

FORMAT_MODULE_PATH = 'chrono.formats'

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'publik_django_templatetags.wcs.context_processors.wcs_objects',
            ],
            'builtins': [
                'publik_django_templatetags.publik.templatetags.publik',
                'publik_django_templatetags.wcs.templatetags.wcs',
            ],
        },
    },
]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# Authentication settings
try:
    import mellon
except ImportError:
    mellon = None

if mellon is not None:
    INSTALLED_APPS += ('mellon',)
    AUTHENTICATION_BACKENDS = (
        'mellon.backends.SAMLBackend',
        'django.contrib.auth.backends.ModelBackend',
    )

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_URL = '/logout/'
LOGOUT_REDIRECT_URL = '/'

MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[email][0]}',
    'first_name': '{attributes[first_name][0]}',
    'last_name': '{attributes[last_name][0]}',
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}

MELLON_USERNAME_TEMPLATE = '{attributes[name_id_content]}'

MELLON_IDENTITY_PROVIDERS = []

# proxies argument passed to all python-request methods
# (see http://docs.python-requests.org/en/master/user/advanced/#proxies)
REQUESTS_PROXIES = None

# default in seconds
CHRONO_LOCK_DURATION = 10 * 60

# timeout used in python-requests call, in seconds
# we use 28s by default: timeout just before web server, which is usually 30s
REQUESTS_TIMEOUT = 28

try:
    import workalendar  # noqa pylint: disable=unused-import

    WORKING_DAY_CALENDAR = 'workalendar.europe.France'
    EXCEPTIONS_SOURCES = {
        'holidays': {'class': WORKING_DAY_CALENDAR, 'label': N_('Holidays')},
    }
except ImportError:
    WORKING_DAY_CALENDAR = None
    EXCEPTIONS_SOURCES = {}

TEMPLATE_VARS = {}
SMS_URL = ''
SMS_SENDER = ''

REST_FRAMEWORK = {'EXCEPTION_HANDLER': 'chrono.api.utils.exception_handler'}

SHARED_CUSTODY_ENABLED = False
PARTIAL_BOOKINGS_ENABLED = False
PARTIAL_BOOKINGS_MEETINGS_ENABLED = False

CHRONO_ANTS_HUB_URL = None

# from solr.thumbnail -- https://sorl-thumbnail.readthedocs.io/en/latest/reference/settings.html
THUMBNAIL_PRESERVE_FORMAT = True
THUMBNAIL_FORCE_OVERWRITE = False

local_settings_file = os.environ.get(
    'CHRONO_SETTINGS_FILE', os.path.join(os.path.dirname(__file__), 'local_settings.py')
)
if os.path.exists(local_settings_file):
    with open(local_settings_file) as fd:
        exec(fd.read())

# update EXCEPTIONS_SOURCES with modified (or not) WORKING_DAY_CALENDAR
if EXCEPTIONS_SOURCES.get('holidays', {}) == {'class': 'workalendar.europe.France', 'label': N_('Holidays')}:
    EXCEPTIONS_SOURCES['holidays']['class'] = WORKING_DAY_CALENDAR
