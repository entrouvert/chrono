# chrono - agendas system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, path, re_path

from . import views

urlpatterns = [
    path('', views.homepage, name='chrono-manager-homepage'),
    path(
        'unavailability-calendars/',
        views.unavailability_calendar_list,
        name='chrono-manager-unavailability-calendar-list',
    ),
    path(
        'unavailability-calendar/add/',
        views.unavailability_calendar_add,
        name='chrono-manager-unavailability-calendar-add',
    ),
    path(
        'unavailability-calendar/<int:pk>/',
        views.unavailability_calendar_view,
        name='chrono-manager-unavailability-calendar-view',
    ),
    path(
        'unavailability-calendar/<int:pk>/edit/',
        views.unavailability_calendar_edit,
        name='chrono-manager-unavailability-calendar-edit',
    ),
    path(
        'unavailability-calendar/<int:pk>/delete/',
        views.unavailability_calendar_delete,
        name='chrono-manager-unavailability-calendar-delete',
    ),
    path(
        'unavailability-calendar/<int:pk>/settings',
        views.unavailability_calendar_settings,
        name='chrono-manager-unavailability-calendar-settings',
    ),
    path(
        'unavailability-calendar/<int:pk>/export',
        views.unavailability_calendar_export,
        name='chrono-manager-unavailability-calendar-export',
    ),
    path(
        'unavailability-calendar/<int:pk>/add-unavailability',
        views.unavailability_calendar_add_unavailability,
        name='chrono-manager-unavailability-calendar-add-unavailability',
    ),
    path(
        'unavailability-calendar/<int:pk>/import-unavailabilities/',
        views.unavailability_calendar_import_unavailabilities,
        name='chrono-manager-unavailability-calendar-import-unavailabilities',
    ),
    path(
        'unavailability-calendar/<int:pk>/inspect/',
        views.unavailability_calendar_inspect,
        name='chrono-manager-unavailability-calendar-inspect',
    ),
    path(
        'unavailability-calendar/<int:pk>/history/',
        views.unavailability_calendar_history,
        name='chrono-manager-unavailability-calendar-history',
    ),
    path(
        'unavailability-calendar/<int:pk>/history/compare/',
        views.unavailability_calendar_history_compare,
        name='chrono-manager-unavailability-calendar-history-compare',
    ),
    path('resources/', views.resource_list, name='chrono-manager-resource-list'),
    path('resource/add/', views.resource_add, name='chrono-manager-resource-add'),
    path('resource/<int:pk>/', views.resource_view, name='chrono-manager-resource-view'),
    re_path(
        r'^resource/(?P<pk>\d+)/month/(?P<year>[1-9][0-9]{3})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$',
        views.resource_monthly_view,
        name='chrono-manager-resource-month-view',
    ),
    re_path(
        r'^resource/(?P<pk>\d+)/week/(?P<year>[1-9][0-9]{3})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$',
        views.resource_weekly_view,
        name='chrono-manager-resource-week-view',
    ),
    re_path(
        r'^resource/(?P<pk>\d+)/day/(?P<year>[1-9][0-9]{3})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$',
        views.resource_day_view,
        name='chrono-manager-resource-day-view',
    ),
    re_path(
        r'^resource/(?P<pk>\d+)/(?P<year>[1-9][0-9]{3})/',
        views.resource_redirect_view,
        name='chrono-manager-resource-redirect-view',
    ),
    path('resource/<int:pk>/edit/', views.resource_edit, name='chrono-manager-resource-edit'),
    path('resource/<int:pk>/delete/', views.resource_delete, name='chrono-manager-resource-delete'),
    path('resource/<int:pk>/inspect/', views.resource_inspect, name='chrono-manager-resource-inspect'),
    path('resource/<int:pk>/history/', views.resource_history, name='chrono-manager-resource-history'),
    path(
        'resource/<int:pk>/history/compare/',
        views.resource_history_compare,
        name='chrono-manager-resource-history-compare',
    ),
    path('categories/', views.category_list, name='chrono-manager-category-list'),
    path('category/add/', views.category_add, name='chrono-manager-category-add'),
    path('category/<int:pk>/edit/', views.category_edit, name='chrono-manager-category-edit'),
    path('category/<int:pk>/delete/', views.category_delete, name='chrono-manager-category-delete'),
    path('category/<int:pk>/inspect/', views.category_inspect, name='chrono-manager-category-inspect'),
    path('category/<int:pk>/history/', views.category_history, name='chrono-manager-category-history'),
    path(
        'category/<int:pk>/history/compare/',
        views.category_history_compare,
        name='chrono-manager-category-history-compare',
    ),
    path('events-types/', views.events_type_list, name='chrono-manager-events-type-list'),
    path('events-type/add/', views.events_type_add, name='chrono-manager-events-type-add'),
    path('events-type/<int:pk>/', views.events_type_view, name='chrono-manager-events-type-view'),
    path('events-type/<int:pk>/edit/', views.events_type_edit, name='chrono-manager-events-type-edit'),
    path(
        'events-type/<int:pk>/delete/',
        views.events_type_delete,
        name='chrono-manager-events-type-delete',
    ),
    path(
        'events-type/<int:pk>/inspect/', views.events_type_inspect, name='chrono-manager-events-type-inspect'
    ),
    path(
        'events-type/<int:pk>/history/', views.events_type_history, name='chrono-manager-events-type-history'
    ),
    path(
        'events-type/<int:pk>/history/compare/',
        views.events_type_history_compare,
        name='chrono-manager-events-type-history-compare',
    ),
    path('agendas/add/', views.agenda_add, name='chrono-manager-agenda-add'),
    path('agendas/import/', views.agendas_import, name='chrono-manager-agendas-import'),
    path('agendas/export/', views.agendas_export, name='chrono-manager-agendas-export'),
    path('agendas/<int:pk>/', views.agenda_view, name='chrono-manager-agenda-view'),
    re_path(r'^agendas/(?P<slug>[-_a-zA-Z0-9]+)/$', views.agenda_view, name='chrono-manager-agenda-view'),
    path(
        'agendas/<int:pk>/month/',
        views.agenda_month_redirect_view,
        name='chrono-manager-agenda-month-redirect-view',
    ),
    re_path(
        r'^agendas/(?P<pk>\d+)/month/(?P<year>[1-9][0-9]{3})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$',
        views.agenda_monthly_view,
        name='chrono-manager-agenda-month-view',
    ),
    path(
        'agendas/<int:pk>/week/',
        views.agenda_week_redirect_view,
        name='chrono-manager-agenda-week-redirect-view',
    ),
    re_path(
        r'^agendas/(?P<pk>\d+)/week/(?P<year>[1-9][0-9]{3})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$',
        views.agenda_weekly_view,
        name='chrono-manager-agenda-week-view',
    ),
    path(
        'agendas/<int:pk>/day/',
        views.agenda_day_redirect_view,
        name='chrono-manager-agenda-day-redirect-view',
    ),
    re_path(
        r'^agendas/(?P<pk>\d+)/day/(?P<year>[1-9][0-9]{3})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$',
        views.agenda_day_view,
        name='chrono-manager-agenda-day-view',
    ),
    re_path(
        r'^agendas/(?P<pk>\d+)/day/(?P<year>[1-9][0-9]{3})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/booking$',
        views.agenda_add_booking_view,
        name='chrono-manager-agenda-add-booking-view',
    ),
    re_path(
        r'^agendas/(?P<pk>\d+)/week/(?P<year>[1-9][0-9]{3})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/booking$',
        views.agenda_add_booking_week_view,
        name='chrono-manager-agenda-add-booking-week-view',
    ),
    re_path(
        r'^agendas/(?P<pk>\d+)/(?P<year>[1-9][0-9]{3})/',
        views.agenda_redirect_view,
        name='chrono-manager-agenda-redirect-view',
    ),
    path(
        'agendas/<int:pk>/events/open/',
        views.agenda_open_events_view,
        name='chrono-manager-agenda-open-events-view',
    ),
    path('agendas/<int:pk>/settings', views.agenda_settings, name='chrono-manager-agenda-settings'),
    re_path(
        r'^agendas/(?P<slug>[-_a-zA-Z0-9]+)/settings/$',
        views.agenda_settings_redirect_view,
        name='chrono-manager-agenda-settings-redirect',
    ),
    path('agendas/<int:pk>/edit', views.agenda_edit, name='chrono-manager-agenda-edit'),
    path(
        'agendas/<int:pk>/booking-delays',
        views.agenda_booking_delays,
        name='chrono-manager-agenda-booking-delays',
    ),
    path('agendas/<int:pk>/roles', views.agenda_roles, name='chrono-manager-agenda-roles'),
    path(
        'agendas/<int:pk>/backoffice-booking',
        views.agenda_backoffice_booking,
        name='chrono-manager-agenda-backoffice-booking',
    ),
    path(
        'agendas/<int:pk>/partial-bookings-settings',
        views.agenda_partial_bookings_settings,
        name='chrono-manager-agenda-partial-bookings-settings',
    ),
    path(
        'agendas/<int:pk>/display-options',
        views.agenda_display_settings,
        name='chrono-manager-agenda-display-settings',
    ),
    path(
        'agendas/<int:pk>/check-options',
        views.agenda_booking_check_settings,
        name='chrono-manager-agenda-booking-check-settings',
    ),
    path(
        'agendas/<int:pk>/invoicing-options',
        views.agenda_invoicing_settings,
        name='chrono-manager-agenda-invoicing-settings',
    ),
    path('agendas/<int:pk>/delete', views.agenda_delete, name='chrono-manager-agenda-delete'),
    path('agendas/<int:pk>/export', views.agenda_export, name='chrono-manager-agenda-export'),
    path('agendas/<int:pk>/add-event', views.agenda_add_event, name='chrono-manager-agenda-add-event'),
    path('agendas/<int:pk>/duplicate', views.agenda_duplicate, name='chrono-manager-agenda-duplicate'),
    path(
        'agendas/<int:pk>/desk-management-toggle',
        views.agenda_desk_management_toggle_view,
        name='chrono-manager-agenda-desk-management-toggle-view',
    ),
    path(
        'agendas/<int:pk>/import-events',
        views.agenda_import_events,
        name='chrono-manager-agenda-import-events',
    ),
    path(
        'agendas/<int:pk>/export-events',
        views.agenda_export_events,
        name='chrono-manager-agenda-export-events',
    ),
    path(
        'agendas/<int:pk>/notifications',
        views.agenda_notifications_settings,
        name='chrono-manager-agenda-notifications-settings',
    ),
    path(
        'agendas/<int:pk>/reminder',
        views.agenda_reminder_settings,
        name='chrono-manager-agenda-reminder-settings',
    ),
    path(
        'agendas/<int:pk>/reminder/test/',
        views.agenda_reminder_test,
        name='chrono-manager-agenda-reminder-test',
    ),
    re_path(
        r'^agendas/(?P<pk>\d+)/reminder/preview/(?P<type>(email|sms))/$',
        views.agenda_reminder_preview,
        name='chrono-manager-agenda-reminder-preview',
    ),
    path(
        'agendas/<int:pk>/events/timesheet',
        views.events_timesheet,
        name='chrono-manager-events-timesheet',
    ),
    path(
        'agendas/<int:pk>/events/check-report/',
        views.events_agenda_check_report,
        name='chrono-manager-events-agenda-check-report',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/',
        views.event_view,
        name='chrono-manager-event-view',
    ),
    re_path(
        r'^agendas/(?P<slug>[-_a-zA-Z0-9]+)/events/(?P<event_slug>[-_a-zA-Z0-9]+)/$',
        views.event_redirect_view,
        name='chrono-manager-event-redirect',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/edit',
        views.event_edit,
        name='chrono-manager-event-edit',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/duplicate',
        views.event_duplicate,
        name='chrono-manager-event-duplicate',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/delete',
        views.event_delete,
        name='chrono-manager-event-delete',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/cancel',
        views.event_cancel,
        name='chrono-manager-event-cancel',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/check',
        views.event_checks,
        name='chrono-manager-event-check',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/presence',
        views.event_presence,
        name='chrono-manager-event-presence',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/absence',
        views.event_absence,
        name='chrono-manager-event-absence',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/checked',
        views.event_checked,
        name='chrono-manager-event-checked',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/timesheet',
        views.events_timesheet,
        name='chrono-manager-event-timesheet',
    ),
    path(
        'agendas/<int:pk>/event_cancellation_report/<int:report_pk>/',
        views.event_cancellation_report,
        name='chrono-manager-event-cancellation-report',
    ),
    path(
        'agendas/<int:pk>/event_cancellation_reports/',
        views.event_cancellation_report_list,
        name='chrono-manager-event-cancellation-report-list',
    ),
    path(
        'agendas/<int:pk>/add-resource/',
        views.agenda_add_resource,
        name='chrono-manager-agenda-add-resource',
    ),
    path(
        'agendas/<int:pk>/resource/<int:resource_pk>/delete/',
        views.agenda_delete_resource,
        name='chrono-manager-agenda-delete-resource',
    ),
    path(
        'agendas/<int:pk>/add-meeting-type',
        views.agenda_add_meeting_type,
        name='chrono-manager-agenda-add-meeting-type',
    ),
    path('meetingtypes/<int:pk>/edit', views.meeting_type_edit, name='chrono-manager-meeting-type-edit'),
    path(
        'meetingtypes/<int:pk>/delete',
        views.meeting_type_delete,
        name='chrono-manager-meeting-type-delete',
    ),
    path(
        'agendas/<int:agenda_pk>/desk/<int:pk>/add-time-period',
        views.agenda_add_time_period,
        name='chrono-manager-agenda-add-time-period',
    ),
    path(
        'agendas/<int:pk>/add-time-period',
        views.virtual_agenda_add_time_period,
        name='chrono-manager-virtual-agenda-add-time-period',
    ),
    path('timeperiods/<int:pk>/edit', views.time_period_edit, name='chrono-manager-time-period-edit'),
    path(
        'timeperiods/<int:pk>/delete',
        views.time_period_delete,
        name='chrono-manager-time-period-delete',
    ),
    path(
        'agendas/<int:agenda_pk>/desk/<int:pk>/add-date-time-period',
        views.agenda_add_date_time_period,
        name='chrono-manager-agenda-add-date-time-period',
    ),
    path(
        'timeperiods/desk/<int:pk>/date-time-period-list',
        views.agenda_date_time_period_list,
        name='chrono-manager-date-time-period-list',
    ),
    path('agendas/<int:pk>/add-desk', views.agenda_add_desk, name='chrono-manager-agenda-add-desk'),
    path('desks/<int:pk>/edit', views.desk_edit, name='chrono-manager-desk-edit'),
    path('desks/<int:pk>/delete', views.desk_delete, name='chrono-manager-desk-delete'),
    path(
        'desk/<int:pk>/unavailability-calendar/<int:unavailability_calendar_pk>/toggle/',
        views.unavailability_calendar_toggle_view,
        name='chrono-manager-unavailability-calendar-toggle-view',
    ),
    path(
        'agendas/<int:agenda_pk>/desk/<int:pk>/add-time-period-exception',
        views.agenda_add_time_period_exception,
        name='chrono-manager-agenda-add-time-period-exception',
    ),
    path(
        'agendas/desk/<int:pk>/import-exceptions-from-ics/',
        views.desk_import_time_period_exceptions,
        name='chrono-manager-desk-add-import-time-period-exceptions',
    ),
    path(
        'agendas/<int:pk>/add-virtual-member',
        views.agenda_add_virtual_member,
        name='chrono-manager-agenda-add-virtual-member',
    ),
    path(
        'virtual-members/<int:pk>/delete',
        views.virtual_member_delete,
        name='chrono-manager-virtual-member-delete',
    ),
    path(
        'time-period-exceptions/<int:pk>/edit',
        views.time_period_exception_edit,
        name='chrono-manager-time-period-exception-edit',
    ),
    path(
        'time-period-exceptions/<int:pk>/delete',
        views.time_period_exception_delete,
        name='chrono-manager-time-period-exception-delete',
    ),
    path(
        'time-period-exceptions/<int:pk>/exception-extract-list',
        views.time_period_exception_extract_list,
        name='chrono-manager-time-period-exception-extract-list',
    ),
    path(
        'time-period-exceptions/<int:pk>/exception-list',
        views.time_period_exception_list,
        name='chrono-manager-time-period-exception-list',
    ),
    path(
        'time-period-exceptions-source/<int:pk>/delete',
        views.time_period_exception_source_delete,
        name='chrono-manager-time-period-exception-source-delete',
    ),
    path(
        'time-period-exceptions-source/<int:pk>/refresh',
        views.time_period_exception_source_refresh,
        name='chrono-manager-time-period-exception-source-refresh',
    ),
    path(
        'time-period-exceptions-source/<int:pk>/toggle',
        views.time_period_exception_source_toggle,
        name='chrono-manager-time-period-exception-source-toggle',
    ),
    path(
        'time-period-exceptions-source/<int:pk>/replace',
        views.time_period_exception_source_replace,
        name='chrono-manager-time-period-exception-source-replace',
    ),
    path(
        'agendas/<int:pk>/bookings/<int:booking_pk>/cancel',
        views.booking_cancel,
        name='chrono-manager-booking-cancel',
    ),
    path(
        'agendas/<int:pk>/bookings/<int:booking_pk>/presence',
        views.booking_presence,
        name='chrono-manager-booking-presence',
    ),
    path(
        'agendas/<int:pk>/bookings/<int:booking_pk>/absence',
        views.booking_absence,
        name='chrono-manager-booking-absence',
    ),
    path(
        'agendas/<int:pk>/bookings/<int:booking_pk>/reset',
        views.booking_reset,
        name='chrono-manager-booking-reset',
    ),
    path(
        'agendas/<int:pk>/subscriptions/<int:subscription_pk>/presence/<int:event_pk>',
        views.subscription_presence,
        name='chrono-manager-subscription-presence',
    ),
    path(
        'agendas/<int:pk>/bookings/<int:booking_pk>/extra-user-block',
        views.booking_extra_user_block,
        name='chrono-manager-booking-extra-user-block',
    ),
    path(
        'agendas/<int:pk>/events/<int:event_pk>/check-bookings/<str:user_external_id>',
        views.partial_booking_check_view,
        name='chrono-manager-partial-booking-check',
    ),
    path(
        'agendas/<int:pk>/subscriptions/<int:subscription_pk>/extra-user-block',
        views.subscription_extra_user_block,
        name='chrono-manager-subscription-extra-user-block',
    ),
    path(
        'agendas/events/report/',
        views.events_report,
        name='chrono-manager-events-report',
    ),
    re_path(
        r'^agendas/events/check-report/(?P<user_external_id>[\w\d_-]+:?[\w\d_-]*)/$',
        views.events_user_check_report,
        name='chrono-manager-events-user-check-report',
    ),
    path(
        'agendas/<int:pk>/events.csv',
        views.agenda_import_events_sample_csv,
        name='chrono-manager-sample-events-csv',
    ),
    path('agendas/<int:pk>/inspect/', views.agenda_inspect, name='chrono-manager-agenda-inspect'),
    path('agendas/<int:pk>/history/', views.agenda_history, name='chrono-manager-agenda-history'),
    path(
        'agendas/<int:pk>/history/compare/',
        views.agenda_history_compare,
        name='chrono-manager-agenda-history-compare',
    ),
    path(
        'agendas/<int:pk>/lease/<uuid:lock_code>/delete',
        views.agenda_lease_delete_view,
        name='chrono-manager-agenda-lease-delete-view',
    ),
    path(
        'shared-custody/settings/',
        views.shared_custody_settings,
        name='chrono-manager-shared-custody-settings',
    ),
    path(
        'shared-custody/<int:pk>/',
        views.shared_custody_agenda_view,
        name='chrono-manager-shared-custody-agenda-view',
    ),
    re_path(
        r'^shared-custody/(?P<pk>\d+)/(?P<year>[0-9]{4})/(?P<month>[0-9]+)/$',
        views.shared_custody_agenda_monthly_view,
        name='chrono-manager-shared-custody-agenda-month-view',
    ),
    path(
        'shared-custody/<int:pk>/settings/',
        views.shared_custody_agenda_settings,
        name='chrono-manager-shared-custody-agenda-settings',
    ),
    path(
        'shared-custody/<int:pk>/delete',
        views.shared_custody_agenda_delete,
        name='chrono-manager-shared-custody-agenda-delete',
    ),
    path(
        'shared-custody/<int:pk>/add-rule',
        views.shared_custody_agenda_add_rule,
        name='chrono-manager-shared-custody-agenda-add-rule',
    ),
    path(
        'shared-custody/<int:pk>/rules/<int:rule_pk>/edit',
        views.shared_custody_agenda_edit_rule,
        name='chrono-manager-shared-custody-agenda-edit-rule',
    ),
    path(
        'shared-custody/<int:pk>/rules/<int:rule_pk>/delete',
        views.shared_custody_agenda_delete_rule,
        name='chrono-manager-shared-custody-agenda-delete-rule',
    ),
    path(
        'shared-custody/<int:pk>/add-holiday-rule',
        views.shared_custody_agenda_add_holiday_rule,
        name='chrono-manager-shared-custody-agenda-add-holiday-rule',
    ),
    path(
        'shared-custody/<int:pk>/holiday-rules/<int:rule_pk>/edit',
        views.shared_custody_agenda_edit_holiday_rule,
        name='chrono-manager-shared-custody-agenda-edit-holiday-rule',
    ),
    path(
        'shared-custody/<int:pk>/holiday-rules/<int:rule_pk>/delete',
        views.shared_custody_agenda_delete_holiday_rule,
        name='chrono-manager-shared-custody-agenda-delete-holiday-rule',
    ),
    path(
        'shared-custody/<int:pk>/add-period',
        views.shared_custody_agenda_add_period,
        name='chrono-manager-shared-custody-agenda-add-period',
    ),
    path(
        'shared-custody/<int:pk>/periods/<int:period_pk>/edit',
        views.shared_custody_agenda_edit_period,
        name='chrono-manager-shared-custody-agenda-edit-period',
    ),
    path(
        'shared-custody/<int:pk>/periods/<int:period_pk>/delete',
        views.shared_custody_agenda_delete_period,
        name='chrono-manager-shared-custody-agenda-delete-period',
    ),
    re_path(r'^menu.json$', views.menu_json),
    path('ants/', include('chrono.apps.ants_hub.urls')),
    path('journal/', include('chrono.apps.journal.urls')),
]
