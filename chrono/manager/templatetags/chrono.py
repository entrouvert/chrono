# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import template
from django.utils.formats import date_format

from chrono.apps.user_preferences.models import UserPreferences

register = template.Library()


@register.filter
def human_date_range(date_start, date_end):
    date_end_format = 'd F Y'
    if date_start.year != date_end.year:
        date_start_format = date_end_format
    elif date_start.month != date_end.month:
        date_start_format = 'd F'
    else:
        date_start_format = 'd'

    return '%s − %s' % (date_format(date_start, date_start_format), date_format(date_end, date_end_format))


@register.filter
def get_preference(user, pref_name):
    user_preferences, dummy = UserPreferences.objects.get_or_create(user=user)
    return user_preferences.preferences.get(pref_name) or False
