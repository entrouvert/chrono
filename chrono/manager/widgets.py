# chrono - agendas system
# Copyright (C) 2016-2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.forms.fields import SplitDateTimeField as DJSplitDateTimeField
from django.forms.widgets import CheckboxSelectMultiple
from django.forms.widgets import SplitDateTimeWidget as DJSplitDateTimeWidget
from django.forms.widgets import TimeInput


class SplitDateTimeWidget(DJSplitDateTimeWidget):
    template_name = 'chrono/splitdatetime.html'

    def __init__(self, *args, **kwargs):
        kwargs['time_format'] = '%H:%M'
        kwargs['date_format'] = '%Y-%m-%d'
        super().__init__(*args, **kwargs)


class SplitDateTimeField(DJSplitDateTimeField):
    widget = SplitDateTimeWidget

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # fix for django<2.1, see https://code.djangoproject.com/ticket/28882
        for f in self.fields:
            if self.disabled:
                f.disabled = True

    def clean(self, value):
        # fix for django<2.1, see https://code.djangoproject.com/ticket/28882
        if self.disabled and not isinstance(value, list):
            value = self.widget.decompress(value)
        return super().clean(value)


class TimeWidget(TimeInput):
    """
    TimeWidget is a widget for time selection, it uses the HTML5 "time"
    input type and has a bit of a fallback mechanism with the presence
    of the pattern attribute in case a standard text input is used.
    """

    input_type = 'time'

    def __init__(self, **kwargs):
        step = kwargs.pop('step', 300)  # 5 minutes by default
        kwargs['format'] = '%H:%M'
        super().__init__(**kwargs)
        self.attrs['step'] = step
        self.attrs['pattern'] = '[0-9]{2}:[0-9]{2}'


class TimeWidgetWithButton(TimeWidget):
    template_name = 'chrono/widgets/time_with_button.html'

    def __init__(self, **kwargs):
        self.button_label = kwargs.pop('button_label')
        super().__init__(**kwargs)

    def get_context(self, *args, **kwargs):
        ctx = super().get_context(*args, **kwargs)
        ctx['widget']['button_label'] = self.button_label
        return ctx


class WeekdaysWidget(CheckboxSelectMultiple):
    template_name = 'chrono/widgets/weekdays.html'

    def id_for_label(self, id_, index=None):
        """Workaround CheckboxSelectMultiple id_for_label, which would return empty string when
        index is None, leading to more complicated JS from our side."""
        if index is None:
            index = ''
        return super(CheckboxSelectMultiple, self).id_for_label(id_, index)
