$(function() {
  const foldableClassObserver = new MutationObserver((mutations) => {
	  mutations.forEach(mu => {
		  const old_folded = (mu.oldValue.indexOf('folded') != -1);
		  const new_folded = mu.target.classList.contains('folded')
		  if (old_folded == new_folded) { return; }
		  var pref_message = Object();
		  pref_message[mu.target.dataset.sectionFoldedPrefName] = new_folded;
		  fetch('/api/user-preferences/save', {
			  method: 'POST',
			  credentials: 'include',
			  headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
			  body: JSON.stringify(pref_message)
		});
	});
    });
  document.querySelectorAll('[data-section-folded-pref-name]').forEach(
	  elt => foldableClassObserver.observe(elt, {attributes: true, attributeFilter: ['class'], attributeOldValue: true})
  );

  $('[data-total]').each(function() {
    var total = $(this).data('total');
    var booked = $(this).data('booked');
    $(this).find('.occupation-bar').css('max-width', 100 * booked / total + '%');
  });
  $('.date-title').on('click', function() {
    const $datePicker = $(this).parent().find('.date-picker');
    $datePicker.toggle();
    if ($datePicker.css("display") !== "none" && document.body.classList.contains("dayview")) {
      const dateInput = document.querySelector('.date-picker--input');
      dateInput.focus();
      if (dateInput.showPicker) dateInput.showPicker();
    }
  });
  $('.date-picker-opener').on('click', function() { $('.date-title').trigger('click'); });
  $('.date-picker button').on('click', function() {
    if (document.body.classList.contains("dayview")) {
     window.location = '../../../' + $('.date-picker--input').val().replaceAll("-", '/') + '/';
     return false;
    }
    if ($('[name=day]').val()) {
       window.location = '../../../' + $('[name=year]').val() + '/' + $('[name=month]').val() + '/' + $('[name=day]').val() + '/';
    } else if ($('[name=month]').val()) {
        window.location = '../../../' + $('[name=year]').val() + '/' + $('[name=month]').val() + '/1/';
    } else {
        window.location = '../../../' + $('[name=week]').val() + '/';
    }
    return false;
  });

  if (window.navigator.userAgent.indexOf('MSIE') > 0 || window.navigator.userAgent.match(/Trident.*rv\:11\./)) {
    // IE doesn't support percentage units inside table cells so we have to go
    // over all <div>s and convert the values to pixels.
    var td_height = $('table.agenda-table tbody td').height();
    $('table.agenda-table tbody td div').each(function(idx, elem) {
      var parsed_height = $(elem).attr('style').match(/height: ([0-9]+)%/)[1];
      $(elem).css('height', (parseInt(parsed_height) * td_height / 100) + 'px');
      var parsed_top = $(elem).attr('style').match(/top: ([0-9]+)%/)[1];
      $(elem).css('top', (parseInt(parsed_top) * td_height / 100) + 'px');
      var parsed_min_height = $(elem).attr('style').match(/min-height: ([0-9]+)%/);
      if (parsed_min_height) {
        $(elem).css('min-height', (parseInt(parsed_min_height[1]) * td_height / 100) + 'px');
      }
    });
  }

  if ($('#add-custom-field-form').length) {
    var property_forms = $('.custom-field-form');
    var total_form = $('#id_form-TOTAL_FORMS');
    var form_num = property_forms.length - 1;
    $('#add-custom-field-form').on('click', function() {
      var new_form = $(property_forms[0]).clone();
      var form_regex = RegExp(`form-(\\d){1}-`,'g');
      form_num++;
      new_form.html(new_form.html().replace(form_regex, `form-${form_num}-`));
      new_form.appendTo('#custom-field-forms tbody');
      $('#id_form-' + form_num + '-varname').val('');
      $('#id_form-' + form_num + '-label').val('');
      $('#id_form-' + form_num + '-field_type').val('');
      total_form.val(form_num + 1);
    })
  }

  function prepare_dynamic_fields() {
    $('[data-dynamic-display-parent]').off('change input').on('change input', function() {
      if($(this)[0].type == 'radio' && !$(this).prop('checked')) {
        return;
      }
      var sel1 = '[data-dynamic-display-child-of="' + $(this).attr('name') + '"]';
      var sel2 = '[data-dynamic-display-value="' + $(this).val() + '"]';
      var sel3 = '[data-dynamic-display-value-in*=" ' + $(this).val() + ' "]';
      var sel4 = '[data-dynamic-display-checked="' + $(this).prop('checked') + '"]';
      var sel5 = '[data-dynamic-display-empty="' + ($(this).val().length == 0 ? 'true' : 'false') + '"]';
      $(sel1).addClass('field-hidden').parents('.widget').hide();
      $(sel1 + sel2).removeClass('field-hidden').parents('.widget').show();
      $(sel1 + sel3).removeClass('field-hidden').parents('.widget').show();
      $(sel1 + sel4).removeClass('field-hidden').parents('.widget').show();
      $(sel1 + sel5).removeClass('field-hidden').parents('.widget').show();
      $(sel1).trigger('change');
    });
    $('[data-dynamic-display-child-of]').addClass('field-hidden').parents('.widget').hide();
    $('[data-dynamic-display-parent]').trigger('change');
  }
  prepare_dynamic_fields();
  $(document).on('gadjo:dialog-loaded', prepare_dynamic_fields);

  const addBookingOnTimeBtns = [...document.querySelectorAll('.add-booking-on-time')];
  addBookingOnTimeBtns.forEach( btn => {
    const sames = addBookingOnTimeBtns.filter((el) => {
      return (el !== btn) && (el.dataset.datetime === btn.dataset.datetime);
    });
    if (sames.length) {
      btn.addEventListener('mouseover', () => {
	sames.forEach( same => {
	  same.classList.add('visible')
	})
      })
      btn.addEventListener('mouseout', () => {
	sames.forEach( same => same.classList.remove('visible'))
      })
    }
    $(btn).on('click', displayPopup);
  })

  const booking_quick_search = document.getElementById('booking-quick-search')
  if (booking_quick_search) {
    ['keyup', 'change'].forEach(event_type =>
      booking_quick_search.addEventListener(event_type, function(evt) {
        var q = window.URLify(booking_quick_search.value || '', 50)
        document.querySelectorAll('[data-quick-search-slug]').forEach(function(elt) {
          if (q && elt.dataset.quickSearchSlug.indexOf(q) === -1) {
            elt.parentElement.style.display = 'none'
          } else {
            elt.parentElement.style.display = 'table-row'
          }
        })
      })
    )
  }
});

// urlify.js from django/contrib/admin/static/admin/js/urlify.js
{
    const LATIN_MAP = {
        'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE',
        'Ç': 'C', 'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I',
        'Î': 'I', 'Ï': 'I', 'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O',
        'Õ': 'O', 'Ö': 'O', 'Ő': 'O', 'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U',
        'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH', 'Ÿ': 'Y', 'ß': 'ss', 'à': 'a',
        'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
        'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i',
        'ï': 'i', 'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o',
        'ö': 'o', 'ő': 'o', 'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u',
        'ű': 'u', 'ý': 'y', 'þ': 'th', 'ÿ': 'y'
    };
    const LATIN_SYMBOLS_MAP = {
        '©': '(c)'
    };
    const GREEK_MAP = {
        'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h',
        'θ': '8', 'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3',
        'ο': 'o', 'π': 'p', 'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f',
        'χ': 'x', 'ψ': 'ps', 'ω': 'w', 'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o',
        'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's', 'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y',
        'ΐ': 'i', 'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z',
        'Η': 'H', 'Θ': '8', 'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N',
        'Ξ': '3', 'Ο': 'O', 'Π': 'P', 'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y',
        'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W', 'Ά': 'A', 'Έ': 'E', 'Ί': 'I',
        'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I', 'Ϋ': 'Y'
    };
    const TURKISH_MAP = {
        'ş': 's', 'Ş': 'S', 'ı': 'i', 'İ': 'I', 'ç': 'c', 'Ç': 'C', 'ü': 'u',
        'Ü': 'U', 'ö': 'o', 'Ö': 'O', 'ğ': 'g', 'Ğ': 'G'
    };
    const ROMANIAN_MAP = {
        'ă': 'a', 'î': 'i', 'ș': 's', 'ț': 't', 'â': 'a',
        'Ă': 'A', 'Î': 'I', 'Ș': 'S', 'Ț': 'T', 'Â': 'A'
    };
    const RUSSIAN_MAP = {
        'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo',
        'ж': 'zh', 'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm',
        'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u',
        'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '',
        'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya',
        'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo',
        'Ж': 'Zh', 'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M',
        'Н': 'N', 'О': 'O', 'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U',
        'Ф': 'F', 'Х': 'H', 'Ц': 'C', 'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '',
        'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu', 'Я': 'Ya'
    };
    const UKRAINIAN_MAP = {
        'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G', 'є': 'ye', 'і': 'i',
        'ї': 'yi', 'ґ': 'g'
    };
    const CZECH_MAP = {
        'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't',
        'ů': 'u', 'ž': 'z', 'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R',
        'Š': 'S', 'Ť': 'T', 'Ů': 'U', 'Ž': 'Z'
    };
    const SLOVAK_MAP = {
        'á': 'a', 'ä': 'a', 'č': 'c', 'ď': 'd', 'é': 'e', 'í': 'i', 'ľ': 'l',
        'ĺ': 'l', 'ň': 'n', 'ó': 'o', 'ô': 'o', 'ŕ': 'r', 'š': 's', 'ť': 't',
        'ú': 'u', 'ý': 'y', 'ž': 'z',
        'Á': 'a', 'Ä': 'A', 'Č': 'C', 'Ď': 'D', 'É': 'E', 'Í': 'I', 'Ľ': 'L',
        'Ĺ': 'L', 'Ň': 'N', 'Ó': 'O', 'Ô': 'O', 'Ŕ': 'R', 'Š': 'S', 'Ť': 'T',
        'Ú': 'U', 'Ý': 'Y', 'Ž': 'Z'
    };
    const POLISH_MAP = {
        'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's',
        'ź': 'z', 'ż': 'z',
        'Ą': 'A', 'Ć': 'C', 'Ę': 'E', 'Ł': 'L', 'Ń': 'N', 'Ó': 'O', 'Ś': 'S',
        'Ź': 'Z', 'Ż': 'Z'
    };
    const LATVIAN_MAP = {
        'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l',
        'ņ': 'n', 'š': 's', 'ū': 'u', 'ž': 'z',
        'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'I', 'Ķ': 'K', 'Ļ': 'L',
        'Ņ': 'N', 'Š': 'S', 'Ū': 'U', 'Ž': 'Z'
    };
    const ARABIC_MAP = {
        'أ': 'a', 'ب': 'b', 'ت': 't', 'ث': 'th', 'ج': 'g', 'ح': 'h', 'خ': 'kh', 'د': 'd',
        'ذ': 'th', 'ر': 'r', 'ز': 'z', 'س': 's', 'ش': 'sh', 'ص': 's', 'ض': 'd', 'ط': 't',
        'ظ': 'th', 'ع': 'aa', 'غ': 'gh', 'ف': 'f', 'ق': 'k', 'ك': 'k', 'ل': 'l', 'م': 'm',
        'ن': 'n', 'ه': 'h', 'و': 'o', 'ي': 'y'
    };
    const LITHUANIAN_MAP = {
        'ą': 'a', 'č': 'c', 'ę': 'e', 'ė': 'e', 'į': 'i', 'š': 's', 'ų': 'u',
        'ū': 'u', 'ž': 'z',
        'Ą': 'A', 'Č': 'C', 'Ę': 'E', 'Ė': 'E', 'Į': 'I', 'Š': 'S', 'Ų': 'U',
        'Ū': 'U', 'Ž': 'Z'
    };
    const SERBIAN_MAP = {
        'ђ': 'dj', 'ј': 'j', 'љ': 'lj', 'њ': 'nj', 'ћ': 'c', 'џ': 'dz',
        'đ': 'dj', 'Ђ': 'Dj', 'Ј': 'j', 'Љ': 'Lj', 'Њ': 'Nj', 'Ћ': 'C',
        'Џ': 'Dz', 'Đ': 'Dj'
    };
    const AZERBAIJANI_MAP = {
        'ç': 'c', 'ə': 'e', 'ğ': 'g', 'ı': 'i', 'ö': 'o', 'ş': 's', 'ü': 'u',
        'Ç': 'C', 'Ə': 'E', 'Ğ': 'G', 'İ': 'I', 'Ö': 'O', 'Ş': 'S', 'Ü': 'U'
    };
    const GEORGIAN_MAP = {
        'ა': 'a', 'ბ': 'b', 'გ': 'g', 'დ': 'd', 'ე': 'e', 'ვ': 'v', 'ზ': 'z',
        'თ': 't', 'ი': 'i', 'კ': 'k', 'ლ': 'l', 'მ': 'm', 'ნ': 'n', 'ო': 'o',
        'პ': 'p', 'ჟ': 'j', 'რ': 'r', 'ს': 's', 'ტ': 't', 'უ': 'u', 'ფ': 'f',
        'ქ': 'q', 'ღ': 'g', 'ყ': 'y', 'შ': 'sh', 'ჩ': 'ch', 'ც': 'c', 'ძ': 'dz',
        'წ': 'w', 'ჭ': 'ch', 'ხ': 'x', 'ჯ': 'j', 'ჰ': 'h'
    };

    const ALL_DOWNCODE_MAPS = [
        LATIN_MAP,
        LATIN_SYMBOLS_MAP,
        GREEK_MAP,
        TURKISH_MAP,
        ROMANIAN_MAP,
        RUSSIAN_MAP,
        UKRAINIAN_MAP,
        CZECH_MAP,
        SLOVAK_MAP,
        POLISH_MAP,
        LATVIAN_MAP,
        ARABIC_MAP,
        LITHUANIAN_MAP,
        SERBIAN_MAP,
        AZERBAIJANI_MAP,
        GEORGIAN_MAP
    ];

    const Downcoder = {
        'Initialize': function() {
            if (Downcoder.map) { // already made
                return;
            }
            Downcoder.map = {};
            for (const lookup of ALL_DOWNCODE_MAPS) {
                Object.assign(Downcoder.map, lookup);
            }
            Downcoder.regex = new RegExp(Object.keys(Downcoder.map).join('|'), 'g');
        }
    };

    function downcode(slug) {
        Downcoder.Initialize();
        return slug.replace(Downcoder.regex, function(m) {
            return Downcoder.map[m];
        });
    }

    function URLify(s, num_chars, allowUnicode) {
        // changes, e.g., "Petty theft" to "petty-theft"
        if (!allowUnicode) {
            s = downcode(s);
        }
        s = s.toLowerCase(); // convert to lowercase
        // if downcode doesn't hit, the char will be stripped here
        if (allowUnicode) {
            // Keep Unicode letters including both lowercase and uppercase
            // characters, whitespace, and dash; remove other characters.
            s = XRegExp.replace(s, XRegExp('[^-_\\p{L}\\p{N}\\s]', 'g'), '');
        } else {
            s = s.replace(/[^-\w\s]/g, ''); // remove unneeded chars
        }
        s = s.replace(/^\s+|\s+$/g, ''); // trim leading/trailing spaces
        s = s.replace(/[-\s]+/g, '-'); // convert spaces to hyphens
        s = s.substring(0, num_chars); // trim to first num_chars chars
        s = s.replace(/-+$/g, ''); // trim any trailing hyphens
        return s;
    }
    window.URLify = URLify;
}
