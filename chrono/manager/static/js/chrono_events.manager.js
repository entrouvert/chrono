function autorefresh() {
  var pathname = window.location.pathname.replace(/^\/+/, '/');
  $.ajax({
    url: pathname + '?ajax=true',
    success: function(html) {
      $('#event_details').html(html);
    },
  });
}

$(function() {
  if ($('#event_details')) {
    /* refresh every 30 seconds (idle_id) after any user activity
    * on inactivity for more than 5 minutes (longidle_id), stop refreshing (clear idle_id)
    */
    var idle_id = null;
    var longidle_id = null;
    $(window).on('mousemove keydown mousedown touchstart', function() {
      /* if refresh timer exists, clear it */
      if (idle_id) window.clearInterval(idle_id);
      /* if stop refreshing timer exists, clear it */
      if (longidle_id) window.clearTimeout(longidle_id);
      /* launch timer to refresh every 30 seconds */
      idle_id = setInterval(autorefresh, 30000);
      /* launch timer to stop refreshing after 5 minutes idle */
      longidle_id = setTimeout(function () {
          if (idle_id) idle_id = window.clearInterval(idle_id);
          longidle_id = undefined;
      }, 300 * 1000);
    });
  }

  $(document).on('submit', '.booking form.with-ajax', function(event) {
    var $form = $(this);
    var formData = {
      'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]', $form).val()
    };
    if ($('select[name=check_type]', $form)) {
      formData['check_type'] = $('select[name=check_type]', $form).val();
    }

    $.ajax({
      type: 'POST',
      url: $form.attr('action'),
      data: formData
    }).done(function(html) {
      $('#appbar .booking-meta-infos').replaceWith($(html).find('.booking-meta-infos'));
      $form.parent().parent().html(html);
      if (!$('.booking-status.without-status').length) {
        $('tr.booking.all-bookings').hide();
      } else {
        $('tr.booking.all-bookings').show();
      }
    }).fail(function() {
      location.reload();
    });

    event.preventDefault();
  });
});
