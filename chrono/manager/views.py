# chrono - agendas system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import collections
import copy
import csv
import datetime
import itertools
import json
import math
import uuid
from operator import attrgetter, itemgetter

import requests
from dateutil.relativedelta import MO, relativedelta
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError, models, transaction
from django.db.models import (
    BooleanField,
    Count,
    Exists,
    ExpressionWrapper,
    F,
    Func,
    Max,
    Min,
    OuterRef,
    Prefetch,
    Q,
    Subquery,
    Value,
)
from django.db.models.deletion import ProtectedError
from django.db.models.functions import Cast
from django.http import Http404, HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.template.defaultfilters import title
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils import lorem_ipsum
from django.utils.dates import MONTHS
from django.utils.encoding import force_str
from django.utils.formats import date_format
from django.utils.functional import cached_property
from django.utils.html import format_html
from django.utils.safestring import SafeString
from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext, pgettext
from django.views.generic import (
    CreateView,
    DayArchiveView,
    DeleteView,
    DetailView,
    FormView,
    ListView,
    RedirectView,
    TemplateView,
    UpdateView,
    View,
)
from django.views.generic.dates import DayMixin, MonthMixin, WeekMixin, YearMixin
from weasyprint import HTML

from chrono.agendas.management.commands.utils import send_reminder
from chrono.agendas.models import (
    Agenda,
    AgendaImportError,
    AgendaNotificationsSettings,
    AgendaReminderSettings,
    Booking,
    BookingCheck,
    BookingColor,
    Category,
    Desk,
    Event,
    EventCancellationReport,
    EventsType,
    ICSError,
    Lease,
    MeetingType,
    Resource,
    SharedCustodyAgenda,
    SharedCustodyHolidayRule,
    SharedCustodyPeriod,
    SharedCustodyRule,
    SharedCustodySettings,
    Subscription,
    TimePeriod,
    TimePeriodException,
    TimePeriodExceptionSource,
    UnavailabilityCalendar,
    VirtualMember,
)
from chrono.api.views import make_booking
from chrono.apps.export_import.models import Application
from chrono.apps.journal.utils import audit, bulk_audit
from chrono.apps.snapshot.models import (
    AgendaSnapshot,
    CategorySnapshot,
    EventsTypeSnapshot,
    ResourceSnapshot,
    UnavailabilityCalendarSnapshot,
)
from chrono.apps.snapshot.views import InstanceWithSnapshotHistoryCompareView, InstanceWithSnapshotHistoryView
from chrono.utils.date import get_weekday_index
from chrono.utils.lingo import LingoError, get_agenda_check_types, get_lingo_service, unlock_agendas
from chrono.utils.requests_wrapper import requests as requests_wrapper
from chrono.utils.timezone import localtime, make_aware, make_naive, now

from .forms import (
    STATUS_CHOICES,
    AgendaAddBookingForm,
    AgendaAddForm,
    AgendaBackofficeBookingForm,
    AgendaBookingCheckSettingsForm,
    AgendaBookingDelaysForm,
    AgendaDisplaySettingsForm,
    AgendaDuplicateForm,
    AgendaEditForm,
    AgendaInvoicingSettingsForm,
    AgendaNotificationsForm,
    AgendaPartialBookingsSettingsForm,
    AgendaReminderForm,
    AgendaReminderTestForm,
    AgendaResourceForm,
    AgendaRolesForm,
    AgendasExportForm,
    AgendasImportForm,
    BookingCancelForm,
    BookingCheckAbsenceForm,
    BookingCheckFilterSet,
    BookingCheckPresenceForm,
    CustomFieldFormSet,
    DateTimePeriodForm,
    DeskExceptionsImportForm,
    DeskForm,
    EventCancelForm,
    EventDuplicateForm,
    EventForm,
    EventsCheckReportForm,
    EventsReportForm,
    EventsTimesheetForm,
    ExcludedPeriodAddForm,
    ImportEventsForm,
    MeetingTypeForm,
    NewDeskForm,
    NewEventForm,
    NewMeetingTypeForm,
    NewTimePeriodExceptionForm,
    PartialBookingCheckForm,
    SharedCustodyHolidayRuleForm,
    SharedCustodyPeriodForm,
    SharedCustodyRuleForm,
    SharedCustodySettingsForm,
    SubscriptionCheckFilterSet,
    TimePeriodAddForm,
    TimePeriodExceptionForm,
    TimePeriodExceptionSourceReplaceForm,
    TimePeriodForm,
    UnavailabilityCalendarAddForm,
    UnavailabilityCalendarEditForm,
    UnavailabilityCalendarExceptionsImportForm,
    VirtualMemberForm,
)
from .utils import export_site, import_site


class CreateDraftError(Exception):
    pass


FUTURE_BOOKING_ERROR_MSG = _('This cannot be removed as there are bookings for a future date.')


def is_ajax(request):
    return request.headers.get('x-requested-with') == 'XMLHttpRequest'


class WithApplicationsMixin:
    def with_applications_dispatch(self, request):
        self.application = None
        self.no_application = False
        if 'application' in self.request.GET:
            self.application = get_object_or_404(
                Application, slug=self.request.GET['application'], visible=True
            )
        elif 'no-application' in self.request.GET:
            self.no_application = True

    def with_applications_context_data(self, context, object_ids=None):
        if self.application:
            context['application'] = self.application
        elif not self.no_application:
            Application.populate_objects(self.model, self.object_list)
            context['applications'] = Application.select_for_object_class(self.model, object_ids)
            orphans = Application.get_orphan_objects_for_object_class(self.model)
            if object_ids:
                orphans = orphans.filter(pk__in=object_ids)
            context['no_application_exists'] = orphans.exists()
        context['no_application'] = self.no_application
        return context

    def with_applications_queryset(self):
        if self.application:
            return self.application.get_objects_for_object_class(self.model)
        if self.no_application:
            return Application.get_orphan_objects_for_object_class(self.model)
        return super().get_queryset()


class HomepageView(WithApplicationsMixin, ListView):
    template_name = 'chrono/manager_home.html'
    model = Agenda

    def dispatch(self, request, *args, **kwargs):
        self.with_applications_dispatch(request)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        if self.queryset:
            return self.queryset
        queryset = self.with_applications_queryset()
        if not self.request.user.is_staff:
            group_ids = [x.id for x in self.request.user.groups.all()]
            queryset = queryset.filter(
                Q(view_role_id__in=group_ids) | Q(admin_role_id__in=group_ids) | Q(edit_role_id__in=group_ids)
            )
        self.queryset = queryset.select_related('category').order_by('category__label', 'label')
        return self.queryset

    def has_access_to_unavailability_calendars(self):
        if self.request.user.is_staff:
            return True
        group_ids = [x.id for x in self.request.user.groups.all()]
        queryset = UnavailabilityCalendar.objects.filter(
            Q(view_role_id__in=group_ids) | Q(edit_role_id__in=group_ids)
        )
        return queryset.exists()

    def has_access(self):
        return self.request.user.is_staff or self.get_queryset().exists()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['has_access_to_unavailability_calendars'] = self.has_access_to_unavailability_calendars()
        context['shared_custody_enabled'] = settings.SHARED_CUSTODY_ENABLED
        context['ants_hub_enabled'] = bool(settings.CHRONO_ANTS_HUB_URL)
        context['with_sidebar'] = True
        context['lingo_enabled'] = bool(get_lingo_service())
        return self.with_applications_context_data(context, self.get_queryset().values('id'))

    def get(self, request, *args, **kwargs):
        if not self.has_access():
            if self.has_access_to_unavailability_calendars():
                return HttpResponseRedirect(reverse('chrono-manager-unavailability-calendar-list'))
            self.template_name = 'chrono/manager_no_access.html'
        return super().get(request, *args, **kwargs)

    def render_to_response(self, context, **response_kwargs):
        if self.template_name == 'chrono/manager_no_access.html':
            response_kwargs['status'] = 403
        return super().render_to_response(context, **response_kwargs)


homepage = HomepageView.as_view()


class AgendasExportView(FormView):
    form_class = AgendasExportForm
    template_name = 'chrono/agendas_export.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        response = HttpResponse(content_type='application/json')
        today = datetime.date.today()
        response['Content-Disposition'] = 'attachment; filename="export_agendas_{}.json"'.format(
            today.strftime('%Y%m%d')
        )
        json.dump(export_site(**form.cleaned_data), response, indent=2)
        return response


agendas_export = AgendasExportView.as_view()


class ResourceListView(WithApplicationsMixin, ListView):
    template_name = 'chrono/manager_resource_list.html'
    model = Resource

    def dispatch(self, request, *args, **kwargs):
        self.with_applications_dispatch(request)
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.with_applications_queryset()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return self.with_applications_context_data(context)


resource_list = ResourceListView.as_view()


class ResourceDetailView(DetailView):
    template_name = 'chrono/manager_resource_detail.html'
    model = Resource

    def dispatch(self, request, *args, **kwargs):
        resource = self.get_object()
        if not resource.can_be_viewed(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['resource'] = self.object
        return context


resource_view = ResourceDetailView.as_view()


class DateMixin:
    def get_days(self):
        return [str(x) for x in range(1, 32)]

    def get_months(self):
        return [(str(x), MONTHS[x]) for x in range(1, 13)]

    def get_weeks(self):
        return [(str(x), _('Week %s') % x) for x in range(1, 53)]

    def get_week_dates(self):
        dates = {}
        for year in self.get_years():
            year = int(year)
            dates[year] = {}
            for week, week_label in self.get_weeks():
                date = datetime.datetime.strptime('%s-W%s-1' % (year, week), '%Y-W%W-%w')
                dates[year][date] = week_label
        return dates

    def get_years(self):
        year = now().year
        return [str(x) for x in range(year - 1, year + 5)]


class ResourceDayView(DateMixin, DayArchiveView):
    template_name = 'chrono/manager_resource_day_view.html'
    model = Event
    month_format = '%m'
    date_field = 'start_datetime'
    allow_empty = True
    allow_future = True

    def dispatch(self, request, *args, **kwargs):
        self.resource = get_object_or_404(Resource, pk=kwargs['pk'])
        if not self.resource.can_be_viewed(request.user):
            raise PermissionDenied()
        # specify 6am time to get the expected timezone on daylight saving time
        # days.
        try:
            self.date = make_aware(
                datetime.datetime.strptime(
                    '%s-%s-%s 06:00' % (self.get_year(), self.get_month(), self.get_day()), '%Y-%m-%d %H:%M'
                )
            )
        except ValueError:  # day is out of range for month
            # redirect to last day of month
            try:
                date = datetime.date(int(self.get_year()), int(self.get_month()), 1)
            except ValueError:
                raise Http404
            date += datetime.timedelta(days=40)
            date = date.replace(day=1)
            date -= datetime.timedelta(days=1)
            return HttpResponseRedirect(
                reverse(
                    'chrono-manager-resource-day-view',
                    kwargs={'pk': self.resource.pk, 'year': date.year, 'month': date.month, 'day': date.day},
                )
            )
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = (
            self.resource.event_set.all()
            .select_related('meeting_type', 'agenda')
            .prefetch_related('booking_set', 'booking_set__lease')
        )
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['resource'] = self.resource
        context['hour_span'] = 1
        durations = MeetingType.objects.filter(agenda__resources=self.resource).values_list(
            'duration', flat=True
        )
        if durations:
            gcd = durations[0]
            for duration in durations[1:]:
                gcd = math.gcd(duration, gcd)
            context['hour_span'] = max(round(60 / gcd), 1)

        return context

    def get_previous_day_url(self):
        previous_day = self.date.date() - datetime.timedelta(days=1)
        return reverse(
            'chrono-manager-resource-day-view',
            kwargs={
                'pk': self.resource.pk,
                'year': previous_day.year,
                'month': previous_day.strftime('%m'),
                'day': previous_day.strftime('%d'),
            },
        )

    def get_next_day_url(self):
        next_day = self.date.date() + datetime.timedelta(days=1)
        return reverse(
            'chrono-manager-resource-day-view',
            kwargs={
                'pk': self.resource.pk,
                'year': next_day.year,
                'month': next_day.strftime('%m'),
                'day': next_day.strftime('%d'),
            },
        )

    def get_timetable_infos(self):
        interval = datetime.timedelta(minutes=60)

        min_event = max_event = None
        timeperiods = TimePeriod.objects.filter(desk__agenda__resources=self.resource).aggregate(
            Min('start_time'), Max('end_time')
        )
        min_timeperiod = timeperiods['start_time__min']
        max_timeperiod = timeperiods['end_time__max']
        active_events = [
            x for x in self.object_list if any([y.cancellation_datetime is None for y in x.booking_set.all()])
        ]
        if active_events:
            min_event = min(localtime(x.start_datetime).time() for x in active_events)
            max_event = max(localtime(x.start_datetime + interval).time() for x in active_events)
        if min_timeperiod is None and min_event is None:
            return
        min_display = min(min_timeperiod or datetime.time(23), min_event or datetime.time(23))
        max_display = max(max_timeperiod or datetime.time(0), max_event or datetime.time(0))

        current_date = self.date.replace(hour=min_display.hour, minute=0)
        max_date = self.date.replace(hour=max_display.hour, minute=0)
        if max_display == max_timeperiod and max_display.minute != 0:
            # until the end of the last hour.
            max_date += datetime.timedelta(hours=1)

        while current_date < max_date:
            info = {}
            info['bookings'] = bookings = []  # bookings for this resource
            finish_datetime = current_date + interval
            for event in [
                x
                for x in self.object_list
                if x.start_datetime >= current_date and x.start_datetime < finish_datetime
            ]:
                # don't consider cancelled bookings
                for booking in [x for x in event.booking_set.all() if not x.cancellation_datetime]:
                    booking.css_top = int(100 * event.start_datetime.minute / 60)
                    booking.css_height = int(100 * event.meeting_type.duration / 60)
                    bookings.append(booking)
            yield current_date, info
            current_date += interval


resource_day_view = ResourceDayView.as_view()


class ResourceWeekMonthMixin:
    model = Event
    month_format = '%m'
    date_field = 'start_datetime'
    allow_empty = True
    allow_future = True

    def dispatch(self, request, *args, **kwargs):
        self.resource = get_object_or_404(Resource, pk=kwargs['pk'])
        if not self.resource.can_be_viewed(request.user):
            raise PermissionDenied()
        try:
            self.date = make_aware(
                datetime.datetime.strptime(
                    '%s-%s-%s 06:00' % (self.get_year(), self.get_month(), self.get_day()), '%Y-%m-%d %H:%M'
                )
            )
        except ValueError:
            raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = (
            self.resource.event_set.all()
            .select_related('meeting_type', 'agenda')
            .prefetch_related('booking_set', 'booking_set__lease')
        )
        return queryset

    @property
    def first_day(self):
        first_day = self.date
        if self.kind == 'month':
            first_day -= datetime.timedelta(days=first_day.day - 1)
        else:
            first_day -= datetime.timedelta(days=first_day.weekday())
        return first_day

    def get_dated_queryset(self, **kwargs):
        # adjust min and max, incorrect as DayArchiveView is used to have Y/m/d in url
        kwargs['start_datetime__gte'] = self._make_date_lookup_arg(self.first_day)
        kwargs['start_datetime__lt'] = self._make_date_lookup_arg(
            getattr(self, 'get_next_%s' % self.kind)(self.first_day)
        )
        return super().get_dated_queryset(**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['resource'] = self.resource
        context['kind'] = self.kind
        context['hour_span'] = 1
        durations = MeetingType.objects.filter(agenda__resources=self.resource).values_list(
            'duration', flat=True
        )
        if durations:
            gcd = durations[0]
            for duration in durations[1:]:
                gcd = math.gcd(duration, gcd)
            context['hour_span'] = max(round(60 / gcd), 1)

        return context

    @cached_property
    def weekdays(self):
        return TimePeriod.objects.filter(desk__agenda__resources=self.resource).values_list(
            'weekday', flat=True
        )

    def get_active_events(self):
        return [
            x for x in self.object_list if any([y.cancellation_datetime is None for y in x.booking_set.all()])
        ]

    @cached_property
    def hide_sunday(self):
        hide_sunday_timeperiod = 6 not in self.weekdays
        hide_sunday_event = not any([x.start_datetime.weekday() == 6 for x in self.get_active_events()])
        return hide_sunday_timeperiod and hide_sunday_event

    @cached_property
    def hide_weekend(self):
        if not self.hide_sunday:
            return False

        hide_weekend_timeperiod = 5 not in self.weekdays
        hide_weekend_event = not any([x.start_datetime.weekday() == 5 for x in self.get_active_events()])
        return hide_weekend_timeperiod and hide_weekend_event

    def get_timetable_infos(self):
        interval = datetime.timedelta(minutes=60)

        min_event = max_event = None
        timeperiods = TimePeriod.objects.filter(desk__agenda__resources=self.resource).aggregate(
            Min('start_time'), Max('end_time')
        )
        min_timeperiod = timeperiods['start_time__min']
        self.max_timeperiod = timeperiods['end_time__max']
        active_events = self.get_active_events()
        if active_events:
            min_event = min(localtime(x.start_datetime).time() for x in active_events)
            max_event = max(localtime(x.start_datetime + interval).time() for x in active_events)
        if min_timeperiod is None and min_event is None:
            return
        self.min_display = min(min_timeperiod or datetime.time(23), min_event or datetime.time(23))
        self.max_display = max(self.max_timeperiod or datetime.time(0), max_event or datetime.time(0))

        # avoid displaying empty first week
        first_week_offset = 0
        first_week_number = self.first_day.isocalendar()[1]
        last_week_number = first_week_number
        if self.kind == 'month':
            first_week_offset = int(
                (self.hide_sunday and self.first_day.weekday() == 6)
                or (self.hide_weekend and self.first_day.weekday() == 5)
            )
            if first_week_number >= 52:
                first_week_number = 0
            last_day = self.get_next_month(self.first_day.date()) - datetime.timedelta(days=1)
            last_week_number = last_day.isocalendar()[1]

            if last_week_number < first_week_number:  # new year
                last_week_number = 53

        for week_number in range(first_week_number + first_week_offset, last_week_number + 1):
            yield self.get_week_timetable_infos(
                week_number - first_week_number,
                week_end_offset=int(self.hide_sunday) + int(self.hide_weekend),
            )

    def get_week_timetable_infos(self, week_index, week_end_offset=0):
        date = self.first_day + datetime.timedelta(week_index * 7)
        dow = date.isocalendar()[2]
        start_date = date - datetime.timedelta(dow)

        interval = datetime.timedelta(minutes=60)
        period = self.first_day.replace(hour=self.min_display.hour, minute=0)
        max_date = self.first_day.replace(hour=self.max_display.hour, minute=0)
        if self.max_display == self.max_timeperiod and self.max_display.minute != 0:
            # until the end of the last hour.
            max_date += datetime.timedelta(hours=1)

        periods = []
        while period < max_date:
            periods.append(period)
            period = period + interval

        return {
            'days': [
                self.get_day_timetable_infos(start_date + datetime.timedelta(i), interval)
                for i in range(1, 8 - week_end_offset)
            ],
            'periods': periods,
        }

    def get_day_timetable_infos(self, day, interval):
        day = make_aware(make_naive(day))  # give day correct timezone
        period = current_date = day.replace(hour=self.min_display.hour, minute=0)
        timetable = {
            'date': current_date,
            'today': day.date() == datetime.date.today(),
            'other_month': day.month != self.date.month,
            'infos': {'booked_slots': []},
        }

        max_date = day.replace(hour=self.max_display.hour, minute=0)

        # compute booking and opening hours only for current month/week
        if self.kind == 'month' and timetable['other_month']:
            return timetable

        while period <= max_date:
            period_end = period + interval
            for event in [
                x for x in self.object_list if x.start_datetime >= period and x.start_datetime < period_end
            ]:
                # don't consider cancelled bookings
                bookings = [x for x in event.booking_set.all() if not x.cancellation_datetime]
                if not bookings:
                    continue
                booking_duration = (event.end_datetime - event.start_datetime).seconds // 60
                booking = {
                    'css_top': 100 * (event.start_datetime - current_date).seconds // 3600,
                    'css_height': 100 * booking_duration // 60,
                    'booking': bookings[0],
                }
                timetable['infos']['booked_slots'].append(booking)
            period += interval

        return timetable


class ResourceWeekView(ResourceWeekMonthMixin, DateMixin, DayArchiveView, WeekMixin):
    template_name = 'chrono/manager_resource_week_view.html'
    kind = 'week'

    def get_previous_week(self, date):
        return date - datetime.timedelta(days=7)

    def get_next_week(self, date):
        return date + datetime.timedelta(days=7)

    def get_previous_week_url(self):
        previous_week = self.get_previous_week(self.first_day.date())
        return reverse(
            'chrono-manager-resource-week-view',
            kwargs={
                'pk': self.resource.pk,
                'year': previous_week.year,
                'month': previous_week.strftime('%m'),
                'day': previous_week.strftime('%d'),
            },
        )

    def get_next_week_url(self):
        next_week = self.get_next_week(self.first_day.date())
        return reverse(
            'chrono-manager-resource-week-view',
            kwargs={
                'pk': self.resource.pk,
                'year': next_week.year,
                'month': next_week.strftime('%m'),
                'day': next_week.strftime('%d'),
            },
        )

    @property
    def last_day(self):
        return self.first_day + datetime.timedelta(days=6 - int(self.hide_sunday) - int(self.hide_weekend))


resource_weekly_view = ResourceWeekView.as_view()


class ResourceMonthView(ResourceWeekMonthMixin, DateMixin, DayArchiveView):
    template_name = 'chrono/manager_resource_month_view.html'
    kind = 'month'

    def get_previous_month_url(self):
        previous_month = self.get_previous_month(self.first_day.date())
        return reverse(
            'chrono-manager-resource-month-view',
            kwargs={
                'pk': self.resource.pk,
                'year': previous_month.year,
                'month': previous_month.strftime('%m'),
                'day': previous_month.strftime('%d'),
            },
        )

    def get_next_month_url(self):
        next_month = self.get_next_month(self.first_day.date())
        return reverse(
            'chrono-manager-resource-month-view',
            kwargs={
                'pk': self.resource.pk,
                'year': next_month.year,
                'month': next_month.strftime('%m'),
                'day': next_month.strftime('%d'),
            },
        )


resource_monthly_view = ResourceMonthView.as_view()


class ResourceRedirectView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return reverse('chrono-manager-resource-view', kwargs={'pk': kwargs['pk']})


resource_redirect_view = ResourceRedirectView.as_view()


class ResourceAddView(CreateView):
    template_name = 'chrono/manager_resource_form.html'
    model = Resource
    fields = ['label', 'description']

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-resource-view', kwargs={'pk': self.object.id})

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request, comment=pgettext('snapshot', 'created'))
        return response


resource_add = ResourceAddView.as_view()


class ResourceEditView(UpdateView):
    template_name = 'chrono/manager_resource_form.html'
    model = Resource
    fields = ['label', 'slug', 'description']

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-resource-view', kwargs={'pk': self.object.id})

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request)
        return response


resource_edit = ResourceEditView.as_view()


class ResourceDeleteView(DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = Resource

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-resource-list')

    def post(self, *args, **kwargs):
        self.get_object().take_snapshot(request=self.request, deletion=True)
        return super().post(*args, **kwargs)


resource_delete = ResourceDeleteView.as_view()


class ResourceInspectView(DetailView):
    template_name = 'chrono/manager_resource_inspect.html'
    model = Resource

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


resource_inspect = ResourceInspectView.as_view()


class ResourceHistoryView(InstanceWithSnapshotHistoryView):
    template_name = 'chrono/manager_resource_history.html'
    model = ResourceSnapshot
    instance_context_key = 'resource'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


resource_history = ResourceHistoryView.as_view()


class ResourceHistoryCompareView(InstanceWithSnapshotHistoryCompareView):
    template_name = 'chrono/manager_resource_history_compare.html'
    inspect_template_name = 'chrono/manager_resource_inspect_fragment.html'
    model = Resource
    instance_context_key = 'resource'
    history_view = 'chrono-manager-resource-history'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


resource_history_compare = ResourceHistoryCompareView.as_view()


class CategoryListView(WithApplicationsMixin, ListView):
    template_name = 'chrono/manager_category_list.html'
    model = Category

    def dispatch(self, request, *args, **kwargs):
        self.with_applications_dispatch(request)
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.with_applications_queryset()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return self.with_applications_context_data(context)


category_list = CategoryListView.as_view()


class CategoryAddView(CreateView):
    template_name = 'chrono/manager_category_form.html'
    model = Category
    fields = ['label']

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-category-list')

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request, comment=pgettext('snapshot', 'created'))
        return response


category_add = CategoryAddView.as_view()


class CategoryEditView(UpdateView):
    template_name = 'chrono/manager_category_form.html'
    model = Category
    fields = ['label', 'slug']

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-category-list')

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request)
        return response


category_edit = CategoryEditView.as_view()


class CategoryDeleteView(DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = Category

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-category-list')

    def post(self, *args, **kwargs):
        self.get_object().take_snapshot(request=self.request, deletion=True)
        return super().post(*args, **kwargs)


category_delete = CategoryDeleteView.as_view()


class CategoryInspectView(DetailView):
    template_name = 'chrono/manager_category_inspect.html'
    model = Category

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


category_inspect = CategoryInspectView.as_view()


class CategoryHistoryView(InstanceWithSnapshotHistoryView):
    template_name = 'chrono/manager_category_history.html'
    model = CategorySnapshot
    instance_context_key = 'category'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


category_history = CategoryHistoryView.as_view()


class CategoryHistoryCompareView(InstanceWithSnapshotHistoryCompareView):
    template_name = 'chrono/manager_category_history_compare.html'
    inspect_template_name = 'chrono/manager_category_inspect_fragment.html'
    model = Category
    instance_context_key = 'category'
    history_view = 'chrono-manager-category-history'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


category_history_compare = CategoryHistoryCompareView.as_view()


class EventsTypeListView(WithApplicationsMixin, ListView):
    template_name = 'chrono/manager_events_type_list.html'
    model = EventsType

    def dispatch(self, request, *args, **kwargs):
        self.with_applications_dispatch(request)
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.with_applications_queryset()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return self.with_applications_context_data(context)


events_type_list = EventsTypeListView.as_view()


class EventsTypeAddView(CreateView):
    template_name = 'chrono/manager_events_type_form.html'
    model = EventsType
    fields = ['label']

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-events-type-view', args=[self.object.pk])

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request, comment=pgettext('snapshot', 'created'))
        return response


events_type_add = EventsTypeAddView.as_view()


class EventsTypeDetailView(DetailView):
    template_name = 'chrono/manager_events_type_detail.html'
    model = EventsType
    context_object_name = 'events_type'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


events_type_view = EventsTypeDetailView.as_view()


class EventsTypeEditView(UpdateView):
    template_name = 'chrono/manager_events_type_form.html'
    model = EventsType
    fields = ['label', 'slug']

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-events-type-view', args=[self.object.pk])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        data = None
        if self.request.method == 'POST':
            data = self.request.POST
        context['formset'] = CustomFieldFormSet(
            data=data,
            initial=self.get_object().get_custom_fields(),
        )
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)  # save object and update cache only once
        return HttpResponseRedirect(self.get_success_url())

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        formset = CustomFieldFormSet(data=self.request.POST)
        if form.is_valid() and formset.is_valid():
            response = self.form_valid(form)
            self.object.custom_fields = []
            for sub_data in formset.cleaned_data:
                if not sub_data.get('varname'):
                    continue
                self.object.custom_fields.append(sub_data)
            self.object.save()
            self.object.take_snapshot(request=self.request)
            return response
        else:
            return self.form_invalid(form)


events_type_edit = EventsTypeEditView.as_view()


class EventsTypeDeleteView(DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = EventsType

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def can_be_deleted(self):
        return self.get_object().agendas.count() == 0

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.can_be_deleted():
            return ctx

        obj = self.get_object()
        ctx['cannot_delete'] = True

        title = '<h3>%(title)s</h3>' % {
            'title': _('Cannot delete event type "%(event_type_name)s"') % {'event_type_name': obj}
        }
        msg = _('Following agendas are still using this event type:')
        lst = '<ul><li>%(agendas)s</li></ul>' % {
            'agendas': '</li><li>'.join(
                [
                    '<a href="%s">%s</a>'
                    % (reverse('chrono-manager-agenda-view', kwargs={'pk': agenda.pk}), agenda)
                    for agenda in obj.agendas.all()
                ]
            )
        }
        ctx['cannot_delete_msg'] = SafeString(
            '%(title)s %(msg)s %(agendas)s' % {'title': title, 'msg': msg, 'agendas': lst}
        )

        return ctx

    def post(self, *args, **kwargs):
        if self.can_be_deleted():
            self.get_object().take_snapshot(request=self.request, deletion=True)
            return super().post(*args, **kwargs)
        raise PermissionDenied()

    def get_success_url(self):
        return reverse('chrono-manager-events-type-list')


events_type_delete = EventsTypeDeleteView.as_view()


class EventsTypeInspectView(DetailView):
    template_name = 'chrono/manager_events_type_inspect.html'
    model = EventsType
    context_object_name = 'events_type'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


events_type_inspect = EventsTypeInspectView.as_view()


class EventsTypeHistoryView(InstanceWithSnapshotHistoryView):
    template_name = 'chrono/manager_events_type_history.html'
    model = EventsTypeSnapshot
    instance_context_key = 'events_type'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


events_type_history = EventsTypeHistoryView.as_view()


class EventsTypeHistoryCompareView(InstanceWithSnapshotHistoryCompareView):
    template_name = 'chrono/manager_events_type_history_compare.html'
    inspect_template_name = 'chrono/manager_events_type_inspect_fragment.html'
    model = EventsType
    instance_context_key = 'events_type'
    history_view = 'chrono-manager-events-type-history'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


events_type_history_compare = EventsTypeHistoryCompareView.as_view()


class AgendaAddView(CreateView):
    template_name = 'chrono/manager_agenda_add_form.html'
    model = Agenda
    form_class = AgendaAddForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-agenda-settings', kwargs={'pk': self.object.id})

    def form_valid(self, form):
        result = super().form_valid(form)
        if self.object.kind == 'events':
            desk = Desk.objects.create(agenda=self.object, slug='_exceptions_holder')
            desk.import_timeperiod_exceptions_from_settings()
        self.object.take_snapshot(request=self.request, comment=pgettext('snapshot', 'created'))
        return result


agenda_add = AgendaAddView.as_view()


class AgendasImportView(FormView):
    form_class = AgendasImportForm
    template_name = 'chrono/agendas_import.html'
    success_url = reverse_lazy('chrono-manager-homepage')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        try:
            agendas_json = json.loads(force_str(self.request.FILES['agendas_json'].read()))
        except ValueError:
            form.add_error('agendas_json', _('File is not in the expected JSON format.'))
            return self.form_invalid(form)

        try:
            results = import_site(agendas_json, overwrite=False)
        except AgendaImportError as exc:
            form.add_error('agendas_json', '%s' % exc)
            return self.form_invalid(form)
        except KeyError as exc:
            form.add_error('agendas_json', _('Key "%s" is missing.') % exc.args[0])
            return self.form_invalid(form)

        import_messages = {
            'agendas': {
                'create_noop': _('No agenda created.'),
                'create': lambda x: ngettext(
                    'An agenda has been created.',
                    '%(count)d agendas have been created.',
                    x,
                ),
                'update_noop': _('No agenda updated.'),
                'update': lambda x: ngettext(
                    'An agenda has been updated.',
                    '%(count)d agendas have been updated.',
                    x,
                ),
            },
            'unavailability_calendars': {
                'create_noop': _('No unavailability calendar created.'),
                'create': lambda x: ngettext(
                    'An unavailability calendar has been created.',
                    '%(count)d unavailability calendars have been created.',
                    x,
                ),
                'update_noop': _('No unavailability calendar updated.'),
                'update': lambda x: ngettext(
                    'An unavailability calendar has been updated.',
                    '%(count)d unavailability calendars have been updated.',
                    x,
                ),
            },
            'events_types': {
                'create_noop': _('No events type created.'),
                'create': lambda x: ngettext(
                    'An events type has been created.',
                    '%(count)d events types have been created.',
                    x,
                ),
                'update_noop': _('No events type updated.'),
                'update': lambda x: ngettext(
                    'An events type has been updated.',
                    '%(count)d events types have been updated.',
                    x,
                ),
            },
            'resources': {
                'create_noop': _('No resource created.'),
                'create': lambda x: ngettext(
                    'A resource has been created.',
                    '%(count)d resources have been created.',
                    x,
                ),
                'update_noop': _('No resource updated.'),
                'update': lambda x: ngettext(
                    'A resource has been updated.',
                    '%(count)d resources have been updated.',
                    x,
                ),
            },
            'categories': {
                'create_noop': _('No category created.'),
                'create': lambda x: ngettext(
                    'A category has been created.',
                    '%(count)d categories have been created.',
                    x,
                ),
                'update_noop': _('No category updated.'),
                'update': lambda x: ngettext(
                    'A category has been updated.',
                    '%(count)d categories have been updated.',
                    x,
                ),
            },
        }

        global_noop = True
        for obj_name, obj_results in results.items():
            for obj in obj_results['all']:
                obj.take_snapshot(request=self.request, comment=_('imported'))
            if obj_results['all']:
                global_noop = False
                count = len(obj_results['created'])
                if not count:
                    message1 = import_messages[obj_name]['create_noop']
                else:
                    message1 = import_messages[obj_name]['create'](count) % {'count': count}

                count = len(obj_results['updated'])
                if not count:
                    message2 = import_messages[obj_name]['update_noop']
                else:
                    message2 = import_messages[obj_name]['update'](count) % {'count': count}

                obj_results['messages'] = '%s %s' % (message1, message2)

        a_count, uc_count = (
            len(results['agendas']['all']),
            len(results['unavailability_calendars']['all']),
        )
        if (a_count, uc_count) == (1, 0):
            # only one agenda imported, redirect to settings page
            return HttpResponseRedirect(
                reverse('chrono-manager-agenda-settings', kwargs={'pk': results['agendas']['all'][0].pk})
            )
        if (a_count, uc_count) == (0, 1):
            # only one unavailability calendar imported, redirect to settings page
            return HttpResponseRedirect(
                reverse(
                    'chrono-manager-unavailability-calendar-settings',
                    kwargs={'pk': results['unavailability_calendars']['all'][0].pk},
                )
            )

        if global_noop:
            messages.info(self.request, _('No data found.'))
        else:
            messages.info(self.request, results['agendas']['messages'])
            messages.info(self.request, results['unavailability_calendars']['messages'])
            messages.info(self.request, results['events_types']['messages'])
            messages.info(self.request, results['resources']['messages'])
            messages.info(self.request, results['categories']['messages'])

        return super().form_valid(form)


agendas_import = AgendasImportView.as_view()


class ViewableAgendaMixin:
    agenda = None

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, id=kwargs.get('pk'))

    def dispatch(self, request, *args, **kwargs):
        self.set_agenda(**kwargs)
        if not self.check_permissions(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def check_permissions(self, user):
        return self.agenda.can_be_viewed(user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = self.agenda
        return context


class EditableAgendaMixin(ViewableAgendaMixin):
    tab_anchor = None

    def check_permissions(self, user):
        return self.agenda.can_be_edited(user)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if isinstance(self, DeleteView):
            return kwargs

        if not kwargs.get('instance'):
            kwargs['instance'] = self.model()
        kwargs['instance'].agenda = self.agenda
        return kwargs

    def get_success_url(self):
        url = reverse('chrono-manager-agenda-settings', kwargs={'pk': self.agenda.id})
        if self.tab_anchor:
            url += '#open:%s' % self.tab_anchor
        return url


class ManagedAgendaMixin(EditableAgendaMixin):
    def check_permissions(self, user):
        return self.agenda.can_be_managed(user)


class AgendaEditView(ManagedAgendaMixin, UpdateView):
    template_name = 'chrono/manager_agenda_form.html'
    title = _('Edit Agenda')
    model = Agenda
    form_class = AgendaEditForm
    comment = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.title
        return context

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.agenda = Agenda.objects.get(pk=self.agenda.pk)  # refresh object, for M2M
        self.agenda.take_snapshot(request=self.request, comment=self.comment)
        return response


agenda_edit = AgendaEditView.as_view()


class AgendaBookingDelaysView(AgendaEditView):
    form_class = AgendaBookingDelaysForm
    title = _('Configure booking delays')
    tab_anchor = 'delays'
    comment = _('changed booking delays')

    def check_permissions(self, user):
        return self.agenda.can_be_edited(user)


agenda_booking_delays = AgendaBookingDelaysView.as_view()


class AgendaRolesView(AgendaEditView):
    form_class = AgendaRolesForm
    title = _('Configure roles')
    tab_anchor = 'permissions'
    comment = _('changed roles')


agenda_roles = AgendaRolesView.as_view()


class AgendaDisplaySettingsView(AgendaEditView):
    form_class = AgendaDisplaySettingsForm
    title = _('Configure display options')
    tab_anchor = 'display-options'
    comment = _('changed display options')

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda.objects.exclude(kind='virtual'), pk=kwargs.get('pk'))

    def get_initial(self):
        return {'booking_user_block_template': self.agenda.get_booking_user_block_template()}


agenda_display_settings = AgendaDisplaySettingsView.as_view()


class AgendaBackofficeBookingView(AgendaEditView):
    form_class = AgendaBackofficeBookingForm
    title = _('Configure backoffice booking options')
    tab_anchor = 'backoffice-booking'
    comment = _('changed backoffice booking options')

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda.objects.filter(kind='meetings'), pk=kwargs.get('pk'))


agenda_backoffice_booking = AgendaBackofficeBookingView.as_view()


class AgendaPartialBookingsSettingsView(AgendaEditView):
    form_class = AgendaPartialBookingsSettingsForm
    title = _('Configure booking settings')
    tab_anchor = 'partial-bookings-settings'
    comment = _('changed booking settings')

    def check_permissions(self, user):
        return self.agenda.can_be_edited(user)


agenda_partial_bookings_settings = AgendaPartialBookingsSettingsView.as_view()


class AgendaBookingCheckSettingsView(AgendaEditView):
    form_class = AgendaBookingCheckSettingsForm
    title = _('Configure booking check options')
    tab_anchor = 'booking-check-options'
    comment = _('changed booking check options')

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='events')


agenda_booking_check_settings = AgendaBookingCheckSettingsView.as_view()


class AgendaInvoicingSettingsView(AgendaEditView):
    form_class = AgendaInvoicingSettingsForm
    title = _('Configure invoicing options')
    tab_anchor = 'invoicing-options'
    comment = _('changed invoicing options')

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='events', partial_bookings=True)


agenda_invoicing_settings = AgendaInvoicingSettingsView.as_view()


class AgendaDeleteView(DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = Agenda
    success_url = reverse_lazy('chrono-manager-homepage')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cannot_delete'] = Booking.objects.filter(
            event__agenda=self.get_object(),
            event__start_datetime__gt=now(),
            cancellation_datetime__isnull=True,
        ).exists()
        context['cannot_delete_msg'] = FUTURE_BOOKING_ERROR_MSG
        return context

    def form_valid(self, form):
        self.object = self.get_object()
        context = self.get_context_data()
        if context['cannot_delete']:
            raise PermissionDenied()
        self.object.take_snapshot(request=self.request, deletion=True)
        return super().form_valid(form)


agenda_delete = AgendaDeleteView.as_view()


class AgendaView(ViewableAgendaMixin, View):
    def set_agenda(self, **kwargs):
        if kwargs.get('pk'):
            self.agenda = get_object_or_404(Agenda, pk=kwargs['pk'])
        else:
            self.agenda = get_object_or_404(Agenda, slug=kwargs['slug'])

    def get(self, request, *args, **kwargs):
        if self.agenda.default_view == 'day':
            return redirect('chrono-manager-agenda-day-redirect-view', pk=self.agenda.pk)

        if self.agenda.default_view == 'week':
            return redirect('chrono-manager-agenda-week-redirect-view', pk=self.agenda.pk)

        if self.agenda.default_view == 'month':
            return redirect('chrono-manager-agenda-month-redirect-view', pk=self.agenda.pk)

        return redirect('chrono-manager-agenda-open-events-view', pk=self.agenda.pk)


agenda_view = AgendaView.as_view()


class AgendaMonthRedirectView(ViewableAgendaMixin, View):
    def get_day(self):
        today = datetime.date.today()
        if self.agenda.kind != 'events':
            return today

        # first day where there are events,
        # otherwise latest day with events, otherwise today.
        event = self.agenda.event_set.filter(start_datetime__gte=today).first()
        if not event:
            event = self.agenda.event_set.filter(start_datetime__lte=today).last()
        if event:
            return localtime(event.start_datetime)
        return today

    def get(self, request, *args, **kwargs):
        day = self.get_day()
        return redirect(
            'chrono-manager-agenda-month-view',
            pk=self.agenda.pk,
            year=day.year,
            month=day.strftime('%m'),
            day=day.strftime('%d'),
        )


agenda_month_redirect_view = AgendaMonthRedirectView.as_view()


class AgendaWeekRedirectView(AgendaMonthRedirectView):
    def get(self, request, *args, **kwargs):
        day = self.get_day()
        return redirect(
            'chrono-manager-agenda-week-view',
            pk=self.agenda.pk,
            year=day.year,
            month=day.strftime('%m'),
            day=day.strftime('%d'),
        )


agenda_week_redirect_view = AgendaWeekRedirectView.as_view()


class AgendaDayRedirectView(AgendaMonthRedirectView):
    def get(self, request, *args, **kwargs):
        day = self.get_day()
        return redirect(
            'chrono-manager-agenda-day-view',
            pk=self.agenda.pk,
            year=day.year,
            month=day.strftime('%m'),
            day=day.strftime('%d'),
        )


agenda_day_redirect_view = AgendaDayRedirectView.as_view()


class AgendaDateView(DateMixin, ViewableAgendaMixin):
    model = Event
    month_format = '%m'
    date_field = 'start_datetime'
    allow_empty = True
    allow_future = True
    allow_booking = False
    add_booking_view_name = 'chrono-manager-agenda-add-booking-view'

    def set_agenda(self, **kwargs):
        super().set_agenda(**kwargs)
        if self.agenda.kind == 'virtual':
            self.agenda._excluded_timeperiods = self.agenda.excluded_timeperiods.all()

    def dispatch(self, request, *args, **kwargs):
        # specify 6am time to get the expected timezone on daylight saving time
        # days.
        try:
            self.date = make_aware(
                datetime.datetime.strptime(
                    '%s-%s-%s 06:00' % (self.get_year(), self.get_month(), self.get_day()), '%Y-%m-%d %H:%M'
                )
            )
        except ValueError:  # day is out of range for month
            # redirect to last day of month
            try:
                date = datetime.date(int(self.get_year()), int(self.get_month()), 1)
            except ValueError:
                raise Http404
            date += datetime.timedelta(days=40)
            date = date.replace(day=1)
            date -= datetime.timedelta(days=1)
            return HttpResponseRedirect(
                reverse(
                    'chrono-manager-agenda-day-view',
                    kwargs={'pk': kwargs['pk'], 'year': date.year, 'month': date.month, 'day': date.day},
                )
            )
        return super().dispatch(request, *args, **kwargs)

    @property
    def first_day(self):
        return self.date

    @property
    def last_day(self):
        return self.date

    @cached_property
    def base_meeting_duration(self):
        try:
            return self.agenda.get_base_meeting_duration()
        except ValueError:
            pass

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = self.agenda
        context['kind'] = self.kind
        if self.agenda.kind != 'events':
            if self.base_meeting_duration:
                context['hour_span'] = max(round(60 / self.base_meeting_duration), 1)
            else:  # no meeting types defined
                context['hour_span'] = 1
            context['booking_colors'] = BookingColor.objects.filter(
                bookings__event__in=self.object_list, bookings__cancellation_datetime__isnull=True
            ).distinct()
        if self.agenda.kind == 'meetings':
            self.allow_booking = self.agenda.allow_booking_from_backoffice(self.first_day, self.last_day)
            context['allow_booking'] = self.allow_booking
            context['add_booking_url'] = reverse(
                self.add_booking_view_name,
                kwargs={
                    'pk': self.agenda.pk,
                    'year': self.kwargs['year'],
                    'month': self.kwargs['month'],
                    'day': self.kwargs['day'],
                },
            )
        context['user_can_edit'] = self.agenda.can_be_edited(self.request.user)
        return context

    def get_queryset(self):
        if self.agenda.kind == 'events':
            queryset = self.agenda.event_set.filter(recurrence_days__isnull=True)
            queryset = Event.annotate_booking_checks(queryset)
        else:
            self.agenda.prefetch_desks_and_exceptions(
                min_date=getattr(self, 'first_day', self.date), max_date=self.get_max_date()
            )
            if self.agenda.kind == 'meetings':
                queryset = self.agenda.event_set.select_related('meeting_type').prefetch_related(
                    'booking_set'
                )
            else:
                queryset = (
                    Event.objects.filter(agenda__virtual_agendas=self.agenda)
                    .select_related('meeting_type', 'agenda')
                    .prefetch_related('booking_set', 'booking_set__lease')
                )
        return queryset

    def get_exceptions_from_excluded_periods(self, date):
        return [
            TimePeriodException(
                start_datetime=make_aware(datetime.datetime.combine(date, period.start_time)),
                end_datetime=make_aware(datetime.datetime.combine(date, period.end_time)),
            )
            for period in getattr(self.agenda, '_excluded_timeperiods', [])
            if period.weekday == date.weekday()
        ]

    @cached_property
    def bookable_slot_datetimes(self):
        shortest_meeting_type = sorted(self.agenda.cached_meetingtypes, key=lambda x: x.duration)[0]
        slots = self.agenda.get_all_slots(
            shortest_meeting_type,
            start_datetime=self.first_day,
            end_datetime=self.last_day + datetime.timedelta(days=1),
            unique=True,
        )
        return {slot.start_datetime for slot in slots if slot.start_datetime > now() and not slot.full}

    def get_opening_hour_data(self, opening_hour, start_date):
        opening_hour_duration = (opening_hour.end - opening_hour.begin).seconds
        data = {
            'css_top': 100 * (opening_hour.begin - start_date).seconds // 3600,
            'css_height': 100 * opening_hour_duration // 3600,
        }

        if self.kind == 'month' or not self.allow_booking:
            return data

        slot_duration = self.base_meeting_duration * 60
        existing_slots = [
            opening_hour.begin + datetime.timedelta(seconds=slot_duration * i)
            for i in range(opening_hour_duration // slot_duration)
        ]
        data.update(
            {
                'slots': [
                    {'datetime': slot, 'disabled': bool(slot not in self.bookable_slot_datetimes)}
                    for slot in existing_slots
                ],
                'css_step': slot_duration / opening_hour_duration * 100,
            }
        )

        return data


class EventChecksMixin:
    def get_filters(self, booked_queryset, subscription_queryset):
        agenda_filters = self.agenda.get_booking_check_filters()
        filters = collections.defaultdict(set)
        extra_data_from_booked = booked_queryset.filter(extra_data__has_any_keys=agenda_filters).values_list(
            'extra_data', flat=True
        )
        extra_data_from_subscriptions = subscription_queryset.filter(
            extra_data__has_any_keys=agenda_filters
        ).values_list('extra_data', flat=True)
        for extra_data in list(extra_data_from_booked) + list(extra_data_from_subscriptions):
            for k, v in extra_data.items():
                if not v:
                    continue
                if k in agenda_filters and not isinstance(v, (list, dict)):
                    filters[k].add(v)
        filters = sorted(filters.items())
        filters = {k: sorted(list(v)) for k, v in filters}
        return filters

    def add_filters_context(self, context, event, add_check_forms=True):
        # booking base queryset
        booking_qs_kwargs = {}
        if not self.agenda.subscriptions.exists():
            booking_qs_kwargs = {'cancellation_datetime__isnull': True}
        booking_qs = event.booking_set.prefetch_related('user_checks').order_by('start_time')
        booked_qs = booking_qs.filter(
            in_waiting_list=False, primary_booking__isnull=True, **booking_qs_kwargs
        )
        booked_qs = booked_qs.annotate(
            places_count=Value(1) + Count('secondary_booking_set', filter=Q(**booking_qs_kwargs))
        )

        # waiting list queryset
        waiting_qs = booking_qs.filter(
            in_waiting_list=True, primary_booking__isnull=True, **booking_qs_kwargs
        ).order_by('user_last_name', 'user_first_name')
        waiting_qs = waiting_qs.annotate(
            places_count=Value(1)
            + Count('secondary_booking_set', filter=Q(cancellation_datetime__isnull=True))
        )

        # subscription base queryset
        subscription_qs = (
            self.agenda.subscriptions.filter(
                date_start__lte=event.start_datetime, date_end__gt=event.start_datetime
            )
            # exclude user_external_id from booked_qs and waiting_qs
            .exclude(user_external_id__in=booked_qs.values('user_external_id')).exclude(
                user_external_id__in=waiting_qs.values('user_external_id')
            )
        )

        # annotate subscriptions with ovelapping booking by user
        start_date = event.start_datetime.date().replace(day=1)
        end_date = start_date + relativedelta(months=1)
        booked_events = Event.objects.filter(
            booking__user_external_id=OuterRef('user_external_id'),
            booking__cancellation_datetime__isnull=True,
            booking__user_checks__presence=True,
            start_datetime__range=[start_date, end_date],
        )
        booked_events = Event.annotate_queryset_with_overlaps(
            booked_events, Event.objects.filter(pk=event.pk)
        )
        overlapping_booked_events = booked_events.filter(has_overlap=True)
        subscription_qs = subscription_qs.annotate(
            overlapping_event=Subquery(overlapping_booked_events.values('label').order_by()[:1])
        )

        # build filters from booked_qs and subscription_qs
        filters = self.get_filters(booked_queryset=booked_qs, subscription_queryset=subscription_qs)
        # and filter booked and subscriptions
        booked_filterset = BookingCheckFilterSet(
            data=self.request.GET or None, queryset=booked_qs, agenda=self.agenda, filters=filters
        )
        subscription_filterset = SubscriptionCheckFilterSet(
            data=self.request.GET or None, queryset=subscription_qs, agenda=self.agenda, filters=filters
        )

        # build results from mixed booked and subscriptions
        results = []
        booked_without_status = False
        bookings = Booking.prefetch_audit_entries(agenda=self.agenda, queryset=booked_filterset.qs)
        for booking in bookings:
            booking.kind = 'booking'
            results.append(booking)

            if not add_check_forms:
                continue

            if booking.cancellation_datetime is None and not booking.user_check:
                booked_without_status = True
            if booking.cancellation_datetime is None:
                booking.absence_form = BookingCheckAbsenceForm(
                    agenda=self.agenda,
                    checked=bool(booking.user_check),
                    initial={'check_type': booking.user_check.type_slug if booking.user_check else None},
                )
            booking.presence_form = BookingCheckPresenceForm(
                agenda=self.agenda,
                initial={'check_type': booking.user_check.type_slug if booking.user_check else None},
            )
        for subscription in subscription_filterset.qs:
            subscription.kind = 'subscription'
            results.append(subscription)

            if not add_check_forms:
                continue

            subscription.presence_form = BookingCheckPresenceForm(
                agenda=self.agenda,
                subscription=True,
            )
        # sort results
        if (
            booked_filterset.form.is_valid()
            and booked_filterset.form.cleaned_data.get('sort') == 'firstname,lastname'
        ):
            sort_fields = ['user_first_name', 'user_last_name']
        else:
            sort_fields = ['user_last_name', 'user_first_name']
        results = sorted(results, key=attrgetter(*sort_fields, 'user_external_id'))

        # set context
        context['booked_without_status'] = booked_without_status
        context['filterset'] = booked_filterset
        context['results'] = results
        context['waiting'] = waiting_qs
        context['booked'] = booked_qs


class AgendaDayView(EventChecksMixin, AgendaDateView, DayArchiveView):
    kind = 'day'

    def get_queryset(self):
        qs = super().get_queryset()
        if self.agenda.kind != 'events':
            return qs
        return qs.order_by('start_datetime', 'label')

    def get_template_names(self):
        if self.agenda.kind == 'virtual':
            return ['chrono/manager_meetings_agenda_day_view.html']
        if self.agenda.partial_bookings_events:
            return ['chrono/manager_partial_bookings_day_view.html']
        return ['chrono/manager_%s_agenda_day_view.html' % self.agenda.kind]

    def get_previous_day_url(self):
        previous_day = self.date.date() - datetime.timedelta(days=1)
        return reverse(
            'chrono-manager-agenda-day-view',
            kwargs={
                'pk': self.agenda.id,
                'year': previous_day.year,
                'month': previous_day.strftime('%m'),
                'day': previous_day.strftime('%d'),
            },
        )

    def get_next_day_url(self):
        next_day = self.date.date() + datetime.timedelta(days=1)
        return reverse(
            'chrono-manager-agenda-day-view',
            kwargs={
                'pk': self.agenda.id,
                'year': next_day.year,
                'month': next_day.strftime('%m'),
                'day': next_day.strftime('%d'),
            },
        )

    def get_max_date(self):
        return self.date.date() + datetime.timedelta(days=1)

    def get_timetable_infos(self):
        timeperiods = itertools.chain(*(d.timeperiod_set.all() for d in self.agenda.prefetched_desks))
        timeperiods = [
            t
            for t in timeperiods
            if t.date == self.date.date()
            or t.weekday == self.date.weekday()
            and (not t.weekday_indexes or get_weekday_index(self.date) in t.weekday_indexes)
        ]

        interval = datetime.timedelta(minutes=60)

        min_timeperiod = max_timeperiod = min_event = max_event = None
        if timeperiods:
            min_timeperiod = min(x.start_time for x in timeperiods)
            max_timeperiod = max(x.end_time for x in timeperiods)
        active_events = [
            x for x in self.object_list if any([y.cancellation_datetime is None for y in x.booking_set.all()])
        ]
        if active_events:
            min_event = min(localtime(x.start_datetime).time() for x in active_events)
            max_event = max(localtime(x.start_datetime + interval).time() for x in active_events)
        if min_timeperiod is None and min_event is None:
            return
        min_display = min(min_timeperiod or datetime.time(23), min_event or datetime.time(23))
        max_display = max(max_timeperiod or datetime.time(0), max_event or datetime.time(0))

        current_date = self.date.replace(hour=min_display.hour, minute=0)
        start_date = current_date
        max_date = self.date.replace(hour=max_display.hour, minute=0)
        if max_display == max_timeperiod and max_display.minute != 0:
            # until the end of the last hour.
            max_date += datetime.timedelta(hours=1)

        first = True
        while current_date < max_date:
            # for each timeslot return the timeslot date and a list of per-desk
            # bookings
            infos = []  # various infos, for each desk
            for desk in self.agenda.prefetched_desks:
                info = {'desk': desk}
                if first:
                    # use first row to include opening hours
                    info['opening_hours'] = opening_hours = []
                    for opening_hour in desk.get_opening_hours(current_date.date()):
                        opening_hours.append(self.get_opening_hour_data(opening_hour, start_date))

                    # and exceptions for this desk
                    exceptions = desk.prefetched_exceptions + self.get_exceptions_from_excluded_periods(
                        current_date.date()
                    )
                    info['exceptions'] = []
                    for exception in exceptions:
                        if exception.end_datetime < current_date:
                            continue
                        if exception.start_datetime > max_date:
                            continue
                        start_max = max(start_date, exception.start_datetime)
                        end_min = min(max_date, exception.end_datetime)
                        exception.css_top = int(100 * (start_max - start_date).seconds // 3600)
                        exception.css_height = int(100 * (end_min - start_max).seconds // 3600)
                        info['exceptions'].append(exception)

                infos.append(info)
                info['bookings'] = bookings = []  # bookings for this desk
                finish_datetime = current_date + interval
                for event in [
                    x
                    for x in self.object_list
                    if x.desk_id == desk.id
                    and x.start_datetime >= current_date
                    and x.start_datetime < finish_datetime
                ]:
                    # don't consider cancelled bookings
                    for booking in [x for x in event.booking_set.all() if not x.cancellation_datetime]:
                        booking_duration = (event.end_datetime - event.start_datetime).seconds // 60
                        booking.css_top = int(100 * event.start_datetime.minute / 60)
                        booking.css_height = int(100 * booking_duration / 60)
                        bookings.append(booking)

            yield current_date, infos
            current_date += interval
            first = False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['today'] = localtime().date()

        if self.agenda.partial_bookings_events:
            self.fill_partial_bookings_context(context)
        return context

    def fill_partial_bookings_context(self, context):
        try:
            event = self.agenda.event_set.get(
                start_datetime__date=self.date.date(), recurrence_days__isnull=True
            )
        except Event.DoesNotExist:
            return

        context['event'] = event
        context['allow_check'] = (
            not event.checked or not self.agenda.disable_check_update
        ) and not event.check_locked
        self.add_filters_context(context, event, add_check_forms=False)

        min_time = localtime(event.start_datetime).time()
        max_time = event.end_time

        start_time = datetime.time(max(min_time.hour - 1, 0), 0)
        end_time = datetime.time(min(max_time.hour + 1, 23), 0)
        context['hours'] = [datetime.time(hour=i) for i in range(start_time.hour, end_time.hour + 1)]

        context['start_datetime'] = datetime.datetime.combine(self.date.date(), start_time)
        context['end_datetime'] = datetime.datetime.combine(self.date.date(), end_time)

        opening_range_minutes = (
            (end_time.hour + 1) * 60 + end_time.minute - (start_time.hour * 60 + start_time.minute)
        )

        def get_time_ratio(t1, t2):
            return str(
                round(100 * ((t1.hour - t2.hour) * 60 + t1.minute - t2.minute) / opening_range_minutes, 2)
            )

        bookings = [x for x in context['results'] if x.kind == 'booking']
        for booking in bookings:
            if booking.start_time:
                booking.css_left = get_time_ratio(booking.start_time, start_time)
                booking.css_width = get_time_ratio(booking.end_time, booking.start_time)

            booking.user_check_list = list(booking.user_checks.all())  # queryset is prefetched
            for check in booking.user_check_list:
                check.css_class = 'present' if check.presence else 'absent'

                if not check.start_time:
                    check.css_class += ' end-only'
                    check.css_left = 0
                    check.css_width = get_time_ratio(check.end_time, start_time)
                elif not check.end_time:
                    check.css_class += ' start-only'
                    check.css_left = get_time_ratio(check.start_time, start_time)
                else:
                    check.css_left = get_time_ratio(check.start_time, start_time)
                    check.css_width = get_time_ratio(check.end_time, check.start_time)

                if check.computed_start_time and check.computed_end_time:
                    check.computed_css_left = get_time_ratio(check.computed_start_time, start_time)
                    check.computed_css_width = get_time_ratio(
                        check.computed_end_time, check.computed_start_time
                    )

        users_info = {}
        for result in context['results']:
            user_info = users_info.setdefault(
                result.user_external_id,
                {
                    'name': result.user_name,
                    'bookings': [],
                    'booking_checks': [],
                    'check_url': reverse(
                        'chrono-manager-partial-booking-check',
                        kwargs={
                            'pk': self.agenda.pk,
                            'event_pk': event.pk,
                            'user_external_id': result.user_external_id,
                        },
                    ),
                },
            )
            if result.kind == 'subscription':
                user_info['subscription_pk'] = result.pk
                continue
            user_info['bookings'].append(result)
            user_info['booking_checks'].extend(result.user_check_list)

        context['users'] = users_info.values()

        if self.agenda.booking_extra_user_block_template:
            for user_info in context['users']:
                if user_info['bookings']:
                    user_info['extra_user_block_url'] = reverse(
                        'chrono-manager-booking-extra-user-block',
                        kwargs={'pk': self.agenda.pk, 'booking_pk': user_info['bookings'][0].pk},
                    )
                else:
                    user_info['extra_user_block_url'] = reverse(
                        'chrono-manager-subscription-extra-user-block',
                        kwargs={'pk': self.agenda.pk, 'subscription_pk': user_info['subscription_pk']},
                    )

        booked_qs = (
            context['booked']
            .annotate(
                computed_end_time=ExpressionWrapper(
                    # trick to exclude upper bound from postgres generate_series function,
                    # because a booking ending at 10:00 should not count for 10:00 - 11:00 slot
                    F('end_time') - datetime.timedelta(minutes=1),
                    output_field=models.TimeField(),
                ),
                start_hour=Cast('start_time__hour', models.IntegerField()),
                end_hour=Cast('computed_end_time__hour', models.IntegerField()),
                hour=Func(F('start_hour'), F('end_hour'), function='generate_series'),
            )
            .order_by()
        )
        booking_counts_qs = booked_qs.values('hour').annotate(count=Count('id'))
        booking_counts_by_hour = {x['hour']: x['count'] for x in booking_counts_qs}

        context['occupation_rates'] = []
        for time in context['hours']:
            booked_places = booking_counts_by_hour.get(time.hour, 0)
            ratio = booked_places / event.places
            percent = int(ratio * 100)

            context['occupation_rates'].append(
                {
                    'booked_places': booked_places,
                    'percent': percent,
                    'height_percent': min(percent, 100),
                    'overbooked': bool(ratio > 1),
                    'start_time': time,
                    'end_time': (datetime.datetime.combine(now(), time) + datetime.timedelta(hours=1)).time(),
                }
            )


agenda_day_view = AgendaDayView.as_view()


class AgendaWeekMonthMixin:
    def get_queryset(self):
        qs = super().get_queryset()
        if self.agenda.kind != 'events':
            return qs
        return qs.order_by('start_datetime', 'label')

    @property
    def first_day(self):
        first_day = self.date
        if self.kind == 'month':
            first_day -= datetime.timedelta(days=first_day.day - 1)
        else:
            first_day -= datetime.timedelta(days=first_day.weekday())
        return first_day

    def get_dated_queryset(self, **kwargs):
        # adjust min and max, incorrect as DayArchiveView is used to have Y/m/d in url
        kwargs['start_datetime__gte'] = self._make_date_lookup_arg(self.first_day)
        kwargs['start_datetime__lt'] = self._make_date_lookup_arg(
            getattr(self, 'get_next_%s' % self.kind)(self.first_day)
        )
        return super().get_dated_queryset(**kwargs)

    def get_dated_items(self):
        date_list, object_list, extra_context = super().get_dated_items()
        if self.agenda.kind == 'events':
            self.events = object_list
            min_start = self.first_day
            max_start = getattr(self, 'get_next_%s' % self.kind)(self.first_day)
            exceptions = TimePeriodException.objects.filter(
                desk__agenda=self.agenda, start_datetime__gte=min_start, end_datetime__lt=max_start
            ).annotate(is_exception=Value(True, BooleanField()))
            object_list = sorted(itertools.chain(object_list, exceptions), key=lambda x: x.start_datetime)
        return date_list, object_list, extra_context

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hide_sunday'] = self.hide_sunday
        context['hide_weekend'] = self.hide_weekend
        if self.agenda.kind == 'events':
            context['cancellation_reports'] = EventCancellationReport.objects.filter(
                event__agenda=self.agenda,
                seen=False,
            ).all()
        else:
            context['single_desk'] = bool(len(self.agenda.prefetched_desks) == 1)
        if self.agenda.partial_bookings_events:
            self.fill_partial_bookings_context(context)
        return context

    def get_timeperiods(self):
        return list(itertools.chain(*(d.timeperiod_set.all() for d in self.agenda.prefetched_desks)))

    def get_active_events(self):
        return [
            x for x in self.object_list if any([y.cancellation_datetime is None for y in x.booking_set.all()])
        ]

    @cached_property
    def hide_sunday(self):
        if self.agenda.kind == 'events':
            return False

        hide_sunday_timeperiod = not any(
            [e.weekday == 6 or (e.date and e.date.weekday() == 6) for e in self.get_timeperiods()]
        )
        hide_sunday_event = not any([x.start_datetime.weekday() == 6 for x in self.get_active_events()])
        return hide_sunday_timeperiod and hide_sunday_event

    @cached_property
    def hide_weekend(self):
        if not self.hide_sunday:
            return False

        hide_weekend_timeperiod = not any(
            [e.weekday == 5 or (e.date and e.date.weekday() == 5) for e in self.get_timeperiods()]
        )
        hide_weekend_event = not any([x.start_datetime.weekday() == 5 for x in self.get_active_events()])
        return hide_weekend_timeperiod and hide_weekend_event

    def get_timetable_infos(self):
        timeperiods = self.get_timeperiods()
        interval = datetime.timedelta(minutes=60)

        min_timeperiod = self.max_timeperiod = min_event = max_event = None
        if timeperiods:
            min_timeperiod = min(x.start_time for x in timeperiods)
            self.max_timeperiod = max(x.end_time for x in timeperiods)

        active_events = self.get_active_events()
        if active_events:
            min_event = min(localtime(x.start_datetime).time() for x in active_events)
            max_event = max(localtime(x.start_datetime + interval).time() for x in active_events)
        if min_timeperiod is None and min_event is None:
            return
        self.min_display = min(min_timeperiod or datetime.time(23), min_event or datetime.time(23))
        self.max_display = max(self.max_timeperiod or datetime.time(0), max_event or datetime.time(0))

        # avoid displaying empty first week
        first_week_offset = 0
        first_week_number = self.first_day.isocalendar()[1]
        last_week_number = first_week_number
        if self.kind == 'month':
            first_week_offset = int(
                (self.hide_sunday and self.first_day.weekday() == 6)
                or (self.hide_weekend and self.first_day.weekday() == 5)
            )
            first_week_number = self.first_day.isocalendar()[1]
            if first_week_number >= 52:
                first_week_number = 0
            last_day = self.get_next_month(self.first_day.date()) - datetime.timedelta(days=1)
            last_week_number = last_day.isocalendar()[1]

            if last_week_number < first_week_number:  # new year
                last_week_number = 53

        for week_number in range(first_week_number + first_week_offset, last_week_number + 1):
            yield self.get_week_timetable_infos(
                week_number - first_week_number,
                week_end_offset=int(self.hide_sunday) + int(self.hide_weekend),
            )

    def get_week_timetable_infos(self, week_index, week_end_offset=0):
        date = self.first_day + datetime.timedelta(week_index * 7)
        dow = date.isocalendar()[2]
        start_date = date - datetime.timedelta(dow)

        interval = datetime.timedelta(minutes=60)
        period = self.first_day.replace(hour=self.min_display.hour, minute=0)
        max_date = self.first_day.replace(hour=self.max_display.hour, minute=0)
        if self.max_display == self.max_timeperiod and self.max_display.minute != 0:
            # until the end of the last hour.
            max_date += datetime.timedelta(hours=1)

        periods = []
        while period < max_date:
            periods.append(period)
            period = period + interval

        return {
            'days': [
                self.get_day_timetable_infos(start_date + datetime.timedelta(i), interval)
                for i in range(1, 8 - week_end_offset)
            ],
            'periods': periods,
        }

    def get_day_timetable_infos(self, day, interval):
        day = make_aware(make_naive(day))  # give day correct timezone
        period = current_date = day.replace(hour=self.min_display.hour, minute=0)
        timetable = {
            'date': current_date,
            'today': day.date() == datetime.date.today(),
            'other_month': day.month != self.date.month,
            'infos': {'opening_hours': [], 'exceptions': [], 'booked_slots': []},
        }

        desks = self.agenda.prefetched_desks
        desks_len = len(desks)
        max_date = day.replace(hour=self.max_display.hour, minute=0)

        # compute booking and opening hours only for current month/week
        if self.kind == 'month' and timetable['other_month']:
            return timetable

        while period <= max_date:
            left = 1
            period_end = period + interval
            for desk in desks:
                width = (98.0 / desks_len) - 1
                for event in [
                    x
                    for x in self.object_list
                    if x.desk_id == desk.id and x.start_datetime >= period and x.start_datetime < period_end
                ]:
                    # don't consider cancelled bookings
                    bookings = [x for x in event.booking_set.all() if not x.cancellation_datetime]
                    if not bookings:
                        continue
                    booking_duration = (event.end_datetime - event.start_datetime).seconds // 60
                    booking = {
                        'css_top': 100 * (event.start_datetime - current_date).seconds // 3600,
                        'css_height': 100 * booking_duration // 60,
                        'css_width': width,
                        'css_left': left,
                        'desk': desk,
                        'booking': bookings[0],
                    }
                    timetable['infos']['booked_slots'].append(booking)

                # get desks opening hours on last period iteration
                if period == max_date:
                    for opening_hour in desk.get_opening_hours(current_date):
                        timetable['infos']['opening_hours'].append(
                            {
                                **self.get_opening_hour_data(opening_hour, current_date),
                                'css_width': width,
                                'css_left': left,
                            }
                        )
                    exceptions = desk.prefetched_exceptions + self.get_exceptions_from_excluded_periods(
                        current_date.date()
                    )
                    for exception in exceptions:
                        if exception.end_datetime < current_date:
                            continue
                        if exception.start_datetime > max_date:
                            continue
                        exception = copy.copy(exception)
                        start_max = max(current_date, exception.start_datetime)
                        end_min = min(max_date, exception.end_datetime)
                        exception.css_top = int(100 * (start_max - current_date).seconds // 3600)
                        exception.css_height = int(100 * (end_min - start_max).seconds // 3600)
                        exception.css_width = width
                        exception.css_left = left
                        timetable['infos']['exceptions'].append(exception)

                left += width + 1
            period += interval

        return timetable

    def fill_partial_bookings_context(self, context):
        first_day_next_month = self.get_next_month(self.first_day)
        context['days'] = days = [
            self.first_day + datetime.timedelta(days=i)
            for i in range((first_day_next_month - self.first_day).days)
        ]
        context['today'] = localtime().date()

        booking_info_by_user = {}
        bookings = Booking.objects.filter(event__in=self.events).prefetch_related('user_checks')
        for booking in bookings:
            booking_info = booking_info_by_user.setdefault(
                booking.user_external_id,
                {
                    'user_name': booking.user_name,
                    'user_first_name': booking.user_first_name,
                    'user_last_name': booking.user_last_name,
                    'bookings': [None] * len(days),
                },
            )
            user_bookings = booking_info['bookings']

            user_checks = list(booking.user_checks.all())
            if all(check.start_time and check.end_time for check in user_checks):
                if len(user_checks) == 2:
                    # one of the checks must be presence, display it
                    booking.check_css_class = 'present'
                elif booking.user_check:
                    booking.check_css_class = 'present' if booking.user_check.presence else 'absent'

            user_bookings[localtime(booking.event.start_datetime).day - 1] = booking

        subscriptions = self.agenda.subscriptions.filter(
            date_start__lt=first_day_next_month,
            date_end__gte=self.first_day,
        ).exclude(user_external_id__in=booking_info_by_user.keys())

        for subscription in subscriptions:
            booking_info_by_user[subscription.user_external_id] = {
                'user_name': subscription.user_name,
                'user_first_name': subscription.user_first_name,
                'user_last_name': subscription.user_last_name,
                'bookings': [None] * len(days),
            }

        context['user_booking_info'] = sorted(
            booking_info_by_user.values(), key=itemgetter('user_last_name', 'user_first_name')
        )


class AgendaWeekView(AgendaWeekMonthMixin, AgendaDateView, DayArchiveView, WeekMixin):
    kind = 'week'
    add_booking_view_name = 'chrono-manager-agenda-add-booking-week-view'

    def get_template_names(self):
        if self.agenda.kind == 'virtual':
            return ['chrono/manager_meetings_agenda_week_view.html']
        return ['chrono/manager_%s_agenda_week_view.html' % self.agenda.kind]

    def get_previous_week(self, date):
        return date - datetime.timedelta(days=7)

    def get_next_week(self, date):
        return date + datetime.timedelta(days=7)

    def get_previous_week_url(self):
        previous_week = self.get_previous_week(self.first_day.date())
        return reverse(
            'chrono-manager-agenda-week-view',
            kwargs={
                'pk': self.agenda.id,
                'year': previous_week.year,
                'month': previous_week.strftime('%m'),
                'day': previous_week.strftime('%d'),
            },
        )

    def get_next_week_url(self):
        next_week = self.get_next_week(self.first_day.date())
        return reverse(
            'chrono-manager-agenda-week-view',
            kwargs={
                'pk': self.agenda.id,
                'year': next_week.year,
                'month': next_week.strftime('%m'),
                'day': next_week.strftime('%d'),
            },
        )

    def get_max_date(self):
        return self.get_next_week(self.first_day.date())

    @property
    def last_day(self):
        return self.first_day + datetime.timedelta(days=6 - int(self.hide_sunday) - int(self.hide_weekend))


agenda_weekly_view = AgendaWeekView.as_view()


class AgendaMonthView(AgendaWeekMonthMixin, AgendaDateView, DayArchiveView):
    kind = 'month'

    def get_template_names(self):
        if self.agenda.kind == 'virtual':
            return ['chrono/manager_meetings_agenda_month_view.html']
        if self.agenda.partial_bookings_events:
            return ['chrono/manager_partial_bookings_month_view.html']
        return ['chrono/manager_%s_agenda_month_view.html' % self.agenda.kind]

    def get_previous_month_url(self):
        previous_month = self.get_previous_month(self.first_day.date())
        return reverse(
            'chrono-manager-agenda-month-view',
            kwargs={
                'pk': self.agenda.id,
                'year': previous_month.year,
                'month': previous_month.strftime('%m'),
                'day': previous_month.strftime('%d'),
            },
        )

    def get_next_month_url(self):
        next_month = self.get_next_month(self.first_day.date())
        return reverse(
            'chrono-manager-agenda-month-view',
            kwargs={
                'pk': self.agenda.id,
                'year': next_month.year,
                'month': next_month.strftime('%m'),
                'day': next_month.strftime('%d'),
            },
        )

    def get_max_date(self):
        return self.get_next_month(self.first_day.date())


agenda_monthly_view = AgendaMonthView.as_view()


class AgendaRedirectView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return reverse('chrono-manager-agenda-view', kwargs={'pk': kwargs['pk']})


agenda_redirect_view = AgendaRedirectView.as_view()


class AgendaOpenEventsView(ViewableAgendaMixin, DetailView):
    model = Agenda
    template_name = 'chrono/manager_agenda_open_events.html'

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='events')

    def get_object(self, **kwargs):
        return self.agenda

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_can_manage'] = self.agenda.can_be_managed(self.request.user)
        context['user_can_edit'] = self.agenda.can_be_edited(self.request.user)
        context['open_events'] = self.agenda.get_open_events()
        return context


agenda_open_events_view = AgendaOpenEventsView.as_view()


class AgendaAddBookingView(ViewableAgendaMixin, YearMixin, MonthMixin, DayMixin, FormView):
    template_name = 'chrono/manager_agenda_add_booking_form.html'
    form_class = AgendaAddBookingForm
    parent_view_name = 'chrono-manager-agenda-day-view'

    def dispatch(self, request, *args, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='meetings')

        try:
            self.date = make_aware(
                datetime.datetime(
                    year=int(self.get_year()), month=int(self.get_month()), day=int(self.get_day())
                )
            )
        except ValueError:
            raise Http404('invalid date')

        if not self.allow_access():
            raise Http404()

        return super().dispatch(request, *args, **kwargs)

    def allow_access(self):
        return self.agenda.allow_booking_from_backoffice(self.date)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['agenda'] = self.agenda
        kwargs['date'] = self.date
        return kwargs

    def get_initial(self):
        slot_datetime = self.request.GET.get('datetime')
        return {'slot_datetime': slot_datetime}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = self.agenda
        context['locale'] = settings.LANGUAGE_CODE
        return context

    def form_valid(self, form):
        slot_datetime = form.cleaned_data['slot_datetime']
        meeting_type = form.cleaned_data['meeting_type']

        all_slots = sorted(
            self.agenda.get_all_slots(
                meeting_type,
                start_datetime=slot_datetime,
                end_datetime=slot_datetime + datetime.timedelta(minutes=meeting_type.duration),
            ),
            key=lambda slot: slot.start_datetime,
        )
        all_slots = [slot for slot in all_slots if not slot.full]
        for slot in all_slots:
            if slot.start_datetime == slot_datetime:
                break
        else:
            form.add_error('slot_datetime', _('Slot is not available anymore.'))
            return self.form_invalid(form)

        event = Event(
            agenda=self.agenda,
            slug=str(uuid.uuid4()),  # set slug to avoid queries during slug generation
            meeting_type=meeting_type,
            start_datetime=slot_datetime,
            full=False,
            places=1,
            desk_id=slot.desk.id,
        )
        lock_code = str(uuid.uuid4())

        try:
            with transaction.atomic():
                event.save()
                booking = make_booking(event, payload={}, extra_data={})
                booking.save()
                audit(
                    'booking:backoffice-create',
                    request=self.request,
                    agenda=self.agenda,
                    extra_data={
                        'booking': booking,
                        'event': event,
                    },
                )
                Lease.objects.create(booking=booking, lock_code=lock_code)
                self.draft_id = self.create_draft(slot_datetime, meeting_type, lock_code)
        except IntegrityError as e:
            if 'tstzrange_constraint' in str(e):
                form.add_error('slot_datetime', _('Slot is not available anymore.'))
                return self.form_invalid(form)
            raise
        except CreateDraftError as e:
            form.add_error(None, _('Error creating draft: %s') % e)
            return self.form_invalid(form)

        return super().form_valid(form)

    def create_draft(self, slot_datetime, meeting_type, lock_code):
        data = {
            self.agenda.booking_form_slot_field_id: '%s:%s'
            % (meeting_type.slug, slot_datetime.strftime('%Y-%m-%d-%H%M')),
        }
        if self.agenda.booking_form_meeting_type_field_id:
            data[self.agenda.booking_form_meeting_type_field_id] = meeting_type.slug
        if self.agenda.booking_form_agenda_field_id:
            data[self.agenda.booking_form_agenda_field_id] = self.agenda.slug

        agenda_url = reverse(
            self.parent_view_name,
            kwargs={
                'pk': self.agenda.pk,
                'year': self.date.year,
                'month': self.date.month,
                'day': self.date.day,
            },
        )

        response_json = None
        try:
            response = requests_wrapper.post(
                '/api/formdefs/%s/submit' % self.agenda.booking_form_slug,
                remote_service=settings.KNOWN_SERVICES['wcs']['eservices'],
                user=self.request.user,
                json={
                    'meta': {'backoffice-submission': True, 'draft': True},
                    'context': {
                        'lock_code': lock_code,
                        'return_url': self.request.build_absolute_uri(agenda_url),
                        'cancel_url': self.request.build_absolute_uri(
                            reverse(
                                'chrono-manager-agenda-lease-delete-view',
                                kwargs={'pk': self.agenda.pk, 'lock_code': lock_code},
                            )
                        )
                        + '?redirect_url=%s' % agenda_url,
                    },
                    'data': data,
                },
            )
            try:
                response_json = response.json()
            except ValueError:
                pass
            if isinstance(response_json, dict) and response_json.get('err_desc'):
                raise CreateDraftError(response_json['err_desc'])
            response.raise_for_status()
        except requests.RequestException as e:
            raise CreateDraftError(e)
        return response_json['data']['id']

    def get_success_url(self):
        wcs_url = settings.KNOWN_SERVICES['wcs']['eservices']['url']
        return f'{wcs_url}backoffice/submission/{self.agenda.booking_form_slug}/{self.draft_id}/'


agenda_add_booking_view = AgendaAddBookingView.as_view()


class AgendaAddBookingWeekView(AgendaAddBookingView):
    parent_view_name = 'chrono-manager-agenda-week-view'

    def allow_access(self):
        self.first_date = self.date - datetime.timedelta(days=self.date.weekday())
        self.last_date = self.first_date + datetime.timedelta(days=6)
        return self.agenda.allow_booking_from_backoffice(self.first_date, self.last_date)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['date'] = self.first_date
        kwargs['last_date'] = self.last_date
        return kwargs

    def get_initial(self):
        initial = super().get_initial()

        slot_datetime = initial['slot_datetime'] or ''
        try:
            initial['day'] = slot_datetime.split()[0]
        except IndexError:
            pass

        return initial


agenda_add_booking_week_view = AgendaAddBookingWeekView.as_view()


class AgendaLeaseDeleteView(ViewableAgendaMixin, DetailView):
    model = Lease
    pk_url_kwarg = None
    slug_field = 'lock_code'
    slug_url_kwarg = 'lock_code'

    def get(self, request, *args, **kwargs):
        redirect_url = request.GET.get('redirect_url')
        if not redirect_url or not redirect_url.startswith('/manage/'):
            raise Http404

        lease = self.get_object()
        if lease.booking.event.agenda != self.agenda:
            raise Http404

        lease.booking.event.delete()
        return redirect(redirect_url)


agenda_lease_delete_view = AgendaLeaseDeleteView.as_view()


class EditableAgendaSubobjectMixin:
    agenda = None
    tab_anchor = None

    def check_permissions(self, user):
        return self.agenda.can_be_edited(user)

    def dispatch(self, request, *args, **kwargs):
        self.agenda = self.get_object().agenda
        if not self.check_permissions(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = self.object.agenda
        return context

    def get_success_url(self):
        url = reverse('chrono-manager-agenda-settings', kwargs={'pk': self.agenda.id})
        if self.tab_anchor:
            url += '#open:%s' % self.tab_anchor
        return url


class ManagedAgendaSubobjectMixin(EditableAgendaSubobjectMixin):
    def check_permissions(self, user):
        return self.agenda.can_be_managed(user)


class ManagedDeskMixin:
    desk = None
    tab_anchor = None

    def dispatch(self, request, *args, **kwargs):
        try:
            self.desk = Desk.objects.get(id=kwargs.get('pk'))
        except Desk.DoesNotExist:
            raise Http404()
        if not self.desk.agenda.can_be_edited(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if not kwargs.get('instance'):
            kwargs['instance'] = self.model()
        kwargs['instance'].desk = self.desk
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['desk'] = self.desk
        context['agenda'] = self.desk.agenda
        return context

    def get_success_url(self):
        url = reverse('chrono-manager-agenda-settings', kwargs={'pk': self.desk.agenda.id})
        if self.tab_anchor:
            url += '#open:%s' % self.tab_anchor
        return url


class ManagedTimePeriodMixin:
    agenda = None
    tab_anchor = None

    def dispatch(self, request, *args, **kwargs):
        self.time_period = self.get_object()
        self.agenda = self.time_period.agenda
        if self.time_period.desk:
            self.agenda = self.time_period.desk.agenda

        if not self.agenda.can_be_edited(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = self.agenda
        return context

    def get_success_url(self):
        url = reverse('chrono-manager-agenda-settings', kwargs={'pk': self.agenda.id})
        if self.tab_anchor:
            url += '#open:%s' % self.tab_anchor
        return url


class ManagedTimePeriodExceptionMixin:
    desk = None
    unavailability_calendar = None
    tab_anchor = None

    def dispatch(self, request, *args, **kwargs):
        object_ = self.get_object()
        if object_.desk:
            self.desk = self.get_object().desk
            if not self.desk.agenda.can_be_edited(request.user):
                raise PermissionDenied()
        elif object_.unavailability_calendar:
            self.unavailability_calendar = object_.unavailability_calendar
            if not self.unavailability_calendar.can_be_managed(request.user):
                raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.desk:
            context['desk'] = self.object.desk
            context['agenda'] = self.object.desk.agenda
            context['base_template'] = 'chrono/manager_agenda_settings.html'
        else:
            context['unavailability_calendar'] = self.unavailability_calendar
            context['base_template'] = 'chrono/manager_unavailability_calendar_settings.html'
        return context

    def get_success_url(self):
        url = ''
        if self.desk:
            url = reverse('chrono-manager-agenda-settings', kwargs={'pk': self.desk.agenda.id})
        elif self.unavailability_calendar:
            url = reverse(
                'chrono-manager-unavailability-calendar-settings',
                kwargs={'pk': self.unavailability_calendar.pk},
            )
        if self.tab_anchor:
            url += '#open:%s' % self.tab_anchor
        return url


class AgendaSettingsRedirectView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        agenda = get_object_or_404(Agenda, slug=kwargs['slug'])
        return reverse('chrono-manager-agenda-settings', kwargs={'pk': agenda.pk})


agenda_settings_redirect_view = AgendaSettingsRedirectView.as_view()


class AgendaSettings(EditableAgendaMixin, DetailView):
    model = Agenda

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(
            Agenda.objects.select_related('admin_role', 'edit_role', 'view_role'),
            pk=kwargs.get('pk'),
        )

    def get_object(self, *args, **kwargs):
        if self.agenda.kind == 'meetings':
            self.agenda.prefetch_desks_and_exceptions(with_sources=True, min_date=now())
        return self.agenda

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_can_manage'] = self.object.can_be_managed(self.request.user)
        if self.agenda.accept_meetings():
            if not self.agenda.partial_bookings:
                context['meeting_types'] = self.object.iter_meetingtypes()
            else:
                context['meeting_type'] = self.object.meetingtype_set.filter(deleted=False).get()
        if self.agenda.kind == 'virtual':
            context['virtual_members'] = [
                (virtual_member, virtual_member.real_agenda.can_be_managed(self.request.user))
                for virtual_member in self.object.get_virtual_members()
            ]
        if self.agenda.kind == 'meetings':
            context['has_resources'] = Resource.objects.exists()
            context['has_unavailability_calendars'] = UnavailabilityCalendar.objects.exists()
            context['agenda_is_available_for_simple_management'] = (
                self.object.is_available_for_simple_management()
                if not self.object.desk_simple_management
                else False
            )
        if self.agenda.kind == 'events':
            context['has_recurring_events'] = self.agenda.event_set.filter(
                recurrence_days__isnull=False
            ).exists()
            desk, created = Desk.objects.get_or_create(agenda=self.agenda, slug='_exceptions_holder')
            if created:
                desk.import_timeperiod_exceptions_from_settings()
            context['exceptions'] = TimePeriodException.objects.filter(
                Q(desk=desk) | Q(unavailability_calendar__desks=desk),
                end_datetime__gt=now(),
            )
            context['desk'] = desk
        return context

    def get_events(self):
        return self.agenda.event_set.filter(primary_event__isnull=True)

    def get_template_names(self):
        return ['chrono/manager_%s_agenda_settings.html' % self.agenda.kind]


agenda_settings = AgendaSettings.as_view()


class AgendaExport(ManagedAgendaMixin, DetailView):
    model = Agenda

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/json')
        today = datetime.date.today()
        response['Content-Disposition'] = 'attachment; filename="export_agenda_{}_{}.json"'.format(
            self.get_object().slug, today.strftime('%Y%m%d')
        )
        json.dump({'agendas': [self.get_object().export_json()]}, response, indent=2)
        return response


agenda_export = AgendaExport.as_view()


class AgendaDuplicate(FormView):
    form_class = AgendaDuplicateForm
    template_name = 'chrono/manager_agenda_duplicate_form.html'

    def dispatch(self, request, *args, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'))
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-agenda-settings', kwargs={'pk': self.new_agenda.pk})

    def get_form(self):
        form = super().get_form()
        if self.agenda.kind != 'events':
            del form.fields['include_events']
        return form

    def form_valid(self, form):
        self.new_agenda = self.agenda.duplicate(
            label=form.cleaned_data['label'], include_events=form.cleaned_data.get('include_events', True)
        )
        self.new_agenda.take_snapshot(request=self.request, comment=pgettext('snapshot', 'created'))
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = self.agenda
        return context


agenda_duplicate = AgendaDuplicate.as_view()


class AgendaAddEventView(EditableAgendaMixin, CreateView):
    template_name = 'chrono/manager_event_form.html'
    model = Event
    form_class = NewEventForm

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.agenda.take_snapshot(request=self.request, comment=_('added event (%s)') % self.object)
        return response


agenda_add_event = AgendaAddEventView.as_view()


class AgendaEventDuplicateView(EditableAgendaMixin, UpdateView):
    model = Event
    queryset = Event.objects.filter(primary_event__isnull=True)
    pk_url_kwarg = 'event_pk'
    form_class = EventDuplicateForm
    template_name = 'chrono/manager_event_duplicate_form.html'

    def get_success_url(self):
        return reverse('chrono-manager-event-edit', kwargs={'pk': self.agenda.id, 'event_pk': self.object.id})

    def form_valid(self, form):
        messages.success(self.request, _('Event successfully duplicated.'))
        response = super().form_valid(form)
        self.agenda.take_snapshot(request=self.request, comment=_('added event (%s)') % self.object)
        return response


event_duplicate = AgendaEventDuplicateView.as_view()


class AgendaImportEventsSampleView(ManagedAgendaMixin, DetailView):
    model = Agenda
    template_name = 'chrono/manager_sample_events.txt'
    content_type = 'text/csv'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        some_future_date = datetime.datetime.now() + datetime.timedelta(days=10)
        some_future_date = some_future_date.replace(hour=14, minute=0, second=0)
        context['some_future_date'] = some_future_date
        if self.agenda.events_type:
            context['custom_fields'] = self.agenda.events_type.get_custom_fields()
            for field in context['custom_fields']:
                # label's CSV double quote escaping
                field['label'] = field['label'].replace('"', '""')
        return context


agenda_import_events_sample_csv = AgendaImportEventsSampleView.as_view()


class AgendaImportEventsView(EditableAgendaMixin, FormView):
    form_class = ImportEventsForm
    template_name = 'chrono/manager_import_events.html'
    agenda = None

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(
            Agenda.objects.select_related('events_type'), pk=kwargs.get('pk'), kind='events'
        )

    def get_form_kwargs(self):
        kwargs = super(FormView, self).get_form_kwargs()
        kwargs['agenda'] = self.agenda
        return kwargs

    def form_valid(self, form):
        if form.events:
            # existing event slugs for this agenda
            for event in form.events:
                event.agenda = self.agenda
                event.save()
            messages.info(self.request, _('%d events have been imported.') % len(form.events))
            for event in form.warnings.values():
                messages.warning(
                    self.request,
                    _('Event "%s" start date has changed. Do not forget to notify the registrants.')
                    % (event.label or event.slug),
                )
        response = super().form_valid(form)
        if form.events:
            self.agenda.take_snapshot(request=self.request, comment=_('imported events'))
        return response


agenda_import_events = AgendaImportEventsView.as_view()


class AgendaExportEventsView(EditableAgendaMixin, View):
    template_name = 'chrono/manager_export_events.html'
    agenda = None

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='events')

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        today = datetime.date.today()
        response['Content-Disposition'] = 'attachment; filename="export_agenda_events_{}_{}.csv"'.format(
            self.agenda.slug, today.strftime('%Y%m%d')
        )
        response.write('\ufeff')
        writer = csv.writer(response, quoting=csv.QUOTE_ALL, delimiter=';')
        # headers
        writer.writerow(
            [
                _('date'),
                _('time'),
                _('number of places'),
                _('number of places in waiting list'),
                _('label'),
                _('identifier'),
                _('description'),
                _('pricing'),
                _('URL'),
                _('publication date/time'),
                _('duration'),
            ]
        )
        for event in self.agenda.event_set.all():
            start_datetime = localtime(event.start_datetime)
            publication_datetime = (
                localtime(event.publication_datetime) if event.publication_datetime else None
            )
            writer.writerow(
                [
                    start_datetime.strftime('%Y-%m-%d'),
                    start_datetime.strftime('%H:%M'),
                    event.places,
                    event.waiting_list_places,
                    event.label,
                    event.slug,
                    event.description,
                    event.pricing,
                    event.url,
                    publication_datetime.strftime('%Y-%m-%d %H:%M') if publication_datetime else '',
                    event.duration,
                ]
            )
        return response


agenda_export_events = AgendaExportEventsView.as_view()


class AgendaDeskManagementToggleView(EditableAgendaMixin, View):
    agenda = None

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='meetings')

    def get(self, request, *args, **kwargs):
        message = None
        if self.agenda.desk_simple_management:
            self.agenda.desk_simple_management = False
            self.agenda.save()
            if self.agenda.partial_bookings:
                message = _('Resource individual management enabled.')
            else:
                message = _('Desk individual management enabled.')
        elif self.agenda.is_available_for_simple_management():
            self.agenda.desk_simple_management = True
            self.agenda.save()
            if self.agenda.partial_bookings:
                message = _('Resource global management enabled.')
            else:
                message = _('Desk global management enabled.')
        if message:
            messages.info(self.request, message)

        if self.agenda.desk_simple_management:
            if self.agenda.partial_bookings:
                comment = _('enabled resource individual management')
            else:
                comment = _('enabled desk individual management')
        else:
            if self.agenda.partial_bookings:
                comment = _('enabled resource global management')
            else:
                comment = _('enabled desk global management')
        self.agenda.take_snapshot(request=self.request, comment=comment)
        return HttpResponseRedirect(reverse('chrono-manager-agenda-settings', kwargs={'pk': self.agenda.pk}))


agenda_desk_management_toggle_view = AgendaDeskManagementToggleView.as_view()


class AgendaNotificationsSettingsView(EditableAgendaMixin, UpdateView):
    template_name = 'chrono/manager_agenda_notifications_form.html'
    model = AgendaNotificationsSettings
    form_class = AgendaNotificationsForm
    tab_anchor = 'notifications'

    def get_object(self):
        try:
            return self.agenda.notifications_settings
        except AgendaNotificationsSettings.DoesNotExist:
            # prevent old events from sending notifications
            statuses = ('almost_full', 'full', 'cancelled')
            timestamp = now()
            for status in statuses:
                filter_kwargs = {status: True}
                update_kwargs = {status + '_notification_timestamp': timestamp}
                self.agenda.event_set.filter(**filter_kwargs).update(**update_kwargs)
            return AgendaNotificationsSettings.objects.create(agenda=self.agenda)

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.agenda.take_snapshot(request=self.request, comment=_('changed notification settings'))
        return response


agenda_notifications_settings = AgendaNotificationsSettingsView.as_view()


class AgendaReminderSettingsView(AgendaEditView):
    title = _('Reminder Settings')
    model = AgendaReminderSettings
    form_class = AgendaReminderForm
    tab_anchor = 'reminders'
    comment = _('changed reminder settings')

    def get_object(self):
        try:
            return self.agenda.reminder_settings
        except AgendaReminderSettings.DoesNotExist:
            return AgendaReminderSettings.objects.create(agenda=self.agenda)

    def check_permissions(self, user):
        return self.agenda.can_be_edited(user)


agenda_reminder_settings = AgendaReminderSettingsView.as_view()


class AgendaReminderTestView(EditableAgendaMixin, FormView):
    template_name = 'chrono/manager_send_reminder_form.html'
    form_class = AgendaReminderTestForm

    def get_form_kwargs(self):
        kwargs = super(FormView, self).get_form_kwargs()
        kwargs['agenda'] = self.agenda
        return kwargs

    def form_valid(self, form):
        booking = form.cleaned_data['booking']

        if form.cleaned_data.get('phone_number'):
            booking.user_phone_number = form.cleaned_data['phone_number']
            booking.extra_phone_numbers.clear()

        if form.cleaned_data.get('email'):
            booking.user_email = form.cleaned_data['email']
            booking.extra_emails.clear()

        for msg_type in form.cleaned_data['msg_type']:
            send_reminder(booking, msg_type)
        return super().form_valid(form)


agenda_reminder_test = AgendaReminderTestView.as_view()


class AgendaReminderPreviewView(EditableAgendaMixin, TemplateView):
    template_name = 'chrono/manager_agenda_reminder_preview.html'

    def dispatch(self, request, *args, **kwargs):
        self.type_ = kwargs['type']
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        kind = self.agenda.kind
        days = getattr(self.agenda.reminder_settings, 'days_before_%s' % self.type_)

        paragraph = lorem_ipsum.paragraphs(1)[0][:232]
        label = title(lorem_ipsum.words(2))
        event = Event(
            start_datetime=datetime.datetime(year=2020, month=6, day=2, hour=14, minute=30),
            label=format_html('{} <small>({})</small>', label, _('event label')),
            description=format_html('{} <small>({})</small>', paragraph, _('event description, if present')),
            pricing=format_html('{} <small>({})</small>', '...', _('event pricing, if present')),
            url='#',
        )

        booking = Booking(user_display_label='{{ user_display_label }}', form_url='#')
        booking.event = event
        button_label = format_html('{}<br>({})', _('More information'), _('link to event url, if present'))
        reminder_ctx = {
            'booking': booking,
            'in_x_days': _('tomorrow') if days == 1 else _('in %s days') % days,
            'date': booking.event.start_datetime,
            'email_extra_info': self.agenda.reminder_settings.email_extra_info,
            'sms_extra_info': self.agenda.reminder_settings.sms_extra_info,
            'event_url_button_label': button_label,
        }

        if self.type_ == 'email':
            ctx['subject'] = render_to_string(
                'agendas/%s_reminder_subject.txt' % kind, reminder_ctx, request=self.request
            ).strip()
            ctx['message'] = render_to_string(
                'agendas/%s_reminder_body.html' % kind, reminder_ctx, request=self.request
            )
        elif self.type_ == 'sms':
            ctx['message'] = render_to_string(
                'agendas/%s_reminder_message.txt' % kind, reminder_ctx, request=self.request
            )

        return ctx


agenda_reminder_preview = AgendaReminderPreviewView.as_view()


class EventsReportMixin:
    def write_group(self, form, writer, slots, activity_display, events_num, dates, grouper):
        # title
        if form.cleaned_data.get('group_by'):
            writer.writerow(['%s: %s' % (form.cleaned_data['group_by'], grouper['grouper'])])
        # headers
        headers = [
            _('First name'),
            _('Last name'),
        ]
        headers += slots['extra_data']
        if activity_display != 'col' and events_num > 1 or form.user_external_id:
            if form.user_external_id:
                headers.append(_('Agenda'))
            headers.append(_('Activity'))
        for date, events in dates:
            if activity_display == 'col':
                for event in events:
                    headers.append(
                        _('%(event)s of %(date)s') % {'event': event, 'date': date_format(date, 'd-m')}
                    )
            else:
                headers.append(date_format(date, 'D d/m'))
        writer.writerow(headers)
        # and data
        for user in grouper['users']:
            if activity_display == 'col':
                data = [
                    user['user_first_name'],
                    user['user_last_name'],
                ]
                data += [user['extra_data'].get(k) or '' for k in slots['extra_data']]
                for date, events in dates:
                    for event in events:
                        for item in user['events']:
                            if item['event'] != event:
                                continue
                            data.append(self.get_booked(item, date))
                            break
                writer.writerow(data)
            else:
                for item in user['events']:
                    data = [
                        user['user_first_name'],
                        user['user_last_name'],
                    ]
                    data += [user['extra_data'].get(k) or '' for k in slots['extra_data']]
                    if events_num > 1 or form.user_external_id:
                        if form.user_external_id:
                            data.append(item['event'].agenda)
                        data.append(item['event'])
                    for date, events in dates:
                        data.append(self.get_booked(item, date))
                    writer.writerow(data)


class EventsTimesheetView(ViewableAgendaMixin, EventsReportMixin, DetailView):
    model = Agenda

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='events')
        self.event = None
        if 'event_pk' in kwargs:
            self.event = get_object_or_404(
                Event,
                pk=kwargs.get('event_pk'),
                agenda=self.agenda,
                recurrence_days__isnull=True,
                cancelled=False,
            )

    def get_object(self, **kwargs):
        return self.agenda

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = EventsTimesheetForm(agenda=self.agenda, event=self.event, data=self.request.GET or None)
        if self.request.GET:
            form.is_valid()
        context['form'] = form
        context['event'] = self.event
        return context

    def get_template_names(self):
        if self.event is not None:
            return ['chrono/manager_event_timesheet.html']
        return ['chrono/manager_events_timesheet.html']

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'pdf' in request.GET and context['form'].is_valid():
            return self.pdf(request, context)
        if 'csv' in request.GET and context['form'].is_valid():
            return self.csv(request, context)
        return self.render_to_response(context)

    def get_export_filename(self, context):
        if self.event is not None:
            return 'timesheet_{}_{}'.format(
                self.agenda.slug,
                self.event.slug,
            )
        else:
            return 'timesheet_{}_{}_{}'.format(
                self.agenda.slug,
                context['form'].cleaned_data['date_start'].strftime('%Y-%m-%d'),
                context['form'].cleaned_data['date_end'].strftime('%Y-%m-%d'),
            )

    def pdf(self, request, context):
        context['base_uri'] = request.build_absolute_uri('/')
        html = HTML(
            string=render_to_string('chrono/manager_events_timesheet_pdf.html', context, request=request)
        )
        pdf = html.write_pdf()
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"' % self.get_export_filename(context)
        return response

    def get_booked(self, item, date):
        data = item['dates'].get(date)
        if data is None:
            return '-'
        if data['booked'] is True:
            return '☐'
        return ''

    def csv(self, request, context):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % self.get_export_filename(context)
        response.write('\ufeff')
        writer = csv.writer(response, quoting=csv.QUOTE_ALL, delimiter=';')
        form = context['form']
        slots = form.get_slots()
        events_num = len(slots['events'])
        activity_display = form.cleaned_data.get('activity_display')

        for dates in slots['dates']:
            for grouper in slots['users']:
                self.write_group(form, writer, slots, activity_display, events_num, dates, grouper)

        return response


events_timesheet = EventsTimesheetView.as_view()


class EventsAgendaCheckReportView(ViewableAgendaMixin, EventsReportMixin, DetailView):
    model = Agenda
    template_name = 'chrono/manager_events_agenda_check_report.html'

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='events')

    def get_object(self, **kwargs):
        return self.agenda

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = EventsCheckReportForm(agenda=self.agenda, data=self.request.GET or None)
        if self.request.GET:
            form.is_valid()
        context['form'] = form
        return context

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        if 'csv' in request.GET and context['form'].is_valid():
            return self.csv(request, context)
        return self.render_to_response(context)

    def get_export_filename(self, context):
        return 'check_report_agenda_{}_{}_{}'.format(
            self.agenda.slug,
            context['form'].cleaned_data['date_start'].strftime('%Y-%m-%d'),
            context['form'].cleaned_data['date_end'].strftime('%Y-%m-%d'),
        )

    def get_booked(self, item, date):
        data = item['dates'].get(date)
        if data is None:
            return '-'
        return data.get('check_type')

    def csv(self, request, context):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % self.get_export_filename(context)
        response.write('\ufeff')
        writer = csv.writer(response, quoting=csv.QUOTE_ALL, delimiter=';')
        form = context['form']
        slots = form.get_slots()
        events_num = len(slots['events'])
        activity_display = form.cleaned_data.get('activity_display')

        for dates in slots['dates']:
            for grouper in slots['users']:
                self.write_group(form, writer, slots, activity_display, events_num, dates, grouper)

        return response


events_agenda_check_report = EventsAgendaCheckReportView.as_view()


class EventDetailView(ViewableAgendaMixin, DetailView):
    model = Event
    pk_url_kwarg = 'event_pk'

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().recurrence_days:
            raise Http404('this view makes no sense for recurring events')
        if self.get_object().agenda.partial_bookings:
            raise Http404('this view makes no sense for partial bookings')
        return super().dispatch(request, *args, **kwargs)

    def get_template_names(self):
        if is_ajax(self.request):
            return ['chrono/manager_event_detail_fragment.html']
        return ['chrono/manager_event_detail.html']

    @classmethod
    def complete_event_attributes(cls, agenda, event, context=None):
        user_subscriptions = Subscription.objects.filter(
            agenda=agenda,
            date_start__lte=event.start_datetime,
            date_end__gt=event.start_datetime,
            user_external_id=OuterRef('user_external_id'),
        )
        bookings = event.booking_set.select_related('lease').annotate(
            with_subscription=Exists(user_subscriptions)
        )
        booked = (
            bookings.filter(cancellation_datetime__isnull=True, in_waiting_list=False)
            .prefetch_related('user_checks')
            .order_by('creation_datetime')
        )
        event.present_count = len([b for b in booked if b.user_check and b.user_check.presence])
        event.absent_count = len([b for b in booked if b.user_check and not b.user_check.presence])
        event.notchecked_count = len([b for b in booked if not b.user_check])

        if context is not None:
            context['booked'] = booked
            context['waiting'] = bookings.filter(
                cancellation_datetime__isnull=True, in_waiting_list=True
            ).order_by('creation_datetime')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user_can_manage'] = self.agenda.can_be_managed(self.request.user)
        event = self.object
        self.complete_event_attributes(self.agenda, event, context)
        return context


event_view = EventDetailView.as_view()


class EventDetailRedirectView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        agenda = get_object_or_404(Agenda, slug=kwargs['slug'], kind='events')
        event = get_object_or_404(Event, slug=kwargs['event_slug'], agenda=agenda)
        if agenda.partial_bookings:
            day = localtime(event.start_datetime)
            return reverse(
                'chrono-manager-agenda-day-view',
                kwargs={
                    'pk': agenda.pk,
                    'year': day.year,
                    'month': day.strftime('%m'),
                    'day': day.strftime('%d'),
                },
            )
        return reverse('chrono-manager-event-view', kwargs={'pk': agenda.pk, 'event_pk': event.pk})


event_redirect_view = EventDetailRedirectView.as_view()


class EventEditView(EditableAgendaMixin, UpdateView):
    template_name = 'chrono/manager_event_form.html'
    model = Event
    form_class = EventForm
    pk_url_kwarg = 'event_pk'

    def get_success_url(self):
        if (
            self.request.GET.get('next') == 'settings'
            or self.request.POST.get('next') == 'settings'
            or self.object.recurrence_days
            or self.object.agenda.partial_bookings
        ):
            return reverse('chrono-manager-agenda-settings', kwargs={'pk': self.agenda.id})
        return reverse('chrono-manager-event-view', kwargs={'pk': self.agenda.id, 'event_pk': self.object.id})

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.agenda.take_snapshot(request=self.request, comment=_('changed event (%s)') % self.object)
        return response


event_edit = EventEditView.as_view()


class EventDeleteView(EditableAgendaMixin, DeleteView):
    template_name = 'chrono/manager_confirm_event_delete.html'
    model = Event
    pk_url_kwarg = 'event_pk'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cannot_delete'] = bool(
            self.object.booking_set.filter(cancellation_datetime__isnull=True).exists()
            and self.object.start_datetime > now()
            or self.object.has_recurrences_booked()
        )
        return context

    def form_valid(self, form):
        self.object = self.get_object()
        context = self.get_context_data()
        if context['cannot_delete']:
            raise PermissionDenied()
        response = super().form_valid(form)
        self.agenda.take_snapshot(request=self.request, comment=_('removed event (%s)') % self.object)
        return response

    def get_success_url(self):
        if self.request.GET.get('next') == 'settings' or self.request.POST.get('next') == 'settings':
            return reverse('chrono-manager-agenda-settings', kwargs={'pk': self.agenda.id})
        day = self.object.start_datetime
        return reverse(
            'chrono-manager-agenda-month-view',
            kwargs={'pk': self.agenda.id, 'year': day.year, 'month': day.month, 'day': day.day},
        )


event_delete = EventDeleteView.as_view()


class EventChecksView(ViewableAgendaMixin, EventChecksMixin, DetailView):
    template_name = 'chrono/manager_event_check.html'
    model = Event
    pk_url_kwarg = 'event_pk'

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(
            Agenda,
            pk=kwargs.get('pk'),
            kind='events',
            partial_bookings=False,
        )

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = Event.annotate_booking_checks(queryset)
        return queryset.filter(
            Q(start_datetime__date__lte=localtime(now()).date())
            | Q(agenda__enable_check_for_future_events=True),
            agenda=self.agenda,
            cancelled=False,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.add_filters_context(context, self.object)
        context['absence_form'] = BookingCheckAbsenceForm(agenda=self.agenda)
        context['presence_form'] = BookingCheckPresenceForm(agenda=self.agenda)
        return context


event_checks = EventChecksView.as_view()


class EventCheckMixin:
    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(
            Agenda,
            pk=kwargs.get('pk'),
            kind='events',
        )
        self.event = get_object_or_404(
            Event,
            Q(checked=False) | Q(agenda__disable_check_update=False),
            Q(start_datetime__date__lte=now().date()) | Q(agenda__enable_check_for_future_events=True),
            pk=kwargs.get('event_pk'),
            agenda=self.agenda,
            cancelled=False,
            check_locked=False,
        )

    def get_bookings(self):
        return self.event.booking_set.filter(
            event__agenda=self.agenda,
            event__cancelled=False,
            cancellation_datetime__isnull=True,
            in_waiting_list=False,
            user_checks__isnull=True,
            primary_booking__isnull=True,
        )

    def get_check_type(self, kind):
        form = self.get_form()
        if form.is_valid() and form.cleaned_data['check_type']:
            check_types = getattr(form, '%s_check_types' % kind)
            for ct in check_types:
                if ct.slug == form.cleaned_data['check_type']:
                    return ct

    def response(self, request):
        if self.agenda.partial_bookings:
            day = localtime(self.event.start_datetime)
            return HttpResponseRedirect(
                reverse(
                    'chrono-manager-agenda-day-view',
                    kwargs={
                        'pk': self.agenda.pk,
                        'year': day.year,
                        'month': day.strftime('%m'),
                        'day': day.strftime('%d'),
                    },
                )
            )
        return HttpResponseRedirect(
            reverse(
                'chrono-manager-event-check',
                kwargs={'pk': self.agenda.pk, 'event_pk': self.event.pk},
            )
        )


class EventPresenceView(EventCheckMixin, ViewableAgendaMixin, FormView):
    form_class = BookingCheckPresenceForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['agenda'] = self.agenda
        return kwargs

    def post(self, request, *args, **kwargs):
        qs_kwargs = {}
        check_type = self.get_check_type(kind='presence')
        qs_kwargs['type_slug'] = check_type.slug if check_type else None
        qs_kwargs['type_label'] = check_type.label if check_type else None
        bookings = self.get_bookings()
        booking_checks_to_create = []
        for booking in bookings:
            if booking.user_check:
                continue

            booking_check = BookingCheck(booking=booking, presence=True, **qs_kwargs)
            booking_checks_to_create.append(booking_check)

        with transaction.atomic():
            aes = [
                audit(
                    'check:mark-unchecked-present',
                    request=request,
                    agenda=self.agenda,
                    extra_data={
                        'event': self.event,
                        'check_type_slug': qs_kwargs['type_slug'],
                    },
                    bulk=True,
                )
            ]
            for booking in bookings:
                aes.append(
                    audit(
                        'check:presence',
                        request=request,
                        agenda=self.agenda,
                        extra_data={
                            'booking': booking,
                        },
                        bulk=True,
                    )
                )
            bulk_audit(aes)
            BookingCheck.objects.filter(booking__in=bookings).update(presence=True, **qs_kwargs)
            BookingCheck.objects.bulk_create(booking_checks_to_create)
            self.event.set_is_checked()
        return self.response(request)


event_presence = EventPresenceView.as_view()


class EventAbsenceView(EventCheckMixin, ViewableAgendaMixin, FormView):
    form_class = BookingCheckAbsenceForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['agenda'] = self.agenda
        return kwargs

    def post(self, request, *args, **kwargs):
        qs_kwargs = {}
        check_type = self.get_check_type(kind='absence')
        qs_kwargs['type_slug'] = check_type.slug if check_type else None
        qs_kwargs['type_label'] = check_type.label if check_type else None
        bookings = self.get_bookings()
        booking_checks_to_create = []
        for booking in bookings:
            if booking.user_check:
                continue

            booking_check = BookingCheck(booking=booking, presence=False, **qs_kwargs)
            booking_checks_to_create.append(booking_check)

        with transaction.atomic():
            aes = [
                audit(
                    'check:mark-unchecked-absent',
                    request=request,
                    agenda=self.agenda,
                    extra_data={
                        'event': self.event,
                        'check_type_slug': qs_kwargs['type_slug'],
                    },
                    bulk=True,
                )
            ]
            for booking in bookings:
                aes.append(
                    audit(
                        'check:absence',
                        request=request,
                        agenda=self.agenda,
                        extra_data={
                            'booking': booking,
                        },
                        bulk=True,
                    )
                )
            bulk_audit(aes)

            BookingCheck.objects.filter(booking__in=bookings).update(presence=False, **qs_kwargs)
            BookingCheck.objects.bulk_create(booking_checks_to_create)
            self.event.set_is_checked()
        return self.response(request)


event_absence = EventAbsenceView.as_view()


class EventCheckedView(EventCheckMixin, ViewableAgendaMixin, View):
    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(
            Agenda,
            pk=kwargs.get('pk'),
            kind='events',
        )
        self.event = get_object_or_404(
            Event,
            Q(checked=False) | Q(agenda__disable_check_update=False),
            Q(start_datetime__date__lte=now().date()) | Q(agenda__enable_check_for_future_events=True),
            pk=kwargs.get('event_pk'),
            agenda=self.agenda,
            cancelled=False,
        )

    def post(self, request, *args, **kwargs):
        if not self.event.checked:
            self.event.checked = True
            self.event.save(update_fields=['checked'])
            audit(
                'check:mark',
                request=request,
                agenda=self.agenda,
                extra_data={
                    'event': self.event,
                },
            )
            self.event.async_notify_checked()
        return self.response(request)


event_checked = EventCheckedView.as_view()


class AgendaAddResourceView(ManagedAgendaMixin, FormView):
    template_name = 'chrono/manager_agenda_resource_form.html'
    form_class = AgendaResourceForm
    tab_anchor = 'resources'

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, id=kwargs.get('pk'), kind='meetings')

    def get_form_kwargs(self):
        kwargs = super(FormView, self).get_form_kwargs()
        kwargs['agenda'] = self.agenda
        return kwargs

    def form_valid(self, form):
        self.agenda.resources.add(form.cleaned_data['resource'])
        response = super().form_valid(form)
        self.agenda.take_snapshot(
            request=self.request, comment=_('added shared resource (%s)') % form.cleaned_data['resource']
        )
        return response


agenda_add_resource = AgendaAddResourceView.as_view()


class AgendaResourceDeleteView(ManagedAgendaMixin, DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = Resource
    pk_url_kwarg = 'resource_pk'
    tab_anchor = 'resources'

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, id=kwargs.get('pk'), kind='meetings')

    def form_valid(self, form):
        self.object = self.get_object()
        self.agenda.resources.remove(self.object)
        self.agenda.take_snapshot(
            request=self.request, comment=_('removed shared resource (%s)') % self.object
        )
        return HttpResponseRedirect(self.get_success_url())


agenda_delete_resource = AgendaResourceDeleteView.as_view()


class UnavailabilityCalendarToggleView(ManagedDeskMixin, DetailView):
    model = UnavailabilityCalendar
    pk_url_kwarg = 'unavailability_calendar_pk'

    def get(self, request, *args, **kwargs):
        unavailability_calendar = self.get_object()
        enabled = False
        try:
            self.desk.unavailability_calendars.get(pk=unavailability_calendar.pk)
            self.desk.unavailability_calendars.remove(unavailability_calendar)
            if self.desk.label and not self.desk.agenda.desk_simple_management:
                if self.desk.agenda.partial_bookings:
                    message = _(
                        'Unavailability calendar %(unavailability_calendar)s has been disabled on resource %(desk)s.'
                    )
                else:
                    message = _(
                        'Unavailability calendar %(unavailability_calendar)s has been disabled on desk %(desk)s.'
                    )
            else:
                message = _('Unavailability calendar %(unavailability_calendar)s has been disabled.')
        except UnavailabilityCalendar.DoesNotExist:
            enabled = True
            self.desk.unavailability_calendars.add(unavailability_calendar)
            if self.desk.label and not self.desk.agenda.desk_simple_management:
                if self.desk.agenda.partial_bookings:
                    message = _(
                        'Unavailability calendar %(unavailability_calendar)s has been enabled on resource %(desk)s.'
                    )
                else:
                    message = _(
                        'Unavailability calendar %(unavailability_calendar)s has been enabled on desk %(desk)s.'
                    )
            else:
                message = _('Unavailability calendar %(unavailability_calendar)s has been enabled.')

        if self.desk.agenda.desk_simple_management:
            for desk in self.desk.agenda.desk_set.exclude(pk=self.desk.pk):
                if enabled:
                    desk.unavailability_calendars.add(unavailability_calendar)
                else:
                    desk.unavailability_calendars.remove(unavailability_calendar)

        messages.info(
            self.request, message % {'unavailability_calendar': unavailability_calendar, 'desk': self.desk}
        )
        if enabled:
            for exception in unavailability_calendar.timeperiodexception_set.all():
                desks = [self.desk]
                if self.desk.agenda.desk_simple_management:
                    desks = self.desk.agenda.desk_set.all()
                for desk in desks:
                    if exception.has_booking_within_time_slot(target_desk=desk):
                        message = _('One or several bookings overlap with exceptions.')
                        messages.warning(self.request, message)
                        break
            self.desk.agenda.take_snapshot(
                request=self.request,
                comment=_('enabled unavailability calendar (%s)') % unavailability_calendar,
            )
        else:
            self.desk.agenda.take_snapshot(
                request=self.request,
                comment=_('disabled unavailability calendar (%s)') % unavailability_calendar,
            )

        return HttpResponseRedirect(
            '%s#open:time-periods'
            % reverse('chrono-manager-agenda-settings', kwargs={'pk': self.desk.agenda_id})
        )


unavailability_calendar_toggle_view = UnavailabilityCalendarToggleView.as_view()


class AgendaAddMeetingTypeView(ManagedAgendaMixin, CreateView):
    template_name = 'chrono/manager_meeting_type_form.html'
    model = MeetingType
    form_class = NewMeetingTypeForm
    tab_anchor = 'meeting-types'

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.agenda.take_snapshot(
            request=self.request, comment=_('added meeting type (%s)') % self.object
        )
        return response


agenda_add_meeting_type = AgendaAddMeetingTypeView.as_view()


class MeetingTypeEditView(ManagedAgendaSubobjectMixin, UpdateView):
    template_name = 'chrono/manager_meeting_type_form.html'
    model = MeetingType
    form_class = MeetingTypeForm
    tab_anchor = 'meeting-types'

    def form_valid(self, form):
        try:
            with transaction.atomic():
                response = super().form_valid(form)
                self.object.agenda.take_snapshot(
                    request=self.request, comment=_('changed meeting type (%s)') % self.object
                )
                return response
        except IntegrityError as e:
            if 'tstzrange_constraint' in str(e):
                form.add_error(
                    'duration', _('Not possible to change duration: existing events will overlap.')
                )
                return self.form_invalid(form)
            raise


meeting_type_edit = MeetingTypeEditView.as_view()


class MeetingTypeDeleteView(ManagedAgendaSubobjectMixin, DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = MeetingType
    tab_anchor = 'meeting-types'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        meeting_type = self.get_object()
        context['cannot_delete'] = False

        for virtual_agenda in self.get_object().agenda.virtual_agendas.all():
            if virtual_agenda.real_agendas.count() == 1:
                continue
            for mt in virtual_agenda.iter_meetingtypes():
                if (
                    meeting_type.slug == mt.slug
                    and meeting_type.label == mt.label
                    and meeting_type.duration == mt.duration
                ):
                    context['cannot_delete'] = True
                    context['cannot_delete_msg'] = _(
                        'This cannot be removed as it used by a virtual agenda: %(agenda)s'
                        % {'agenda': virtual_agenda}
                    )
                    break

        return context

    def form_valid(self, form):
        self.object = self.get_object()
        context = self.get_context_data()
        if context['cannot_delete']:
            raise PermissionDenied()

        # rewrite django/views/generic/edit.py::DeletionMixin.delete
        # to mark for deletion instead of actually delete
        success_url = self.get_success_url()
        self.object.deleted = True
        self.object.slug += '__deleted__' + str(uuid.uuid4())
        self.object.save()
        self.object.agenda.take_snapshot(
            request=self.request, comment=_('removed meeting type (%s)') % self.object
        )
        return HttpResponseRedirect(success_url)


meeting_type_delete = MeetingTypeDeleteView.as_view()


def process_time_period_add_form(form, desk=None, agenda=None):
    assert desk or agenda, 'a time period requires a desk or a agenda'
    for weekday in form.cleaned_data.get('weekdays'):
        period = TimePeriod(
            weekday=weekday,
            start_time=form.cleaned_data['start_time'],
            end_time=form.cleaned_data['end_time'],
            weekday_indexes=form.cleaned_data.get('weekday_indexes'),
        )
        if desk:
            period.desk = desk
        elif agenda:
            period.agenda = agenda
        period.save()


class AgendaAddTimePeriodView(ManagedDeskMixin, FormView):
    template_name = 'chrono/manager_time_period_form.html'
    form_class = TimePeriodAddForm
    tab_anchor = 'time-periods'

    def get_form_kwargs(self):
        return super(FormView, self).get_form_kwargs()

    def form_valid(self, form):
        if self.desk.agenda.desk_simple_management:
            for desk in self.desk.agenda.desk_set.all():
                process_time_period_add_form(form, desk=desk)
        else:
            process_time_period_add_form(form, desk=self.desk)
        self.desk.agenda.take_snapshot(request=self.request, comment=_('added repeating period'))
        return super().form_valid(form)


agenda_add_time_period = AgendaAddTimePeriodView.as_view()


class VirtualAgendaAddTimePeriodView(ManagedAgendaMixin, FormView):
    template_name = 'chrono/manager_time_period_form.html'
    form_class = ExcludedPeriodAddForm
    tab_anchor = 'time-periods'

    def get_form_kwargs(self):
        return super(FormView, self).get_form_kwargs()

    def form_valid(self, form):
        process_time_period_add_form(form, agenda=self.agenda)
        self.agenda.take_snapshot(request=self.request, comment=_('added excluded period'))
        return super().form_valid(form)


virtual_agenda_add_time_period = VirtualAgendaAddTimePeriodView.as_view()


class TimePeriodEditView(ManagedTimePeriodMixin, UpdateView):
    model = TimePeriod
    tab_anchor = 'time-periods'

    def get_form_class(self):
        if self.object.weekday is not None:
            return TimePeriodForm
        else:
            return DateTimePeriodForm

    def get_template_names(self):
        if self.object.weekday is not None:
            return ['chrono/manager_time_period_form.html']
        else:
            return ['chrono/manager_date_time_period_form.html']

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.object.desk:
            if self.object.date:
                self.object.desk.agenda.take_snapshot(
                    request=self.request, comment=_('changed unique period (%s)') % self.object
                )
            else:
                self.object.desk.agenda.take_snapshot(
                    request=self.request, comment=_('changed repeating period (%s)') % self.object
                )
        else:
            self.object.agenda.take_snapshot(
                request=self.request, comment=_('changed excluded period (%s)') % self.object
            )
        return response


time_period_edit = TimePeriodEditView.as_view()


class TimePeriodDeleteView(ManagedTimePeriodMixin, DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = TimePeriod
    tab_anchor = 'time-periods'

    def take_snapshot(self, time_period):
        if self.object.desk:
            if time_period.date:
                self.time_period.desk.agenda.take_snapshot(
                    request=self.request, comment=_('removed unique period (%s)') % time_period
                )
            else:
                self.time_period.desk.agenda.take_snapshot(
                    request=self.request, comment=_('removed repeating period (%s)') % time_period
                )
        else:
            self.object.agenda.take_snapshot(
                request=self.request, comment=_('removed excluded period (%s)') % self.object
            )

    def form_valid(self, form):
        time_period = self.get_object()
        response = super().form_valid(form)

        if not time_period.desk:
            self.take_snapshot(time_period)
            return response

        if not time_period.desk.agenda.desk_simple_management:
            self.take_snapshot(time_period)
            return response

        for desk in time_period.desk.agenda.desk_set.exclude(pk=time_period.desk.pk):
            tp = desk.timeperiod_set.filter(
                weekday=time_period.weekday,
                start_time=time_period.start_time,
                end_time=time_period.end_time,
                date=time_period.date,
            ).first()
            if tp is not None:
                tp.delete()

        self.take_snapshot(time_period)
        return response


time_period_delete = TimePeriodDeleteView.as_view()


class AgendaAddDateTimePeriodView(ManagedDeskMixin, FormView):
    template_name = 'chrono/manager_date_time_period_form.html'
    model = TimePeriod
    form_class = DateTimePeriodForm
    tab_anchor = 'time-periods'

    def form_valid(self, form):
        create_kwargs = {
            'date': form.cleaned_data['date'],
            'start_time': form.cleaned_data['start_time'],
            'end_time': form.cleaned_data['end_time'],
        }

        if self.desk.agenda.desk_simple_management:
            for desk in self.desk.agenda.desk_set.all():
                time_period = TimePeriod.objects.create(desk=desk, **create_kwargs)
        else:
            time_period = TimePeriod.objects.create(desk=self.desk, **create_kwargs)
        self.desk.agenda.take_snapshot(
            request=self.request, comment=_('added unique period (%s)') % time_period
        )

        return super().form_valid(form)


agenda_add_date_time_period = AgendaAddDateTimePeriodView.as_view()


class AgendaDateTimePeriodListView(ManagedDeskMixin, ListView):
    template_name = 'chrono/manager_date_time_period_list.html'
    model = TimePeriod
    paginate_by = 20

    def get_queryset(self):
        return self.model.objects.filter(desk=self.desk, date__isnull=False).order_by('-date')


agenda_date_time_period_list = AgendaDateTimePeriodListView.as_view()


class AgendaDeskMixin:
    def get_success_url(self):
        if not self.object.agenda.desk_simple_management:
            self.tab_anchor = 'time-periods'
        return super().get_success_url()


class AgendaAddDesk(AgendaDeskMixin, EditableAgendaMixin, CreateView):
    template_name = 'chrono/manager_desk_form.html'
    model = Desk
    form_class = NewDeskForm
    agenda = None
    tab_anchor = 'desks'

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(Agenda, pk=kwargs.get('pk'), kind='meetings')

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        message = _('added resource (%s)') if self.agenda.partial_bookings else _('added desk (%s)')
        self.object.agenda.take_snapshot(request=self.request, comment=message % self.object)
        return response


agenda_add_desk = AgendaAddDesk.as_view()


class DeskEditView(AgendaDeskMixin, EditableAgendaSubobjectMixin, UpdateView):
    template_name = 'chrono/manager_desk_form.html'
    model = Desk
    form_class = DeskForm
    tab_anchor = 'desks'

    def get_queryset(self):
        return super().get_queryset().filter(agenda__kind='meetings')

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        message = _('changed resource (%s)') if self.agenda.partial_bookings else _('changed desk (%s)')
        self.object.agenda.take_snapshot(request=self.request, comment=message % self.object)
        return response


desk_edit = DeskEditView.as_view()


class DeskDeleteView(AgendaDeskMixin, EditableAgendaSubobjectMixin, DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = Desk
    tab_anchor = 'desks'

    def dispatch(self, request, *args, **kwargs):
        agenda = self.get_object().agenda
        if agenda.desk_set.count() == 1:
            raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(agenda__kind='meetings')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cannot_delete'] = Booking.objects.filter(
            event__desk=self.get_object(), event__start_datetime__gt=now(), cancellation_datetime__isnull=True
        ).exists()
        context['cannot_delete_msg'] = FUTURE_BOOKING_ERROR_MSG
        return context

    def form_valid(self, form):
        self.object = self.get_object()
        context = self.get_context_data()
        if context['cannot_delete']:
            raise PermissionDenied()
        response = super().form_valid(form)
        message = _('removed resource (%s)') if self.agenda.partial_bookings else _('removed desk (%s)')
        self.object.agenda.take_snapshot(request=self.request, comment=message % self.object)
        return response


desk_delete = DeskDeleteView.as_view()


class VirtualMemberAddView(ManagedAgendaMixin, CreateView):
    template_name = 'chrono/manager_virtual_member_form.html'
    form_class = VirtualMemberForm
    model = VirtualMember

    def get_form_kwargs(self):
        kwargs = super(CreateView, self).get_form_kwargs()
        kwargs['instance'] = VirtualMember(virtual_agenda=self.agenda)
        return kwargs

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.agenda.take_snapshot(
            request=self.request, comment=_('added agenda (%s)') % self.object.real_agenda
        )
        return response


agenda_add_virtual_member = VirtualMemberAddView.as_view()


class VirtualMemberDeleteView(DeleteView):
    template_name = 'chrono/manager_confirm_virtual_member_delete.html'
    model = VirtualMember

    def dispatch(self, request, *args, **kwargs):
        self.agenda = self.get_object().virtual_agenda
        if not self.agenda.can_be_managed(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = self.agenda
        return context

    def get_success_url(self):
        return reverse('chrono-manager-agenda-settings', kwargs={'pk': self.agenda.pk})


virtual_member_delete = VirtualMemberDeleteView.as_view()


class AgendaAddTimePeriodExceptionView(ManagedDeskMixin, CreateView):
    template_name = 'chrono/manager_time_period_exception_form.html'
    model = TimePeriodException
    form_class = NewTimePeriodExceptionForm
    tab_anchor = 'time-periods'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['base_template'] = 'chrono/manager_agenda_settings.html'
        context['cancel_url'] = reverse('chrono-manager-agenda-settings', kwargs={'pk': self.desk.agenda.pk})
        return context

    def form_valid(self, form):
        result = super().form_valid(form)
        all_desks = form.cleaned_data.get('all_desks')
        message = ngettext(
            'Exception added.',
            'Exceptions added.',
            len(form.exceptions) if all_desks else 1,
        )
        messages.info(self.request, message)
        for exception in form.exceptions:
            if exception.has_booking_within_time_slot():
                messages.warning(self.request, _('One or several bookings exists within this time slot.'))
                break
        self.desk.agenda.take_snapshot(request=self.request, comment=_('added exception (%s)') % self.object)
        return result


agenda_add_time_period_exception = AgendaAddTimePeriodExceptionView.as_view()


class TimePeriodExceptionEditView(ManagedTimePeriodExceptionMixin, UpdateView):
    template_name = 'chrono/manager_time_period_exception_form.html'
    model = TimePeriodException
    form_class = TimePeriodExceptionForm
    tab_anchor = 'time-periods'

    def form_valid(self, form):
        result = super().form_valid(form)
        if not self.desk:
            self.unavailability_calendar.take_snapshot(
                request=self.request, comment=_('changed unavailability (%s)') % self.object
            )
        else:
            self.desk.agenda.take_snapshot(
                request=self.request, comment=_('changed exception (%s)') % self.object
            )
        messages.info(self.request, _('Exception updated.'))
        for exception in form.exceptions:
            if exception.has_booking_within_time_slot():
                messages.warning(self.request, _('One or several bookings exists within this time slot.'))
                break
        return result


time_period_exception_edit = TimePeriodExceptionEditView.as_view()


class TimePeriodExceptionListView(ManagedDeskMixin, ListView):
    template_name = 'chrono/manager_time_period_exception_list.html'
    model = TimePeriodException
    paginate_by = 20

    def get_queryset(self):
        return self.model.objects.filter(
            Q(desk=self.desk) | Q(unavailability_calendar__desks=self.desk), end_datetime__gte=now()
        )


time_period_exception_list = TimePeriodExceptionListView.as_view()


class TimePeriodExceptionExtractListView(TimePeriodExceptionListView):
    paginate_by = None  # no pagination

    def get_queryset(self):
        # but limit queryset
        return super().get_queryset()[:10]


time_period_exception_extract_list = TimePeriodExceptionExtractListView.as_view()


class TimePeriodExceptionDeleteView(ManagedTimePeriodExceptionMixin, DeleteView):
    template_name = 'chrono/manager_confirm_exception_delete.html'
    model = TimePeriodException
    tab_anchor = 'time-periods'

    def get_success_url(self):
        if self.desk:
            referer = self.request.headers.get('Referer')
            success_url = reverse('chrono-manager-time-period-exception-list', kwargs={'pk': self.desk.pk})
            if success_url in referer:
                return success_url

        success_url = super().get_success_url()
        if self.desk and 'from_popup' in self.request.GET:
            success_url = f'{success_url}?display_exceptions={self.desk.pk}'
        return success_url

    def get_queryset(self):
        return super().get_queryset().filter(source__settings_slug__isnull=True)

    def form_valid(self, form):
        exception = self.get_object()
        response = super().form_valid(form)
        if not self.desk:
            self.unavailability_calendar.take_snapshot(
                request=self.request, comment=_('removed unavailability (%s)') % self.object
            )
        else:
            self.desk.agenda.take_snapshot(
                request=self.request, comment=_('removed exception (%s)') % self.object
            )

        if not exception.desk_id:
            return response

        if not exception.desk.agenda.desk_simple_management:
            return response

        for desk in exception.desk.agenda.desk_set.exclude(pk=exception.desk.pk):
            exc = desk.timeperiodexception_set.filter(
                source__isnull=True,
                label=exception.label,
                start_datetime=exception.start_datetime,
                end_datetime=exception.end_datetime,
            ).first()
            if exc is not None:
                exc.delete()

        return response


time_period_exception_delete = TimePeriodExceptionDeleteView.as_view()


class DeskImportTimePeriodExceptionsView(EditableAgendaSubobjectMixin, UpdateView):
    model = Desk
    form_class = DeskExceptionsImportForm
    template_name = 'chrono/manager_import_exceptions.html'
    tab_anchor = 'time-periods'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        desk = self.get_object()
        context['exception_sources'] = desk.timeperiodexceptionsource_set.all()
        context['desk'] = desk
        context['unavailability_calendars'] = (
            UnavailabilityCalendar.objects.filter(desks=desk)
            .annotate(enabled=Value(True, BooleanField()))
            .union(
                UnavailabilityCalendar.objects.exclude(desks=desk).annotate(
                    enabled=Value(False, BooleanField())
                )
            )
        )
        context['base_template'] = 'chrono/manager_agenda_settings.html'
        return context

    def import_file(self, desk, form):
        if form.cleaned_data['ics_file']:
            ics_file = form.cleaned_data['ics_file']
            source = desk.timeperiodexceptionsource_set.create(ics_filename=ics_file.name, ics_file=ics_file)
            ics_file.seek(0)
        elif form.cleaned_data['ics_url']:
            source = desk.timeperiodexceptionsource_set.create(ics_url=form.cleaned_data['ics_url'])
        parsed = source._check_ics_content()
        source._parsed = parsed
        return source

    def form_valid(self, form):
        desk = self.get_object()
        sources = []
        all_desks = form.cleaned_data.get('all_desks')
        try:
            with transaction.atomic():
                if all_desks or desk.agenda.desk_simple_management:
                    for _desk in desk.agenda.desk_set.all():
                        sources.append(self.import_file(_desk, form))
                else:
                    sources.append(self.import_file(desk, form))
        except ICSError as e:
            form.add_error(None, force_str(e))
            return self.form_invalid(form)

        try:
            for source in sources:
                source.refresh_timeperiod_exceptions(data=source._parsed)
        except ICSError as e:
            form.add_error(None, force_str(e))
            return self.form_invalid(form)

        messages.info(self.request, _('Exceptions will be imported in a few minutes.'))
        desk.agenda.take_snapshot(request=self.request, comment=_('imported exceptions (%s)') % sources[0])
        return super().form_valid(form)


desk_import_time_period_exceptions = DeskImportTimePeriodExceptionsView.as_view()


class TimePeriodExceptionSourceDeleteView(ManagedTimePeriodExceptionMixin, DeleteView):
    template_name = 'chrono/manager_confirm_source_delete.html'
    model = TimePeriodExceptionSource
    tab_anchor = 'time-periods'

    def form_valid(self, form):
        source = self.get_object()
        response = super().form_valid(form)

        if not source.desk:
            self.unavailability_calendar.take_snapshot(
                request=self.request, comment=_('removed exceptions (%s)') % source
            )
            return response
        if not source.desk.agenda.desk_simple_management:
            self.desk.agenda.take_snapshot(
                request=self.request, comment=_('removed exceptions (%s)') % source
            )
            return response

        for desk in source.desk.agenda.desk_set.exclude(pk=source.desk_id):
            if source.ics_filename:
                queryset = desk.timeperiodexceptionsource_set.filter(ics_filename=source.ics_filename)
            else:
                queryset = desk.timeperiodexceptionsource_set.filter(ics_url=source.ics_url)
            _source = queryset.first()
            if _source is not None:
                _source.delete()

        self.desk.agenda.take_snapshot(request=self.request, comment=_('removed exceptions (%s)') % source)
        return response


time_period_exception_source_delete = TimePeriodExceptionSourceDeleteView.as_view()


class TimePeriodExceptionSourceReplaceView(ManagedTimePeriodExceptionMixin, UpdateView):
    model = TimePeriodExceptionSource
    form_class = TimePeriodExceptionSourceReplaceForm
    template_name = 'chrono/manager_replace_exceptions.html'
    tab_anchor = 'time-periods'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(ics_filename__isnull=False)

    def import_file(self, obj, form):
        source = obj.timeperiodexceptionsource_set.filter(ics_filename=self.get_object().ics_filename).first()
        if source is not None:
            source.refresh_timeperiod_exceptions()

    def form_valid(self, form):
        try:
            if self.desk and self.desk.agenda.desk_simple_management:
                for _desk in self.desk.agenda.desk_set.all():
                    self.import_file(_desk, form)
            else:
                self.import_file(self.desk or self.unavailability_calendar, form)
        except ICSError as e:
            form.add_error(None, force_str(e))
            return self.form_invalid(form)

        messages.info(self.request, _('Exceptions will be synchronized in a few minutes.'))
        response = super().form_valid(form)
        if self.desk:
            self.desk.agenda.take_snapshot(
                request=self.request, comment=_('imported exceptions (%s)') % self.object
            )
        else:
            self.unavailability_calendar.take_snapshot(
                request=self.request, comment=_('imported exceptions (%s)') % self.object
            )
        return response


time_period_exception_source_replace = TimePeriodExceptionSourceReplaceView.as_view()


class TimePeriodExceptionSourceRefreshView(ManagedTimePeriodExceptionMixin, DetailView):
    model = TimePeriodExceptionSource
    tab_anchor = 'time-periods'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(ics_url__isnull=False)

    def import_file(self, obj):
        source = obj.timeperiodexceptionsource_set.filter(ics_url=self.get_object().ics_url).first()
        if source is not None:
            source.refresh_timeperiod_exceptions()

    def get(self, request, *args, **kwargs):
        try:
            if self.desk and self.desk.agenda.desk_simple_management:
                for _desk in self.desk.agenda.desk_set.all():
                    self.import_file(_desk)
            else:
                self.import_file(self.desk or self.unavailability_calendar)
        except ICSError as e:
            messages.error(self.request, force_str(e))

        messages.info(self.request, _('Exceptions will be synchronized in a few minutes.'))
        # redirect to settings
        return HttpResponseRedirect(self.get_success_url())


time_period_exception_source_refresh = TimePeriodExceptionSourceRefreshView.as_view()


class AgendaInspectView(ManagedAgendaMixin, DetailView):
    template_name = 'chrono/manager_agenda_inspect.html'
    model = Agenda

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(
            Agenda.objects.select_related(
                'category', 'events_type', 'admin_role', 'edit_role', 'view_role'
            ).prefetch_related(
                'resources',
                Prefetch(
                    'desk_set',
                    queryset=Desk.objects.prefetch_related(
                        'timeperiod_set',
                        'timeperiodexceptionsource_set',
                        'unavailability_calendars',
                        Prefetch(
                            'timeperiodexception_set',
                            queryset=TimePeriodException.objects.filter(source__isnull=True),
                        ),
                    ),
                ),
                Prefetch('event_set', queryset=Event.objects.filter(primary_event__isnull=True)),
            ),
            id=kwargs.get('pk'),
        )

    def get_object(self):
        return self.agenda


agenda_inspect = AgendaInspectView.as_view()


class AgendaHistoryView(ManagedAgendaMixin, InstanceWithSnapshotHistoryView):
    template_name = 'chrono/manager_agenda_history.html'
    model = AgendaSnapshot
    instance_context_key = 'agenda'


agenda_history = AgendaHistoryView.as_view()


class AgendaHistoryCompareView(ManagedAgendaMixin, InstanceWithSnapshotHistoryCompareView):
    template_name = 'chrono/manager_agenda_history_compare.html'
    inspect_template_name = 'chrono/manager_agenda_inspect_fragment.html'
    model = Agenda
    instance_context_key = 'agenda'
    history_view = 'chrono-manager-agenda-history'


agenda_history_compare = AgendaHistoryCompareView.as_view()


class BookingCancelView(ViewableAgendaMixin, UpdateView):
    template_name = 'chrono/manager_confirm_booking_cancellation.html'
    model = Booking
    pk_url_kwarg = 'booking_pk'
    form_class = BookingCancelForm

    def dispatch(self, request, *args, **kwargs):
        self.booking = self.get_object()
        if self.booking.event.agenda.kind != 'meetings':
            try:
                subscription = Subscription.objects.get(
                    agenda=self.booking.event.agenda,
                    date_start__lte=self.booking.event.start_datetime,
                    date_end__gt=self.booking.event.start_datetime,
                    user_external_id=self.booking.user_external_id,
                )
            except (Subscription.DoesNotExist, Subscription.MultipleObjectsReturned):
                subscription = None
            if subscription:
                raise Http404
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(cancellation_datetime__isnull=True, primary_booking__isnull=True)

    def form_valid(self, form):
        trigger_callback = not form.cleaned_data['disable_trigger']
        try:
            self.booking.cancel(trigger_callback, request=self.request)
        except requests.RequestException:
            form.add_error(None, _('There has been an error sending cancellation notification to form.'))
            form.add_error(None, _('Check this box if you are sure you want to proceed anyway.'))
            form.show_trigger_checkbox()
            return self.form_invalid(form)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        next_url = self.request.POST.get('next')
        if next_url:
            return next_url
        event = self.booking.event
        day = event.start_datetime
        return reverse(
            'chrono-manager-agenda-month-view',
            kwargs={'pk': event.agenda.pk, 'year': day.year, 'month': day.month, 'day': day.day},
        )


booking_cancel = BookingCancelView.as_view()


class BookingCheckMixin:
    def get_booking(self, **kwargs):
        booking = get_object_or_404(
            Booking,
            Q(event__checked=False) | Q(event__agenda__disable_check_update=False),
            Q(event__start_datetime__date__lte=now().date())
            | Q(event__agenda__enable_check_for_future_events=True),
            pk=kwargs['booking_pk'],
            event__agenda=self.agenda,
            event__cancelled=False,
            event__check_locked=False,
            in_waiting_list=False,
            primary_booking__isnull=True,
        )
        if not booking.event.agenda.subscriptions.exists() and booking.cancellation_datetime is not None:
            raise Http404
        return booking

    def get_check_type(self, kind):
        form = self.get_form()
        if form.is_valid() and form.cleaned_data['check_type']:
            check_types = getattr(form, '%s_check_types' % kind)
            for ct in check_types:
                if ct.slug == form.cleaned_data['check_type']:
                    return ct

    def response(self, request, booking, event=None):
        event = event or booking.event
        if is_ajax(request):
            if booking is None:
                return HttpResponse()
            if isinstance(booking, Subscription):
                booking.presence_form = BookingCheckPresenceForm(
                    agenda=self.agenda,
                    subscription=True,
                )
                booking.kind = 'subscription'
            else:
                if booking.cancellation_datetime is None:
                    booking.absence_form = BookingCheckAbsenceForm(
                        agenda=self.agenda,
                        checked=bool(booking.user_check),
                        initial={'check_type': booking.user_check.type_slug if booking.user_check else None},
                    )
                booking.presence_form = BookingCheckPresenceForm(
                    agenda=self.agenda,
                    initial={'check_type': booking.user_check.type_slug if booking.user_check else None},
                )
                booking.kind = 'booking'

            EventDetailView.complete_event_attributes(self.agenda, event)

            return render(
                request,
                'chrono/manager_event_check_booking_fragment.html',
                {'booking': booking, 'agenda': self.agenda, 'event': event, 'include_event_meta': True},
            )
        return HttpResponseRedirect(
            reverse(
                'chrono-manager-event-check',
                kwargs={'pk': event.agenda_id, 'event_pk': event.pk},
            )
        )


class PresenceViewMixin:
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['agenda'] = self.agenda
        return kwargs

    def post(self, request, *args, **kwargs):
        booking = self.get_booking(**kwargs)
        check_type = self.get_check_type(kind='presence')
        booking.mark_user_presence(
            check_type_slug=check_type.slug if check_type else None,
            check_type_label=check_type.label if check_type else None,
            request=request,
        )
        return self.response(request, booking)


class AbsenceViewMixin:
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['agenda'] = self.agenda
        return kwargs

    def post(self, request, *args, **kwargs):
        booking = self.get_booking(**kwargs)
        if getattr(booking, 'cancellation_datetime', None) is not None:
            raise Http404
        check_type = self.get_check_type(kind='absence')
        booking.mark_user_absence(
            check_type_slug=check_type.slug if check_type else None,
            check_type_label=check_type.label if check_type else None,
            request=request,
        )
        return self.response(request, booking)


class BookingPresenceView(ViewableAgendaMixin, BookingCheckMixin, PresenceViewMixin, FormView):
    form_class = BookingCheckPresenceForm


booking_presence = BookingPresenceView.as_view()


class BookingAbsenceView(ViewableAgendaMixin, BookingCheckMixin, AbsenceViewMixin, FormView):
    form_class = BookingCheckAbsenceForm


booking_absence = BookingAbsenceView.as_view()


class BookingResetView(ViewableAgendaMixin, BookingCheckMixin, FormView):
    def post(self, request, *args, **kwargs):
        booking = self.get_booking(**kwargs)
        event = booking.event
        try:
            subscription = Subscription.objects.get(
                agenda=self.agenda,
                date_start__lte=event.start_datetime,
                date_end__gt=event.start_datetime,
                user_external_id=booking.user_external_id,
            )
        except (Subscription.DoesNotExist, Subscription.MultipleObjectsReturned):
            subscription = None
        booking = booking.reset_user_was_present(request=request)
        return self.response(request, booking or subscription, event)


booking_reset = BookingResetView.as_view()


class SubscriptionCheckMixin(BookingCheckMixin):
    def get_booking(self, **kwargs):
        event = get_object_or_404(
            Event,
            Q(checked=False) | Q(agenda__disable_check_update=False),
            Q(start_datetime__date__lte=now().date()) | Q(agenda__enable_check_for_future_events=True),
            agenda=self.agenda,
            cancelled=False,
            check_locked=False,
            pk=kwargs['event_pk'],
        )
        subscription = get_object_or_404(
            Subscription,
            agenda=self.agenda,
            pk=kwargs['subscription_pk'],
            date_start__lte=event.start_datetime,
            date_end__gt=event.start_datetime,
        )
        with transaction.atomic():
            try:
                booking, created = event.booking_set.get_or_create(
                    user_external_id=subscription.user_external_id,
                    defaults={
                        'user_last_name': subscription.user_last_name,
                        'user_first_name': subscription.user_first_name,
                        'user_email': subscription.user_email,
                        'user_phone_number': subscription.user_phone_number,
                        'extra_data': subscription.extra_data,
                    },
                )
            except Booking.MultipleObjectsReturned:
                raise Http404
            if not created:
                raise Http404
            audit(
                'booking:create-from-subscription',
                request=self.request,
                agenda=event.agenda,
                extra_data={
                    'booking': booking,
                },
            )
            return booking


class SubscriptionPresenceView(ViewableAgendaMixin, SubscriptionCheckMixin, PresenceViewMixin, FormView):
    form_class = BookingCheckPresenceForm


subscription_presence = SubscriptionPresenceView.as_view()


class BookingExtraUserBlock(ViewableAgendaMixin, View):
    def get(self, request, *args, **kwargs):
        booking = get_object_or_404(
            Booking,
            pk=kwargs['booking_pk'],
            event__agenda=self.agenda,
            event__cancelled=False,
            primary_booking__isnull=True,
        )
        return HttpResponse(booking.get_extra_user_block(request))


booking_extra_user_block = BookingExtraUserBlock.as_view()


class SubscriptionExtraUserBlock(ViewableAgendaMixin, View):
    def get(self, request, *args, **kwargs):
        subscription = get_object_or_404(
            Subscription,
            agenda=self.agenda,
            pk=kwargs['subscription_pk'],
        )
        return HttpResponse(subscription.get_extra_user_block(request))


subscription_extra_user_block = SubscriptionExtraUserBlock.as_view()


class EventCancelView(ViewableAgendaMixin, UpdateView):
    template_name = 'chrono/manager_confirm_event_cancellation.html'
    model = Event
    pk_url_kwarg = 'event_pk'
    form_class = EventCancelForm

    def dispatch(self, request, *args, **kwargs):
        self.event = self.get_object()
        if self.event.cancellation_status:
            raise PermissionDenied()
        self.cancel_bookings = not (self.request.GET.get('force_cancellation'))
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        self.event.cancel(self.cancel_bookings)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cancel_bookings'] = self.cancel_bookings
        context['bookings_count'] = self.event.booking_set.filter(cancellation_datetime__isnull=True).count()
        context['cancellation_forbidden'] = (
            self.event.booking_set.filter(cancellation_datetime__isnull=True, cancel_callback_url='')
            .exclude(backoffice_url='')
            .exists()
        )
        return context

    def get_success_url(self):
        self.event.refresh_from_db()
        if self.event.cancellation_scheduled:
            messages.info(self.request, _('Event "%s" will be cancelled in a few minutes.') % self.event)
        next_url = self.request.POST.get('next')
        if next_url:
            return next_url
        day = localtime(self.event.start_datetime)
        return reverse(
            'chrono-manager-agenda-month-view',
            kwargs={'pk': self.event.agenda.pk, 'year': day.year, 'month': day.month, 'day': day.day},
        )


event_cancel = EventCancelView.as_view()


class EventCancellationReportView(ViewableAgendaMixin, DetailView):
    model = EventCancellationReport
    template_name = 'chrono/manager_event_cancellation_report.html'
    context_object_name = 'report'
    pk_url_kwarg = 'report_pk'

    def get(self, *args, **kwargs):
        self.report = self.get_object()
        self.report.seen = True
        self.report.save()
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        bookings = self.report.bookings.all()
        errors = self.report.booking_errors
        context['errors'] = {
            booking: errors[str(booking.pk)] for booking in bookings if str(booking.pk) in errors
        }
        return context


event_cancellation_report = EventCancellationReportView.as_view()


class EventCancellationReportListView(ViewableAgendaMixin, ListView):
    model = EventCancellationReport
    context_object_name = 'cancellation_reports'
    template_name = 'chrono/manager_event_cancellation_reports.html'


event_cancellation_report_list = EventCancellationReportListView.as_view()


class EventsReportView(View):
    template_name = 'chrono/manager_event_report.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        form = EventsReportForm(user=self.request.user, data=self.request.GET or None)
        if self.request.GET:
            form.is_valid()
        context = {'form': form}
        if form.is_valid() and 'not-checked' in form.cleaned_data['status']:
            agenda_seens = set()
            events = form.get_events()
            for event in events['not_checked']:
                if event.agenda.pk in agenda_seens:
                    continue
                agenda_seens.add(event.agenda.pk)
                check_types = get_agenda_check_types(event.agenda)
                unjustified_absences = [
                    ct for ct in check_types if ct.unjustified_absence and ct.kind == 'absence'
                ]
                if unjustified_absences:
                    context['has_unjustified_absences'] = True
                    break

        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        if 'csv' in request.GET and context['form'].is_valid():
            return self.csv(request, context)
        if 'check-absences' in request.GET and context['form'].is_valid():
            return self.check_absences(request, context)
        if 'lock' in request.GET and context['form'].is_valid():
            return self.lock_events(request, context)
        if 'unlock' in request.GET and context['form'].is_valid():
            return self.unlock_events(request, context)
        return render(request, self.template_name, context)

    def csv(self, request, context):
        form = context['form']
        filename = 'events_report_{}_{}'.format(
            form.cleaned_data['date_start'].strftime('%Y-%m-%d'),
            form.cleaned_data['date_end'].strftime('%Y-%m-%d'),
        )
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % filename
        response.write('\ufeff')
        writer = csv.writer(response, quoting=csv.QUOTE_ALL, delimiter=';')
        events = form.get_events()

        headers = [
            _('agenda'),
            _('label'),
            _('date'),
            _('time'),
        ]

        for status, label in STATUS_CHOICES:
            if status in form.cleaned_data['status']:
                writer.writerow([label])
                writer.writerow(headers)
                for event in events[status.replace('-', '_')]:
                    writer.writerow(
                        [
                            event.agenda.label,
                            event.label,
                            event.start_datetime.strftime('%Y-%m-%d'),
                            event.start_datetime.strftime('%H:%M'),
                        ]
                    )

        return response

    def check_absences(self, request, context):
        form = context['form']
        events = form.get_events()
        check_types_by_agendas = {}
        for event in events['not_checked']:
            if event.check_locked:
                continue
            if event.start_datetime.date() > now().date() and not event.agenda.enable_check_for_future_events:
                continue
            if event.agenda.pk not in check_types_by_agendas:
                check_types_by_agendas[event.agenda.pk] = get_agenda_check_types(event.agenda)
            check_types = check_types_by_agendas[event.agenda.pk]
            unjustified_absences = [
                ct for ct in check_types if ct.unjustified_absence and ct.kind == 'absence'
            ]
            if unjustified_absences:
                for booking in event.booking_set.filter(
                    user_checks__isnull=True,
                    cancellation_datetime__isnull=True,
                    in_waiting_list=False,
                    primary_booking__isnull=True,
                ):
                    booking.mark_user_absence(
                        check_type_slug=unjustified_absences[0].slug,
                        check_type_label=unjustified_absences[0].label,
                        request=request,
                    )
        messages.info(self.request, _('Bookings without status have been checked.'))
        return render(request, self.template_name, context)

    def _mark_events(self, form, check_locked):
        agendas = set()
        events = form.get_events()
        events = events['not_locked'] if check_locked else events['locked']
        if not check_locked:
            for event in events:
                if event.agenda.partial_bookings:
                    # ignore partial bookings agendas
                    continue
                agendas.add(event.agenda)
            unlock_agendas(
                agenda_slugs=sorted([a.slug for a in agendas]),
                date_start=form.cleaned_data['date_start'],
                # in this form the selected period includes the end date; this is different than
                # on the lingo side that will be called by unlock_agendas(), where the period
                # doesn't include it, so the end date is adjusted here to match what is expected.
                date_end=form.cleaned_data['date_end'] + datetime.timedelta(days=1),
            )
        aes = []
        for event in events:
            event.check_locked = check_locked
            event.save()
            if check_locked is False:
                event.async_refresh_booking_computed_times()
            aes.append(
                audit(
                    'check:lock' if check_locked else 'check:unlock',
                    request=self.request,
                    agenda=event.agenda,
                    extra_data={
                        'event': event,
                    },
                    bulk=True,
                )
            )
        bulk_audit(aes)

    def lock_events(self, request, context):
        form = context['form']
        self._mark_events(form, check_locked=True)
        messages.info(self.request, _('Events have been locked.'))
        return render(request, self.template_name, context)

    def unlock_events(self, request, context):
        form = context['form']
        try:
            self._mark_events(form, check_locked=False)
        except LingoError as e:
            messages.error(self.request, str(e))
        else:
            messages.info(self.request, _('Events have been unlocked.'))
        return render(request, self.template_name, context)


events_report = EventsReportView.as_view()


class EventsUserCheckReportView(EventsReportMixin, View):
    template_name = 'chrono/manager_events_user_check_report.html'

    def get_context_data(self, **kwargs):
        form = EventsCheckReportForm(
            user_external_id=kwargs['user_external_id'], data=self.request.GET or None
        )
        if self.request.GET:
            form.is_valid()
        return {'form': form, 'user_external_id': kwargs['user_external_id']}

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if 'csv' in request.GET and context['form'].is_valid():
            return self.csv(request, context)
        return render(request, self.template_name, context)

    def get_export_filename(self, context):
        return 'check_report_user_{}_{}'.format(
            context['form'].cleaned_data['date_start'].strftime('%Y-%m-%d'),
            context['form'].cleaned_data['date_end'].strftime('%Y-%m-%d'),
        )

    def get_booked(self, item, date):
        data = item['dates'].get(date)
        if data is None:
            return '-'
        return data.get('check_type')

    def csv(self, request, context):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % self.get_export_filename(context)
        response.write('\ufeff')
        writer = csv.writer(response, quoting=csv.QUOTE_ALL, delimiter=';')
        form = context['form']
        slots = form.get_slots()
        events_num = len(slots['events'])
        activity_display = form.cleaned_data.get('activity_display')

        for dates in slots['dates']:
            for grouper in slots['users']:
                self.write_group(form, writer, slots, activity_display, events_num, dates, grouper)

        return response


events_user_check_report = EventsUserCheckReportView.as_view()


class TimePeriodExceptionSourceToggleView(ManagedTimePeriodExceptionMixin, DetailView):
    model = TimePeriodExceptionSource

    def get_object(self, queryset=None):
        source = super().get_object(queryset)
        if source.settings_slug is None:
            raise Http404('This source cannot be enabled nor disabled')
        return source

    def get(self, request, *args, **kwargs):
        source = self.get_object()

        if source.enabled:
            source.disable()
            was_enabled = False
            if source.desk.label:
                if self.desk.agenda.partial_bookings:
                    message = _('Exception source %(source)s has been disabled on resource %(desk)s.')
                else:
                    message = _('Exception source %(source)s has been disabled on desk %(desk)s.')
            else:
                message = _('Exception source %(source)s has been disabled.')
        else:
            source.enable()
            was_enabled = True
            if source.desk.label:
                if self.desk.agenda.partial_bookings:
                    message = _('Exception source %(source)s has been enabled on resource %(desk)s.')
                else:
                    message = _('Exception source %(source)s has been enabled on desk %(desk)s.')
            else:
                message = _('Exception source %(source)s has been enabled.')

        if self.desk.agenda.desk_simple_management:
            for desk in self.desk.agenda.desk_set.exclude(pk=self.desk.pk):
                _source = desk.timeperiodexceptionsource_set.filter(
                    settings_slug=source.settings_slug
                ).first()
                if _source is None:
                    continue
                if was_enabled:
                    _source.enable()
                    message = _(
                        'Exception source %(source)s has been enabled. Exceptions will be imported in a few minutes.'
                    )
                else:
                    _source.disable()
                    message = _('Exception source %(source)s has been disabled.')

        if was_enabled:
            self.desk.agenda.take_snapshot(
                request=self.request, comment=_('disabled exceptions (%s)') % source
            )
        else:
            self.desk.agenda.take_snapshot(
                request=self.request, comment=_('enabled exceptions (%s)') % source
            )
        messages.info(self.request, message % {'source': source, 'desk': source.desk})
        return HttpResponseRedirect(
            '%s#open:time-periods'
            % reverse('chrono-manager-agenda-settings', kwargs={'pk': source.desk.agenda_id})
        )


time_period_exception_source_toggle = TimePeriodExceptionSourceToggleView.as_view()


class ViewableUnavailabilityCalendarMixin:
    unavailability_calendar = None

    def set_unavailability_calendar(self, **kwargs):
        self.unavailability_calendar = get_object_or_404(UnavailabilityCalendar, id=kwargs.get('pk'))

    def dispatch(self, request, *args, **kwargs):
        self.set_unavailability_calendar(**kwargs)
        if not self.check_permissions(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def check_permissions(self, user):
        return self.unavailability_calendar.can_be_viewed(user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['unavailability_calendar'] = self.unavailability_calendar
        context['user_can_manage'] = self.unavailability_calendar.can_be_managed(self.request.user)
        return context


class ManagedUnavailabilityCalendarMixin(ViewableUnavailabilityCalendarMixin):
    def check_permissions(self, user):
        return self.unavailability_calendar.can_be_managed(user)

    def get_success_url(self):
        return reverse(
            'chrono-manager-unavailability-calendar-settings', kwargs={'pk': self.unavailability_calendar.id}
        )


class UnavailabilityCalendarListView(WithApplicationsMixin, ListView):
    template_name = 'chrono/manager_unavailability_calendar_list.html'
    model = UnavailabilityCalendar

    def dispatch(self, request, *args, **kwargs):
        self.with_applications_dispatch(request)
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = self.with_applications_queryset()
        if not self.request.user.is_staff:
            group_ids = [x.id for x in self.request.user.groups.all()]
            queryset = queryset.filter(Q(view_role_id__in=group_ids) | Q(edit_role_id__in=group_ids))
            if not queryset.count():
                raise PermissionDenied
        return queryset.order_by('label')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return self.with_applications_context_data(context)


unavailability_calendar_list = UnavailabilityCalendarListView.as_view()


class UnavailabilityCalendarAddView(CreateView):
    template_name = 'chrono/manager_unavailability_calendar_form.html'
    model = UnavailabilityCalendar
    form_class = UnavailabilityCalendarAddForm

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-unavailability-calendar-settings', kwargs={'pk': self.object.id})

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request, comment=pgettext('snapshot', 'created'))
        return response


unavailability_calendar_add = UnavailabilityCalendarAddView.as_view()


class UnavailabilityCalendarDetailView(ViewableUnavailabilityCalendarMixin, DetailView):
    template_name = 'chrono/manager_unavailability_calendar_detail.html'
    model = UnavailabilityCalendar

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agendas'] = Agenda.objects.filter(
            desk__unavailability_calendars__pk=self.unavailability_calendar.pk
        ).distinct()
        return context


unavailability_calendar_view = UnavailabilityCalendarDetailView.as_view()


class UnavailabilityCalendarEditView(ManagedUnavailabilityCalendarMixin, UpdateView):
    template_name = 'chrono/manager_unavailability_calendar_form.html'
    model = UnavailabilityCalendar
    form_class = UnavailabilityCalendarEditForm

    def form_valid(self, *args, **kwargs):
        response = super().form_valid(*args, **kwargs)
        self.object.take_snapshot(request=self.request)
        return response


unavailability_calendar_edit = UnavailabilityCalendarEditView.as_view()


class UnavailabilityCalendarDeleteView(DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = UnavailabilityCalendar

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-unavailability-calendar-list')

    def post(self, *args, **kwargs):
        self.get_object().take_snapshot(request=self.request, deletion=True)
        try:
            return super().post(*args, **kwargs)
        except ProtectedError:
            messages.warning(
                self.request,
                _('This calendar cannot be deleted because it is used by shared custody agendas.'),
            )
            return HttpResponseRedirect(self.get_object().get_absolute_url())


unavailability_calendar_delete = UnavailabilityCalendarDeleteView.as_view()


class UnavailabilityCalendarSettings(ManagedUnavailabilityCalendarMixin, DetailView):
    template_name = 'chrono/manager_unavailability_calendar_settings.html'
    model = UnavailabilityCalendar

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['unavailability_calendar'] = self.object
        return context


unavailability_calendar_settings = UnavailabilityCalendarSettings.as_view()


class UnavailabilityCalendarExport(ManagedUnavailabilityCalendarMixin, DetailView):
    model = UnavailabilityCalendar

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/json')
        today = datetime.date.today()
        response['Content-Disposition'] = (
            'attachment; filename="export_unavailability-calendar_{}_{}.json"'.format(
                self.get_object().slug, today.strftime('%Y%m%d')
            )
        )
        json.dump({'unavailability_calendars': [self.get_object().export_json()]}, response, indent=2)
        return response


unavailability_calendar_export = UnavailabilityCalendarExport.as_view()


class UnavailabilityCalendarAddUnavailabilityView(ManagedUnavailabilityCalendarMixin, CreateView):
    template_name = 'chrono/manager_time_period_exception_form.html'
    form_class = NewTimePeriodExceptionForm
    model = TimePeriodException

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if not kwargs.get('instance'):
            kwargs['instance'] = self.model()
        kwargs['instance'].unavailability_calendar = self.unavailability_calendar
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['base_template'] = 'chrono/manager_unavailability_calendar_settings.html'
        context['cancel_url'] = reverse(
            'chrono-manager-unavailability-calendar-settings', kwargs={'pk': self.unavailability_calendar.pk}
        )
        return context

    def form_valid(self, form):
        result = super().form_valid(form)
        self.unavailability_calendar.take_snapshot(
            request=self.request, comment=_('added unavailability (%s)') % self.object
        )
        messages.info(self.request, _('Unavailability added.'))
        if self.object.has_booking_within_time_slot():
            messages.warning(self.request, _('One or several bookings exists within this time slot.'))
        return result


unavailability_calendar_add_unavailability = UnavailabilityCalendarAddUnavailabilityView.as_view()


class UnavailabilityCalendarImportUnavailabilitiesView(ManagedUnavailabilityCalendarMixin, UpdateView):
    model = UnavailabilityCalendar
    form_class = UnavailabilityCalendarExceptionsImportForm
    template_name = 'chrono/manager_import_exceptions.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        unavailability_calendar = self.get_object()
        context['exception_sources'] = unavailability_calendar.timeperiodexceptionsource_set.all()
        context['base_template'] = 'chrono/manager_unavailability_calendar_settings.html'
        return context

    def import_file(self, form):
        unavailability_calendar = self.get_object()
        if form.cleaned_data['ics_file']:
            ics_file = form.cleaned_data['ics_file']
            source = unavailability_calendar.timeperiodexceptionsource_set.create(
                ics_filename=ics_file.name, ics_file=ics_file
            )
            ics_file.seek(0)
        elif form.cleaned_data['ics_url']:
            source = unavailability_calendar.timeperiodexceptionsource_set.create(
                ics_url=form.cleaned_data['ics_url']
            )
        parsed = source._check_ics_content()
        source._parsed = parsed
        return source

    def form_valid(self, form):
        try:
            with transaction.atomic():
                source = self.import_file(form)
        except ICSError as e:
            form.add_error(None, force_str(e))
            return self.form_invalid(form)

        try:
            source.refresh_timeperiod_exceptions(data=source._parsed)
        except ICSError as e:
            form.add_error(None, force_str(e))
            return self.form_invalid(form)

        messages.info(self.request, _('Exceptions will be imported in a few minutes.'))
        response = super().form_valid(form)
        self.object.take_snapshot(request=self.request, comment=_('imported exceptions (%s)') % source)
        return response


unavailability_calendar_import_unavailabilities = UnavailabilityCalendarImportUnavailabilitiesView.as_view()


class UnavailabilityCalendarInspectView(ManagedUnavailabilityCalendarMixin, DetailView):
    template_name = 'chrono/manager_unavailability_calendar_inspect.html'
    model = UnavailabilityCalendar
    context_object_name = 'unavailability_calendar'

    def set_unavailability_calendar(self, **kwargs):
        self.unavailability_calendar = get_object_or_404(
            UnavailabilityCalendar.objects.select_related('edit_role', 'view_role'), pk=kwargs.get('pk')
        )

    def get_object(self):
        return self.unavailability_calendar


unavailability_calendar_inspect = UnavailabilityCalendarInspectView.as_view()


class UnavailabilityCalendarHistoryView(ManagedUnavailabilityCalendarMixin, InstanceWithSnapshotHistoryView):
    template_name = 'chrono/manager_unavailability_calendar_history.html'
    model = UnavailabilityCalendarSnapshot
    instance_context_key = 'unavailability_calendar'


unavailability_calendar_history = UnavailabilityCalendarHistoryView.as_view()


class UnavailabilityCalendarHistoryCompareView(
    ManagedUnavailabilityCalendarMixin, InstanceWithSnapshotHistoryCompareView
):
    template_name = 'chrono/manager_unavailability_calendar_history_compare.html'
    inspect_template_name = 'chrono/manager_unavailability_calendar_inspect_fragment.html'
    model = UnavailabilityCalendar
    instance_context_key = 'unavailability_calendar'
    history_view = 'chrono-manager-unavailability-calendar-history'


unavailability_calendar_history_compare = UnavailabilityCalendarHistoryCompareView.as_view()


class SharedCustodyAgendaMixin:
    agenda = None
    tab_anchor = None

    def set_agenda(self, **kwargs):
        self.agenda = get_object_or_404(SharedCustodyAgenda, id=kwargs.get('pk'))

    def dispatch(self, request, *args, **kwargs):
        self.set_agenda(**kwargs)
        if not self.check_permissions(request.user):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def check_permissions(self, user):
        if user.is_staff:
            return True
        management_role = SharedCustodySettings.get_singleton().management_role
        return bool(management_role in user.groups.all())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['agenda'] = self.agenda
        context['user_can_manage'] = True
        context['user_can_edit'] = True
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if isinstance(self, DeleteView):
            return kwargs

        if not kwargs.get('instance'):
            kwargs['instance'] = self.model()
        kwargs['instance'].agenda = self.agenda
        return kwargs

    def get_success_url(self):
        url = reverse('chrono-manager-shared-custody-agenda-settings', kwargs={'pk': self.agenda.id})
        if self.tab_anchor:
            url += '#open:%s' % self.tab_anchor
        return url


class SharedCustodyAgendaView(SharedCustodyAgendaMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return reverse(
            'chrono-manager-shared-custody-agenda-month-view',
            kwargs={'pk': self.agenda.pk, 'year': now().year, 'month': now().month},
        )


shared_custody_agenda_view = SharedCustodyAgendaView.as_view()


class SharedCustodyAgendaMonthView(SharedCustodyAgendaMixin, YearMixin, MonthMixin, DateMixin, DetailView):
    template_name = 'chrono/manager_shared_custody_agenda_month_view.html'
    model = SharedCustodyAgenda

    def dispatch(self, request, *args, **kwargs):
        try:
            self.date = datetime.date(year=int(self.get_year()), month=int(self.get_month()), day=1)
        except ValueError:
            raise Http404('invalid date')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['today'] = localtime().date()

        first_monday_this_month = self.date - datetime.timedelta(days=self.date.weekday())
        first_monday_next_month = self.date + relativedelta(months=1, day=1, weekday=MO(1))
        slots = self.object.get_custody_slots(first_monday_this_month, first_monday_next_month)

        slots_by_week = collections.defaultdict(list)
        for slot in slots:
            slots_by_week[slot.date.isocalendar()[1]].append(slot)

        context['slots_by_week'] = dict(slots_by_week)
        return context

    def get_previous_month_url(self):
        previous_month = self.date - relativedelta(months=1)
        return reverse(
            'chrono-manager-shared-custody-agenda-month-view',
            kwargs={'pk': self.object.id, 'year': previous_month.year, 'month': previous_month.month},
        )

    def get_next_month_url(self):
        next_month = self.date + relativedelta(months=1)
        return reverse(
            'chrono-manager-shared-custody-agenda-month-view',
            kwargs={'pk': self.object.id, 'year': next_month.year, 'month': next_month.month},
        )


shared_custody_agenda_monthly_view = SharedCustodyAgendaMonthView.as_view()


class SharedCustodyAgendaSettings(SharedCustodyAgendaMixin, DetailView):
    template_name = 'chrono/manager_shared_custody_agenda_settings.html'
    model = SharedCustodyAgenda

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['has_holidays'] = bool(SharedCustodySettings.get_singleton().holidays_calendar_id)
        context['exceptional_periods'] = SharedCustodyPeriod.objects.filter(holiday_rule__isnull=True)
        return context


shared_custody_agenda_settings = SharedCustodyAgendaSettings.as_view()


class SharedCustodyAgendaDeleteView(SharedCustodyAgendaMixin, DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = SharedCustodyAgenda
    pk_url_kwarg = 'pk'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('chrono-manager-homepage')


shared_custody_agenda_delete = SharedCustodyAgendaDeleteView.as_view()


class SharedCustodyAgendaAddRuleView(SharedCustodyAgendaMixin, CreateView):
    title = _('Add custody rule')
    template_name = 'chrono/manager_agenda_form.html'
    form_class = SharedCustodyRuleForm
    model = SharedCustodyRule


shared_custody_agenda_add_rule = SharedCustodyAgendaAddRuleView.as_view()


class SharedCustodyAgendaEditRuleView(SharedCustodyAgendaMixin, UpdateView):
    title = _('Edit custody rule')
    template_name = 'chrono/manager_agenda_form.html'
    form_class = SharedCustodyRuleForm
    model = SharedCustodyRule
    pk_url_kwarg = 'rule_pk'


shared_custody_agenda_edit_rule = SharedCustodyAgendaEditRuleView.as_view()


class SharedCustodyAgendaDeleteRuleView(SharedCustodyAgendaMixin, DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = SharedCustodyRule
    pk_url_kwarg = 'rule_pk'


shared_custody_agenda_delete_rule = SharedCustodyAgendaDeleteRuleView.as_view()


class SharedCustodyAgendaAddHolidayRuleView(SharedCustodyAgendaMixin, CreateView):
    title = _('Add custody rule during holidays')
    template_name = 'chrono/manager_agenda_form.html'
    form_class = SharedCustodyHolidayRuleForm
    model = SharedCustodyHolidayRule
    tab_anchor = 'holidays'


shared_custody_agenda_add_holiday_rule = SharedCustodyAgendaAddHolidayRuleView.as_view()


class SharedCustodyAgendaEditHolidayRuleView(SharedCustodyAgendaMixin, UpdateView):
    title = _('Edit custody rule during holidays')
    template_name = 'chrono/manager_agenda_form.html'
    form_class = SharedCustodyHolidayRuleForm
    model = SharedCustodyHolidayRule
    pk_url_kwarg = 'rule_pk'
    tab_anchor = 'holidays'


shared_custody_agenda_edit_holiday_rule = SharedCustodyAgendaEditHolidayRuleView.as_view()


class SharedCustodyAgendaDeleteHolidayRuleView(SharedCustodyAgendaMixin, DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = SharedCustodyHolidayRule
    pk_url_kwarg = 'rule_pk'
    tab_anchor = 'holidays'


shared_custody_agenda_delete_holiday_rule = SharedCustodyAgendaDeleteHolidayRuleView.as_view()


class SharedCustodyAgendaAddPeriodView(SharedCustodyAgendaMixin, CreateView):
    title = _('Add custody period')
    template_name = 'chrono/manager_agenda_form.html'
    form_class = SharedCustodyPeriodForm
    model = SharedCustodyPeriod
    tab_anchor = 'time-periods'


shared_custody_agenda_add_period = SharedCustodyAgendaAddPeriodView.as_view()


class SharedCustodyAgendaEditPeriodView(SharedCustodyAgendaMixin, UpdateView):
    title = _('Edit custody period')
    template_name = 'chrono/manager_agenda_form.html'
    form_class = SharedCustodyPeriodForm
    model = SharedCustodyPeriod
    pk_url_kwarg = 'period_pk'
    tab_anchor = 'time-periods'


shared_custody_agenda_edit_period = SharedCustodyAgendaEditPeriodView.as_view()


class SharedCustodyAgendaDeletePeriodView(SharedCustodyAgendaMixin, DeleteView):
    template_name = 'chrono/manager_confirm_delete.html'
    model = SharedCustodyPeriod
    pk_url_kwarg = 'period_pk'
    tab_anchor = 'time-periods'


shared_custody_agenda_delete_period = SharedCustodyAgendaDeletePeriodView.as_view()


class SharedCustodySettingsView(UpdateView):
    title = _('Shared custody settings')
    template_name = 'chrono/manager_agenda_form.html'
    form_class = SharedCustodySettingsForm
    model = SharedCustodySettings
    success_url = reverse_lazy('chrono-manager-homepage')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_object(self):
        return SharedCustodySettings.get_singleton()


shared_custody_settings = SharedCustodySettingsView.as_view()


class PartialBookingCheckView(ViewableAgendaMixin, TemplateView):
    template_name = 'chrono/manager_partial_booking_form.html'
    form_class = PartialBookingCheckForm

    def dispatch(self, *args, **kwargs):
        self.set_agenda(**kwargs)
        self.event = get_object_or_404(
            Event,
            Q(checked=False) | Q(agenda__disable_check_update=False),
            pk=kwargs['event_pk'],
            agenda=self.agenda,
            check_locked=False,
        )

        self.bookings = Booking.objects.filter(
            event=self.event, user_external_id=kwargs['user_external_id']
        ).order_by('start_time')

        if not self.bookings:
            subscription = get_object_or_404(
                Subscription,
                agenda=self.agenda,
                user_external_id=kwargs['user_external_id'],
                date_start__lte=self.event.start_datetime,
                date_end__gt=self.event.start_datetime,
            )

            # create dummy booking to allow check
            booking = self.event.booking_set.create(
                user_external_id=subscription.user_external_id,
                user_last_name=subscription.user_last_name,
                user_first_name=subscription.user_first_name,
                user_email=subscription.user_email,
                user_phone_number=subscription.user_phone_number,
                extra_data=subscription.extra_data,
            )
            self.bookings = [booking]

        self.multiple_bookings = bool(len(self.bookings) > 1)
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, forms=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['forms'] = forms or self.get_forms()
        context['multiple_bookings'] = self.multiple_bookings
        return context

    def get_forms(self):
        kwargs = {'agenda': self.agenda, 'event': self.event}
        if self.request.method == 'POST':
            kwargs['data'] = self.request.POST

        forms = []
        for i, booking in enumerate(self.bookings):
            checks = booking.user_checks.all()
            first_check = checks[0] if len(checks) > 0 else BookingCheck(booking=booking)
            second_check = checks[1] if len(checks) > 1 else BookingCheck(booking=booking)

            first_check_initial = {}
            if i == 0 and first_check.presence is None:
                first_check_initial['presence'] = True

            booking.check_forms = [
                PartialBookingCheckForm(
                    instance=first_check,
                    prefix=self.get_prefix(booking),
                    initial=first_check_initial,
                    **kwargs,
                ),
            ]

            # do not show second form if checking subscription
            if booking.start_time:
                booking.check_forms.append(
                    PartialBookingCheckForm(
                        instance=second_check,
                        prefix=self.get_prefix(booking, second_check=True),
                        first_check_form=booking.check_forms[0],
                        **kwargs,
                    )
                )

            forms.extend(booking.check_forms)

        return forms

    def get_prefix(self, booking, second_check=False):
        parts = []
        if self.multiple_bookings:
            parts.append('booking-%s' % booking.pk)

        if second_check:
            parts.append('check-2')

        return '-'.join(parts)

    def post(self, *args, **kwargs):
        forms = self.get_forms()

        all_valid = all(form.is_valid() for form in forms)
        if all_valid and not self.checks_overlap(forms):
            for form in forms:
                form.save()
            return HttpResponseRedirect(self.get_success_url())

        return self.render_to_response(self.get_context_data(forms=forms))

    def checks_overlap(self, forms):
        Interval = collections.namedtuple('Interval', ['start', 'end', 'form'])
        intervals = [
            Interval(
                # if check has no start/end time, take (time, time) as an interval
                form.cleaned_data['start_time'] or form.cleaned_data['end_time'],
                form.cleaned_data['end_time'] or form.cleaned_data['start_time'],
                form,
            )
            for form in forms
            if form.cleaned_data['presence'] is not None
        ]
        intervals.sort(key=lambda x: x.start)

        for i in range(1, len(intervals)):
            if intervals[i - 1].end > intervals[i].start:
                intervals[i].form.add_error(None, _('Booking check hours are overlapping.'))
                return True

        return False

    def get_success_url(self):
        date = self.event.start_datetime
        return reverse(
            'chrono-manager-agenda-day-view',
            kwargs={'pk': self.agenda.pk, 'year': date.year, 'month': date.month, 'day': date.day},
        )


partial_booking_check_view = PartialBookingCheckView.as_view()


def menu_json(request):
    if not request.user.is_staff:
        homepage_view = HomepageView(request=request)
        homepage_view.application = None
        homepage_view.no_application = False
        if not (
            homepage_view.get_queryset().exists() or homepage_view.has_access_to_unavailability_calendars()
        ):
            return HttpResponseForbidden()
    label = _('Agendas')
    json_str = json.dumps(
        [
            {
                'label': force_str(label),
                'slug': 'calendar',
                'url': request.build_absolute_uri(reverse('chrono-manager-homepage')),
            }
        ]
    )
    content_type = 'application/json'
    for variable in ('jsonpCallback', 'callback'):
        if variable in request.GET:
            json_str = '%s(%s);' % (request.GET[variable], json_str)
            content_type = 'application/javascript'
            break
    response = HttpResponse(content_type=content_type)
    response.write(json_str)
    return response
