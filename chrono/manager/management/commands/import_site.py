# chrono - agendas system
# Copyright (C) 2016-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import sys

from django.core.management.base import BaseCommand, CommandError

from chrono.agendas.models import AgendaImportError
from chrono.manager.utils import import_site


class Command(BaseCommand):
    help = 'Import an exported site'

    def add_arguments(self, parser):
        parser.add_argument('filename', metavar='FILENAME', type=str, help='name of file to import')
        parser.add_argument('--clean', action='store_true', default=False, help='Clean site before importing')
        parser.add_argument(
            '--if-empty', action='store_true', default=False, help='Import only if site is empty'
        )
        parser.add_argument('--overwrite', action='store_true', default=False, help='Overwrite existing data')

    def handle(self, filename, **options):
        def do_import(fd):
            try:
                import_site(
                    json.load(fd),
                    if_empty=options['if_empty'],
                    clean=options['clean'],
                    overwrite=options['overwrite'],
                )
            except AgendaImportError as exc:
                raise CommandError('%s' % exc)

        if filename == '-':
            fd = sys.stdin
            do_import(fd)
        else:
            with open(filename) as fd:
                do_import(fd)
