{% extends "emails/body_base.txt" %}
{% load i18n %}

{% block content %}{% autoescape off %}{% blocktrans %}Hi,

You have been notified because the status of event "{{ event }}" has changed.
You can view it here: {{ event_url }}.{% endblocktrans %}
{% endautoescape %}
{% endblock %}
