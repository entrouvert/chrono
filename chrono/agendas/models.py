# chrono - agendas system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import collections
import copy
import dataclasses
import datetime
import functools
import itertools
import logging
import math
import sys
import uuid
from contextlib import contextmanager

import icalendar
import recurring_ical_events
import requests
from dateutil.relativedelta import SU, relativedelta
from dateutil.rrule import DAILY, WEEKLY, rrule, rruleset
from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.humanize.templatetags.humanize import ordinal
from django.contrib.postgres.expressions import ArraySubquery
from django.contrib.postgres.fields import ArrayField, DateTimeRangeField
from django.contrib.postgres.indexes import GistIndex
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.core.serializers.json import DjangoJSONEncoder
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import connection, connections, models, transaction
from django.db.models import (
    Count,
    Exists,
    ExpressionWrapper,
    F,
    Func,
    IntegerField,
    Max,
    OuterRef,
    Prefetch,
    Q,
    Subquery,
    Value,
)
from django.db.models.expressions import Col
from django.db.models.functions import Cast, Coalesce, Concat, ExtractIsoWeekDay, ExtractWeek, Least, Trunc
from django.db.models.sql.compiler import SQLCompiler
from django.db.models.sql.query import Query
from django.template import (
    Context,
    RequestContext,
    Template,
    TemplateSyntaxError,
    VariableDoesNotExist,
    engines,
)
from django.template.defaultfilters import yesno
from django.urls import reverse
from django.utils import functional, timezone
from django.utils.dates import WEEKDAYS
from django.utils.encoding import force_str
from django.utils.formats import date_format
from django.utils.functional import cached_property
from django.utils.html import escape, linebreaks
from django.utils.module_loading import import_string
from django.utils.safestring import mark_safe
from django.utils.text import slugify
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from django.utils.translation import ngettext, pgettext_lazy

from chrono.apps.export_import.models import WithApplicationMixin
from chrono.apps.journal.models import AuditEntry
from chrono.apps.journal.utils import audit
from chrono.apps.snapshot.models import (
    AgendaSnapshot,
    CategorySnapshot,
    EventsTypeSnapshot,
    ResourceSnapshot,
    UnavailabilityCalendarSnapshot,
    WithSnapshotManager,
    WithSnapshotMixin,
)
from chrono.utils.date import get_weekday_index
from chrono.utils.db import SumCardinality
from chrono.utils.interval import Interval, IntervalSet
from chrono.utils.lingo import get_lingo_service
from chrono.utils.misc import AgendaImportError, ICSError, clean_import_data, generate_slug
from chrono.utils.publik_urls import translate_from_publik_url
from chrono.utils.requests_wrapper import requests as requests_wrapper
from chrono.utils.timezone import is_aware, localtime, make_aware, make_naive, now, utc

AGENDA_KINDS = (
    ('events', _('Events')),
    ('meetings', _('Meetings')),
    ('virtual', _('Virtual')),
)

AGENDA_VIEWS = (
    ('day', _('Day view')),
    ('week', _('Week view')),
    ('month', _('Month view')),
    ('open_events', _('Open events')),
)

ISO_WEEKDAYS = {index + 1: day for index, day in WEEKDAYS.items()}
ISO_WEEKDAYS_PLURAL = {
    1: _('Mondays'),
    2: _('Tuesdays'),
    3: _('Wednesdays'),
    4: _('Thursdays'),
    5: _('Fridays'),
    6: _('Saturdays'),
    7: _('Sundays'),
}

WEEKDAY_CHOICES = [
    (1, _('Mo')),
    (2, _('Tu')),
    (3, _('We')),
    (4, _('Th')),
    (5, _('Fr')),
    (6, _('Sa')),
    (7, _('Su')),
]


def is_midnight(dtime):
    dtime = localtime(dtime)
    return dtime.hour == 0 and dtime.minute == 0


def validate_not_digit(value):
    if value.isdigit():
        raise ValidationError(_('This value cannot be a number.'))


def django_template_validator(value):
    try:
        engines['django'].from_string(value)
    except TemplateSyntaxError as e:
        raise ValidationError(_('syntax error: %s') % e)


def event_template_validator(value):
    example_event = Event(
        start_datetime=now(),
        publication_datetime=now(),
        recurrence_end_date=now().date(),
        places=1,
        duration=1,
    )
    try:
        Template(value).render(Context({'event': example_event}))
    except (VariableDoesNotExist, TemplateSyntaxError) as e:
        raise ValidationError(_('syntax error: %s') % e)


def booking_template_validator(value):
    example_event = Event(
        start_datetime=now(),
        publication_datetime=now(),
        recurrence_end_date=now().date(),
        places=1,
        duration=1,
    )
    example_booking = Booking(event=example_event)
    try:
        Template(value).render(Context({'booking': example_booking}))
    except TemplateSyntaxError as e:
        raise ValidationError(_('syntax error: %s') % e)
    except VariableDoesNotExist:
        pass


class WithInspectMixin:
    def get_inspect_fields(self, keys=None):
        keys = keys or self.get_inspect_keys()
        for key in keys:
            field = self._meta.get_field(key)
            get_value_method = 'get_%s_inspect_value' % key
            get_display_method = 'get_%s_display' % key
            if hasattr(self, get_value_method):
                value = getattr(self, get_value_method)()
            elif hasattr(self, get_display_method):
                value = getattr(self, get_display_method)()
            else:
                value = getattr(self, key)
            if value in [None, '']:
                continue
            if isinstance(value, bool):
                value = yesno(value)
            if isinstance(field, models.TextField):
                value = mark_safe(linebreaks(value))
            yield (field.verbose_name, value)


TimeSlot = collections.namedtuple(
    'TimeSlot', ['start_datetime', 'end_datetime', 'full', 'desk', 'booked_for_external_user']
)


# pylint: disable=too-many-public-methods
class Agenda(WithSnapshotMixin, WithApplicationMixin, WithInspectMixin, models.Model):
    # mark temporarily restored snapshots
    snapshot = models.ForeignKey(
        AgendaSnapshot, on_delete=models.CASCADE, null=True, related_name='temporary_instance'
    )

    label = models.CharField(_('Label'), max_length=150)
    slug = models.SlugField(_('Identifier'), max_length=160, unique=True)
    kind = models.CharField(_('Kind'), max_length=20, choices=AGENDA_KINDS, default='events')
    minimal_booking_delay = models.PositiveIntegerField(
        _('Minimal booking delay (in days)'),
        default=None,
        null=True,
        blank=True,
        validators=[MaxValueValidator(10000)],
    )
    minimal_booking_delay_in_working_days = models.BooleanField(
        _('Minimal booking delay in working days'),
        default=False,
    )
    maximal_booking_delay = models.PositiveIntegerField(
        _('Maximal booking delay (in days)'),
        default=None,
        null=True,
        blank=True,
        validators=[MaxValueValidator(10000)],
    )  # eight weeks
    minimal_cancellation_delay = models.PositiveIntegerField(
        _('Minimal cancellation delay (in days)'),
        default=None,
        null=True,
        blank=True,
        validators=[MaxValueValidator(10000)],
    )
    minimal_cancellation_delay_in_working_days = models.BooleanField(
        _('Minimal cancellation delay in working days'),
        default=False,
    )
    anonymize_delay = models.PositiveIntegerField(
        _('Anonymize delay (in days)'),
        default=None,
        null=True,
        blank=True,
        validators=[MinValueValidator(30), MaxValueValidator(1000)],
        help_text=_('User data will be kept for the specified number of days passed the booking date.'),
    )
    real_agendas = models.ManyToManyField(
        'self',
        related_name='virtual_agendas',
        symmetrical=False,
        through='VirtualMember',
        through_fields=('virtual_agenda', 'real_agenda'),
    )
    admin_role = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        default=None,
        related_name='+',
        verbose_name=_('Admin Role'),
        on_delete=models.SET_NULL,
    )
    edit_role = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        default=None,
        related_name='+',
        verbose_name=_('Edit Role'),
        on_delete=models.SET_NULL,
    )
    view_role = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        default=None,
        related_name='+',
        verbose_name=_('View Role'),
        on_delete=models.SET_NULL,
    )
    resources = models.ManyToManyField('Resource')
    category = models.ForeignKey(
        'Category', verbose_name=_('Category'), blank=True, null=True, on_delete=models.SET_NULL
    )
    default_view = models.CharField(_('Default view'), max_length=20, choices=AGENDA_VIEWS)
    allow_backoffice_booking = models.BooleanField(_('Enable backoffice booking'), default=False)
    booking_form_url = models.CharField(
        _('Booking form URL'), max_length=200, blank=True, validators=[django_template_validator]
    )
    booking_form_slug = models.SlugField(
        _('Booking form identifier'),
        max_length=250,
        blank=True,
    )
    booking_form_slot_field_id = models.SlugField(
        _('Slot field identifier'),
        max_length=200,
        blank=True,
        help_text=_('If field identifier is "form_var_slot", input should be "slot".'),
    )
    booking_form_meeting_type_field_id = models.SlugField(
        _('Meeting type field identifier'),
        max_length=200,
        blank=True,
        help_text=_(
            'If form allows meeting type selection and field identifier is "form_var_meeting_type", input should be "meeting_type".'
        ),
    )
    booking_form_agenda_field_id = models.SlugField(
        _('Agenda form field identifier'),
        max_length=200,
        blank=True,
        help_text=_(
            'If form allows agenda selection and field identifier is "form_var_agenda", input should be "agenda".'
        ),
    )
    desk_simple_management = models.BooleanField(_('Global desk management'), default=False)
    mark_event_checked_auto = models.BooleanField(
        _('Automatically mark event as checked when all bookings have been checked'), default=False
    )
    disable_check_update = models.BooleanField(
        _('Prevent the check of bookings when event was marked as checked'), default=False
    )
    enable_check_for_future_events = models.BooleanField(
        _('Enable the check of bookings when event has not passed'), default=False
    )
    booking_check_filters = models.CharField(
        _('Filters'),
        max_length=250,
        blank=True,
        help_text=_('Comma separated list of keys defined in extra_data.'),
    )
    booking_user_block_template = models.TextField(
        _('User block template'),
        blank=True,
        validators=[django_template_validator],
    )
    booking_extra_user_block_template = models.TextField(
        _('Extra user block template'),
        blank=True,
        validators=[django_template_validator],
        help_text=_('Displayed on check page'),
    )
    event_display_template = models.CharField(
        _('Event display template'),
        max_length=256,
        blank=True,
        validators=[event_template_validator],
        help_text=_(
            'By default event labels will be displayed to users. '
            'This allows for a custom template to include additional informations. '
            'For example, "{{ event.label }} - {{ event.start_datetime }}" will show event datetime after label. '
            'Available variables: event.label (label), event.start_datetime (start date/time), event.places (places), '
            'event.remaining_places (remaining places), event.duration (duration), event.pricing (pricing).'
        ),
    )
    events_type = models.ForeignKey(
        'agendas.EventsType',
        verbose_name=_('Events type'),
        on_delete=models.SET_NULL,
        related_name='agendas',
        null=True,
        blank=True,
    )
    minimal_booking_time = models.TimeField(
        verbose_name=_('Booking opening time'),
        default=datetime.time(0, 0, 0),  # booking is possible starting and finishin at 00:00
        help_text=_('If left empty, available events will be those that are later than the current time.'),
        null=True,
        blank=True,
    )
    partial_bookings = models.BooleanField(default=False)
    invoicing_unit = models.CharField(
        verbose_name=_('Invoicing'),
        max_length=10,
        choices=[
            ('hour', _('Per hour')),
            ('half_hour', _('Per half hour')),
            ('quarter', _('Per quarter-hour')),
            ('minute', _('Per minute')),
        ],
        default='hour',
    )
    invoicing_tolerance = models.PositiveSmallIntegerField(
        verbose_name=_('Tolerance'),
        default=0,
        validators=[MaxValueValidator(59)],
    )
    minimal_booking_duration = models.PositiveSmallIntegerField(
        _('Minimal booking duration (in minutes)'),
        null=True,
        blank=True,
        validators=[MinValueValidator(5)],
    )
    maximal_booking_duration = models.PositiveSmallIntegerField(
        _('Maximal booking duration (in minutes)'),
        null=True,
        blank=True,
        validators=[MinValueValidator(5)],
    )
    minimal_time_between_bookings = models.PositiveSmallIntegerField(
        _('Minimal time between bookings (in minutes)'), default=0
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    application_component_type = 'agendas'
    application_label_singular = _('Agenda')
    application_label_plural = _('Agendas')

    objects = WithSnapshotManager()
    snapshots = WithSnapshotManager(snapshots=True)

    class Meta:
        ordering = ['label']

    def __str__(self):
        return self.label

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        elif self.pk is None:
            self.slug = generate_slug(self, base_slug=self.slug)
        if self.kind != 'virtual':
            if self.minimal_booking_delay is None:
                self.minimal_booking_delay = 1
            if self.maximal_booking_delay is None:
                self.maximal_booking_delay = 8 * 7
        if not self.default_view:
            if self.kind == 'events':
                self.default_view = 'month'
            else:
                self.default_view = 'day'
        super().save(*args, **kwargs)

    @property
    def base_slug(self):
        return slugify(self.label)

    @property
    def partial_bookings_events(self):
        return bool(self.partial_bookings and self.kind == 'events')

    @property
    def partial_bookings_meetings(self):
        return bool(self.partial_bookings and self.kind == 'meetings')

    def get_absolute_url(self):
        return reverse('chrono-manager-agenda-view', kwargs={'pk': self.id})

    def get_settings_url(self):
        return reverse('chrono-manager-agenda-settings', kwargs={'pk': self.id})

    def get_real_kind_display(self):
        if self.partial_bookings:
            if self.kind == 'events':
                return _('Partial bookings (events)')
            else:
                return _('Partial bookings')

        return self.get_kind_display()

    def get_lingo_url(self):
        lingo = get_lingo_service()
        if not lingo:
            return
        lingo_url = lingo.get('url') or ''
        return '%smanage/pricing/agenda/%s/' % (lingo_url, self.slug)

    def can_be_managed(self, user):
        if user.is_staff:
            return True
        group_ids = [x.id for x in user.groups.all()]
        return bool(self.admin_role_id in group_ids)

    def can_be_edited(self, user):
        if self.can_be_managed(user):
            return True
        group_ids = [x.id for x in user.groups.all()]
        return bool(self.edit_role_id in group_ids)

    def can_be_viewed(self, user):
        if self.can_be_edited(user):
            return True
        group_ids = [x.id for x in user.groups.all()]
        return bool(self.view_role_id in group_ids)

    def accept_meetings(self):
        if self.kind == 'virtual':
            return not self.real_agendas.filter(~Q(kind='meetings')).exists()
        return self.kind == 'meetings'

    def allow_booking_from_backoffice(self, date_start, date_end=None):
        date_end = date_end or date_start
        return bool(
            'eservices' in getattr(settings, 'KNOWN_SERVICES', {}).get('wcs', {})
            and self.allow_backoffice_booking
            and self.get_meeting_types_with_slots(date_start, date_end)
        )

    def get_real_agendas(self):
        if self.kind == 'virtual':
            return self.real_agendas.all()
        return [self]

    @cached_property
    def cached_meetingtypes(self):
        return list(self.iter_meetingtypes())

    def iter_meetingtypes(self, excluded_agenda=None, order_by=('slug',)):
        """Expose agenda's meetingtypes.
        straighforward on a real agenda
        On a virtual agenda we expose transient meeting types based on on the
        the real ones shared by every real agendas.
        """
        if self.kind == 'virtual':
            base_qs = MeetingType.objects.filter(agenda__virtual_agendas__in=[self], deleted=False)
            real_agendas = self.real_agendas
            if excluded_agenda:
                base_qs = base_qs.exclude(agenda=excluded_agenda)
                real_agendas = real_agendas.exclude(pk=excluded_agenda.pk)
            queryset = (
                base_qs.values('slug', 'duration', 'label')
                .annotate(total=Count('*'))
                .filter(total=real_agendas.count())
            )
            return [
                MeetingType(duration=mt['duration'], label=mt['label'], slug=mt['slug'], agenda=self)
                for mt in queryset.order_by(*order_by)
            ]

        return self.meetingtype_set.filter(deleted=False).all().order_by('slug')

    def get_meetingtype(self, id_=None, slug=None):
        match = id_ or slug
        assert match, 'an identifier or a slug should be specified'

        if self.kind == 'virtual':
            match = id_ or slug
            meeting_type = None
            for mt in self.cached_meetingtypes:
                if mt.slug == match:
                    meeting_type = mt
                    break
            if meeting_type is None:
                raise MeetingType.DoesNotExist()
            return meeting_type

        if id_:
            return MeetingType.objects.get(id=id_, agenda=self, deleted=False)
        return MeetingType.objects.get(slug=slug, agenda=self, deleted=False)

    def get_virtual_members(self):
        return VirtualMember.objects.filter(virtual_agenda=self)

    def get_max_meeting_duration(self):
        if self.cached_meetingtypes:
            duration = max(x.duration for x in self.cached_meetingtypes)
        else:
            duration = 0
        return datetime.timedelta(minutes=duration)

    def get_base_meeting_duration(self):
        durations = [x.duration for x in self.cached_meetingtypes]
        if not durations:
            raise ValueError()
        gcd = durations[0]
        for duration in durations[1:]:
            gcd = math.gcd(duration, gcd)
        if gcd == 0:
            raise ValueError()
        return gcd

    def get_dependencies(self):
        yield self.view_role
        yield self.admin_role
        yield self.edit_role
        yield self.category
        if self.kind == 'virtual':
            yield from self.real_agendas.all()
        if self.kind == 'meetings':
            yield from self.resources.all()
            for desk in self.desk_set.all():
                yield from desk.get_dependencies()
        if self.kind == 'events':
            yield self.events_type
            yield from self.desk_set.get().get_dependencies()

    def export_json(self):
        agenda = {
            'label': self.label,
            'slug': self.slug,
            'kind': self.kind,
            'category': self.category.slug if self.category else None,
            'minimal_booking_delay': self.minimal_booking_delay,
            'maximal_booking_delay': self.maximal_booking_delay,
            'anonymize_delay': self.anonymize_delay,
            'permissions': {
                'view': self.view_role.name if self.view_role else None,
                'edit': self.edit_role.name if self.edit_role else None,
                'admin': self.admin_role.name if self.admin_role else None,
            },
            'default_view': self.default_view,
            'partial_bookings': self.partial_bookings,
        }
        if hasattr(self, 'reminder_settings'):
            agenda['reminder_settings'] = self.reminder_settings.export_json()
        if self.kind == 'events':
            agenda['booking_form_url'] = self.booking_form_url
            agenda['events'] = [x.export_json() for x in self.event_set.filter(primary_event__isnull=True)]
            if hasattr(self, 'notifications_settings'):
                agenda['notifications_settings'] = self.notifications_settings.export_json()
            agenda['exceptions_desk'] = self.desk_set.get().export_json()
            agenda['minimal_booking_delay_in_working_days'] = self.minimal_booking_delay_in_working_days
            agenda['booking_user_block_template'] = self.booking_user_block_template
            agenda['booking_check_filters'] = self.booking_check_filters
            agenda['event_display_template'] = self.event_display_template
            agenda['mark_event_checked_auto'] = self.mark_event_checked_auto
            agenda['disable_check_update'] = self.disable_check_update
            agenda['enable_check_for_future_events'] = self.enable_check_for_future_events
            agenda['booking_extra_user_block_template'] = self.booking_extra_user_block_template
            agenda['events_type'] = self.events_type.slug if self.events_type else None
            if self.partial_bookings_events:
                agenda['invoicing_tolerance'] = self.invoicing_tolerance
                agenda['invoicing_unit'] = self.invoicing_unit
            else:
                agenda['minimal_cancellation_delay'] = self.minimal_cancellation_delay
                agenda['minimal_cancellation_delay_in_working_days'] = (
                    self.minimal_cancellation_delay_in_working_days
                )
        elif self.kind == 'meetings':
            agenda['meetingtypes'] = [x.export_json() for x in self.meetingtype_set.filter(deleted=False)]
            agenda['desks'] = [desk.export_json() for desk in self.desk_set.all()]
            agenda['desk_simple_management'] = self.desk_simple_management
            agenda['resources'] = [x.slug for x in self.resources.all()]
            agenda['allow_backoffice_booking'] = self.allow_backoffice_booking
            agenda['booking_form_slug'] = self.booking_form_slug
            agenda['booking_form_slot_field_id'] = self.booking_form_slot_field_id
            agenda['booking_form_meeting_type_field_id'] = self.booking_form_meeting_type_field_id
            agenda['booking_form_agenda_field_id'] = self.booking_form_agenda_field_id
            if self.partial_bookings:
                agenda['minimal_booking_duration'] = self.minimal_booking_duration
                agenda['maximal_booking_duration'] = self.maximal_booking_duration
                agenda['minimal_time_between_bookings'] = self.minimal_time_between_bookings
        elif self.kind == 'virtual':
            agenda['excluded_timeperiods'] = [x.export_json() for x in self.excluded_timeperiods.all()]
            agenda['real_agendas'] = [{'slug': x.slug, 'kind': x.kind} for x in self.real_agendas.all()]
        return agenda

    @classmethod
    def import_json(cls, data, overwrite=False, snapshot=None):
        data = copy.deepcopy(data)
        permissions = data.pop('permissions') or {}
        reminder_settings = data.pop('reminder_settings', None)
        if data['kind'] == 'events':
            events = data.pop('events')
            notifications_settings = data.pop('notifications_settings', None)
            exceptions_desk = data.pop('exceptions_desk', None)
        elif data['kind'] == 'meetings':
            meetingtypes = data.pop('meetingtypes')
            desks = data.pop('desks')
        elif data['kind'] == 'virtual':
            excluded_timeperiods = data.pop('excluded_timeperiods')
            real_agendas = data.pop('real_agendas')
        for permission in ('view', 'admin', 'edit'):
            if permissions.get(permission):
                data[permission + '_role'] = Group.objects.get(name=permissions[permission])
        resources_slug = data.pop('resources', [])
        resources_by_slug = {r.slug: r for r in Resource.objects.filter(slug__in=resources_slug)}
        for resource_slug in resources_slug:
            if resource_slug not in resources_by_slug:
                raise AgendaImportError(_('Missing "%s" resource') % resource_slug)
        data = clean_import_data(cls, data)
        desk_simple_management = data.pop('desk_simple_management', None)
        if data.get('category'):
            try:
                data['category'] = Category.objects.get(slug=data['category'])
            except Category.DoesNotExist:
                del data['category']
        if data.get('events_type'):
            try:
                data['events_type'] = EventsType.objects.get(slug=data['events_type'])
            except EventsType.DoesNotExist:
                raise AgendaImportError(_('Missing "%s" events type') % data['events_type'])
        slug = data.pop('slug')
        qs_kwargs = {}
        if snapshot:
            qs_kwargs = {'snapshot': snapshot}  # don't take slug from snapshot: it has to be unique !
            data['slug'] = str(uuid.uuid4())  # random slug
        else:
            qs_kwargs = {'slug': slug}
        agenda, created = cls.objects.update_or_create(defaults=data, **qs_kwargs)
        if overwrite:
            AgendaReminderSettings.objects.filter(agenda=agenda).delete()
        if reminder_settings:
            reminder_settings['agenda'] = agenda
            AgendaReminderSettings.import_json(reminder_settings)
        if data['kind'] == 'events':
            if overwrite:
                Event.objects.filter(agenda=agenda).delete()
                AgendaNotificationsSettings.objects.filter(agenda=agenda).delete()
            for event_data in events:
                event_data['agenda'] = agenda
                Event.import_json(event_data, snapshot=snapshot)
            if notifications_settings:
                notifications_settings['agenda'] = agenda
                AgendaNotificationsSettings.import_json(notifications_settings)
            if exceptions_desk:
                exceptions_desk['agenda'] = agenda
                Desk.import_json(exceptions_desk)
        elif data['kind'] == 'meetings':
            if overwrite:
                MeetingType.objects.filter(agenda=agenda).delete()
                Desk.objects.filter(agenda=agenda).delete()
            for type_data in meetingtypes:
                type_data['agenda'] = agenda
                MeetingType.import_json(type_data)
            for desk in desks:
                desk['agenda'] = agenda
                Desk.import_json(desk)
            agenda.resources.set(resources_by_slug.values())
        elif data['kind'] == 'virtual':
            if overwrite:
                TimePeriod.objects.filter(agenda=agenda).delete()
                VirtualMember.objects.filter(virtual_agenda=agenda).delete()
            for excluded_timeperiod in excluded_timeperiods:
                excluded_timeperiod['agenda'] = agenda
                TimePeriod.import_json(excluded_timeperiod)
            for real_agenda in real_agendas:
                try:
                    real_agenda = Agenda.objects.get(slug=real_agenda['slug'], kind=real_agenda['kind'])
                except Agenda.DoesNotExist:
                    raise AgendaImportError(_('The real agenda "%s" does not exist.') % real_agenda['slug'])
                try:
                    vm, created = VirtualMember.objects.get_or_create(
                        virtual_agenda=agenda, real_agenda=real_agenda
                    )
                    vm.clean()
                except ValidationError as exc:
                    raise AgendaImportError(' '.join(exc.messages))

        if data['kind'] == 'meetings' and desk_simple_management is not None:
            if desk_simple_management is True and not agenda.desk_simple_management:
                if agenda.is_available_for_simple_management():
                    agenda.desk_simple_management = True
                    agenda.save()
            elif desk_simple_management is False and agenda.desk_simple_management:
                agenda.desk_simple_management = False
                agenda.save()

        return created, agenda

    def get_inspect_keys(self):
        keys = ['label', 'slug', 'kind', 'category', 'anonymize_delay', 'default_view']
        if self.kind == 'events':
            keys += ['booking_form_url', 'events_type']
        elif self.kind == 'meetings':
            keys += ['desk_simple_management']
        return keys

    def get_permissions_inspect_fields(self):
        yield from self.get_inspect_fields(keys=['admin_role', 'edit_role', 'view_role'])

    def get_display_inspect_fields(self):
        keys = []
        if self.kind == 'events':
            keys += ['event_display_template']
        keys += [
            'booking_user_block_template',
        ]
        yield from self.get_inspect_fields(keys=keys)

    def get_booking_check_inspect_fields(self):
        keys = [
            'booking_check_filters',
            'mark_event_checked_auto',
            'disable_check_update',
            'enable_check_for_future_events',
            'booking_extra_user_block_template',
        ]
        yield from self.get_inspect_fields(keys=keys)

    def get_invoicing_inspect_fields(self):
        keys = ['invoicing_unit', 'invoicing_tolerance']
        yield from self.get_inspect_fields(keys=keys)

    def get_notifications_inspect_fields(self):
        if hasattr(self, 'notifications_settings'):
            yield from self.notifications_settings.get_inspect_fields()
        return []

    def get_reminder_inspect_fields(self):
        if hasattr(self, 'reminder_settings'):
            yield from self.reminder_settings.get_inspect_fields()
        return []

    def get_booking_delays_inspect_fields(self):
        keys = [
            'minimal_booking_delay',
        ]
        if self.kind == 'events':
            keys += ['minimal_booking_delay_in_working_days']
            if not self.partial_bookings:
                keys += ['minimal_cancellation_delay', 'minimal_cancellation_delay_in_working_days']
        keys += [
            'maximal_booking_delay',
            'minimal_booking_time',
        ]
        yield from self.get_inspect_fields(keys=keys)

    def get_partial_bookings_settings_inspect_fields(self):
        keys = [
            'minimal_booking_duration',
            'maximal_booking_duration',
            'minimal_time_between_bookings',
        ]
        yield (_('Step between bookable times (in minutes)'), self.cached_meetingtypes[0].duration)
        yield from self.get_inspect_fields(keys=keys)

    def get_backoffice_booking_inspect_fields(self):
        keys = [
            'allow_backoffice_booking',
            'booking_form_slug',
            'booking_form_slot_field_id',
            'booking_form_meeting_type_field_id',
            'booking_form_agenda_field_id',
        ]
        yield from self.get_inspect_fields(keys=keys)

    def get_kind_inspect_value(self):
        return self.get_real_kind_display()

    def duplicate(self, label=None, slug=None, include_events=True):
        # clone current agenda
        new_agenda = copy.deepcopy(self)
        new_agenda.pk = None
        new_agenda.label = label or _('Copy of %s') % self.label
        # set slug (reset it to None if not provided)
        new_agenda.slug = slug
        new_agenda.save()

        # clone related objects
        if self.kind == 'meetings':
            for meeting_type in self.meetingtype_set.all():
                meeting_type.duplicate(agenda_target=new_agenda)
            for desk in self.desk_set.all():
                desk.duplicate(agenda_target=new_agenda)
            new_agenda.resources.set(self.resources.all())

        elif self.kind == 'events':
            if include_events:
                for event in self.event_set.filter(recurrence_days__isnull=True, primary_event__isnull=True):
                    event.duplicate(agenda_target=new_agenda)
                for primary_event in self.event_set.filter(recurrence_days__isnull=False):
                    dup_primary_event = primary_event.duplicate(agenda_target=new_agenda)
                    for child_event in self.event_set.filter(primary_event=primary_event):
                        child_event.duplicate(agenda_target=new_agenda, primary_event=dup_primary_event)
            self.desk_set.get().duplicate(agenda_target=new_agenda, reset_slug=False)
            if hasattr(self, 'notifications_settings'):
                self.notifications_settings.duplicate(agenda_target=new_agenda)

        elif self.kind == 'virtual':
            for timeperiod in self.excluded_timeperiods.all():
                timeperiod.duplicate(agenda_target=new_agenda)
            for real_agenda in self.real_agendas.all():
                VirtualMember.objects.create(virtual_agenda=new_agenda, real_agenda=real_agenda)
        if hasattr(self, 'reminder_settings'):
            self.reminder_settings.duplicate(agenda_target=new_agenda)
        return new_agenda

    def get_effective_time_periods(self, min_datetime=None, max_datetime=None, desk=None):
        """Regroup timeperiods by desks.

        List all timeperiods, timeperiods having the same begin_time and
        end_time are regrouped in a SharedTimePeriod object, which has a
        list of desks instead of only one desk.
        """
        min_date = min_datetime.date() if min_datetime else None
        max_date = max_datetime.date() if max_datetime else None
        if self.kind == 'virtual':
            return self.get_effective_time_periods_virtual(min_date, max_date)
        elif self.kind == 'meetings':
            return self.get_effective_time_periods_meetings(min_date, max_date, desk=desk)
        else:
            raise ValueError('does not work with kind %r' % self.kind)

    def get_effective_time_periods_meetings(self, min_date, max_date, desk=None):
        """List timeperiod instances for all desks of the agenda, convert them
        into an Interval of WeekTime which can be compared and regrouped using
        itertools.groupby.
        """
        if desk:
            time_periods = TimePeriod.objects.filter(desk=desk, desk__agenda=self)
        else:
            time_periods = TimePeriod.objects.filter(desk__agenda=self)

        if min_date:
            time_periods.filter(Q(date__isnull=True) | Q(date__gte=min_date))
        if max_date:
            time_periods.filter(Q(date__isnull=True) | Q(date__lte=max_date))

        yield from (
            SharedTimePeriod.from_weektime_interval(
                weektime_interval,
                desks=[time_period.desk for time_period in time_periods],
            )
            for weektime_interval, time_periods in itertools.groupby(
                time_periods.prefetch_related('desk').order_by('weekday', 'start_time', 'end_time'),
                key=TimePeriod.as_weektime_interval,
            )
        )

    def get_effective_time_periods_virtual(self, min_date, max_date):
        """List timeperiod instances for all desks of all real agendas of this
        virtual agenda, convert them into an Interval of WeekTime which can be
        compared and regrouped using itertools.groupby.
        """
        time_periods = TimePeriod.objects.filter(desk__agenda__virtual_agendas=self)
        if min_date:
            time_periods.filter(Q(date__isnull=True) | Q(date__gte=min_date))
        if max_date:
            time_periods.filter(Q(date__isnull=True) | Q(date__lte=max_date))

        closed_hours_by_days = IntervalSet.from_ordered(
            [
                time_period.as_weektime_interval()
                for time_period in self.excluded_timeperiods.order_by('weekday', 'start_time', 'end_time')
            ]
        )
        for time_period_interval, time_periods in itertools.groupby(
            time_periods.order_by('weekday', 'start_time', 'end_time').prefetch_related('desk'),
            key=lambda tp: tp.as_weektime_interval(),
        ):
            time_periods = list(time_periods)
            desks = [time_period.desk for time_period in time_periods]
            if not closed_hours_by_days:
                yield SharedTimePeriod.from_weektime_interval(time_period_interval, desks=desks)
            else:
                date = time_period_interval.begin.date
                weekday_indexes = time_period_interval.begin.weekday_indexes
                for weektime_interval in IntervalSet.simple(*time_period_interval) - closed_hours_by_days:
                    yield SharedTimePeriod.from_weektime_interval(
                        weektime_interval, desks=desks, date=date, weekday_indexes=weekday_indexes
                    )

    @functional.cached_property
    def max_booking_datetime(self):
        if self.maximal_booking_delay is None:
            return None

        # reference is now, in local timezone
        t = localtime(now())

        maximal_booking_delay = self.maximal_booking_delay
        if self.minimal_booking_time is None or t.time() < self.minimal_booking_time:
            maximal_booking_delay -= 1
        t += datetime.timedelta(days=maximal_booking_delay)

        if self.minimal_booking_time:
            t = t.replace(hour=0, minute=0, second=0, microsecond=0)

        # t could not exist, recompute it as an existing datetime by converting to UTC then to localtime
        return localtime(t.astimezone(utc))

    def _min_datetime(self, field, default_field=None):
        value = getattr(self, field)
        if value is None:
            if default_field is not None:
                value = getattr(self, default_field)
            if value is None:
                return None

        # reference is now, in local timezone
        t = localtime(now())

        # add delay
        if settings.WORKING_DAY_CALENDAR is not None and getattr(self, '%s_in_working_days' % field):
            source_class = import_string(settings.WORKING_DAY_CALENDAR)
            calendar = source_class()
            t = calendar.add_working_days(t, value, keep_datetime=True)
        else:
            t += datetime.timedelta(days=value)

        t = t.replace(hour=0, minute=0, second=0, microsecond=0)
        # t could not exist, recompute it as an existing datetime by converting to UTC then to localtime
        return localtime(t.astimezone(utc))

    @functional.cached_property
    def min_booking_datetime(self):
        return self._min_datetime('minimal_booking_delay')

    @functional.cached_property
    def min_cancellation_datetime(self):
        return self._min_datetime('minimal_cancellation_delay', default_field='minimal_booking_delay')

    def _get_events(
        self,
        prefetched_queryset=None,
        min_start=None,
        max_start=None,
        bypass_delays=False,
        show_out_of_minimal_delay=False,
        with_past=True,
    ):
        assert self.kind == 'events'

        if prefetched_queryset:
            entries = self.prefetched_events
            if not with_past:
                # we may have past events in prefetched_events
                entries = [e for e in entries if e.start_datetime >= localtime(now())]
        else:
            # recurring events are never opened
            entries = self.event_set.filter(recurrence_days__isnull=True)
            # exclude canceled events
            entries = entries.filter(cancelled=False)
            if not with_past:
                # exclude past events.
                entries = entries.filter(start_datetime__gte=localtime(now()))
            # exclude events in future, with a publication_date not past
            # this is equivalent to NOT min(start, publication || -infinity) > now
            # or to min(start, publication || -infinity) <= now
            entries = entries.annotate(
                least_dates=Least(
                    F('start_datetime'),
                    Coalesce(
                        F('publication_datetime'),
                        Cast(Value('-infinity'), output_field=models.DateTimeField()),
                    ),
                )
            ).filter(least_dates__lte=now())

        if not bypass_delays and not show_out_of_minimal_delay and self.minimal_booking_delay:
            min_start = max(self.min_booking_datetime, min_start) if min_start else self.min_booking_datetime

        if min_start:
            if prefetched_queryset:
                entries = [e for e in entries if e.start_datetime >= min_start]
            else:
                entries = entries.filter(start_datetime__gte=min_start)

        if not bypass_delays and self.maximal_booking_delay:
            max_start = min(self.max_booking_datetime, max_start) if max_start else self.max_booking_datetime

        if max_start:
            if prefetched_queryset:
                entries = [e for e in entries if e.start_datetime < max_start]
            else:
                entries = entries.filter(start_datetime__lt=max_start)

        return entries

    def get_open_events(
        self,
        prefetched_queryset=False,
        min_start=None,
        max_start=None,
        bypass_delays=False,
        show_out_of_minimal_delay=False,
    ):
        return self._get_events(
            prefetched_queryset=prefetched_queryset,
            min_start=min_start,
            max_start=max_start,
            bypass_delays=bypass_delays,
            show_out_of_minimal_delay=show_out_of_minimal_delay,
            with_past=False,
        )

    def get_past_events(
        self,
        prefetched_queryset=False,
        min_start=None,
        max_start=None,
    ):
        assert self.kind == 'events'

        if prefetched_queryset:
            entries = self.prefetched_events
            # we may have future events
            entries = [e for e in entries if e.start_datetime < localtime(now())]
        else:
            # no recurring events
            entries = self.event_set.filter(recurrence_days__isnull=True)
            # exclude canceled events
            entries = entries.filter(cancelled=False)
            # we want only past events
            entries = entries.filter(start_datetime__lt=localtime(now()))

        if min_start and not prefetched_queryset:
            entries = entries.filter(start_datetime__gte=min_start)

        if max_start and not prefetched_queryset:
            entries = entries.filter(start_datetime__lt=max_start)

        return entries

    def get_all_events(
        self,
        min_start=None,
        max_start=None,
        bypass_delays=False,
        show_out_of_minimal_delay=False,
    ):
        return self._get_events(
            min_start=min_start,
            max_start=max_start,
            bypass_delays=bypass_delays,
            show_out_of_minimal_delay=show_out_of_minimal_delay,
            with_past=True,
        )

    def get_open_recurring_events(self):
        return [
            e
            for e in self.prefetched_recurring_events
            if not e.recurrence_end_date or e.recurrence_end_date > localtime(now()).date()
        ]

    @transaction.atomic
    def update_event_recurrences(self):
        recurring_events = self.event_set.filter(recurrence_days__isnull=False)
        recurrences = self.event_set.filter(primary_event__isnull=False)

        if recurrences.exists():
            self.remove_recurrences_inside_exceptions(recurring_events, recurrences)

        Event.create_events_recurrences(recurring_events)

    def remove_recurrences_inside_exceptions(self, recurring_events, recurrences):
        datetimes = []
        min_start = localtime(now())
        max_start = recurrences.aggregate(dt=Max('start_datetime'))['dt']

        exceptions = self.get_recurrence_exceptions(min_start, max_start)
        for event in recurring_events:
            events = event.get_recurrences(min_start, max_start, exceptions=exceptions)
            datetimes.extend([event.start_datetime for event in events])

        events = recurrences.filter(start_datetime__gt=min_start).exclude(start_datetime__in=datetimes)
        # do not delete events where start_datetime was modified
        events_to_delete = [
            event.pk for event in events if event.datetime_slug == make_naive(event.start_datetime)
        ]

        recurrences.filter(
            Q(booking__isnull=True) | ~Q(booking__cancellation_datetime__isnull=True), pk__in=events_to_delete
        ).delete()

        # report events that weren't deleted because they have bookings
        report, dummy = RecurrenceExceptionsReport.objects.get_or_create(agenda=self)
        report.events.set(recurrences.filter(pk__in=events_to_delete))

    def get_booking_form_url(self):
        if not self.booking_form_url:
            return
        template_vars = Context(settings.TEMPLATE_VARS)
        try:
            return Template(self.booking_form_url).render(template_vars)
        except (VariableDoesNotExist, TemplateSyntaxError):
            return

    def get_booking_check_filters(self):
        if not self.booking_check_filters:
            return []
        return [x.strip() for x in self.booking_check_filters.split(',')]

    def get_booking_user_block_template(self):
        if self.kind == 'events':
            default = '{{ booking.user_name|default:booking.label|default:"%s" }}' % _('Anonymous')
        else:
            default = """{%% if booking.label and booking.user_name %%}
{{ booking.label }} - {{ booking.user_name }}
{%% else %%}
{{ booking.user_name|default:booking.label|default:"%s" }}
{%% endif %%}""" % _(
                'booked'
            )
        return self.booking_user_block_template or default

    def get_recurrence_exceptions(self, min_start, max_start):
        return TimePeriodException.objects.filter(
            Q(desk__slug='_exceptions_holder', desk__agenda=self)
            | Q(
                unavailability_calendar__desks__slug='_exceptions_holder',
                unavailability_calendar__desks__agenda=self,
            ),
            start_datetime__lt=max_start,
            end_datetime__gt=min_start,
        )

    def prefetch_desks_and_exceptions(self, min_date, max_date=None, with_sources=False):
        if self.kind == 'meetings':
            desks = self.desk_set.all()
        elif self.kind == 'virtual':
            desks = (
                Desk.objects.filter(agenda__virtual_agendas=self)
                .select_related('agenda')
                .order_by('agenda', 'label')
            )
        else:
            raise ValueError('does not work with kind %r' % self.kind)

        past_date_time_periods = TimePeriod.objects.filter(desk=OuterRef('pk'), date__lt=min_date)
        desks = desks.annotate(has_past_date_time_periods=Exists(past_date_time_periods))

        time_period_queryset = TimePeriod.objects.filter(Q(date__isnull=True) | Q(date__gte=min_date))
        if max_date:
            time_period_queryset = time_period_queryset.filter(Q(date__isnull=True) | Q(date__lte=max_date))

        self.prefetched_desks = desks.prefetch_related(
            'unavailability_calendars', Prefetch('timeperiod_set', queryset=time_period_queryset)
        )
        if with_sources:
            self.prefetched_desks = self.prefetched_desks.prefetch_related('timeperiodexceptionsource_set')
        unavailability_calendar_ids = UnavailabilityCalendar.objects.filter(
            desks__in=self.prefetched_desks
        ).values('pk')
        all_desks_exceptions = TimePeriodException.objects.filter(
            Q(desk__in=self.prefetched_desks) | Q(unavailability_calendar__in=unavailability_calendar_ids)
        )
        for desk in self.prefetched_desks:
            uc_ids = [uc.pk for uc in desk.unavailability_calendars.all()]
            desk.prefetched_exceptions = [
                e
                for e in all_desks_exceptions
                if e.desk_id == desk.pk or e.unavailability_calendar_id in uc_ids
            ]

    @staticmethod
    def filter_for_guardian(qs, guardian_external_id, child_external_id, min_start=None, max_start=None):
        agendas = SharedCustodyAgenda.objects.filter(child__user_external_id=child_external_id).order_by(
            'date_start'
        )
        if max_start:
            agendas = agendas.filter(date_start__lte=max_start)
        if min_start:
            agendas = agendas.filter(Q(date_end__isnull=True) | Q(date_end__gte=min_start))

        if not agendas:
            return qs

        qs = (
            qs.annotate(week=ExtractWeek('start_datetime'))
            .annotate(week_number=Cast('week', models.IntegerField()))
            .annotate(odd_week=F('week_number') % 2)
        )

        previous_date_end = None
        filtered_qs = Event.objects.none()
        for agenda in agendas:
            filtered_qs |= Agenda.filter_for_custody_agenda(qs, agenda, guardian_external_id)

            if not previous_date_end:
                # first shared custody agenda, include all events before it begins
                filtered_qs |= qs.filter(start_datetime__lt=agenda.date_start)
            else:
                # include all events between agendas
                filtered_qs |= qs.filter(
                    start_datetime__lt=agenda.date_start, start_datetime__date__gt=previous_date_end
                )
            previous_date_end = agenda.date_end

        if previous_date_end:
            # last agenda has end date, include all events after it
            filtered_qs |= qs.filter(start_datetime__gt=previous_date_end)

        return filtered_qs

    @staticmethod
    def filter_for_custody_agenda(qs, agenda, guardian_external_id):
        rules = (
            SharedCustodyRule.objects.filter(
                guardian__user_external_id=guardian_external_id,
                agenda=agenda,
            )
            .annotate(week_day=Func(F('days'), function='unnest', output_field=models.IntegerField()))
            .values('week_day')
        )

        rules_lookup = (
            Q(start_datetime__iso_week_day__in=rules.filter(weeks=''))
            | Q(start_datetime__iso_week_day__in=rules.filter(weeks='even'), odd_week=False)
            | Q(start_datetime__iso_week_day__in=rules.filter(weeks='odd'), odd_week=True)
        )

        all_periods = SharedCustodyPeriod.objects.filter(
            agenda=agenda,
            date_start__lte=OuterRef('start_datetime'),
            date_end__gt=OuterRef('start_datetime'),
        )
        holiday_periods = all_periods.filter(holiday_rule__isnull=False)
        exceptional_periods = all_periods.filter(holiday_rule__isnull=True)
        qs = qs.annotate(
            in_holiday_period=Exists(holiday_periods.filter(guardian__user_external_id=guardian_external_id)),
            in_excluded_holiday_period=Exists(
                holiday_periods.exclude(guardian__user_external_id=guardian_external_id)
            ),
            in_exceptional_period=Exists(
                exceptional_periods.filter(guardian__user_external_id=guardian_external_id)
            ),
            in_excluded_exceptional_period=Exists(
                exceptional_periods.exclude(guardian__user_external_id=guardian_external_id)
            ),
        )

        rules_lookup = (rules_lookup | Q(in_holiday_period=True)) & Q(in_excluded_holiday_period=False)
        qs = qs.filter(
            (rules_lookup | Q(in_exceptional_period=True)) & Q(in_excluded_exceptional_period=False),
            start_datetime__gte=agenda.date_start,
        )
        if agenda.date_end:
            qs = qs.filter(start_datetime__date__lte=agenda.date_end)

        return qs

    @staticmethod
    def prefetch_recurring_events(
        qs,
        with_overlaps=None,
        user_external_id=None,
        start_datetime=None,
        end_datetime=None,
        unnest_recurrence_days=False,
    ):
        recurring_event_queryset = Event.objects.filter(
            Q(publication_datetime__isnull=True) | Q(publication_datetime__lte=now()),
            recurrence_days__isnull=False,
        )

        if unnest_recurrence_days:
            recurring_event_queryset = recurring_event_queryset.unnest_recurrence_days()

        if with_overlaps:
            recurring_event_queryset = Event.annotate_recurring_events_with_overlaps(
                recurring_event_queryset, agendas=qs
            )
            recurring_event_queryset = Event.annotate_recurring_events_with_booking_overlaps(
                recurring_event_queryset, with_overlaps, user_external_id, start_datetime, end_datetime
            )

        qs = qs.prefetch_related(
            Prefetch(
                'event_set',
                queryset=recurring_event_queryset,
                to_attr='prefetched_recurring_events',
            )
        )
        return qs

    @staticmethod
    def prefetch_events(qs, user_external_id=None, guardian_external_id=None, annotate_for_user=True):
        event_queryset = Event.objects.filter(
            Q(publication_datetime__isnull=True) | Q(publication_datetime__lte=now()),
            recurrence_days__isnull=True,
            cancelled=False,
            start_datetime__gte=localtime(now()),
        ).order_by()
        if user_external_id and annotate_for_user:
            event_queryset = Event.annotate_queryset_for_user(event_queryset, user_external_id)
        if guardian_external_id and user_external_id:
            event_queryset = Agenda.filter_for_guardian(
                event_queryset, guardian_external_id, user_external_id
            )
        event_queryset = event_queryset.prefetch_related(
            Prefetch('primary_event', queryset=Event.objects.all().order_by())
        )

        return qs.filter(kind='events').prefetch_related(
            Prefetch(
                'event_set',
                queryset=event_queryset,
                to_attr='prefetched_events',
            ),
        )

    def is_available_for_simple_management(self):
        if self.kind != 'meetings':
            return False

        was_prefetched = False
        if hasattr(self, 'prefetched_desks'):
            desks = self.prefetched_desks
            was_prefetched = True
        else:
            desks = self.desk_set.all()
        if len(desks) < 2:
            # no desk or just one, it's ok
            return True

        desk = desks[0]

        def values_list(obj, qs_name, qs, fields, for_exception=False):
            if not was_prefetched:
                prefetched_qs = getattr(obj, qs).values_list(*fields)
                if for_exception:
                    prefetched_qs = prefetched_qs.filter(source__isnull=True)
                return prefetched_qs
            values = []
            if for_exception:
                prefetched_qs = obj.prefetched_exceptions
            else:
                prefetched_qs = obj._prefetched_objects_cache.get(qs_name)  # XXX django 1.11 compat
                if prefetched_qs is None:
                    prefetched_qs = obj._prefetched_objects_cache.get(qs)
            for inst in prefetched_qs:
                # queryset is prefetched, fake values_list
                if for_exception and inst.source_id is not None:
                    continue
                values.append(tuple(getattr(inst, f) for f in fields))
            return values

        period_fields = ['weekday', 'start_time', 'end_time']
        exception_fields = ['label', 'start_datetime', 'end_datetime']
        source_fields = ['ics_filename', 'ics_url', 'settings_slug', 'enabled']
        desk_time_periods = set(values_list(desk, 'timeperiod', 'timeperiod_set', period_fields))
        desk_exceptions = set(
            values_list(
                desk, 'timeperiodexception', 'timeperiodexception_set', exception_fields, for_exception=True
            )
        )
        desk_sources = set(
            values_list(desk, 'timeperiodexceptionsource', 'timeperiodexceptionsource_set', source_fields)
        )
        desk_unavaibility_calendars = set(
            values_list(desk, 'unavailability_calendars', 'unavailability_calendars', ['pk'])
        )
        for other_desk in desks[1:]:
            # compare time periods
            other_desk_time_periods = set(
                values_list(other_desk, 'timeperiod', 'timeperiod_set', period_fields)
            )
            if desk_time_periods != other_desk_time_periods:
                return False

            # compare exceptions
            other_desk_exceptions = set(
                values_list(
                    other_desk,
                    'timeperiodexception',
                    'timeperiodexception_set',
                    exception_fields,
                    for_exception=True,
                )
            )
            if desk_exceptions != other_desk_exceptions:
                return False

            # compare sources
            other_desk_sources = set(
                values_list(
                    other_desk, 'timeperiodexceptionsource', 'timeperiodexceptionsource_set', source_fields
                )
            )
            if desk_sources != other_desk_sources:
                return False

            # compare unavailability calendars
            other_desk_unavaibility_calendars = set(
                values_list(other_desk, 'unavailability_calendars', 'unavailability_calendars', ['pk'])
            )
            if desk_unavaibility_calendars != other_desk_unavaibility_calendars:
                return False

        return True

    def event_overlaps(self, start_datetime, recurrence_days, recurrence_end_date, instance=None):
        qs = self.event_set.filter(
            # exclude recurrences, check only recurring and normal events
            primary_event__isnull=True
        )
        if hasattr(instance, 'pk'):
            qs = qs.exclude(pk=instance.pk)

        if recurrence_days:
            qs = qs.filter(
                # check overlap with other recurring events
                Q(recurrence_end_date__isnull=True) | Q(recurrence_end_date__gte=start_datetime),
                Q(recurrence_days__overlap=recurrence_days)
                # check overlap with normal events
                | Q(start_datetime__gte=start_datetime, start_datetime__iso_week_day__in=recurrence_days),
            )
            if recurrence_end_date:
                qs = qs.filter(start_datetime__lte=recurrence_end_date)
        else:
            qs = qs.filter(
                # check overlap with other normal events
                Q(start_datetime__date=start_datetime.date())
                # check overlap with recurring events
                | Q(
                    Q(recurrence_end_date__isnull=True) | Q(recurrence_end_date__gte=start_datetime),
                    recurrence_days__contains=[localtime(start_datetime).isoweekday()],
                )
            )

        return qs.exists()

    def get_min_datetime(self, start_datetime=None):
        if self.minimal_booking_delay is None:
            return start_datetime

        if start_datetime is None:
            return self.min_booking_datetime
        return max(self.min_booking_datetime, start_datetime)

    def get_max_datetime(self, end_datetime=None):
        if self.maximal_booking_delay is None:
            return end_datetime

        if end_datetime is None:
            return self.max_booking_datetime
        return min(self.max_booking_datetime, end_datetime)

    def group_agendas_by_opening_period(self, start_datetime, end_datetime, base_min_datetime):
        agenda_ids_by_min_max_datetimes = collections.defaultdict(set)
        agenda_id_min_max_datetime = {}
        base_max_datetime = self.get_max_datetime(end_datetime)

        for agenda in self.get_real_agendas():
            used_min_datetime = base_min_datetime
            if self.minimal_booking_delay is None:
                used_min_datetime = agenda.get_min_datetime(start_datetime)
            used_max_datetime = base_max_datetime
            if self.maximal_booking_delay is None:
                used_max_datetime = agenda.get_max_datetime(end_datetime)
            agenda_ids_by_min_max_datetimes[(used_min_datetime, used_max_datetime)].add(agenda.id)
            agenda_id_min_max_datetime[agenda.id] = (used_min_datetime, used_max_datetime)

        return agenda_ids_by_min_max_datetimes, agenda_id_min_max_datetime

    def get_exceptions_intervals_by_desk(self, desk=None):
        # aggregate time period exceptions by desk as IntervalSet for fast querying
        # 1. sort exceptions by start_datetime
        # 2. group them by desk
        # 3. convert each desk's list of exception to intervals then IntervalSet
        agendas = self.get_real_agendas()

        if desk:
            qs = TimePeriodException.objects.filter(desk=desk, desk__agenda__in=agendas)
        else:
            qs = TimePeriodException.objects.filter(desk__agenda__in=agendas)

        desks_exceptions = {
            time_period_desk: IntervalSet.from_ordered(
                map(TimePeriodException.as_interval, time_period_exceptions)
            )
            for time_period_desk, time_period_exceptions in itertools.groupby(
                qs.select_related('desk').order_by('desk_id', 'start_datetime', 'end_datetime'),
                key=lambda time_period: time_period.desk,
            )
        }

        # add exceptions from unavailability calendar
        time_period_exception_queryset = (
            TimePeriodException.objects.all()
            .select_related('unavailability_calendar')
            .prefetch_related(
                Prefetch(
                    'unavailability_calendar__desks',
                    queryset=Desk.objects.filter(agenda__in=agendas),
                    to_attr='prefetched_desks',
                )
            )
            .filter(unavailability_calendar__desks__agenda__in=agendas)
            .order_by('start_datetime', 'end_datetime')
        )
        for time_period_exception in time_period_exception_queryset:
            # unavailability calendar can be used in all desks;
            # ignore desks outside of current agenda(s)
            for desk in time_period_exception.unavailability_calendar.prefetched_desks:
                if desk not in desks_exceptions:
                    desks_exceptions[desk] = IntervalSet()
                desks_exceptions[desk].add(
                    time_period_exception.start_datetime, time_period_exception.end_datetime
                )

        return desks_exceptions

    def get_booking_intervals(
        self, event_qs, start_datetime, end_datetime, lock_code=None, group_by_desk=False
    ):
        max_meeting_duration_td = self.get_max_meeting_duration()

        if lock_code:
            event_qs = event_qs.exclude(booking__lease__lock_code=lock_code)

        event_qs = event_qs.filter(
            start_datetime__gte=start_datetime - max_meeting_duration_td,
            start_datetime__lte=end_datetime,
            booking__cancellation_datetime__isnull=True,
        )

        def make_interval_set(values):
            return IntervalSet.from_ordered(
                (
                    event['start_datetime']
                    - datetime.timedelta(minutes=event['agenda__minimal_time_between_bookings']),
                    event['end_datetime']
                    + datetime.timedelta(minutes=(event['agenda__minimal_time_between_bookings'])),
                )
                for event in values
            )

        fields = ['start_datetime', 'end_datetime', 'agenda__minimal_time_between_bookings']
        if group_by_desk:
            fields = ['desk_id'] + fields

        # ordering is important for the later groupby, it works like sort | uniq
        event_qs = event_qs.order_by(*fields).values(*fields)

        if not group_by_desk:
            return make_interval_set(event_qs)

        # compute exclusion set by desk from all bookings, using
        # itertools.groupby() to group them by desk_id
        return {
            desk_id: make_interval_set(values)
            for desk_id, values in itertools.groupby(event_qs, lambda be: be['desk_id'])
        }

    def get_opened_time_intervals_by_desk(self, start_datetime, end_datetime):
        # returns a {desk: interval_set} dictionary of opened time

        # first computes opened time intervals based on time periods
        base_opened_time = {}
        for time_period in self.get_effective_time_periods(start_datetime, end_datetime):
            base_interval_set = IntervalSet.from_ordered(
                time_period.get_intervals(start_datetime, end_datetime)
            )
            for desk in time_period.desks:
                if desk in base_opened_time:
                    base_opened_time[desk] += base_interval_set
                else:
                    base_opened_time[desk] = base_interval_set

        # then reduce intervals with exceptions intervals
        opened_time = {}
        desks_exceptions = self.get_exceptions_intervals_by_desk()
        for desk, intervals in base_opened_time.items():
            if not intervals:
                continue
            if desk in desks_exceptions:
                intervals -= desks_exceptions[desk]
            opened_time[desk] = intervals

        return opened_time

    def get_all_slots(
        self,
        meeting_type,
        resources=None,
        unique=False,
        start_datetime=None,
        end_datetime=None,
        user_external_id=None,
        lock_code=None,
        desk=None,
    ):
        """Get all occupation state of all possible slots for the given agenda (of
        its real agendas for a virtual agenda) and the given meeting_type.

        The process is done in four phases:
        - first phase: aggregate time intervals, during which a meeting is impossible
          due to TimePeriodException models, by desk in IntervalSet (compressed
          and ordered list of intervals).
        - second phase: aggregate time intervals by desk for already booked slots, again
          to make IntervalSet,
        - third phase: for a meetings agenda, if resources has to be booked,
          aggregate time intervals for already booked resources, to make IntervalSet.
        - fourth and last phase: generate time slots from each time period based
          on the time period definition and on the desk's respective agenda real
          min/max_datetime; for each time slot check its status in the exclusion
          and bookings sets.
          If it is excluded, ignore it completely.
          It if is booked, report the slot as full.
          If it is booked but match the lock code, report the slot as open.
        """
        resources = resources or []
        # virtual agendas have one constraint :
        # all the real agendas MUST have the same meetingstypes, the consequence is
        # that the base_meeting_duration for the virtual agenda is always the same
        # as the base meeting duration of each real agenda.
        base_meeting_duration = self.get_base_meeting_duration()
        base_min_datetime = self.get_min_datetime(start_datetime)
        base_max_datetime = self.get_max_datetime(end_datetime)

        meeting_duration = meeting_type.duration
        meeting_duration_td = datetime.timedelta(minutes=meeting_duration)

        now_datetime = now()
        base_date = now_datetime.date()
        agendas = self.get_real_agendas()

        agenda_ids_by_min_max_datetimes, agenda_id_min_max_datetime = self.group_agendas_by_opening_period(
            start_datetime, end_datetime, base_min_datetime
        )
        desks_exceptions = self.get_exceptions_intervals_by_desk(desk=desk)

        # compute reduced min/max_datetime windows by desks based on exceptions
        desk_min_max_datetime = {}
        for exception_desk, desk_exception in desks_exceptions.items():
            base = IntervalSet([agenda_id_min_max_datetime[exception_desk.agenda_id]])
            base = base - desk_exception
            if not base:
                # ignore this desk, exceptions cover all opening time
                # use an empty interval (begin == end) for this
                desk_min_max_datetime[exception_desk] = (now_datetime, now_datetime)
                continue
            min_datetime = base.min().replace(hour=0, minute=0, second=0, microsecond=0)
            if base_min_datetime:
                min_datetime = max(min_datetime, base_min_datetime)
            max_datetime = base.max()
            if base_max_datetime:
                max_datetime = min(max_datetime, base_max_datetime)
            desk_min_max_datetime[exception_desk] = (min_datetime, max_datetime)

        # aggregate already booked time intervals by desk
        bookings = {}
        for (used_min_datetime, used_max_datetime), agenda_ids in agenda_ids_by_min_max_datetimes.items():
            events = Event.objects.filter(agenda__in=agenda_ids)
            bookings.update(
                self.get_booking_intervals(
                    events, used_min_datetime, used_max_datetime, lock_code=lock_code, group_by_desk=True
                )
            )

        # aggregate already booked time intervals for resources
        resources_bookings = IntervalSet()
        if self.kind == 'meetings' and resources:
            used_min_datetime, used_max_datetime = agenda_id_min_max_datetime[self.pk]
            event_ids_queryset = Event.resources.through.objects.filter(
                resource__in=[r.pk for r in resources]
            ).values('event')
            events = Event.objects.filter(pk__in=event_ids_queryset)
            resources_bookings = self.get_booking_intervals(
                events, used_min_datetime, used_max_datetime, lock_code=lock_code
            )

        # aggregate already booked time intervals by excluded_user_external_id
        user_bookings = IntervalSet()
        if user_external_id:
            used_min_datetime, used_max_datetime = (
                min(v[0] for v in agenda_id_min_max_datetime.values()),
                max(v[1] for v in agenda_id_min_max_datetime.values()),
            )
            events = Event.objects.filter(agenda__in=agendas, booking__user_external_id=user_external_id)
            user_bookings = self.get_booking_intervals(
                events, used_min_datetime, used_max_datetime, lock_code=lock_code
            )

        unique_booked = {}
        for time_period in self.get_effective_time_periods(base_min_datetime, base_max_datetime, desk=desk):
            duration = (
                datetime.datetime.combine(base_date, time_period.end_time)
                - datetime.datetime.combine(base_date, time_period.start_time)
            ).seconds / 60

            if duration < meeting_type.duration:
                # skip time period that can't even hold a single meeting
                continue

            desks_by_min_max_datetime = collections.defaultdict(list)
            for desk in time_period.desks:
                min_max = desk_min_max_datetime.get(desk, agenda_id_min_max_datetime[desk.agenda_id])
                desks_by_min_max_datetime[min_max].append(desk)

            # aggregate agendas based on their real min/max_datetime :
            # the get_time_slots() result is dependant upon these values, so even
            # if we deduplicated a TimePeriod for some desks, if their respective
            # agendas have different real min/max_datetime we must unduplicate them
            # at time slot generation phase.
            for (used_min_datetime, used_max_datetime), desks in desks_by_min_max_datetime.items():
                for start_datetime in time_period.get_time_slots(
                    min_datetime=used_min_datetime,
                    max_datetime=used_max_datetime,
                    meeting_duration=meeting_duration,
                    base_duration=base_meeting_duration,
                ):
                    end_datetime = start_datetime + meeting_duration_td
                    timestamp = start_datetime.timestamp()

                    # skip generating datetimes if we already know that this
                    # datetime is available
                    if unique and unique_booked.get(timestamp) is False:
                        continue

                    for desk in sorted(desks, key=lambda desk: desk.label):
                        # ignore the slot for this desk, if it overlaps and exclusion period for this desk
                        excluded = desk in desks_exceptions and desks_exceptions[desk].overlaps(
                            start_datetime, end_datetime
                        )
                        if excluded:
                            continue
                        # slot is full if an already booked event overlaps it
                        # check resources first
                        booked = resources_bookings.overlaps(start_datetime, end_datetime)
                        # then check user boookings
                        booked_for_external_user = user_bookings.overlaps(start_datetime, end_datetime)
                        booked = booked or booked_for_external_user
                        # then bookings if resources are free
                        if not booked:
                            booked = desk.id in bookings and bookings[desk.id].overlaps(
                                start_datetime, end_datetime
                            )
                        if unique and unique_booked.get(timestamp) is booked:
                            continue
                        unique_booked[timestamp] = booked
                        yield TimeSlot(
                            start_datetime=start_datetime,
                            end_datetime=end_datetime,
                            desk=desk,
                            full=booked,
                            booked_for_external_user=booked_for_external_user,
                        )
                        if unique and not booked:
                            break

    def get_free_time(
        self,
        start_datetime=None,
        end_datetime=None,
    ):
        """Get open time on this agenda in the future.

        The process is done in three phases:

        1. aggregate exceptions by desk,
        2. aggregate booked events by desks,
        3. for each desk compute the normal opening time based on timeperiods,
        then remove expceptions and booked events, and aggregate all that as
        the result.
        """
        assert self.kind != 'events', 'get_all_slots() does not work on events agendas'

        base_min_datetime = self.get_min_datetime(start_datetime) or now()
        base_max_datetime = self.get_max_datetime(end_datetime)

        agenda_ids_by_min_max_datetimes, agenda_id_min_max_datetime = self.group_agendas_by_opening_period(
            start_datetime, end_datetime, base_min_datetime
        )

        # aggregate already booked time intervals by desk
        bookings = {}
        for (used_min_datetime, used_max_datetime), agenda_ids in agenda_ids_by_min_max_datetimes.items():
            events = Event.objects.filter(agenda__in=agenda_ids)
            bookings.update(
                self.get_booking_intervals(events, used_min_datetime, used_max_datetime, group_by_desk=True)
            )

        free_time_by_desk = self.get_opened_time_intervals_by_desk(base_min_datetime, base_max_datetime)

        # reduce desks' open time by agenda effective min/max datetime
        free_time = IntervalSet()
        for desk, value in free_time_by_desk.items():
            min_max = agenda_id_min_max_datetime[desk.agenda_id]
            desk_free_time = value
            if not desk_free_time:
                continue
            if desk_free_time.min() < min_max[0]:
                desk_free_time -= IntervalSet([(desk_free_time.min(), min_max[0])])
            if min_max[1] < desk_free_time.max():
                desk_free_time -= IntervalSet([(min_max[1], desk_free_time.max())])
            if desk.id in bookings:
                desk_free_time -= bookings[desk.id]
            free_time += desk_free_time
        return free_time

    def async_refresh_booking_computed_times(self):
        if self.kind != 'events' or not self.partial_bookings:
            return

        if 'uwsgi' in sys.modules:
            from chrono.utils.spooler import refresh_booking_computed_times_from_agenda

            tenant = getattr(connection, 'tenant', None)
            transaction.on_commit(
                lambda: refresh_booking_computed_times_from_agenda.spool(
                    agenda_id=str(self.pk), domain=getattr(tenant, 'domain_url', None)
                )
            )
            return

        self.refresh_booking_computed_times()

    def refresh_booking_computed_times(self):
        bookings_queryset = (
            Booking.objects.filter(
                event__agenda__kind='events',
                event__agenda__partial_bookings=True,
                event__agenda=self,
                event__check_locked=False,
                event__invoiced=False,
                event__cancelled=False,
                cancellation_datetime__isnull=True,
            )
            .prefetch_related('user_checks')
            .select_related('event__agenda')
        )
        to_update = []
        for booking in bookings_queryset:
            to_update += booking.refresh_computed_times()
        if to_update:
            BookingCheck.objects.bulk_update(to_update, ['computed_start_time', 'computed_end_time'])

    def get_datetimes_url(self):
        assert self.kind == 'events'
        return reverse('api-agenda-datetimes', kwargs={'agenda_identifier': self.slug})

    def get_fillslot_url(self, event_identifier):
        if self.partial_bookings_meetings:
            return reverse(
                'api-agenda-partial-bookings-meeting-fillslot',
                kwargs={'agenda_identifier': self.slug},
            )

        return reverse(
            'api-fillslot',
            kwargs={'agenda_identifier': self.slug, 'event_identifier': event_identifier},
        )

    def get_meeting_types_with_slots(self, date_start, date_end=None):
        meeting_types = []
        for meeting_type in self.cached_meetingtypes:
            slots = self.get_all_slots(
                meeting_type,
                start_datetime=date_start,
                end_datetime=(date_end or date_start) + datetime.timedelta(days=1),
                unique=True,
            )
            slots = [slot for slot in slots if slot.start_datetime > now() and not slot.full]

            if not slots:
                continue

            meeting_type.slots = slots
            meeting_types.append(meeting_type)

        return meeting_types


class VirtualMember(models.Model):
    """Trough model to link virtual agendas to their real agendas.

    Real agendas linked to a virtual agenda MUST all have the same list of
    MeetingType based on their label, slug and duration. It's enforced by
    VirtualMember.clean() and the realted management views.
    """

    virtual_agenda = models.ForeignKey(Agenda, on_delete=models.CASCADE, related_name='real_members')
    real_agenda = models.ForeignKey(
        Agenda, on_delete=models.CASCADE, related_name='virtual_members', verbose_name='Agenda'
    )

    class Meta:
        unique_together = (('virtual_agenda', 'real_agenda'),)

    def clean(self):
        error_msg = [_('This agenda does not have the same meeting types provided by the virtual agenda.')]
        virtual_meetingtypes = self.virtual_agenda.iter_meetingtypes(excluded_agenda=self.real_agenda)
        if not virtual_meetingtypes:
            return
        virtual_meetingtypes = {(mt.label, mt.slug, mt.duration) for mt in virtual_meetingtypes}
        real_meetingtypes = self.real_agenda.iter_meetingtypes()
        real_meetingtypes = {(mt.label, mt.slug, mt.duration) for mt in real_meetingtypes}
        if virtual_meetingtypes - real_meetingtypes:
            # missing meeting type in real agenda
            for mt in virtual_meetingtypes - real_meetingtypes:
                error_msg += [
                    _(
                        'Meeting type "%(label)s" (%(duration)s minutes) '
                        '(identifier: %(slug)s) does no exist.'
                    )
                    % {'label': mt[0], 'slug': mt[1], 'duration': mt[2]}
                ]
            raise ValidationError(error_msg)
        if real_meetingtypes - virtual_meetingtypes:
            # extra meeting type in real agenda
            for mt in real_meetingtypes - virtual_meetingtypes:
                error_msg += ['Extra meeting type, "%s".' % mt[0]]
            raise ValidationError(error_msg)


WEEKDAYS_LIST = sorted(WEEKDAYS.items(), key=lambda x: x[0])


class WeekTime(collections.namedtuple('WeekTime', ['weekday', 'weekday_indexes', 'date', 'time'])):
    """Representation of a time point in a weekday, ex.: Monday at 5 o'clock."""

    def __new__(cls, weekday, weekday_indexes, date, time):
        if date:
            weekday = date.weekday()
        return super().__new__(cls, weekday, weekday_indexes, date, time)

    def __repr__(self):
        return '%s / %s' % (
            self.date or force_str(WEEKDAYS[self.weekday]),
            date_format(self.time, 'TIME_FORMAT'),
        )

    def keep_only_weekday_and_time(self):
        return WeekTime(weekday=self.weekday, time=self.time, date=None, weekday_indexes=None)


WEEK_CHOICES = [
    (1, _('First of the month')),
    (2, _('Second of the month')),
    (3, _('Third of the month')),
    (4, _('Fourth of the month')),
    (5, _('Fifth of the month')),
]


class TimePeriod(WithInspectMixin, models.Model):
    weekday = models.IntegerField(_('Week day'), choices=WEEKDAYS_LIST, null=True)
    weekday_indexes = ArrayField(
        models.IntegerField(choices=WEEK_CHOICES),
        verbose_name=_('Repeat'),
        blank=True,
        null=True,
    )
    date = models.DateField(_('Date'), null=True)
    start_time = models.TimeField(_('Start'))
    end_time = models.TimeField(_('End'))
    desk = models.ForeignKey('Desk', on_delete=models.CASCADE, null=True)
    agenda = models.ForeignKey(
        Agenda, on_delete=models.CASCADE, null=True, related_name='excluded_timeperiods'
    )

    class Meta:
        ordering = ['weekday', 'date', 'start_time']
        constraints = [
            models.CheckConstraint(
                check=Q(date__isnull=True, weekday__isnull=False)
                | Q(date__isnull=False, weekday__isnull=True),
                name='date_xor_weekday',
            )
        ]

    def __str__(self):
        if self.date:
            label = date_format(self.date, 'l d F Y')
        else:
            label = force_str(WEEKDAYS[self.weekday])
            if self.weekday_indexes:
                label = _('%(weekday)s (%(ordinals)s of the month)') % {
                    'weekday': label,
                    'ordinals': ', '.join(ordinal(i) for i in self.weekday_indexes),
                }

        label = '%s / %s → %s' % (
            label,
            date_format(self.start_time, 'TIME_FORMAT'),
            date_format(self.end_time, 'TIME_FORMAT'),
        )
        return mark_safe(label)

    def save(self, *args, **kwargs):
        if self.agenda:
            assert self.agenda.kind == 'virtual', 'a time period can only reference a virtual agenda'
        super().save(*args, **kwargs)

    @property
    def weekday_str(self):
        return WEEKDAYS[self.weekday]

    @classmethod
    def import_json(cls, data):
        data = clean_import_data(cls, data)
        cls.objects.update_or_create(defaults=data, **data)

    def export_json(self):
        return {
            'weekday': self.weekday,
            'weekday_indexes': self.weekday_indexes,
            'date': self.date.strftime('%Y-%m-%d') if self.date else None,
            'start_time': self.start_time.strftime('%H:%M'),
            'end_time': self.end_time.strftime('%H:%M'),
        }

    def get_inspect_keys(self):
        return [
            'weekday',
            'weekday_indexes',
            'date',
            'start_time',
            'end_time',
        ]

    def duplicate(self, desk_target=None, agenda_target=None):
        # clone current period
        new_period = copy.deepcopy(self)
        new_period.pk = None
        # set desk
        new_period.desk = desk_target or self.desk
        # set agenda
        new_period.agenda = agenda_target or self.agenda
        # store new period
        new_period.save()

        return new_period

    def as_weektime_interval(self):
        return Interval(
            WeekTime(self.weekday, self.weekday_indexes, self.date, self.start_time),
            WeekTime(self.weekday, self.weekday_indexes, self.date, self.end_time),
        )

    def as_shared_timeperiods(self):
        return SharedTimePeriod(
            weekday=self.weekday,
            weekday_indexes=self.weekday_indexes,
            start_time=self.start_time,
            end_time=self.end_time,
            date=self.date,
            desks=[self.desk],
        )


@functools.total_ordering
class SharedTimePeriod:
    """
    Hold common timeperiod for multiple desks.

    To improve performance when generating meetings slots for virtual
    agendas or agendas with many desks, we deduplicate time-periods between
    all desks of all agendas.

    Deduplication is based on a common key, and implemented through __eq__
    and __lt__ which will be used by itertools.groupby().

       (weekday, start_datetime, end_datetime)

    it's done in the deduplicate() classmethod.

    At the level of gel_all_slots() timeperiod are re-duplicated if the
    min_datetime,max_datetime of the desk's agendas differs (see the code
    of get_all_slots() for details).
    """

    __slots__ = ['weekday', 'weekday_indexes', 'start_time', 'end_time', 'date', 'desks']

    def __init__(self, weekday, weekday_indexes, start_time, end_time, date, desks):
        self.weekday = weekday
        self.weekday_indexes = weekday_indexes
        self.start_time = start_time
        self.end_time = end_time
        self.date = date
        self.desks = set(desks)

    def __repr__(self):
        return '%s / %s → %s' % (
            WEEKDAYS[self.weekday],
            date_format(self.start_time, 'TIME_FORMAT'),
            date_format(self.end_time, 'TIME_FORMAT'),
        )

    def __eq__(self, other):
        return (self.weekday, self.start_time, self.end_time, self.date) == (
            other.weekday,
            other.start_time,
            other.end_time,
            other.date,
        )

    def __lt__(self, other):
        return (self.weekday, self.start_time, self.end_time, self.date) < (
            other.weekday,
            other.start_time,
            other.end_time,
            other.date,
        )

    def get_time_slots(self, min_datetime, max_datetime, meeting_duration, base_duration):
        """Generate all possible time slots between min_datetime and max_datime
        of duration meeting_duration minutes and spaced by base_duration
        minutes, i.e.

           compute a list [a,b] -> [c,d] -> ...
           where b-a = meeting_duration and c-a = base_duration.

        We start with the first time following min_datetime and being on
        the same weekday of the current period.

        Then we iterate, advancing by base_duration minutes each time.

        If we cross the end_time of the period or end of the current_day
        (means end_time is midnight), it advance time to self.start_time on
        the next week (same weekday, same start, one week in the future).

        When it crosses end_datetime it stops.

        Generated start_datetime MUST be in the local timezone, and the local
        timezone must not change, as the API needs it to generate stable ids.
        """
        if self.date and not (min_datetime.date() <= self.date <= max_datetime.date()):
            return

        meeting_duration = datetime.timedelta(minutes=meeting_duration)
        duration = datetime.timedelta(minutes=base_duration)

        if not self.date:
            real_min_datetime = min_datetime + datetime.timedelta(days=self.weekday - min_datetime.weekday())
            if real_min_datetime < min_datetime:
                real_min_datetime += datetime.timedelta(days=7)
        else:
            real_min_datetime = make_aware(
                datetime.datetime(day=self.date.day, month=self.date.month, year=self.date.year)
            )

        # make sure datetime in local timezone, it's ABSOLUTELY necessary
        # to have stable event ids in the API.
        real_min_datetime = real_min_datetime.replace(
            hour=12
        )  # so aware datetime will be int the dst of the day
        event_datetime = make_aware(make_naive(real_min_datetime)).replace(
            hour=self.start_time.hour, minute=self.start_time.minute, second=0, microsecond=0
        )

        # don't start before min_datetime
        event_datetime = max(event_datetime, min_datetime)

        # get slots
        slot_end = datetime.datetime.combine(
            event_datetime.date(), self.end_time, tzinfo=event_datetime.tzinfo
        )

        while event_datetime < max_datetime:
            end_time = event_datetime + meeting_duration
            next_time = event_datetime + duration
            if (
                end_time > slot_end
                or event_datetime.date() != next_time.date()
                or (self.weekday_indexes and get_weekday_index(event_datetime) not in self.weekday_indexes)
            ):
                # if time slot is not repeating, end now
                if self.date:
                    break

                # switch to naive time for day/week changes
                event_datetime = make_naive(event_datetime)
                # back to morning
                event_datetime = event_datetime.replace(
                    hour=self.start_time.hour, minute=self.start_time.minute
                )
                # but next week
                event_datetime += datetime.timedelta(days=7)
                slot_end += datetime.timedelta(days=7)

                # and re-align to timezone afterwards
                event_datetime = make_aware(event_datetime)
                continue

            # don't end after max_datetime
            if event_datetime > max_datetime:
                break

            yield event_datetime
            event_datetime = next_time

    @classmethod
    def from_weektime_interval(cls, weektime_interval, desks=(), date=None, weekday_indexes=None):
        begin, end = weektime_interval
        assert begin.weekday == end.weekday

        return cls(
            weekday=begin.weekday,
            weekday_indexes=begin.weekday_indexes or end.weekday_indexes or weekday_indexes,
            start_time=begin.time,
            end_time=end.time,
            date=begin.date or end.date or date,
            desks=desks,
        )

    def get_intervals(self, min_datetime, max_datetime):
        """Generate all possible intervals of time between min_datetime and
        max_datetime, corresponding to the this timeperiod.
        """
        min_datetime = localtime(min_datetime)
        max_datetime = localtime(max_datetime)

        if self.date:
            # if self.date if out of the current period, returns early
            if not (min_datetime.date() <= self.date <= max_datetime.date()):
                return
            start_datetime = make_aware(datetime.datetime.combine(self.date, self.start_time))
        else:
            start_datetime = make_aware(datetime.datetime.combine(min_datetime.date(), self.start_time))
            start_datetime += datetime.timedelta(days=self.weekday - min_datetime.weekday())
            if start_datetime < min_datetime:
                start_datetime += datetime.timedelta(days=7)

        while start_datetime < max_datetime:
            if not self.weekday_indexes or get_weekday_index(start_datetime) in self.weekday_indexes:
                end_datetime = make_aware(datetime.datetime.combine(start_datetime.date(), self.end_time))
                yield (max(start_datetime, min_datetime), min(end_datetime, max_datetime))
            if self.date:
                break
            start_datetime += datetime.timedelta(days=7)


class MeetingType(WithInspectMixin, models.Model):
    agenda = models.ForeignKey(Agenda, on_delete=models.CASCADE)
    label = models.CharField(_('Label'), max_length=150)
    slug = models.SlugField(_('Identifier'), max_length=160)
    duration = models.IntegerField(_('Duration (in minutes)'), default=30, validators=[MinValueValidator(1)])
    deleted = models.BooleanField(_('Deleted'), default=False)

    class Meta:
        ordering = ['duration', 'label']
        unique_together = ['agenda', 'slug']
        verbose_name = _('Meeting type')

    def __str__(self):
        return '%s - %s' % (self._meta.verbose_name, self.label)

    def save(self, *args, **kwargs):
        assert self.agenda.kind != 'virtual', "a meetingtype can't reference a virtual agenda"
        if not self.slug:
            self.slug = generate_slug(self, agenda=self.agenda)
        super().save(*args, **kwargs)

    @property
    def base_slug(self):
        return slugify(self.label)

    @classmethod
    def import_json(cls, data):
        data = clean_import_data(cls, data)
        cls.objects.update_or_create(slug=data['slug'], agenda=data['agenda'], defaults=data)

    def export_json(self):
        return {
            'label': self.label,
            'slug': self.slug,
            'duration': self.duration,
        }

    def get_inspect_keys(self):
        return [
            'label',
            'slug',
            'duration',
        ]

    def duplicate(self, agenda_target=None):
        new_meeting_type = copy.deepcopy(self)
        new_meeting_type.pk = None
        if agenda_target:
            new_meeting_type.agenda = agenda_target
        else:
            new_meeting_type.slug = None
        new_meeting_type.save()

        return new_meeting_type

    def get_datetimes_url(self):
        return reverse(
            'api-agenda-meeting-datetimes',
            kwargs={
                'agenda_identifier': self.agenda.slug,
                'meeting_identifier': self.slug,
            },
        )


class EventQuery(Query):
    def get_compiler(self, using=None, connection=None, elide_empty=True):
        # Copied from django/db/models/sql/query.py
        if using is None and connection is None:
            raise ValueError('Need either using or connection')
        if using:
            connection = connections[using]
        # pylint: disable=too-many-function-args
        return EventCompiler(self, connection, using, elide_empty)


class EventCompiler(SQLCompiler):
    def get_from_clause(self):
        result, params = super().get_from_clause()
        table = self.query.base_table
        result.append(f', LATERAL unnest({table}."recurrence_days") AS "{table}_day"')
        return result, params


class EventDayColumn(Col):
    def __init__(self, db_table=None):
        self.db_table = db_table or Event._meta.db_table
        target = models.IntegerField()
        target.column = '%s_day' % self.db_table
        super().__init__(alias='', target=target)

    def relabeled_clone(self, relabels):
        return EventDayColumn(db_table=relabels.get(self.db_table, self.db_table))


class EventQuerySet(models.QuerySet):
    def unnest_recurrence_days(self):
        self.query.__class__ = EventQuery
        qs = self.annotate(day=EventDayColumn())
        return qs


class Event(WithInspectMixin, models.Model):
    id = models.BigAutoField(primary_key=True)
    INTERVAL_CHOICES = [
        (1, _('Every week')),
        (2, _('Every two weeks')),
        (3, _('Every three weeks')),
    ]

    agenda = models.ForeignKey(Agenda, on_delete=models.CASCADE)
    start_datetime = models.DateTimeField(_('Date/time'), db_index=True)
    end_datetime = models.DateTimeField(db_column='_end_datetime', null=True, blank=True)
    end_time = models.TimeField(_('End time'), null=True)
    recurrence_days = ArrayField(
        models.IntegerField(choices=WEEKDAY_CHOICES),
        verbose_name=_('Recurrence days'),
        blank=True,
        null=True,
    )
    recurrence_week_interval = models.IntegerField(_('Repeat'), choices=INTERVAL_CHOICES, default=1)
    recurrence_end_date = models.DateField(
        _('Recurrence end date'),
        null=True,
        blank=True,
        help_text=_('If left blank, a one-year maximal booking delay will be applied for this event.'),
    )
    primary_event = models.ForeignKey('self', null=True, on_delete=models.CASCADE, related_name='recurrences')
    duration = models.PositiveIntegerField(_('Duration (in minutes)'), default=None, null=True, blank=True)
    publication_datetime = models.DateTimeField(_('Publication date/time'), blank=True, null=True)
    places = models.PositiveIntegerField(_('Places'))
    waiting_list_places = models.PositiveIntegerField(_('Places in waiting list'), default=0)
    label = models.CharField(
        _('Label'),
        max_length=150,
        null=True,
        blank=True,
        help_text=_('Optional label to identify this date.'),
    )
    slug = models.SlugField(_('Identifier'), max_length=160, blank=True, validators=[validate_not_digit])
    description = models.TextField(
        _('Description'), null=True, blank=True, help_text=_('Optional event description.')
    )
    pricing = models.CharField(_('Pricing'), max_length=150, null=True, blank=True)
    url = models.URLField(_('URL'), max_length=200, null=True, blank=True)
    booked_places = models.PositiveSmallIntegerField(default=0)
    booked_waiting_list_places = models.PositiveSmallIntegerField(default=0)
    almost_full = models.BooleanField(default=False)
    full = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    cancellation_scheduled = models.BooleanField(default=False)
    checked = models.BooleanField(default=False)
    check_locked = models.BooleanField(default=False)
    invoiced = models.BooleanField(default=False)
    meeting_type = models.ForeignKey(MeetingType, null=True, on_delete=models.CASCADE)
    desk = models.ForeignKey('Desk', null=True, on_delete=models.CASCADE)
    resources = models.ManyToManyField('Resource')
    custom_fields = models.JSONField(blank=True, default=dict, encoder=DjangoJSONEncoder)

    almost_full_notification_timestamp = models.DateTimeField(null=True, blank=True)
    full_notification_timestamp = models.DateTimeField(null=True, blank=True)
    cancelled_notification_timestamp = models.DateTimeField(null=True, blank=True)

    # Store alternate version of booked_places and booked_waiting_list_places,
    # when an Event queryset is annotated with
    # Event.annotate_queryset_for_lock_code()
    unlocked_booked_places = None
    unlocked_booked_waiting_list_places = None

    objects = models.Manager.from_queryset(EventQuerySet)()

    class Meta:
        ordering = ['agenda', 'start_datetime', 'duration', 'label']
        unique_together = ('agenda', 'slug')
        indexes = [
            models.Index(
                ExtractIsoWeekDay('start_datetime'),
                'start_datetime',
                name='start_datetime_dow_index',
                condition=models.Q(cancelled=False),
            ),
            GistIndex(
                F('agenda'),
                Func(
                    F('start_datetime'),
                    F('duration'),
                    function='hack_immutable_tstzrange',
                ),
                name='hack_immutable_tstzrange_idx',
                condition=models.Q(recurrence_days__isnull=True, cancelled=False),
            ),
            models.Index(
                F('agenda'),
                Least(
                    F('start_datetime'),
                    Coalesce(
                        F('publication_datetime'),
                        Cast(Value('-infinity'), output_field=models.DateTimeField()),
                    ),
                ),
                name='event_least_dates_idx',
                include=['id'],
                condition=models.Q(recurrence_days__isnull=True, cancelled=False),
            ),
        ]

    def __str__(self):
        if self.label:
            return self.label
        return date_format(localtime(self.start_datetime), format='DATETIME_FORMAT')

    def __repr__(self):
        # like __str__ but always show date. usefull when returning api errors
        result = date_format(localtime(self.start_datetime), format='DATETIME_FORMAT')
        if self.label:
            result = f'{self.label} ({result})'
        return result

    def get_journal_label(self):
        date_str = date_format(localtime(self.start_datetime), format='SHORT_DATETIME_FORMAT')
        if self.label:
            return f'{self.label} ({date_str})'
        return date_str

    @functional.cached_property
    def cancellation_status(self):
        if self.cancelled:
            return _('Cancelled')
        if self.cancellation_scheduled:
            return _('Cancellation in progress')

    def save(self, seen_slugs=None, *args, **kwargs):
        assert self.agenda.kind != 'virtual', "an event can't reference a virtual agenda"
        assert not (self.slug and self.slug.isdigit()), 'slug cannot be a number'
        self.start_datetime = self.start_datetime.replace(second=0, microsecond=0)
        self.end_datetime = self.get_end_datetime()
        if not self.slug:
            self.slug = generate_slug(self, seen_slugs=seen_slugs, agenda=self.agenda)
            assert self.slug, 'An Event cannot have an empty slug'
        return super().save(*args, **kwargs)

    @contextmanager
    def update_recurrences(self, changed_data, cleaned_data, protected_fields, exclude_fields):
        with transaction.atomic():
            if any(field for field in changed_data if field in protected_fields):
                self.recurrences.all().delete()
            elif self.recurrence_days:
                update_fields = {
                    field: value for field, value in cleaned_data.items() if field not in exclude_fields
                }
                self.recurrences.update(**update_fields)
            yield

            if self.recurrence_days:
                if self.recurrence_end_date:
                    self.recurrences.filter(start_datetime__gt=self.recurrence_end_date).delete()
                self.create_all_recurrences()

    @property
    def base_slug(self):
        # label can be empty
        return slugify(self.label or ('%s-event' % self.agenda.label))

    def main_list_full(self):
        return bool(self.booked_places >= self.places)

    def set_is_checked(self):
        if not self.agenda.mark_event_checked_auto:
            return
        if self.checked:
            return
        booking_qs = self.booking_set.filter(
            cancellation_datetime__isnull=True,
            in_waiting_list=False,
            user_checks__isnull=True,
            primary_booking__isnull=True,
        )
        if booking_qs.exists():
            return
        self.checked = True
        self.save(update_fields=['checked'])
        self.async_notify_checked()

    def async_notify_checked(self):
        if 'uwsgi' in sys.modules:
            from chrono.utils.spooler import event_notify_checked

            tenant = getattr(connection, 'tenant', None)
            transaction.on_commit(
                lambda: event_notify_checked.spool(
                    event_id=str(self.pk), domain=getattr(tenant, 'domain_url', None)
                )
            )
            return

        self.notify_checked()

    def notify_checked(self):
        partial_bookings = self.agenda.partial_bookings
        for user_check in BookingCheck.objects.filter(booking__event=self).select_related('booking'):
            if user_check.presence is True and user_check.booking.presence_callback_url:
                url = user_check.booking.presence_callback_url
            elif user_check.presence is False and user_check.booking.absence_callback_url:
                url = user_check.booking.absence_callback_url
            else:
                continue
            payload = {
                'user_was_present': user_check.presence,
                'user_check_type_slug': user_check.type_slug,
                'user_check_type_label': user_check.type_label,
            }
            if partial_bookings:
                payload.update(
                    {
                        'start_time': user_check.start_time.isoformat() if user_check.start_time else None,
                        'end_time': user_check.end_time.isoformat() if user_check.end_time else None,
                        'computed_start_time': (
                            user_check.computed_start_time.isoformat()
                            if user_check.computed_start_time
                            else None
                        ),
                        'computed_end_time': (
                            user_check.computed_end_time.isoformat() if user_check.computed_end_time else None
                        ),
                    }
                )
            try:
                response = requests_wrapper.post(url, json=payload, remote_service='auto', timeout=15)
                if response and not response.ok:
                    logging.error(
                        'error (HTTP %s) notifying checked booking (%s)',
                        response.status_code,
                        user_check.booking_id,
                    )
            except requests.Timeout:
                logging.error('error (timeout) notifying checked booking (%s)', user_check.booking_id)
            except Exception as e:  # noqa pylint: disable=broad-except
                logging.error('error (%s) notifying checked booking (%s)', e, user_check.booking_id)

    def async_refresh_booking_computed_times(self):
        if self.agenda.kind != 'events' or not self.agenda.partial_bookings:
            return
        if self.check_locked or self.invoiced or self.cancelled:
            return

        if 'uwsgi' in sys.modules:
            from chrono.utils.spooler import refresh_booking_computed_times_from_event

            tenant = getattr(connection, 'tenant', None)
            transaction.on_commit(
                lambda: refresh_booking_computed_times_from_event.spool(
                    event_id=str(self.pk), domain=getattr(tenant, 'domain_url', None)
                )
            )
            return

        self.refresh_booking_computed_times()

    def refresh_booking_computed_times(self):
        bookings_queryset = Booking.objects.filter(
            event__agenda__kind='events',
            event__agenda__partial_bookings=True,
            event=self,
            event__check_locked=False,
            event__invoiced=False,
            event__cancelled=False,
            cancellation_datetime__isnull=True,
        ).prefetch_related('user_checks')
        to_update = []
        for booking in bookings_queryset:
            booking.event = self  # to avoid lots of querysets
            to_update += booking.refresh_computed_times()
        if to_update:
            BookingCheck.objects.bulk_update(to_update, ['computed_start_time', 'computed_end_time'])

    def in_bookable_period(self, bypass_delays=False):
        if self.publication_datetime and now() < self.publication_datetime:
            return False
        if (
            not bypass_delays
            and self.agenda.maximal_booking_delay
            and self.start_datetime > self.agenda.max_booking_datetime
        ):
            return False
        if self.recurrence_days is not None:
            # bookable recurrences probably exist
            return True
        if not self.agenda.partial_bookings and getattr(self, 'user_places_count', 0):
            if (
                not bypass_delays
                and self.agenda.min_cancellation_datetime
                and self.start_datetime < self.agenda.min_cancellation_datetime
            ):
                # event is already booked, is out of minimal delay for cancellation and we don't want to bypass delays
                return True
        elif (
            not bypass_delays
            and self.agenda.minimal_booking_delay
            and self.start_datetime < self.agenda.min_booking_datetime
        ):
            return False
        if self.start_datetime < now():
            return False
        return True

    def is_day_past(self):
        return localtime(self.start_datetime).date() <= localtime(now()).date()

    @staticmethod
    def annotate_queryset_for_user(
        qs, user_external_id, with_status=False, with_waiting_list=True, extra_data_keys=None
    ):
        if user_external_id is None:
            return qs
        qs = qs.annotate(
            user_bookings=models.FilteredRelation(
                'booking', condition=models.Q(booking__user_external_id=user_external_id)
            ),
            user_places_count=Count(
                'user_bookings',
                filter=Q(
                    user_bookings__cancellation_datetime__isnull=True,
                    user_bookings__in_waiting_list=False,
                ),
            ),
        )
        if with_waiting_list:
            qs = qs.annotate(
                user_waiting_places_count=Count(
                    'user_bookings',
                    filter=Q(
                        user_bookings__cancellation_datetime__isnull=True,
                        user_bookings__in_waiting_list=True,
                    ),
                ),
            )
        if with_status:
            qs = qs.annotate(
                user_absence_count=Count(
                    'user_bookings',
                    filter=Q(
                        user_bookings__cancellation_datetime__isnull=True,
                        user_bookings__user_checks__presence=False,
                    ),
                ),
                user_cancelled_count=Count(
                    'user_bookings',
                    filter=Q(
                        user_bookings__cancellation_datetime__isnull=False,
                    ),
                ),
            )
        if extra_data_keys:
            user_bookings = Booking.objects.filter(
                in_waiting_list=False,
                user_external_id=user_external_id,
                event=OuterRef('pk'),
            ).order_by('creation_datetime')
            qs = qs.annotate(
                **{
                    f'extra_data__{key}': Subquery(
                        # take the first one, if many
                        user_bookings.values(f'extra_data__{key}').order_by('pk')[:1],
                    )
                    for key in extra_data_keys
                },
            )
        return qs

    @staticmethod
    def annotate_queryset_for_lock_code(qs, lock_code):
        qs = qs.annotate(
            unlocked_booked_places=Count(
                'booking',
                filter=Q(
                    booking__cancellation_datetime__isnull=True,
                    booking__in_waiting_list=False,
                )
                & ~Q(booking__lease__lock_code=lock_code),
            ),
            unlocked_booked_waiting_list_places=Count(
                'booking',
                filter=Q(
                    booking__cancellation_datetime__isnull=True,
                    booking__in_waiting_list=True,
                )
                & ~Q(booking__lease__lock_code=lock_code),
            ),
        )
        return qs

    @staticmethod
    def filter_custom_field_overlapping_events(qs):
        lookup = Q(pk__in=[])
        for events_type in EventsType.objects.filter(agendas__in=qs.values('agenda')):
            for field in events_type.get_custom_fields():
                period = field.get('booking_limit_period')
                if not period:
                    continue

                field_name = 'custom_fields__' + field['varname']
                lookups = {
                    field_name: OuterRef(field_name),
                    'agenda__events_type': OuterRef('agenda__events_type'),
                }

                if period != 'any':
                    lookups[period] = OuterRef(period)

                lookup |= Q(**lookups) & ~Q((field_name, None)) & ~Q((field_name, ''))

        return qs.filter(lookup).exclude(pk=OuterRef('pk'))

    @staticmethod
    def annotate_queryset_with_overlaps(qs, other_events=None):
        if other_events is None:
            other_events = qs

        common_annotations = {
            'computed_tzrange': Func(
                F('start_datetime'),
                F('duration'),
                function='hack_immutable_tstzrange',
                output_field=DateTimeRangeField(),
            ),
            'computed_slug': Concat('agenda__slug', Value('@'), 'slug', output_field=models.CharField()),
            'day': Trunc('start_datetime', 'day'),
            'week': Trunc('start_datetime', 'week'),
            'month': Trunc('start_datetime', 'month'),
            'year': Trunc('start_datetime', 'year'),
        }

        qs = qs.annotate(**common_annotations)
        other_events = other_events.annotate(**common_annotations)
        other_events = other_events.select_related()  # remove unecessary joins

        overlapping_events = other_events.filter(
            computed_tzrange__overlap=Func(
                OuterRef('start_datetime'),
                OuterRef('duration'),
                function='hack_immutable_tstzrange',
                output_field=DateTimeRangeField(),
            ),
        ).exclude(pk=OuterRef('pk'))

        overlapping_events |= Event.filter_custom_field_overlapping_events(other_events)

        return qs.annotate(
            overlaps=ArraySubquery(
                overlapping_events.values('computed_slug'),
                output_field=ArrayField(models.CharField()),
            ),
            has_overlap=Exists(overlapping_events),
        )

    @staticmethod
    def filter_custom_field_overlapping_recurring_events(qs, agendas):
        # TODO: add support for Event.recurrence_week_interval
        # For now, value other than 1 is not correctly handled.

        lookup = Q(pk__in=[])
        # remove duplicated values
        eventstype_ids_queryset = EventsType.objects.filter(agendas__in=agendas).values('pk')
        for events_type in EventsType.objects.filter(pk__in=eventstype_ids_queryset):
            for field in events_type.get_custom_fields():
                if not field.get('booking_limit_period'):
                    continue

                field_name = 'custom_fields__' + field['varname']
                lookups = {
                    field_name: OuterRef(field_name),
                    'agenda__events_type': OuterRef('agenda__events_type'),
                }

                if field['booking_limit_period'] == 'day':
                    lookups['day'] = OuterRef('day')

                lookup |= Q(**lookups) & ~Q((field_name, None)) & ~Q((field_name, ''))

        return qs.filter(lookup).exclude(pk=OuterRef('pk'))

    @staticmethod
    def annotate_recurring_events_with_overlaps(qs, agendas=None):
        # TODO: add support for Event.recurrence_week_interval
        # For now, value other than 1 is not correctly handled.

        qs = qs.annotate(
            start_hour=Cast('start_datetime', models.TimeField()),
            computed_end_datetime=ExpressionWrapper(
                F('start_datetime') + datetime.timedelta(minutes=1) * F('duration'),
                output_field=models.DateTimeField(),
            ),
            end_hour=Cast('computed_end_datetime', models.TimeField()),
            computed_slug=Concat(
                'agenda__slug', Value('@'), 'slug', Value(':'), 'day', output_field=models.CharField()
            ),
        )

        events_subquery = qs.annotate(
            outer_recurrence_end_date=ExpressionWrapper(
                OuterRef('recurrence_end_date'), output_field=models.DateField()
            )
        )
        events_subquery = events_subquery.filter(
            Q(recurrence_end_date__isnull=True) | Q(recurrence_end_date__gt=OuterRef('start_datetime')),
            Q(outer_recurrence_end_date__isnull=True) | Q(start_datetime__lt=OuterRef('recurrence_end_date')),
        )

        overlapping_events = events_subquery.filter(
            start_hour__lt=OuterRef('end_hour'),
            end_hour__gt=OuterRef('start_hour'),
            day=OuterRef('day'),
        ).exclude(pk=OuterRef('pk'))

        overlapping_events |= Event.filter_custom_field_overlapping_recurring_events(
            events_subquery, agendas=agendas or qs.values('agenda')
        )

        if agendas:
            overlapping_events = overlapping_events.filter(agenda__in=agendas)

        return qs.annotate(
            overlaps=ArraySubquery(
                overlapping_events.values('computed_slug'),
                output_field=ArrayField(models.CharField()),
            ),
        )

    @staticmethod
    def annotate_recurring_events_with_booking_overlaps(
        qs, agenda_slugs, user_external_id, start_datetime, end_datetime
    ):
        recurrences = Event.objects.annotate(weekday=ExtractIsoWeekDay('start_datetime'))
        recurrences_with_overlaps = Event.annotate_queryset_with_booked_event_overlaps(
            recurrences, agenda_slugs, user_external_id, start_datetime, end_datetime
        ).filter(
            has_overlap=True,
            primary_event=OuterRef('pk'),
            weekday=OuterRef('day'),
        )

        return qs.annotate(
            has_booking_overlaps=Exists(recurrences_with_overlaps),
        )

    @staticmethod
    def annotate_queryset_with_booked_event_overlaps(
        qs, agenda_slugs, user_external_id, start_datetime, end_datetime, exclude_events=None
    ):
        booked_events = Event.objects.filter(
            agenda__slug__in=agenda_slugs,
            start_datetime__gte=start_datetime,
            booking__user_external_id=user_external_id,
            booking__cancellation_datetime__isnull=True,
        )

        if end_datetime:
            booked_events = booked_events.filter(start_datetime__lte=end_datetime)
        if exclude_events:
            booked_events = booked_events.exclude(pk__in=[e.pk for e in exclude_events])

        return Event.annotate_queryset_with_overlaps(qs, booked_events)

    @staticmethod
    def annotate_booking_checks(qs):
        bookings = (
            Booking.objects.filter(
                event=OuterRef('pk'), cancellation_datetime__isnull=True, in_waiting_list=False
            )
            .order_by()
            .values('event')
        )
        present_count = (
            bookings.filter(Q(user_checks__presence=True) | Q(primary_booking__user_checks__presence=True))
            .annotate(count=Count('event'))
            .values('count')
        )
        absent_count = (
            bookings.filter(Q(user_checks__presence=False) | Q(primary_booking__user_checks__presence=False))
            .annotate(count=Count('event'))
            .values('count')
        )
        notchecked_count = (
            bookings.filter(user_checks__isnull=True, primary_booking__user_checks__presence__isnull=True)
            .annotate(count=Count('event'))
            .values('count')
        )
        return qs.annotate(
            present_count=Coalesce(Subquery(present_count, output_field=IntegerField()), Value(0)),
            absent_count=Coalesce(Subquery(absent_count, output_field=IntegerField()), Value(0)),
            notchecked_count=Coalesce(Subquery(notchecked_count, output_field=IntegerField()), Value(0)),
        )

    def get_booked_places(self):
        if self.unlocked_booked_places is None:
            return self.booked_places
        else:
            return self.unlocked_booked_places

    def get_booked_waiting_list_places(self):
        if self.unlocked_booked_waiting_list_places is None:
            return self.booked_waiting_list_places
        else:
            return self.unlocked_booked_waiting_list_places

    def get_full(self):
        if self.agenda.partial_bookings:
            return False
        elif self.unlocked_booked_places is None:
            return self.full
        else:
            if self.waiting_list_places == 0:
                return self.get_booked_places() >= self.places
            else:
                return self.get_booked_waiting_list_places() >= self.waiting_list_places

    @property
    def remaining_places(self):
        return max(0, self.places - self.get_booked_places())

    @property
    def remaining_waiting_list_places(self):
        return max(0, self.waiting_list_places - self.get_booked_waiting_list_places())

    def get_end_datetime(self):
        if self.end_time:
            return localtime(self.start_datetime).replace(
                hour=self.end_time.hour, minute=self.end_time.minute
            )

        if self.meeting_type_id:
            minutes = self.meeting_type.duration
        else:
            minutes = self.duration
        if minutes is None:
            return self.end_datetime
        return self.start_datetime + datetime.timedelta(minutes=minutes)

    def get_absolute_url(self):
        return reverse('chrono-manager-event-edit', kwargs={'pk': self.agenda.id, 'event_pk': self.id})

    def get_absolute_view_url(self):
        return reverse('chrono-manager-event-view', kwargs={'pk': self.agenda.id, 'event_pk': self.id})

    def get_booking_form_url(self):
        if not self.agenda.booking_form_url:
            return
        template_vars = Context(settings.TEMPLATE_VARS)
        try:
            url = Template(self.agenda.booking_form_url).render(template_vars)
            url += '&' if '?' in url else '?'
            url += 'agenda=%s&event=%s' % (self.agenda.slug, self.slug)
            return mark_safe(url)
        except (VariableDoesNotExist, TemplateSyntaxError):
            return

    @classmethod
    def import_json(cls, data, snapshot=None):
        try:
            data['start_datetime'] = make_aware(
                datetime.datetime.strptime(data['start_datetime'], '%Y-%m-%d %H:%M:%S')
            )
        except ValueError:
            raise AgendaImportError(_('Bad datetime format "%s"') % data['start_datetime'])

        if data.get('end_time'):
            try:
                data['end_time'] = datetime.datetime.strptime(data['end_time'], '%H:%M').time()
            except ValueError:
                raise AgendaImportError(_('Bad time format "%s"') % data['end_time'])

        if data.get('recurrence_days'):
            # keep stable weekday numbering after switch to ISO in db
            data['recurrence_days'] = [i + 1 for i in data['recurrence_days']]

        data = clean_import_data(cls, data)
        if data.get('slug'):
            event, dummy = cls.objects.update_or_create(
                agenda=data['agenda'], slug=data['slug'], defaults=data
            )
        else:
            event = cls(**data)
            event.save()
        if snapshot is None and event.recurrence_days:
            event.refresh_from_db()
            if event.recurrence_end_date:
                event.recurrences.filter(start_datetime__gt=event.recurrence_end_date).delete()
            update_fields = {
                field: getattr(event, field)
                for field in [
                    'end_time',
                    'label',
                    'duration',
                    'publication_datetime',
                    'places',
                    'waiting_list_places',
                    'description',
                    'pricing',
                    'url',
                    'custom_fields',
                ]
            }
            event.recurrences.update(**update_fields)
            event.create_all_recurrences()

    def export_json(self):
        recurrence_end_date = (
            self.recurrence_end_date.strftime('%Y-%m-%d') if self.recurrence_end_date else None
        )
        return {
            'start_datetime': make_naive(self.start_datetime).strftime('%Y-%m-%d %H:%M:%S'),
            'end_time': self.end_time.strftime('%H:%M') if self.end_time else None,
            'publication_datetime': (
                make_naive(self.publication_datetime).strftime('%Y-%m-%d %H:%M:%S')
                if self.publication_datetime
                else None
            ),
            'recurrence_days': (
                [
                    # keep stable weekday numbering after switch to ISO in db
                    i - 1
                    for i in self.recurrence_days
                ]
                if self.recurrence_days
                else None
            ),
            'recurrence_week_interval': self.recurrence_week_interval,
            'recurrence_end_date': recurrence_end_date,
            'places': self.places,
            'waiting_list_places': self.waiting_list_places,
            'label': self.label,
            'slug': self.slug,
            'description': self.description,
            'url': self.url,
            'pricing': self.pricing,
            'duration': self.duration,
            'custom_fields': self.get_custom_fields(),
        }

    def get_inspect_keys(self):
        return [
            'label',
            'slug',
            'description',
            'start_datetime',
            'duration',
            'recurrence_days',
            'recurrence_week_interval',
            'recurrence_end_date',
            'publication_datetime',
            'places',
            'waiting_list_places',
            'url',
            'pricing',
        ]

    def duplicate(self, agenda_target=None, primary_event=None, label=None, start_datetime=None):
        new_event = copy.deepcopy(self)
        new_event.pk = None
        if label:
            new_event.label = label
        if start_datetime:
            new_event.start_datetime = start_datetime
        if agenda_target:
            new_event.agenda = agenda_target
        else:
            new_event.slug = None
        if primary_event:
            new_event.primary_event = primary_event
        new_event.checked = False
        new_event.check_locked = False
        new_event.invoiced = False
        new_event.save()

        return new_event

    def cancel(self, cancel_bookings=True):
        bookings_to_cancel = self.booking_set.filter(cancellation_datetime__isnull=True).all()
        if cancel_bookings and bookings_to_cancel.exclude(cancel_callback_url='').exists():
            # booking cancellation needs network calls, schedule it for later
            self.cancellation_scheduled = True
            self.save()
        else:
            with transaction.atomic():
                for booking in bookings_to_cancel:
                    booking.cancel()
                self.cancelled = True
                self.save()

    def get_recurrences(self, min_datetime, max_datetime, exceptions=None):
        recurrences = []
        rrule_set = rruleset()

        if exceptions is None:
            exceptions = self.agenda.get_recurrence_exceptions(min_datetime, max_datetime)
        for exception in exceptions:
            exception_start = localtime(exception.start_datetime)
            event_start = localtime(self.start_datetime)
            if event_start.time() < exception_start.time():
                exception_start += datetime.timedelta(days=1)
            exception_start = exception_start.replace(
                hour=event_start.hour, minute=event_start.minute, second=0, microsecond=0
            )
            rrule_set.exrule(
                rrule(
                    freq=DAILY,
                    dtstart=make_naive(exception_start),
                    until=make_naive(exception.end_datetime),
                )
            )

        event_base = Event(
            agenda=self.agenda,
            primary_event=self,
            slug=self.slug,
            end_time=self.end_time,
            duration=self.duration,
            places=self.places,
            waiting_list_places=self.waiting_list_places,
            publication_datetime=self.publication_datetime,
            label=self.label,
            description=self.description,
            pricing=self.pricing,
            url=self.url,
            custom_fields=self.custom_fields,
        )

        # remove pytz info because dateutil doesn't support DST changes
        min_datetime = make_naive(min_datetime)
        max_datetime = make_naive(max_datetime)
        rrule_set.rrule(rrule(dtstart=make_naive(self.start_datetime), **self.recurrence_rule))

        for start_datetime in rrule_set.between(min_datetime, max_datetime, inc=True):
            event = copy.copy(event_base)
            # add timezone back
            aware_start_datetime = make_aware(start_datetime)
            event.slug = '%s--%s' % (
                event.slug,
                aware_start_datetime.strftime('%Y-%m-%d-%H%M'),
            )
            event.start_datetime = aware_start_datetime.astimezone(utc)
            event.end_datetime = event.get_end_datetime()
            recurrences.append(event)

        return recurrences

    def get_recurrence_display(self):
        time = date_format(localtime(self.start_datetime), 'TIME_FORMAT')

        days_count = len(self.recurrence_days)
        if days_count == 7:
            repeat = _('Daily')
        elif days_count > 1 and (self.recurrence_days[-1] - self.recurrence_days[0]) == days_count - 1:
            # days are contiguous
            repeat = _('From %(weekday)s to %(last_weekday)s') % {
                'weekday': str(ISO_WEEKDAYS[self.recurrence_days[0]]),
                'last_weekday': str(ISO_WEEKDAYS[self.recurrence_days[-1]]),
            }
        else:
            repeat = _('On %(weekdays)s') % {
                'weekdays': ', '.join([str(ISO_WEEKDAYS_PLURAL[i]) for i in self.recurrence_days])
            }

        recurrence_display = _('%(On_day_x)s at %(time)s') % {'On_day_x': repeat, 'time': time}

        if self.recurrence_week_interval > 1:
            if self.recurrence_week_interval == 2:
                every_x_weeks = _('every two weeks')
            elif self.recurrence_week_interval == 3:
                every_x_weeks = _('every three weeks')
            recurrence_display = _('%(Every_x_days)s, once %(every_x_weeks)s') % {
                'Every_x_days': recurrence_display,
                'every_x_weeks': every_x_weeks,
            }

        if self.start_datetime > now():
            start_date = date_format(self.start_datetime, 'DATE_FORMAT')
            recurrence_display = _('%(Every_x_days)s, from %(date)s') % {
                'Every_x_days': recurrence_display,
                'date': start_date,
            }

        if self.recurrence_end_date:
            end_date = date_format(self.recurrence_end_date, 'DATE_FORMAT')
            recurrence_display = _('%(Every_x_days)s, until %(date)s') % {
                'Every_x_days': recurrence_display,
                'date': end_date,
            }
        return recurrence_display

    @property
    def recurrence_rule(self):
        recurrence_rule = {
            'freq': WEEKLY,
            'byweekday': [i - 1 for i in self.recurrence_days or []],
            'interval': self.recurrence_week_interval,
        }
        if self.recurrence_end_date:
            recurrence_rule['until'] = datetime.datetime.combine(
                self.recurrence_end_date, datetime.time(0, 0)
            )
        else:
            recurrence_rule['until'] = make_naive(now() + datetime.timedelta(days=365))
        return recurrence_rule

    def has_recurrences_booked(self, after=None):
        return Booking.objects.filter(
            event__primary_event=self,
            event__start_datetime__gt=after or now(),
            cancellation_datetime__isnull=True,
        ).exists()

    def create_all_recurrences(self):
        Event.create_events_recurrences([self])

    @classmethod
    def create_events_recurrences(cls, events):
        for event in events:
            if event.recurrence_end_date:
                max_datetime = datetime.datetime.combine(event.recurrence_end_date, datetime.time(0, 0))
            else:
                max_datetime = make_naive(now() + datetime.timedelta(days=365))
            existing_recurrences = event.recurrences.values_list('slug', flat=True)
            all_recurrences = event.get_recurrences(localtime(event.start_datetime), make_aware(max_datetime))
            recurrences_to_create = [r for r in all_recurrences if r.slug not in existing_recurrences]
            if recurrences_to_create:
                Event.objects.bulk_create(recurrences_to_create, ignore_conflicts=True)

    @property
    def datetime_slug(self):
        assert self.primary_event_id is not None, 'only for event recurrence'

        datetime_part = self.slug.rsplit('--')[-1]
        return datetime.datetime.strptime(datetime_part, '%Y-%m-%d-%H%M')

    def get_custom_fields(self):
        if not self.agenda.events_type:
            return {}
        custom_fields = {}
        for custom_field in self.agenda.events_type.get_custom_fields():
            custom_fields[custom_field['varname']] = self.custom_fields.get(custom_field['varname'])
        return custom_fields


class EventsType(WithSnapshotMixin, WithApplicationMixin, WithInspectMixin, models.Model):
    # mark temporarily restored snapshots
    snapshot = models.ForeignKey(
        EventsTypeSnapshot, on_delete=models.CASCADE, null=True, related_name='temporary_instance'
    )

    slug = models.SlugField(_('Identifier'), max_length=160, unique=True)
    label = models.CharField(_('Label'), max_length=150)
    custom_fields = models.JSONField(blank=True, default=list)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    application_component_type = 'events_types'
    application_label_singular = _('Events type')
    application_label_plural = _('Events types')

    objects = WithSnapshotManager()
    snapshots = WithSnapshotManager(snapshots=True)

    def __str__(self):
        return self.label

    class Meta:
        ordering = ['label']

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)

    @property
    def base_slug(self):
        return slugify(self.label)

    def get_custom_fields(self):
        custom_fields = []
        if not isinstance(self.custom_fields, list):
            return custom_fields
        for values in self.custom_fields:
            if not isinstance(values, dict):
                continue
            complete = True
            for k in ['varname', 'label', 'field_type']:
                if not values.get(k):
                    complete = False
                    break
            if complete:
                custom_fields.append(values)
        return custom_fields

    def get_custom_fields_for_display(self):
        custom_fields = self.get_custom_fields()
        if not isinstance(self.custom_fields, list):
            return custom_fields
        result = []
        for values in self.custom_fields:
            if not isinstance(values, dict):
                continue
            field_types = {
                'text': _('Text'),
                'textarea': _('Textarea'),
                'bool': _('Boolean'),
                'date': _('Date'),
            }
            limits = {
                'any': _('Yes'),
                'day': _('By day'),
                'week': _('By week'),
                'month': _('By month'),
                'year': _('By year'),
            }
            values['field_type_display'] = field_types.get(values['field_type'], values['field_type'])
            values['booking_limit_period_display'] = limits.get(
                values.get('booking_limit_period'), values.get('booking_limit_period')
            )
            result.append(values)
        return result

    def get_dependencies(self):
        return []

    @classmethod
    def import_json(cls, data, overwrite=False, snapshot=None):
        data = clean_import_data(cls, data)
        slug = data.pop('slug')
        qs_kwargs = {}
        if snapshot:
            qs_kwargs = {'snapshot': snapshot}  # don't take slug from snapshot: it has to be unique !
            data['slug'] = str(uuid.uuid4())  # random slug
        else:
            qs_kwargs = {'slug': slug}
        events_type, created = cls.objects.update_or_create(defaults=data, **qs_kwargs)
        return created, events_type

    def export_json(self):
        return {
            'slug': self.slug,
            'label': self.label,
            'custom_fields': self.custom_fields,
        }

    def get_inspect_keys(self):
        return ['label', 'slug']


class BookingColor(models.Model):
    COLOR_COUNT = 8

    label = models.CharField(_('Label'), max_length=250)
    index = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = ('label',)
        ordering = ('pk',)

    def save(self, *args, **kwargs):
        if self.index is None:
            last_color = BookingColor.objects.last() or BookingColor(index=-1)
            self.index = (last_color.index + 1) % self.COLOR_COUNT
        super().save(*args, **kwargs)

    def __str__(self):
        return '%s' % self.label


class Booking(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    extra_data = models.JSONField(null=True)
    anonymization_datetime = models.DateTimeField(null=True)
    cancellation_datetime = models.DateTimeField(null=True)
    email_reminder_datetime = models.DateTimeField(null=True)
    sms_reminder_datetime = models.DateTimeField(null=True)
    in_waiting_list = models.BooleanField(default=False)
    creation_datetime = models.DateTimeField(auto_now_add=True)
    # primary booking is used to group multiple bookings together
    primary_booking = models.ForeignKey(
        'self', null=True, on_delete=models.CASCADE, related_name='secondary_booking_set'
    )
    from_recurring_fillslots = models.BooleanField(default=False)

    label = models.CharField(max_length=250, blank=True)
    user_display_label = models.CharField(
        verbose_name=_('Label displayed to user'), max_length=250, blank=True
    )
    user_external_id = models.CharField(max_length=250, blank=True)
    user_last_name = models.CharField(max_length=250, blank=True)
    user_first_name = models.CharField(max_length=250, blank=True)
    user_email = models.EmailField(blank=True)
    user_phone_number = models.CharField(max_length=30, blank=True)
    out_of_min_delay = models.BooleanField(default=False)

    extra_emails = ArrayField(models.EmailField(), default=list)
    extra_phone_numbers = ArrayField(models.CharField(max_length=16), default=list)

    form_url = models.URLField(blank=True, max_length=500)
    backoffice_url = models.URLField(blank=True, max_length=500)
    cancel_callback_url = models.URLField(blank=True, max_length=500)
    presence_callback_url = models.URLField(blank=True, max_length=500)
    absence_callback_url = models.URLField(blank=True, max_length=500)
    color = models.ForeignKey(BookingColor, null=True, on_delete=models.SET_NULL, related_name='bookings')
    sms_counter = models.CharField(max_length=250, blank=True)

    request_uuid = models.UUIDField(editable=False, null=True)
    previous_state = models.CharField(max_length=10, null=True)

    start_time = models.TimeField(null=True)
    end_time = models.TimeField(null=True)

    class Meta:
        indexes = [
            models.Index(fields=['event', 'user_external_id']),
            models.Index(fields=['user_external_id', 'event']),
        ]

    def get_journal_label(self):
        parts = [_('ID: %s') % self.id]
        if self.user_name:
            parts.append(_('user: %s') % self.user_name)
        if self.in_waiting_list:
            parts.append(_('in waiting list'))
        if self.cancellation_datetime:
            parts.append(
                _('cancelled at %s')
                % date_format(localtime(self.cancellation_datetime), format='SHORT_DATETIME_FORMAT')
            )
        if self.start_time and self.end_time:
            parts.append(
                '%s → %s'
                % (
                    date_format(self.start_time, 'TIME_FORMAT'),
                    date_format(self.end_time, 'TIME_FORMAT'),
                )
            )
        elif self.start_time:
            parts.append('%s → ?' % date_format(self.start_time, 'TIME_FORMAT'))
        elif self.end_time:
            parts.append('? → %s' % date_format(self.end_time, 'TIME_FORMAT'))
        return ' / '.join([str(x) for x in parts])

    @property
    def user_name(self):
        return ('%s %s' % (self.user_first_name or '', self.user_last_name or '')).strip()

    @cached_property
    def user_check(self):  # pylint: disable=method-hidden
        user_checks = self.user_checks if not self.primary_booking else self.primary_booking.user_checks
        user_checks = list(user_checks.all())

        if len(user_checks) > 1:
            raise AttributeError('booking has multiple checks')

        return user_checks[0] if user_checks else None

    @cached_property
    def emails(self):
        emails = set(self.extra_emails)
        if self.user_email:
            emails.add(self.user_email)
        return list(emails)

    @cached_property
    def phone_numbers(self):
        phone_numbers = set(self.extra_phone_numbers)
        if self.user_phone_number:
            phone_numbers.add(self.user_phone_number)
        return list(phone_numbers)

    def refresh_from_db(self, *args, **kwargs):
        if hasattr(self, 'user_check'):
            del self.user_check
        return super().refresh_from_db(*args, **kwargs)

    def cancel(self, trigger_callback=False, request=None):
        timestamp = now()
        with transaction.atomic():
            audit(
                'booking:cancel',
                request=request,
                agenda=self.event.agenda,
                extra_data={'booking': self},
            )
            self.secondary_booking_set.update(cancellation_datetime=timestamp)
            self.cancellation_datetime = timestamp
            self.save()
            if self.cancel_callback_url and trigger_callback:
                r = requests_wrapper.post(self.cancel_callback_url, remote_service='auto', timeout=15)
                r.raise_for_status()

    def accept(self):
        self.in_waiting_list = False
        with transaction.atomic():
            self.secondary_booking_set.update(in_waiting_list=False)
            self.save()

    def suspend(self):
        self.in_waiting_list = True
        with transaction.atomic():
            self.secondary_booking_set.update(in_waiting_list=True)
            self.save()

    @staticmethod
    def prefetch_audit_entries(agenda, queryset):
        audit_entries_qs = AuditEntry.objects.filter(
            agenda=agenda,
            extra_data__booking_id__in=[b.pk for b in queryset],
        ).order_by('-pk')
        audit_entries = collections.defaultdict(list)
        for ae in audit_entries_qs:
            audit_entries[ae.extra_data['booking_id']].append(ae)
        for booking in queryset:
            booking.prefetched_audit_entries = audit_entries[booking.pk]
        return queryset

    @property
    def state_before_check(self):
        if hasattr(self, 'prefetched_audit_entries'):
            audit_entries = self.prefetched_audit_entries
        else:
            audit_entries = AuditEntry.objects.filter(
                agenda_id=self.event.agenda_id, extra_data__booking_id=self.pk
            ).order_by('-pk')
        previous_actions = [ae.message_type for ae in audit_entries]
        for previous_action in previous_actions:
            if previous_action == 'booking:create-from-subscription':
                # booking was created from subsription by an agent on check page
                return 'subscribed'
            elif previous_action == 'booking:cancel':
                # booking was cancelled by the user
                return 'cancelled'
            elif previous_action == 'booking:create':
                # booking was created and not cancelled
                return 'booked'
            else:
                continue
        return 'booked'

    def reset_user_was_present(self, request=None):
        state_before_check = self.state_before_check
        with transaction.atomic():
            audit(
                'check:reset',
                request=request,
                agenda=self.event.agenda,
                extra_data={
                    'booking': self,
                },
            )
            if self.user_check:
                self.user_check.delete()
                self.user_check = None
            if state_before_check == 'subscribed':
                # delete booking
                self.delete()
                self.event.set_is_checked()
                return None
            elif state_before_check == 'cancelled':
                # reset cancellation_datetime
                self.cancellation_datetime = now()
                self.save(update_fields=['cancellation_datetime'])
            self.event.set_is_checked()
        return self

    def set_user_presence(self, presence=True, request=None, **defaults):
        audit_action = 'check:presence' if presence else 'check:absence'
        defaults.update(presence=presence)
        self.cancellation_datetime = None
        with transaction.atomic():
            audit(
                audit_action,
                request=request,
                agenda=self.event.agenda,
                extra_data={
                    'booking': self,
                },
            )
            self.user_check, dummy = BookingCheck.objects.update_or_create(booking=self, defaults=defaults)
            self.secondary_booking_set.update(cancellation_datetime=None)
            self.save()
            self.event.set_is_checked()

    def mark_user_absence(
        self, check_type_slug=None, check_type_label=None, start_time=None, end_time=None, request=None
    ):
        self.set_user_presence(
            presence=False,
            type_slug=check_type_slug,
            type_label=check_type_label,
            start_time=start_time,
            end_time=end_time,
            request=request,
        )

    def mark_user_presence(
        self, check_type_slug=None, check_type_label=None, start_time=None, end_time=None, request=None
    ):
        self.set_user_presence(
            presence=True,
            type_slug=check_type_slug,
            type_label=check_type_label,
            start_time=start_time,
            end_time=end_time,
            request=request,
        )

    def refresh_computed_times(self, commit=False):
        to_update = []
        user_checks = self.user_checks.all()

        if len(user_checks) == 1:
            user_check = user_checks[0]
            changed = user_check._refresh_computed_times()
            if changed:
                to_update.append(user_check)

        elif len(user_checks) == 2:
            user_check1, user_check2 = user_checks

            if user_check1.presence is True:
                # first check is presence, compute it first
                changed = user_check1._refresh_computed_times(adjust_end_to_booking=False)
                if changed:
                    to_update.append(user_check1)
                changed = user_check2._refresh_computed_times(other_user_check=user_check1)
                if changed:
                    to_update.append(user_check2)
            else:
                # second check is presence, compute it first
                changed = user_check2._refresh_computed_times(adjust_start_to_booking=False)
                if changed:
                    to_update.append(user_check2)
                changed = user_check1._refresh_computed_times(other_user_check=user_check2)
                if changed:
                    to_update.append(user_check1)

        if commit and to_update:
            BookingCheck.objects.bulk_update(to_update, ['computed_start_time', 'computed_end_time'])
        return to_update

    def get_user_block(self):
        template_vars = Context(settings.TEMPLATE_VARS, autoescape=False)
        template_vars.update(
            {
                'booking': self,
            }
        )
        try:
            return escape(Template(self.event.agenda.get_booking_user_block_template()).render(template_vars))
        except (VariableDoesNotExist, TemplateSyntaxError):
            return

    def get_extra_user_block(self, request):
        context = RequestContext(request)
        context.update(
            {
                'booking': self,
            }
        )
        try:
            return Template(self.event.agenda.booking_extra_user_block_template).render(context)
        except (VariableDoesNotExist, TemplateSyntaxError):
            return

    @classmethod
    def anonymize_bookings(cls, bookings_queryset):
        bookings_queryset.update(
            label='',
            user_display_label='',
            user_email='',
            user_external_id='',
            user_last_name='',
            user_first_name='',
            user_phone_number='',
            extra_data={},
            anonymization_datetime=now(),
        )

    def get_vevent_ics(self, request=None):
        event = icalendar.Event()
        event.add('dtstamp', now())
        event.add(
            'uid',
            '%s-%s-%s'
            % (
                self.event.start_datetime.isoformat(),
                self.event.agenda.pk,
                self.pk,
            ),
        )

        event.add('summary', self.get_display_label())
        event.add('dtstart', self.event.start_datetime)
        if self.user_name:
            event.add('attendee', self.user_name)
        if request is None or request.GET.get('organizer') != 'no':
            organizer_name = getattr(settings, 'TEMPLATE_VARS', {}).get('global_title', 'chrono')
            organizer_email = getattr(settings, 'TEMPLATE_VARS', {}).get(
                'default_from_email', 'chrono@example.net'
            )
            organizer = icalendar.vCalAddress(f'mailto:{organizer_email}')
            organizer.params['cn'] = organizer_name
            event.add('organizer', organizer)

        if self.event.end_datetime:
            event.add('dtend', self.event.end_datetime)

        for field in ('description', 'location', 'comment', 'url'):
            field_value = (
                request
                and request.GET.get(field)
                or (self.extra_data or {}).get(field)
                or getattr(self.event, field, None)
            )
            if field_value:
                event.add(field, field_value)
        return event

    def get_ics(self, request=None):
        cal = icalendar.Calendar()
        cal.add('prodid', '-//Entr\'ouvert//NON SGML Publik')
        cal.add('version', '2.0')
        cal.add_component(self.get_vevent_ics(request))
        return cal.to_ical().decode('utf-8')

    def clone(self, primary_booking=None, save=True):
        new_booking = copy.deepcopy(self)
        new_booking.id = None
        new_booking.primary_booking = primary_booking
        if save:
            new_booking.save()
        return new_booking

    def events_display(self):
        name = self.user_name or self.label or _('Anonymous')
        return '%s, %s' % (name, date_format(localtime(self.creation_datetime), 'DATETIME_FORMAT'))

    def get_form_url(self):
        return translate_from_publik_url(self.form_url)

    def get_backoffice_url(self):
        return translate_from_publik_url(self.backoffice_url)

    def get_display_label(self):
        return self.user_display_label or self.label or self.event.label or ''


class BookingCheck(models.Model):
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE, related_name='user_checks')

    presence = models.BooleanField()

    start_time = models.TimeField(_('Arrival'), null=True, blank=True)
    end_time = models.TimeField(_('Departure'), null=True, blank=True)
    computed_start_time = models.TimeField(null=True)
    computed_end_time = models.TimeField(null=True)

    type_slug = models.CharField(max_length=160, blank=True, null=True)
    type_label = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        ordering = ['start_time']
        constraints = [
            models.UniqueConstraint(fields=['booking', 'presence'], name='max_2_checks_on_booking')
        ]

    def _get_previous_and_next_slots(self, _time):
        minutes = {
            'hour': 60,
            'half_hour': 30,
            'quarter': 15,
        }[self.booking.event.agenda.invoicing_unit]

        time_minutes = _time.hour * 60 + _time.minute
        previous_slot_minutes = math.trunc(time_minutes / minutes) * minutes
        previous_slot = datetime.time(*divmod(previous_slot_minutes, 60))
        next_slot = datetime.time(*divmod(previous_slot_minutes + minutes, 60))
        return previous_slot, next_slot

    def get_computed_start_time(self, other_user_check=None, adjust_to_booking=True):
        if self.start_time is None:
            return None

        start_time = self.start_time
        if self.booking.start_time and adjust_to_booking:
            # adjust start_time to the start of the booking if requested
            start_time = min(self.start_time, self.booking.start_time)
        if other_user_check and other_user_check.computed_start_time and other_user_check.computed_end_time:
            # if other user_check exists and is completely computed
            if other_user_check.start_time < self.start_time:
                # if other user_check is the first of the day, start_time is the end of other user_check
                start_time = other_user_check.computed_end_time
        if self.booking.event.agenda.invoicing_unit == 'minute':
            return start_time

        tolerance = self.booking.event.agenda.invoicing_tolerance

        # compute previous and next slot
        previous_slot, next_slot = self._get_previous_and_next_slots(start_time)

        # in tolerance ? take next_slot
        if (next_slot.minute or 60 - start_time.minute) <= tolerance:
            return next_slot

        # else take previous_slot
        return previous_slot

    def get_computed_end_time(self, other_user_check=None, adjust_to_booking=True):
        if self.end_time is None:
            return None

        end_time = self.end_time
        if self.booking.end_time and adjust_to_booking:
            # adjust end_time to the end of the booking if requested
            end_time = max(self.end_time, self.booking.end_time)
        if other_user_check and other_user_check.computed_start_time and other_user_check.computed_end_time:
            # if other user_check exists and is completely computed
            if other_user_check.start_time > self.start_time:
                # if other user_check is the second of the day, end_time is the start of other user_check
                end_time = other_user_check.computed_start_time
        if self.booking.event.agenda.invoicing_unit == 'minute':
            return end_time

        tolerance = self.booking.event.agenda.invoicing_tolerance

        # compute previous and next slot
        previous_slot, next_slot = self._get_previous_and_next_slots(end_time)

        # in tolerance ? take previous_slot
        if (end_time.minute - previous_slot.minute) <= tolerance:
            return previous_slot

        # else take next_slot
        return next_slot

    def _refresh_computed_times(
        self, other_user_check=None, adjust_start_to_booking=True, adjust_end_to_booking=True
    ):
        old_computed_start_time = self.computed_start_time
        old_computed_end_time = self.computed_end_time
        self.computed_start_time = self.get_computed_start_time(
            other_user_check=other_user_check, adjust_to_booking=adjust_start_to_booking
        )
        self.computed_end_time = self.get_computed_end_time(
            other_user_check=other_user_check, adjust_to_booking=adjust_end_to_booking
        )
        # return True if changed, else False
        if (
            old_computed_start_time == self.computed_start_time
            and old_computed_end_time == self.computed_end_time
        ):
            return False
        return True


OpeningHour = collections.namedtuple('OpeningHour', ['begin', 'end'])


class Desk(WithInspectMixin, models.Model):
    agenda = models.ForeignKey(Agenda, on_delete=models.CASCADE)
    label = models.CharField(_('Label'), max_length=150)
    slug = models.SlugField(_('Identifier'), max_length=160)

    def __str__(self):
        return self.label

    class Meta:
        ordering = ['label', 'slug']
        unique_together = ['agenda', 'slug']

    def save(self, *args, **kwargs):
        assert self.agenda.kind != 'virtual', "a desk can't reference a virtual agenda"
        if not self.slug:
            self.slug = generate_slug(self, agenda=self.agenda)
        super().save(*args, **kwargs)

    @property
    def base_slug(self):
        return slugify(self.label)

    def get_dependencies(self):
        yield from self.unavailability_calendars.all()

    @classmethod
    def import_json(cls, data):
        timeperiods = data.pop('timeperiods', [])
        exceptions = data.pop('exceptions', [])
        sources = data.pop('exception_sources', [])
        unavailability_calendars = data.pop('unavailability_calendars', [])
        data = clean_import_data(cls, data)
        desk, dummy = cls.objects.update_or_create(slug=data['slug'], agenda=data['agenda'], defaults=data)
        for timeperiod in timeperiods:
            timeperiod['desk'] = desk
            TimePeriod.import_json(timeperiod)
        for exception in exceptions:
            exception['desk'] = desk
            TimePeriodException.import_json(exception)
        for source in sources:
            source['desk'] = desk
            TimePeriodExceptionSource.import_json(source)
        for unavailability_calendar in unavailability_calendars:
            slug = unavailability_calendar['slug']
            try:
                target_calendar = UnavailabilityCalendar.objects.get(slug=slug)
            except UnavailabilityCalendar.DoesNotExist:
                raise AgendaImportError(_('The unavailability calendar "%s" does not exist.') % slug)
            desk.unavailability_calendars.add(target_calendar)

    def export_json(self):
        time_period_exceptions = self.timeperiodexception_set.filter(source__isnull=True)
        return {
            'label': self.label,
            'slug': self.slug,
            'timeperiods': [time_period.export_json() for time_period in self.timeperiod_set.filter()],
            'exceptions': [exception.export_json() for exception in time_period_exceptions],
            'exception_sources': [
                source.export_json() for source in self.timeperiodexceptionsource_set.all()
            ],
            'unavailability_calendars': [{'slug': x.slug} for x in self.unavailability_calendars.all()],
        }

    def get_inspect_keys(self):
        return [
            'label',
            'slug',
        ]

    def duplicate(self, label=None, agenda_target=None, reset_slug=True):
        # clone current desk
        new_desk = copy.deepcopy(self)
        new_desk.pk = None
        # set label
        new_desk.label = label or new_desk.label
        # reset slug
        if reset_slug:
            new_desk.slug = None
        # set agenda
        if agenda_target:
            new_desk.agenda = agenda_target
        # store new desk
        new_desk.save()

        # clone related objects
        for time_period in self.timeperiod_set.all():
            time_period.duplicate(desk_target=new_desk)
        for time_period_exception in self.timeperiodexception_set.filter(source__isnull=True):
            time_period_exception.duplicate(desk_target=new_desk)
        for time_period_exception_source in self.timeperiodexceptionsource_set.all():
            time_period_exception_source.duplicate(desk_target=new_desk)
        new_desk.unavailability_calendars.set(self.unavailability_calendars.all())

        return new_desk

    def get_exceptions_within_two_weeks(self):
        # prefetched_exceptions contains desks exceptions + unavailability_calendars exceptions
        # default ordering: start_datetime
        in_two_weeks = make_aware(datetime.datetime.today() + datetime.timedelta(days=14))
        exceptions = []
        for exception in self.prefetched_exceptions:
            if exception.end_datetime < now():
                # exception ends in the past, skip it
                continue
            if exception.end_datetime <= in_two_weeks:
                # ends in less than 2 weeks
                exceptions.append(exception)
            elif exception.start_datetime < now():
                # has already started
                exceptions.append(exception)
        if exceptions:
            return exceptions
        # if none found within the 2 coming weeks, return the next one
        for exception in self.prefetched_exceptions:
            if exception.start_datetime < now():
                # exception starts in the past, skip it
                continue
            # returns the first exception found
            return [exception]
        return []

    def are_all_exceptions_displayed(self):
        in_two_weeks = self.get_exceptions_within_two_weeks()
        return len(self.prefetched_exceptions) == len(in_two_weeks)

    def get_opening_hours(self, date):
        openslots = IntervalSet()
        weekday_index = get_weekday_index(date)
        real_date = date.date() if isinstance(date, datetime.datetime) else date
        for timeperiod in self.timeperiod_set.all():
            if timeperiod.weekday_indexes and weekday_index not in timeperiod.weekday_indexes:
                continue
            # timeperiod_set.all() are prefetched, do not filter in queryset
            if timeperiod.date != real_date and timeperiod.weekday != date.weekday():
                continue
            start_datetime = make_aware(datetime.datetime.combine(date, timeperiod.start_time))
            end_datetime = make_aware(datetime.datetime.combine(date, timeperiod.end_time))
            openslots.add(start_datetime, end_datetime)

        aware_date = make_aware(datetime.datetime(date.year, date.month, date.day))
        exceptions = IntervalSet()
        aware_next_date = aware_date + datetime.timedelta(days=1)
        for exception in self.prefetched_exceptions:
            if exception.end_datetime < aware_date:
                continue
            if exception.start_datetime > aware_next_date:
                continue
            exceptions.add(exception.start_datetime, exception.end_datetime)

        return [OpeningHour(*time_range) for time_range in (openslots - exceptions)]

    def import_timeperiod_exceptions_from_settings(self, enable=False, spool=True):
        start_update = now()
        for slug, source_info in settings.EXCEPTIONS_SOURCES.items():
            label = source_info['label']
            try:
                source = TimePeriodExceptionSource.objects.get(desk=self, settings_slug=slug)
            except TimePeriodExceptionSource.DoesNotExist:
                source = TimePeriodExceptionSource.objects.create(
                    desk=self, settings_slug=slug, enabled=False
                )
            source.settings_label = _(label)
            source.save()
            if enable or source.enabled:  # if already enabled, update anyway
                source.enable(spool=spool)
        TimePeriodExceptionSource.objects.filter(
            desk=self, settings_slug__isnull=False, last_update__lt=start_update
        ).delete()  # source was not in settings anymore


class Resource(WithSnapshotMixin, WithApplicationMixin, WithInspectMixin, models.Model):
    # mark temporarily restored snapshots
    snapshot = models.ForeignKey(
        ResourceSnapshot, on_delete=models.CASCADE, null=True, related_name='temporary_instance'
    )

    slug = models.SlugField(_('Identifier'), max_length=160, unique=True)
    label = models.CharField(_('Label'), max_length=150)
    description = models.TextField(_('Description'), blank=True, help_text=_('Optional description.'))

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    application_component_type = 'resources'
    application_label_singular = _('Shared resource')
    application_label_plural = _('Shared resources')

    objects = WithSnapshotManager()
    snapshots = WithSnapshotManager(snapshots=True)

    def __str__(self):
        return self.label

    class Meta:
        ordering = ['label']

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)

    @property
    def base_slug(self):
        return slugify(self.label)

    def can_be_viewed(self, user):
        if user.is_staff:
            return True
        group_ids = [x.id for x in user.groups.all()]
        return self.agenda_set.filter(admin_role_id__in=group_ids).exists()

    def get_dependencies(self):
        return []

    @classmethod
    def import_json(cls, data, overwrite=False, snapshot=None):
        data = clean_import_data(cls, data)
        slug = data.pop('slug')
        qs_kwargs = {}
        if snapshot:
            qs_kwargs = {'snapshot': snapshot}  # don't take slug from snapshot: it has to be unique !
            data['slug'] = str(uuid.uuid4())  # random slug
        else:
            qs_kwargs = {'slug': slug}
        resource, created = cls.objects.update_or_create(defaults=data, **qs_kwargs)
        return created, resource

    def export_json(self):
        return {
            'slug': self.slug,
            'label': self.label,
            'description': self.description,
        }

    def get_inspect_keys(self):
        return ['label', 'slug', 'description']


class Category(WithSnapshotMixin, WithApplicationMixin, WithInspectMixin, models.Model):
    # mark temporarily restored snapshots
    snapshot = models.ForeignKey(
        CategorySnapshot, on_delete=models.CASCADE, null=True, related_name='temporary_instance'
    )

    slug = models.SlugField(_('Identifier'), max_length=160, unique=True)
    label = models.CharField(_('Label'), max_length=150)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    application_component_type = 'agendas_categories'
    application_label_singular = _('Category (agendas)')
    application_label_plural = _('Categories (agendas)')

    objects = WithSnapshotManager()
    snapshots = WithSnapshotManager(snapshots=True)

    def __str__(self):
        return self.label

    class Meta:
        ordering = ['label']

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)

    @property
    def base_slug(self):
        return slugify(self.label)

    def get_dependencies(self):
        return []

    @classmethod
    def import_json(cls, data, overwrite=False, snapshot=None):
        data = clean_import_data(cls, data)
        slug = data.pop('slug')
        qs_kwargs = {}
        if snapshot:
            qs_kwargs = {'snapshot': snapshot}  # don't take slug from snapshot: it has to be unique !
            data['slug'] = str(uuid.uuid4())  # random slug
        else:
            qs_kwargs = {'slug': slug}
        category, created = cls.objects.update_or_create(defaults=data, **qs_kwargs)
        return created, category

    def export_json(self):
        return {
            'label': self.label,
            'slug': self.slug,
        }

    def get_inspect_keys(self):
        return ['label', 'slug']


def ics_directory_path(instance, filename):
    return f'ics/{str(uuid.uuid4())}/{filename}'


class TimePeriodExceptionSource(WithInspectMixin, models.Model):
    desk = models.ForeignKey(Desk, on_delete=models.CASCADE, null=True)
    unavailability_calendar = models.ForeignKey('UnavailabilityCalendar', on_delete=models.CASCADE, null=True)
    ics_filename = models.CharField(null=True, max_length=256)
    ics_file = models.FileField(upload_to=ics_directory_path, blank=True, null=True)
    ics_url = models.URLField(null=True, max_length=500)
    settings_slug = models.CharField(null=True, max_length=150)
    settings_label = models.CharField(null=True, max_length=150)
    last_update = models.DateTimeField(auto_now=True, null=True)
    enabled = models.BooleanField(default=True)

    class Meta:
        unique_together = ['desk', 'settings_slug']

    def __str__(self):
        if self.ics_filename is not None:
            return self.ics_filename
        if self.settings_label is not None:
            return gettext(self.settings_label)
        return self.ics_url

    def duplicate(self, desk_target=None):
        # clone current source
        new_source = copy.deepcopy(self)
        new_source.pk = None
        # set desk
        new_source.desk = desk_target or self.desk
        # set ics_file
        if self.ics_file:
            with open(self.ics_file.path) as ics_file:
                new_source.ics_file.save(self.ics_filename, ics_file, save=False)
        # store new source
        new_source.save()
        # clone related objects
        for time_period_exception in self.timeperiodexception_set.all():
            time_period_exception.duplicate(desk_target=desk_target, source_target=new_source)

        return new_source

    def enable(self, spool=True):
        self.enabled = True
        self.save()

        if spool and 'uwsgi' in sys.modules:
            from chrono.utils.spooler import refresh_exceptions_from_settings

            tenant = getattr(connection, 'tenant', None)
            transaction.on_commit(
                lambda: refresh_exceptions_from_settings.spool(
                    source_id=str(self.pk), domain=getattr(tenant, 'domain_url', None)
                )
            )
            return

        self.refresh_from_settings()

    def refresh_from_settings(self):
        if not self.enabled:
            return
        source_info = settings.EXCEPTIONS_SOURCES.get(self.settings_slug)
        if not source_info:
            return
        source_class = import_string(source_info['class'])
        calendar = source_class()
        this_year = now().year
        days = [day for year in range(this_year, this_year + 3) for day in calendar.holidays(year)]
        with transaction.atomic():
            self.timeperiodexception_set.all().delete()
            for day, label in days:
                start_datetime = make_aware(datetime.datetime.combine(day, datetime.datetime.min.time()))
                end_datetime = start_datetime + datetime.timedelta(days=1)
                TimePeriodException.objects.create(
                    desk=self.desk,
                    source=self,
                    label=_(label),
                    start_datetime=start_datetime,
                    end_datetime=end_datetime,
                )

    def disable(self):
        self.timeperiodexception_set.all().delete()
        self.enabled = False
        self.save()

    def render_ics_url(self):
        return Template(self.ics_url).render(Context(settings.TEMPLATE_VARS))

    def _check_ics_content(self):
        if self.ics_url:
            ics_url = self.render_ics_url()
            try:
                response = requests.get(ics_url, proxies=settings.REQUESTS_PROXIES, timeout=15)
                response.raise_for_status()
            except requests.HTTPError as e:
                raise ICSError(
                    _('Failed to retrieve remote calendar (%(url)s, HTTP error %(status_code)s).')
                    % {'url': ics_url, 'status_code': e.response.status_code}
                )
            except requests.RequestException as e:
                raise ICSError(
                    _('Failed to retrieve remote calendar (%(url)s, %(exception)s).')
                    % {'url': ics_url, 'exception': e}
                )
            try:
                # override response encoding received in HTTP headers as it may
                # often be missing and defaults to iso-8859-15.
                response.content.decode('utf-8')
                response.encoding = 'utf-8'
            except UnicodeDecodeError:
                pass
            data = response.text
        else:
            data = force_str(self.ics_file.read())

        try:
            cal = icalendar.Calendar.from_ical(data)
        except ValueError:
            raise ICSError(_('File format is invalid.'))

        vevents = list(cal.walk('vevent'))
        if len(vevents) == 0:
            raise ICSError(_('The file doesn\'t contain any events.'))

        for vevent in vevents:
            summary = self._get_summary_from_vevent(vevent)
            if 'dtstart' not in vevent:
                raise ICSError(_('Event "%s" has no start date.') % summary)
            # with icalendar date parse error lead to None properties
            # and then raises an Attribute error when trying to decode it
            if vevent['dtstart'] is None:
                raise ICSError(_('File format is invalid.'))
            if 'dtend' in vevent and vevent['dtend'] is None:
                raise ICSError(_('File format is invalid.'))

        return cal

    def _get_summary_from_vevent(self, vevent):
        if 'summary' in vevent:
            return vevent.decoded('summary').decode('utf-8')
        return _('Exception')

    def refresh_timeperiod_exceptions(self, data=None):
        if 'uwsgi' in sys.modules:
            from chrono.utils.spooler import refresh_exception_source

            tenant = getattr(connection, 'tenant', None)
            transaction.on_commit(
                lambda: refresh_exception_source.spool(
                    source_id=str(self.pk), domain=getattr(tenant, 'domain_url', None)
                )
            )
            return

        self.refresh_timeperiod_exceptions_from_ics(data=data)

    def refresh_timeperiod_exceptions_from_ics(self, data=None, recurring_days=600):
        if data is None:
            parsed = self._check_ics_content()
        else:
            parsed = data

        categories = collections.defaultdict(list)
        with transaction.atomic():
            # delete old exceptions related to this source
            self.timeperiodexception_set.all().delete()
            # create new exceptions
            update_datetime = now()
            for vevent in parsed.walk('vevent'):
                summary = self._get_summary_from_vevent(vevent)
                if 'dtstart' in vevent:
                    start_dt = vevent.decoded('dtstart')
                    if not isinstance(start_dt, datetime.datetime):
                        start_dt = datetime.datetime.combine(start_dt, datetime.datetime.min.time())
                    if not is_aware(start_dt):
                        start_dt = make_aware(start_dt)
                    else:
                        # Enforce local timezone to calculate the end of the day
                        # when no DTEND and no duration in the local tz
                        start_dt = start_dt.astimezone(timezone.get_current_timezone())
                else:
                    raise ICSError(_('Event "%s" has no start date.') % summary)
                if 'dtend' in vevent:
                    end_dt = vevent.decoded('dtend')
                    if not isinstance(end_dt, datetime.datetime):
                        end_dt = datetime.datetime.combine(end_dt, datetime.datetime.min.time())
                    if not is_aware(end_dt):
                        end_dt = make_aware(end_dt)
                else:
                    try:
                        duration = vevent.decoded('duration')
                        end_dt = start_dt + duration
                    except (KeyError, AttributeError):
                        # events without end date and with no/invalid duration are
                        # considered as ending the same day leading in "strange"
                        # ics files with a DTEND set at 23:59:59.999999 meaning
                        # that the event ends at 23:59:59.999998 (DTEND is excluded)
                        end_dt = make_aware(datetime.datetime.combine(start_dt, datetime.datetime.max.time()))

                event = {
                    'start_datetime': start_dt,
                    'end_datetime': end_dt,
                    'label': summary,
                    'desk_id': self.desk_id,
                    'unavailability_calendar_id': self.unavailability_calendar_id,
                    'source': self,
                    'recurrence_id': 0,
                }

                if 'categories' in vevent and len(vevent['categories'].cats) > 0:
                    category = str(vevent['categories'].cats[0])
                else:
                    category = None

                # Updating vevent to match calculated start & end so the
                # recurrence matches what we calculated
                vevent.pop('dtstart')
                vevent.add('dtstart', start_dt)
                if 'duration' in vevent:
                    vevent.pop('duration')
                if 'dtend' in vevent:
                    vevent.pop('dtend')
                vevent.add('dtend', end_dt)

                rrule = recurring_ical_events.of(vevent)
                if 'rrule' not in vevent:
                    # classical event
                    exception = TimePeriodException.objects.create(**event)
                    if category:
                        categories[category].append(exception)
                elif len(rrule.repetitions) > 0:
                    # recurring event until recurring_days in the future
                    from_dt = start_dt
                    until_dt = update_datetime + datetime.timedelta(days=recurring_days)
                    for i, revent in enumerate(rrule.between(from_dt, until_dt)):
                        start_dt = revent.decoded('dtstart')
                        end_dt = revent.decoded('dtend')
                        event['recurrence_id'] = i
                        event['start_datetime'] = start_dt
                        event['end_datetime'] = end_dt
                        if end_dt >= update_datetime:
                            exception = TimePeriodException.objects.create(**event)
                            if category:
                                categories[category].append(exception)

            if self.unavailability_calendar_id:
                for category, exceptions in categories.items():
                    exception_group, dummy = TimePeriodExceptionGroup.objects.get_or_create(
                        unavailability_calendar_id=self.unavailability_calendar_id,
                        slug=category,
                        defaults={'label': exceptions[0].label},
                    )
                    exception_group.exceptions.add(*exceptions)

    @classmethod
    def import_json(cls, data):
        data = clean_import_data(cls, data)

        if data.get('ics_file'):
            try:
                data['ics_file'] = ContentFile(base64.b64decode(data['ics_file']), name=data['ics_filename'])
            except base64.binascii.Error:
                raise AgendaImportError(_('Bad ics file'))
        elif data.get('ics_filename'):
            # filename but no file content, skip this source
            return

        desk = data.pop('desk')
        settings_slug = data.pop('settings_slug')
        ics_url = data.pop('ics_url', None)
        ics_filename = data.pop('ics_filename', None)
        source = cls.objects.update_or_create(
            desk=desk, settings_slug=settings_slug, ics_filename=ics_filename, ics_url=ics_url, defaults=data
        )[0]
        if settings_slug:
            if source.enabled:
                source.enable()
        else:
            try:
                source.refresh_timeperiod_exceptions_from_ics()
            except ICSError:
                pass

    def export_json(self):
        return {
            'ics_filename': self.ics_filename,
            'ics_file': base64.b64encode(self.ics_file.read()).decode() if self.ics_file else None,
            'ics_url': self.ics_url,
            'settings_slug': self.settings_slug,
            'settings_label': self.settings_label,
            'enabled': self.enabled,
        }

    def get_inspect_keys(self):
        return [
            'ics_filename',
            'ics_file',
            'ics_url',
            'settings_slug',
            'settings_label',
            'enabled',
        ]


class UnavailabilityCalendar(WithSnapshotMixin, WithApplicationMixin, WithInspectMixin, models.Model):
    # mark temporarily restored snapshots
    snapshot = models.ForeignKey(
        UnavailabilityCalendarSnapshot, on_delete=models.CASCADE, null=True, related_name='temporary_instance'
    )

    label = models.CharField(_('Label'), max_length=150)
    slug = models.SlugField(_('Identifier'), max_length=160, unique=True)
    desks = models.ManyToManyField(Desk, related_name='unavailability_calendars')
    edit_role = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        default=None,
        related_name='+',
        verbose_name=_('Edit Role'),
        on_delete=models.SET_NULL,
    )
    view_role = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        default=None,
        related_name='+',
        verbose_name=_('View Role'),
        on_delete=models.SET_NULL,
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    application_component_type = 'unavailability_calendars'
    application_label_singular = _('Unavailability calendar')
    application_label_plural = _('Unavailability calendars')

    objects = WithSnapshotManager()
    snapshots = WithSnapshotManager(snapshots=True)

    class Meta:
        ordering = ['label']

    def __str__(self):
        return self.label

    @property
    def base_slug(self):
        return slugify(self.label)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = generate_slug(self)
        super().save(*args, **kwargs)

    def can_be_managed(self, user):
        if user.is_staff:
            return True
        group_ids = [x.id for x in user.groups.all()]
        return bool(self.edit_role_id in group_ids)

    def can_be_viewed(self, user):
        if self.can_be_managed(user):
            return True
        group_ids = [x.id for x in user.groups.all()]
        return bool(self.view_role_id in group_ids)

    def get_absolute_url(self):
        return reverse('chrono-manager-unavailability-calendar-view', kwargs={'pk': self.id})

    def get_dependencies(self):
        yield self.view_role
        yield self.edit_role

    def export_json(self):
        unavailability_calendar = {
            'label': self.label,
            'slug': self.slug,
            'permissions': {
                'view': self.view_role.name if self.view_role else None,
                'edit': self.edit_role.name if self.edit_role else None,
            },
            'exceptions': [exception.export_json() for exception in self.timeperiodexception_set.all()],
        }
        return unavailability_calendar

    @classmethod
    def import_json(cls, data, overwrite=False, snapshot=None):
        data = data.copy()
        permissions = data.pop('permissions', {})
        exceptions = data.pop('exceptions', [])
        for permission in ('view', 'edit'):
            if permissions.get(permission):
                data[permission + '_role'] = Group.objects.get(name=permissions[permission])
        data = clean_import_data(cls, data)
        slug = data.pop('slug')
        qs_kwargs = {}
        if snapshot:
            qs_kwargs = {'snapshot': snapshot}  # don't take slug from snapshot: it has to be unique !
            data['slug'] = str(uuid.uuid4())  # random slug
        else:
            qs_kwargs = {'slug': slug}
        unavailability_calendar, created = cls.objects.update_or_create(defaults=data, **qs_kwargs)
        if overwrite:
            TimePeriodException.objects.filter(unavailability_calendar=unavailability_calendar).delete()
        for exception in exceptions:
            exception['unavailability_calendar'] = unavailability_calendar
            TimePeriodException.import_json(exception)

        return created, unavailability_calendar

    def get_inspect_keys(self):
        return ['label', 'slug']

    def get_permissions_inspect_fields(self):
        yield from self.get_inspect_fields(keys=['edit_role', 'view_role'])


class TimePeriodExceptionGroup(models.Model):
    unavailability_calendar = models.ForeignKey(UnavailabilityCalendar, on_delete=models.CASCADE)
    slug = models.SlugField(_('Identifier'), max_length=160)
    label = models.CharField(_('Label'), max_length=150)

    class Meta:
        ordering = ['label']
        unique_together = ['unavailability_calendar', 'slug']

    def __str__(self):
        return self.label


class TimePeriodException(WithInspectMixin, models.Model):
    desk = models.ForeignKey(Desk, on_delete=models.CASCADE, null=True)
    unavailability_calendar = models.ForeignKey(UnavailabilityCalendar, on_delete=models.CASCADE, null=True)
    source = models.ForeignKey(TimePeriodExceptionSource, on_delete=models.CASCADE, null=True)
    label = models.CharField(_('Optional Label'), max_length=150, blank=True, null=True)
    start_datetime = models.DateTimeField(_('Exception start time'))
    end_datetime = models.DateTimeField(_('Exception end time'))
    update_datetime = models.DateTimeField(auto_now=True)
    recurrence_id = models.PositiveIntegerField(_('Recurrence ID'), default=0)
    group = models.ForeignKey(
        TimePeriodExceptionGroup, on_delete=models.CASCADE, null=True, related_name='exceptions'
    )

    @property
    def read_only(self):
        if self.source_id:
            return True
        if self.unavailability_calendar_id:
            return True
        return False

    class Meta:
        ordering = ['start_datetime']

    def __str__(self):
        if is_midnight(self.start_datetime) and is_midnight(self.end_datetime):
            # if both dates are at midnight don't include the time part
            if self.end_datetime == self.start_datetime + datetime.timedelta(days=1):
                # a single day
                exc_repr = '%s' % date_format(localtime(self.start_datetime), 'SHORT_DATE_FORMAT')
            else:
                exc_repr = '%s → %s' % (
                    date_format(localtime(self.start_datetime), 'SHORT_DATE_FORMAT'),
                    date_format(localtime(self.end_datetime), 'SHORT_DATE_FORMAT'),
                )
        else:
            if localtime(self.start_datetime).date() == localtime(self.end_datetime).date():
                # same day
                exc_repr = '%s → %s' % (
                    date_format(localtime(self.start_datetime), 'SHORT_DATETIME_FORMAT'),
                    date_format(localtime(self.end_datetime), 'TIME_FORMAT'),
                )
            else:
                exc_repr = '%s → %s' % (
                    date_format(localtime(self.start_datetime), 'SHORT_DATETIME_FORMAT'),
                    date_format(localtime(self.end_datetime), 'SHORT_DATETIME_FORMAT'),
                )

        if self.label:
            exc_repr = '%s (%s)' % (self.label, exc_repr)

        return exc_repr

    def has_booking_within_time_slot(self, target_desk=None):
        if not (self.start_datetime and self.end_datetime):
            # incomplete time period, can't tell
            return False

        query = Event.objects
        if self.desk:
            query = query.filter(desk=self.desk)
        elif self.unavailability_calendar and not target_desk:
            query = query.filter(desk__in=self.unavailability_calendar.desks.all())
        elif target_desk:
            query = query.filter(desk=target_desk)
        else:
            # orphan exception
            return False

        for event in query.filter(booking__isnull=False, booking__cancellation_datetime__isnull=True):
            if self.start_datetime <= event.start_datetime < self.end_datetime:
                return True
            if event.meeting_type:
                if (
                    event.start_datetime
                    <= self.start_datetime
                    < event.start_datetime + datetime.timedelta(minutes=event.meeting_type.duration)
                ):
                    return True
        return False

    @classmethod
    def import_json(cls, data):
        def import_datetime(s):
            '''Import datetime as a naive ISO8601 serialization'''
            try:
                return make_aware(datetime.datetime.strptime(s, '%Y-%m-%d %H:%M:%S'))
            except ValueError:
                raise AgendaImportError(_('Bad datetime format "%s"') % s)

        for k, v in data.items():
            if k.endswith('_datetime'):
                data[k] = import_datetime(v)
        data = clean_import_data(cls, data)
        query_data = data.copy()
        query_data.pop('update_datetime')
        try:
            cls.objects.update_or_create(defaults=data, **query_data)
        except cls.MultipleObjectsReturned:
            cls.objects.filter(**query_data).update(update_datetime=data['update_datetime'])

    def export_json(self):
        def export_datetime(dt):
            '''Export datetime as a naive ISO8601 serialization'''
            return make_naive(dt).strftime('%Y-%m-%d %H:%M:%S')

        return {
            'label': self.label,
            'start_datetime': export_datetime(self.start_datetime),
            'end_datetime': export_datetime(self.end_datetime),
            'recurrence_id': self.recurrence_id,
            'update_datetime': export_datetime(self.update_datetime),
        }

    def get_inspect_keys(self):
        return [
            'label',
            'start_datetime',
            'end_datetime',
        ]

    def duplicate(self, desk_target=None, source_target=None):
        # clone current exception
        new_exception = copy.deepcopy(self)
        new_exception.pk = None
        # set desk
        new_exception.desk = desk_target or self.desk
        # set source
        new_exception.source = source_target or self.source
        # store new exception
        new_exception.save()

        return new_exception

    def as_interval(self):
        '''Simplify insertion into IntervalSet'''
        return Interval(self.start_datetime, self.end_datetime)


class EventCancellationReport(models.Model):
    event = models.ForeignKey(Event, related_name='cancellation_reports', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    seen = models.BooleanField(default=False)
    bookings = models.ManyToManyField(Booking)
    booking_errors = models.JSONField(default=dict)

    def __str__(self):
        return '%s - %s' % (self.timestamp.strftime('%Y-%m-%d %H:%M:%S'), self.event)

    class Meta:
        ordering = ['-timestamp']


class RecurrenceExceptionsReport(models.Model):
    agenda = models.OneToOneField(
        Agenda, related_name='recurrence_exceptions_report', on_delete=models.CASCADE
    )
    events = models.ManyToManyField(Event)


class NotificationType:
    def __init__(self, name, related_field, settings):
        self.name = name
        self.related_field = related_field
        self.settings = settings

    @property
    def enabled(self):
        choice = getattr(self.settings, self.name)
        if not choice:
            return False

        if choice == self.settings.EMAIL_FIELD:
            return bool(getattr(self.settings, self.name + '_emails'))

        return True

    def get_recipients(self):
        choice = getattr(self.settings, self.name)
        if not choice:
            return []

        if choice == self.settings.EMAIL_FIELD:
            return getattr(self.settings, self.name + '_emails')

        role = self.settings.get_role_from_choice(choice)
        if not role or not hasattr(role, 'role'):
            return []
        emails = role.role.emails
        if role.role.emails_to_members:
            emails.extend(role.user_set.values_list('email', flat=True))
        return emails

    @property
    def display_value(self):
        choice = getattr(self.settings, self.name)
        if not choice:
            return ''

        if choice == self.settings.EMAIL_FIELD:
            emails = getattr(self.settings, self.name + '_emails')
            return ', '.join(emails)

        role = self.settings.get_role_from_choice(choice) or _('undefined')
        display_name = getattr(self.settings, 'get_%s_display' % self.name)()
        return '%s (%s)' % (display_name, role)

    @property
    def label(self):
        return self.settings._meta.get_field(self.name).verbose_name


class AgendaNotificationsSettings(WithInspectMixin, models.Model):
    EMAIL_FIELD = 'use-email-field'
    VIEW_ROLE = 'view-role'
    ADMIN_ROLE = 'admin-role'
    EDIT_ROLE = 'edit-role'

    CHOICES = [
        (ADMIN_ROLE, _('Admin Role')),
        (VIEW_ROLE, _('View Role')),
        (EDIT_ROLE, _('Edit Role')),
        (EMAIL_FIELD, _('Specify email addresses manually')),
    ]

    FIELD_NAMES_MAP = {
        'almost_full_event': 'almost_full_event_emails',
        'full_event': 'full_event_emails',
        'cancelled_event': 'cancelled_event_emails',
    }

    agenda = models.OneToOneField(Agenda, on_delete=models.CASCADE, related_name='notifications_settings')

    almost_full_event = models.CharField(
        max_length=16, blank=True, choices=CHOICES, verbose_name=_('Almost full event (90%)')
    )
    almost_full_event_emails = ArrayField(models.EmailField(), blank=True, null=True)

    full_event = models.CharField(max_length=16, blank=True, choices=CHOICES, verbose_name=_('Full event'))
    full_event_emails = ArrayField(models.EmailField(), blank=True, null=True)

    cancelled_event = models.CharField(
        max_length=16, blank=True, choices=CHOICES, verbose_name=_('Cancelled event')
    )
    cancelled_event_emails = ArrayField(models.EmailField(), blank=True, null=True)

    @classmethod
    def get_role_field_names(cls):
        return cls.FIELD_NAMES_MAP.keys()

    def get_notification_types(self):
        for field in self.get_role_field_names():
            notification_type = NotificationType(
                name=field, related_field=field.replace('_event', ''), settings=self
            )
            if notification_type.enabled:
                yield notification_type

    def get_role_from_choice(self, choice):
        if choice == self.ADMIN_ROLE:
            return self.agenda.admin_role
        elif choice == self.VIEW_ROLE:
            return self.agenda.view_role
        elif choice == self.EDIT_ROLE:
            return self.agenda.edit_role

    @classmethod
    def import_json(cls, data):
        data = clean_import_data(cls, data)
        agenda = data.pop('agenda')
        cls.objects.update_or_create(agenda=agenda, defaults=data)

    def export_json(self):
        return {
            'almost_full_event': self.almost_full_event,
            'almost_full_event_emails': self.almost_full_event_emails,
            'full_event': self.full_event,
            'full_event_emails': self.full_event_emails,
            'cancelled_event': self.cancelled_event,
            'cancelled_event_emails': self.cancelled_event_emails,
        }

    def get_inspect_keys(self):
        return [
            'almost_full_event',
            'full_event',
            'cancelled_event',
        ]

    def duplicate(self, agenda_target):
        new_settings = copy.deepcopy(self)
        new_settings.pk = None
        new_settings.agenda = agenda_target
        new_settings.save()
        return new_settings


class AgendaReminderSettings(WithInspectMixin, models.Model):
    ONE_DAY_BEFORE = 1
    TWO_DAYS_BEFORE = 2
    THREE_DAYS_BEFORE = 3
    FOUR_DAYS_BEFORE = 4
    FIVE_DAYS_BEFORE = 5

    CHOICES = [
        (None, _('Never')),
        (ONE_DAY_BEFORE, _('One day before')),
        (TWO_DAYS_BEFORE, _('Two days before')),
        (THREE_DAYS_BEFORE, _('Three days before')),
        (FOUR_DAYS_BEFORE, _('Four days before')),
        (FIVE_DAYS_BEFORE, _('Five days before')),
    ]

    agenda = models.OneToOneField(Agenda, on_delete=models.CASCADE, related_name='reminder_settings')
    days_before_email = models.IntegerField(
        null=True,
        blank=True,
        choices=CHOICES,
        verbose_name=_('Send email reminder'),
        help_text=_(
            'In order to prevent users from getting a reminder shortly after booking, '
            'a reminder is sent less only if at least 12 hours have elapsed since booking time.'
        ),
    )
    email_extra_info = models.TextField(
        blank=True,
        verbose_name=_('Additional text to include in emails'),
        validators=[booking_template_validator],
        help_text=_(
            'Basic information such as event name, time and date are already included. '
            'Booking object can be accessed using standard template syntax. '
            'This allows to access agenda name via {{ booking.event.agenda.label }}, '
            'meeting type name via {{ booking.event.meeting_type.label }}, or any extra '
            'parameter passed on booking creation via {{ booking.extra_data.xxx }}.'
        ),
    )
    days_before_sms = models.IntegerField(
        null=True,
        blank=True,
        choices=CHOICES,
        verbose_name=_('Send SMS reminder'),
        help_text=_(
            'In order to prevent users from getting a reminder shortly after booking, '
            'a reminder is sent less only if at least 12 hours have elapsed since booking time.'
        ),
    )
    sms_extra_info = models.TextField(
        blank=True,
        verbose_name=_('Additional text to include in SMS'),
        validators=[booking_template_validator],
        help_text=email_extra_info.help_text,
    )

    def display_info(self):
        def get_message(days, by_email_or_sms):
            return ngettext(
                'Users will be reminded of their booking %(by_email_or_sms)s, one day in advance.',
                'Users will be reminded of their booking %(by_email_or_sms)s, %(days)s days in advance.',
                days,
            ) % {'days': days, 'by_email_or_sms': by_email_or_sms}

        if self.days_before_email and self.days_before_email == self.days_before_sms:
            return [get_message(self.days_before_email, _('both by email and by SMS'))]

        messages = []
        if self.days_before_email:
            messages.append(get_message(self.days_before_email, _('by email')))
        if self.days_before_sms:
            messages.append(get_message(self.days_before_sms, _('by SMS')))

        return messages

    @classmethod
    def import_json(cls, data):
        data = clean_import_data(cls, data)
        agenda = data.pop('agenda')
        cls.objects.update_or_create(agenda=agenda, defaults=data)

    def export_json(self):
        return {
            'days_before_email': self.days_before_email,
            'days_before_sms': self.days_before_sms,
            'email_extra_info': self.email_extra_info,
            'sms_extra_info': self.sms_extra_info,
        }

    def get_inspect_keys(self):
        return [
            'days_before_email',
            'days_before_sms',
            'email_extra_info',
            'sms_extra_info',
        ]

    def duplicate(self, agenda_target):
        new_settings = copy.deepcopy(self)
        new_settings.pk = None
        new_settings.agenda = agenda_target
        new_settings.save()
        return new_settings


class Subscription(models.Model):
    agenda = models.ForeignKey(Agenda, on_delete=models.CASCADE, related_name='subscriptions')
    user_external_id = models.CharField(max_length=250)
    user_last_name = models.CharField(max_length=250)
    user_first_name = models.CharField(max_length=250)
    user_email = models.EmailField(blank=True)
    user_phone_number = models.CharField(max_length=30, blank=True)
    extra_data = models.JSONField(null=True)
    date_start = models.DateField()
    date_end = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        indexes = [models.Index(fields=['agenda', 'user_external_id'])]

    @property
    def user_name(self):
        return ('%s %s' % (self.user_first_name, self.user_last_name)).strip()

    @property
    def label(self):
        return _('Subscription')

    def get_user_block(self):
        template_vars = Context(settings.TEMPLATE_VARS, autoescape=False)
        template_vars.update(
            {
                'booking': self,
            }
        )
        try:
            return escape(Template(self.agenda.get_booking_user_block_template()).render(template_vars))
        except (VariableDoesNotExist, TemplateSyntaxError):
            return

    def get_extra_user_block(self, request):
        context = RequestContext(request)
        context.update(
            {
                'booking': self,
            }
        )
        try:
            return Template(self.agenda.booking_extra_user_block_template).render(context)
        except (VariableDoesNotExist, TemplateSyntaxError):
            return


class Person(models.Model):
    user_external_id = models.CharField(max_length=250, unique=True)
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


@dataclasses.dataclass(frozen=True)
class SharedCustodySlot:
    guardian: Person = dataclasses.field(compare=False)
    date: datetime.date
    label: str = dataclasses.field(compare=False, default='')

    def __str__(self):
        if self.label:
            return '%s (%s)' % (self.guardian, self.label)
        else:
            return str(self.guardian)


class SharedCustodyAgenda(models.Model):
    first_guardian = models.ForeignKey(
        Person, verbose_name=_('First guardian'), on_delete=models.CASCADE, related_name='+'
    )
    second_guardian = models.ForeignKey(
        Person, verbose_name=_('Second guardian'), on_delete=models.CASCADE, related_name='+'
    )
    child = models.ForeignKey(Person, verbose_name=_('Child'), on_delete=models.CASCADE, related_name='+')
    date_start = models.DateField(_('Start'))
    date_end = models.DateField(_('End'), null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['child'], condition=Q(date_end__isnull=True), name='unique_child_no_date_end'
            )
        ]

    @property
    def label(self):
        return _('Custody agenda of %(first_guardian)s and %(second_guardian)s for %(child)s') % {
            'first_guardian': self.first_guardian,
            'second_guardian': self.second_guardian,
            'child': self.child,
        }

    def get_absolute_url(self):
        return reverse('chrono-manager-shared-custody-agenda-view', kwargs={'pk': self.pk})

    def get_settings_url(self):
        return reverse('chrono-manager-shared-custody-agenda-settings', kwargs={'pk': self.pk})

    def get_custody_slots(self, min_date, max_date):
        slots = set()

        periods = (
            self.periods.filter(date_start__lt=max_date, date_end__gt=min_date)
            .order_by('-holiday_rule')
            .select_related('holiday_rule__holiday', 'guardian')
        )
        for period in periods:
            date = max(period.date_start, min_date)
            label = period.holiday_rule.holiday.label if period.holiday_rule else ''
            while date < period.date_end and date < max_date:
                slots.add(SharedCustodySlot(guardian=period.guardian, date=date, label=label))
                date += datetime.timedelta(days=1)

        for rule in self.rules.all().select_related('guardian'):
            slots.update(rule.get_slots(min_date, max_date))

        slots = sorted(slots, key=lambda x: x.date)
        return slots

    def is_complete(self):
        day_counts = self.rules.aggregate(
            all_week=Coalesce(
                SumCardinality('days', filter=Q(weeks='')), 0, output_field=models.IntegerField()
            ),
            even_week=Coalesce(
                SumCardinality('days', filter=Q(weeks='even')), 0, output_field=models.IntegerField()
            ),
            odd_week=Coalesce(
                SumCardinality('days', filter=Q(weeks='odd')), 0, output_field=models.IntegerField()
            ),
        )
        even_week_day_count = day_counts['all_week'] + day_counts['even_week']
        odd_week_day_count = day_counts['all_week'] + day_counts['odd_week']
        return bool(even_week_day_count == 7 and odd_week_day_count == 7)

    def rule_overlaps(self, days, weeks, instance=None):
        qs = self.rules
        if hasattr(instance, 'pk'):
            qs = qs.exclude(pk=instance.pk)

        if weeks:
            qs = qs.filter(Q(weeks='') | Q(weeks=weeks))

        qs = qs.filter(days__overlap=days)
        return qs.exists()

    def holiday_rule_overlaps(self, holiday, years, periodicity, instance=None):
        qs = self.holiday_rules.filter(holiday=holiday)
        if hasattr(instance, 'pk'):
            qs = qs.exclude(pk=instance.pk)

        if years:
            qs = qs.filter(Q(years='') | Q(years=years))

        if periodicity == 'first-half':
            qs = qs.exclude(periodicity='second-half')
        elif periodicity == 'second-half':
            qs = qs.exclude(periodicity='first-half')
        elif periodicity == 'first-and-third-quarters':
            qs = qs.exclude(periodicity='second-and-fourth-quarters')
        elif periodicity == 'second-and-fourth-quarters':
            qs = qs.exclude(periodicity='first-and-third-quarters')

        return qs.exists()

    def period_overlaps(self, date_start, date_end, instance=None):
        qs = self.periods.filter(holiday_rule__isnull=True)
        if hasattr(instance, 'pk'):
            qs = qs.exclude(pk=instance.pk)

        qs = qs.extra(
            where=['(date_start, date_end) OVERLAPS (%s, %s)'],
            params=[date_start, date_end],
        )
        return qs.exists()


class SharedCustodyRule(models.Model):
    WEEK_CHOICES = [
        ('', pgettext_lazy('weeks', 'All')),
        ('even', pgettext_lazy('weeks', 'Even')),
        ('odd', pgettext_lazy('weeks', 'Odd')),
    ]

    agenda = models.ForeignKey(SharedCustodyAgenda, on_delete=models.CASCADE, related_name='rules')
    days = ArrayField(
        models.IntegerField(choices=WEEKDAY_CHOICES),
        verbose_name=_('Days'),
    )
    weeks = models.CharField(_('Weeks'), choices=WEEK_CHOICES, blank=True, max_length=16)
    guardian = models.ForeignKey(Person, verbose_name=_('Guardian'), on_delete=models.CASCADE)

    def get_slots(self, min_date, max_date):
        recurrence_rule = {
            'freq': WEEKLY,
            'byweekday': [i - 1 for i in self.days],
        }
        if self.weeks == 'odd':
            recurrence_rule['byweekno'] = list(range(1, 55, 2))
        elif self.weeks == 'even':
            recurrence_rule['byweekno'] = list(range(0, 54, 2))

        return [
            SharedCustodySlot(self.guardian, dt.date())
            for dt in rrule(dtstart=min_date, until=max_date - datetime.timedelta(days=1), **recurrence_rule)
        ]

    @property
    def label(self):
        days_count = len(self.days)
        if days_count == 7:
            repeat = _('daily')
        elif days_count > 1 and (self.days[-1] - self.days[0]) == days_count - 1:
            # days are contiguous
            repeat = _('from %(weekday)s to %(last_weekday)s') % {
                'weekday': str(ISO_WEEKDAYS[self.days[0]]),
                'last_weekday': str(ISO_WEEKDAYS[self.days[-1]]),
            }
        else:
            repeat = _('on %(weekdays)s') % {
                'weekdays': ', '.join([str(ISO_WEEKDAYS_PLURAL[i]) for i in self.days])
            }

        if self.weeks == 'odd':
            repeat = '%s, %s' % (repeat, _('on odd weeks'))
        elif self.weeks == 'even':
            repeat = '%s, %s' % (repeat, _('on even weeks'))

        return repeat

    class Meta:
        ordering = ['days__0', 'weeks']


class SharedCustodyHolidayRule(models.Model):
    YEAR_CHOICES = [
        ('', pgettext_lazy('years', 'All')),
        ('even', pgettext_lazy('years', 'Even')),
        ('odd', pgettext_lazy('years', 'Odd')),
    ]

    PERIODICITY_CHOICES = [
        ('first-half', _('First half')),
        ('second-half', _('Second half')),
        ('first-and-third-quarters', _('First and third quarters')),
        ('second-and-fourth-quarters', _('Second and fourth quarters')),
    ]

    agenda = models.ForeignKey(SharedCustodyAgenda, on_delete=models.CASCADE, related_name='holiday_rules')
    holiday = models.ForeignKey(TimePeriodExceptionGroup, verbose_name=_('Holiday'), on_delete=models.PROTECT)
    years = models.CharField(_('Years'), choices=YEAR_CHOICES, blank=True, max_length=16)
    periodicity = models.CharField(_('Periodicity'), choices=PERIODICITY_CHOICES, blank=True, max_length=32)
    guardian = models.ForeignKey(Person, verbose_name=_('Guardian'), on_delete=models.CASCADE)

    def update_or_create_periods(self):
        shared_custody_periods = []
        for exception in self.holiday.exceptions.all():
            date_start = localtime(exception.start_datetime).date()

            if self.years == 'even' and date_start.year % 2:
                continue
            if self.years == 'odd' and not date_start.year % 2:
                continue

            date_start_sunday = date_start + relativedelta(weekday=SU)
            date_end = localtime(exception.end_datetime).date()

            number_of_weeks = (date_end - date_start_sunday).days // 7

            periods = []
            if self.periodicity == 'first-half':
                date_end = date_start_sunday + datetime.timedelta(days=7 * (number_of_weeks // 2))
                periods = [(date_start, date_end)]
            elif self.periodicity == 'second-half':
                date_start = date_start_sunday + datetime.timedelta(days=7 * (number_of_weeks // 2))
                periods = [(date_start, date_end)]
            elif self.periodicity == 'first-and-third-quarters' and number_of_weeks >= 4:
                weeks_in_quarters = round(number_of_weeks / 4)
                first_quarters_date_end = date_start_sunday + datetime.timedelta(days=7 * weeks_in_quarters)
                third_quarters_date_start = date_start_sunday + datetime.timedelta(
                    days=7 * weeks_in_quarters * 2
                )
                third_quarters_date_end = date_start_sunday + datetime.timedelta(
                    days=7 * weeks_in_quarters * 3
                )
                periods = [
                    (date_start, first_quarters_date_end),
                    (third_quarters_date_start, third_quarters_date_end),
                ]
            elif self.periodicity == 'second-and-fourth-quarters' and number_of_weeks >= 4:
                weeks_in_quarters = round(number_of_weeks / 4)
                second_quarters_date_start = date_start_sunday + datetime.timedelta(
                    days=7 * weeks_in_quarters
                )
                second_quarters_date_end = date_start_sunday + datetime.timedelta(
                    days=7 * weeks_in_quarters * 2
                )
                fourth_quarters_date_start = date_start_sunday + datetime.timedelta(
                    days=7 * weeks_in_quarters * 3
                )
                periods = [
                    (second_quarters_date_start, second_quarters_date_end),
                    (fourth_quarters_date_start, date_end),
                ]
            elif not self.periodicity:
                periods = [(date_start, date_end)]

            for date_start, date_end in periods:
                shared_custody_periods.append(
                    SharedCustodyPeriod(
                        guardian=self.guardian,
                        agenda=self.agenda,
                        holiday_rule=self,
                        date_start=date_start,
                        date_end=date_end,
                    )
                )

        with transaction.atomic():
            SharedCustodyPeriod.objects.filter(
                guardian=self.guardian, agenda=self.agenda, holiday_rule=self
            ).delete()
            SharedCustodyPeriod.objects.bulk_create(shared_custody_periods)

    @property
    def label(self):
        label = self.holiday.label

        if self.periodicity == 'first-half':
            label = '%s, %s' % (label, _('the first half'))
        elif self.periodicity == 'second-half':
            label = '%s, %s' % (label, _('the second half'))
        elif self.periodicity == 'first-and-third-quarters':
            label = '%s, %s' % (label, _('the first and third quarters'))
        elif self.periodicity == 'second-and-fourth-quarters':
            label = '%s, %s' % (label, _('the second and fourth quarters'))

        if self.years == 'odd':
            label = '%s, %s' % (label, _('on odd years'))
        elif self.years == 'even':
            label = '%s, %s' % (label, _('on even years'))

        return label

    class Meta:
        ordering = ['holiday__label', 'guardian', 'years', 'periodicity']


class SharedCustodyPeriod(models.Model):
    agenda = models.ForeignKey(SharedCustodyAgenda, on_delete=models.CASCADE, related_name='periods')
    guardian = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='+')
    holiday_rule = models.ForeignKey(SharedCustodyHolidayRule, null=True, on_delete=models.CASCADE)
    date_start = models.DateField(_('Start'))
    date_end = models.DateField(_('End'))

    class Meta:
        ordering = ['date_start']

    def __str__(self):
        if self.date_end == self.date_start + datetime.timedelta(days=1):
            exc_repr = '%s' % date_format(self.date_start, 'SHORT_DATE_FORMAT')
        else:
            exc_repr = '%s → %s' % (
                date_format(self.date_start, 'SHORT_DATE_FORMAT'),
                date_format(self.date_end, 'SHORT_DATE_FORMAT'),
            )
        return '%s, %s' % (self.guardian, exc_repr)


class SharedCustodySettings(models.Model):
    management_role = models.ForeignKey(
        Group,
        blank=True,
        null=True,
        default=None,
        related_name='+',
        verbose_name=_('Management role'),
        on_delete=models.SET_NULL,
    )
    holidays_calendar = models.ForeignKey(
        UnavailabilityCalendar,
        verbose_name=_('Holidays calendar'),
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.SET_NULL,
    )

    def export_json(self):
        return {
            'management_role': self.management_role.name if self.management_role else None,
            'holidays_calendar': self.holidays_calendar.slug if self.holidays_calendar else None,
        }

    @classmethod
    def import_json(cls, data):
        if data.get('management_role'):
            data['management_role'] = Group.objects.get(name=data['management_role'])

        if data.get('holidays_calendar'):
            try:
                data['holidays_calendar'] = UnavailabilityCalendar.objects.get(slug=data['holidays_calendar'])
            except UnavailabilityCalendar.DoesNotExist:
                raise AgendaImportError(
                    _('The unavailability calendar "%s" does not exist.') % data['holidays_calendar']
                )

        cls.objects.update_or_create(defaults=data)

    @classmethod
    def get_singleton(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


def get_lease_expiration():
    return now() + datetime.timedelta(seconds=settings.CHRONO_LOCK_DURATION)


class Lease(models.Model):
    booking = models.OneToOneField(Booking, on_delete=models.CASCADE, verbose_name=_('Booking'))
    lock_code = models.CharField(verbose_name=_('Lock code'), max_length=64, blank=False)
    expiration_datetime = models.DateTimeField(
        verbose_name=_('Lease expiration time'), default=get_lease_expiration
    )

    class Meta:
        verbose_name = _('Lease')
        verbose_name_plural = _('Leases')

    @classmethod
    def clean(cls):
        '''Clean objects linked to leases.'''

        # Delete expired meeting's events, bookings and leases.'''
        Event.objects.filter(agenda__kind='meetings', booking__lease__expiration_datetime__lt=now()).delete()

        # Delete expired event's bookings and leases'''
        Booking.objects.filter(event__agenda__kind='events', lease__expiration_datetime__lt=now()).delete()
