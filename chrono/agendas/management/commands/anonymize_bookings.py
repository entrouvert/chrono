# chrono - agendas system
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import timedelta

from django.core.management.base import BaseCommand
from django.db.models import F

from chrono.agendas.models import Booking
from chrono.utils import timezone


class Command(BaseCommand):
    help = 'Anonymize bookings according to agendas delays'

    def handle(self, **options):
        bookings_to_anonymize = Booking.objects.filter(
            anonymization_datetime__isnull=True,
            event__start_datetime__lt=timezone.now()
            - timedelta(days=1) * F('event__agenda__anonymize_delay'),
        )
        Booking.anonymize_bookings(bookings_to_anonymize)
