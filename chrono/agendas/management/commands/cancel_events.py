# chrono - agendas system
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import timedelta

from django.core.management.base import BaseCommand
from django.db import transaction
from requests import RequestException

from chrono.agendas.models import Event, EventCancellationReport
from chrono.utils import timezone


class Command(BaseCommand):
    help = 'Cancel events and related bookings'

    def handle(self, **options):
        events_to_cancel = list(Event.objects.filter(cancellation_scheduled=True))

        # prevent overlapping cron conflicts in case actual cancelling takes a long time
        for event in events_to_cancel:
            event.cancellation_scheduled = False
            event.save()

        for event in events_to_cancel:
            errors = {}
            bookings = []
            for booking in event.booking_set.filter(cancellation_datetime__isnull=True).all():
                try:
                    booking.cancel(trigger_callback=True)
                except RequestException as e:
                    bookings.append(booking)
                    errors[booking.pk] = str(e)

            if not errors:
                event.cancelled = True
                event.save()
            else:
                with transaction.atomic():
                    report = EventCancellationReport.objects.create(event=event, booking_errors=errors)
                    report.bookings.set(bookings)

        # clean old reports
        EventCancellationReport.objects.filter(timestamp__lt=timezone.now() - timedelta(days=30)).delete()
