# chrono - agendas system
# Copyright (C) 2016-2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys

from django.core.management.base import BaseCommand
from django.db.models import Q

from chrono.agendas.models import ICSError, TimePeriodExceptionSource


class Command(BaseCommand):
    help = 'Synchronize time period exceptions from desks remote ics'

    def handle(self, **options):
        qs_url = TimePeriodExceptionSource.objects.filter(
            Q(ics_file='') | Q(ics_file__isnull=True), ics_url__isnull=False
        )
        qs_file = TimePeriodExceptionSource.objects.filter(ics_url__isnull=True).exclude(
            Q(ics_file='') | Q(ics_file__isnull=True)
        )
        for source in qs_url.union(qs_file):
            try:
                source.refresh_timeperiod_exceptions_from_ics()
            except ICSError as e:
                print(
                    'unable to create timeperiod exceptions for "%s": %s' % (source.desk, e), file=sys.stderr
                )
