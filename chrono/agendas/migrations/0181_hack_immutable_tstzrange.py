import os

from django.db import migrations

with open(
    os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'sql', 'hack_immutable_tstzrange.sql')
) as sql_file:
    sql_forwards = sql_file.read()

sql_backwards = """
DROP FUNCTION IF EXISTS hack_immutable_tstzrange;
"""


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0180_event_index'),
    ]

    operations = [migrations.RunSQL(sql=sql_forwards, reverse_sql=sql_backwards)]
