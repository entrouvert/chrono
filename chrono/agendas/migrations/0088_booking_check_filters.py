from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0087_booking_user_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='booking_check_filters',
            field=models.CharField(
                blank=True,
                help_text='Comma separated list of keys defined in extra_data.',
                max_length=250,
                verbose_name='Filters',
            ),
        ),
    ]
