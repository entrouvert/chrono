from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0122_user_check_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='user_check_type',
        ),
        migrations.RenameField(
            model_name='booking',
            old_name='new_user_check_type',
            new_name='user_check_type',
        ),
    ]
