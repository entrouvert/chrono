import os

from django.db import migrations, transaction
from django.db.utils import InternalError, OperationalError, ProgrammingError

with open(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        '..',
        'sql',
        'event_triggers_for_tstzrange_constraint.sql',
    )
) as sql_file:
    sql_triggers = sql_file.read()

sql_forwards = (
    """
-- Add technical columns
ALTER TABLE agendas_event ADD COLUMN IF NOT EXISTS _ignore_reason CHARACTER VARYING(20);

%s

-- Init legacy
UPDATE agendas_event SET _ignore_reason = 'history';
UPDATE
    agendas_event
SET
    _end_datetime = agendas_event.start_datetime + (agendas_meetingtype.duration ||' minutes')::interval
FROM
    agendas_meetingtype
WHERE
    agendas_meetingtype.id = agendas_event.meeting_type_id
    AND _end_datetime IS NULL;
"""
    % sql_triggers
)


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0128_event_date_range_constraint'),
    ]

    operations = [
        migrations.RunSQL(sql=sql_forwards, reverse_sql=migrations.RunSQL.noop),
    ]

    def _check_db(self, project_state, schema_editor):
        try:
            with transaction.atomic():
                try:
                    # will fail if extension does not exist and can not be created
                    schema_editor.execute('CREATE EXTENSION IF NOT EXISTS btree_Gist SCHEMA public')
                except (OperationalError, ProgrammingError):
                    # if no extension, do not create columns and triggers
                    return project_state
        except InternalError:
            return project_state

    def apply(self, project_state, schema_editor, *args, **kwargs):
        result = self._check_db(project_state, schema_editor)
        return result or super().apply(project_state, schema_editor, *args, **kwargs)

    def unapply(self, project_state, schema_editor, *args, **kwargs):
        result = self._check_db(project_state, schema_editor)
        return result or super().unapply(project_state, schema_editor, *args, **kwargs)
