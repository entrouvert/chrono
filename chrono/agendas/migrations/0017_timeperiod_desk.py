from django.db import migrations, models


def set_timeperiod_desk(apps, schema_editor):
    TimePeriod = apps.get_model('agendas', 'TimePeriod')
    Desk = apps.get_model('agendas', 'Desk')
    for time_period in TimePeriod.objects.all():
        desk, _ = Desk.objects.get_or_create(label='Guichet 1', slug='guichet-1', agenda=time_period.agenda)
        time_period.desk = desk
        time_period.save()


def unset_timeperiod_desk(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0016_desk'),
    ]

    operations = [
        migrations.AddField(
            model_name='timeperiod',
            name='desk',
            field=models.ForeignKey(to='agendas.Desk', null=True, on_delete=models.CASCADE),
        ),
        migrations.RunPython(set_timeperiod_desk, unset_timeperiod_desk),
        migrations.AlterField(
            model_name='timeperiod',
            name='desk',
            field=models.ForeignKey(to='agendas.Desk', on_delete=models.CASCADE),
        ),
        migrations.RemoveField(
            model_name='timeperiod',
            name='agenda',
        ),
    ]
