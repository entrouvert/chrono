import datetime

from django.db import migrations, models

from chrono.utils.timezone import utc


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0019_timeperiodexception'),
    ]

    operations = [
        migrations.AddField(
            model_name='desk',
            name='timeperiod_exceptions_remote_url',
            field=models.URLField(verbose_name='URL to fetch time period exceptions from', blank=True),
        ),
        migrations.AddField(
            model_name='timeperiodexception',
            name='external_id',
            field=models.CharField(max_length=256, verbose_name='External ID', blank=True),
        ),
        migrations.AddField(
            model_name='timeperiodexception',
            name='update_datetime',
            field=models.DateTimeField(
                default=datetime.datetime(2017, 11, 2, 10, 21, 1, 826837, tzinfo=utc), auto_now=True
            ),
            preserve_default=False,
        ),
    ]
