from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0070_auto_20201202_1834'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='user_was_present',
            field=models.BooleanField(null=True),
        ),
    ]
