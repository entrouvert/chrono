import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0118_booking_out_of_min_delay'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='AbsenceReason',
            new_name='CheckType',
        ),
        migrations.RenameModel(
            old_name='AbsenceReasonGroup',
            new_name='CheckTypeGroup',
        ),
        migrations.RenameField(
            model_name='agenda',
            old_name='absence_reasons_group',
            new_name='check_type_group',
        ),
        migrations.AlterField(
            model_name='agenda',
            name='check_type_group',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to='agendas.CheckTypeGroup',
                verbose_name='Check type group',
            ),
        ),
        migrations.AlterField(
            model_name='checktype',
            name='group',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='check_types',
                to='agendas.CheckTypeGroup',
            ),
        ),
        migrations.RenameField(
            model_name='booking',
            old_name='user_absence_reason',
            new_name='user_check_type',
        ),
    ]
