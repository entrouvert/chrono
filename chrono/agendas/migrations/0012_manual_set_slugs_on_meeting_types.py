from django.db import migrations
from django.utils.text import slugify


def set_slug_on_meeting_types(apps, schema_editor):
    MeetingType = apps.get_model('agendas', 'MeetingType')
    for meeting_type in MeetingType.objects.all():
        meeting_type.slug = slugify(meeting_type.label)
        meeting_type.save()


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0011_meetingtype_slug'),
    ]

    operations = [
        migrations.RunPython(set_slug_on_meeting_types, lambda x, y: None),
    ]
