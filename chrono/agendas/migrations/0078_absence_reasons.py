import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0077_meetings_default_view'),
    ]

    operations = [
        migrations.CreateModel(
            name='AbsenceReason',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('label', models.CharField(max_length=150, verbose_name='Label')),
            ],
            options={
                'ordering': ['label'],
            },
        ),
        migrations.CreateModel(
            name='AbsenceReasonGroup',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('slug', models.SlugField(max_length=160, unique=True, verbose_name='Identifier')),
                ('label', models.CharField(max_length=150, verbose_name='Label')),
            ],
            options={
                'ordering': ['label'],
            },
        ),
        migrations.AddField(
            model_name='absencereason',
            name='group',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to='agendas.AbsenceReasonGroup',
                related_name='absence_reasons',
            ),
        ),
        migrations.AddField(
            model_name='agenda',
            name='absence_reasons_group',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to='agendas.AbsenceReasonGroup',
                verbose_name='Absence reasons group',
            ),
        ),
    ]
