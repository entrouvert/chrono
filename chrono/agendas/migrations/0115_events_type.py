from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0114_auto_20220324_1702'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventsType',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('slug', models.SlugField(max_length=160, unique=True, verbose_name='Identifier')),
                ('label', models.CharField(max_length=150, verbose_name='Label')),
                ('custom_fields', models.JSONField(blank=True, default=list)),
            ],
        ),
    ]
