from django.db import migrations


def create_source(apps, schema_editor):
    Desk = apps.get_model('agendas', 'Desk')
    TimePeriodExceptionSource = apps.get_model('agendas', 'TimePeriodExceptionSource')
    for desk in Desk.objects.exclude(timeperiod_exceptions_remote_url=''):
        # create a source for each remote url
        source = TimePeriodExceptionSource.objects.create(
            desk=desk, ics_url=desk.timeperiod_exceptions_remote_url
        )
        # clear timeperiod_exceptions_remote_url
        desk.timeperiod_exceptions_remote_url = None
        desk.save()
        # attach exceptions to the created source
        desk.timeperiodexception_set.filter(external_id__isnull=False).exclude(external_id='').update(
            source=source
        )


def init_remote_url(apps, schema_editor):
    Desk = apps.get_model('agendas', 'Desk')
    TimePeriodExceptionSource = apps.get_model('agendas', 'TimePeriodExceptionSource')
    for source in TimePeriodExceptionSource.objects.filter(ics_url__isnull=False):
        # set timeperiod_exceptions_remote_url
        source.desk.timeperiod_exceptions_remote_url = source.ics_url
        source.desk.save()
        # unlink exceptions
        source.timeperiodexception_set.update(source=None)
        # delete the source
        source.delete()
    # reset remote_url
    for desk in Desk.objects.filter(timeperiod_exceptions_remote_url__isnull=True):
        desk.timeperiod_exceptions_remote_url = ''
        desk.save()


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0033_timeperiodexceptionsource'),
    ]

    operations = [
        migrations.RunPython(create_source, init_remote_url),
    ]
