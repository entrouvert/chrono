import uuid

from django.db import migrations
from django.utils.text import slugify


def generate_slug(instance, seen_slugs, **query_filters):
    base_slug = slugify(instance.label or ('%s-event' % instance.agenda.label))
    slug = base_slug
    i = 1
    while True:
        if slug not in seen_slugs:
            queryset = instance._meta.model.objects.filter(slug=slug, **query_filters).exclude(pk=instance.pk)
            if not queryset.exists():
                break
            seen_slugs.add(slug)
        slug = '%s-%s' % (base_slug, i)
        i += 1
    seen_slugs.add(slug)
    return slug


def set_slug_on_events(apps, schema_editor):
    Event = apps.get_model('agendas', 'Event')
    for event in Event.objects.filter(agenda__kind='meetings', slug__isnull=True):
        # set slug on meeting events
        event.slug = str(uuid.uuid4())
        event.save(update_fields=['slug'])
    seen_slugs = set()
    for event in Event.objects.filter(agenda__kind='events', slug__isnull=True).order_by('-pk'):
        event.slug = generate_slug(event, seen_slugs)
        event.save(update_fields=['slug'])


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0048_meeting_type_deleted_flag'),
    ]

    operations = [
        migrations.RunPython(set_slug_on_events, lambda x, y: None),
    ]
