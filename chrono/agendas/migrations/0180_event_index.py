from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0179_subscription_created_at_subscription_updated_at'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='booking',
            index=models.Index(fields=['event', 'user_external_id'], name='agendas_boo_event_i_18b80e_idx'),
        ),
    ]
