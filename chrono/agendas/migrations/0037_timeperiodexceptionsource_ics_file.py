from django.db import migrations, models

import chrono.agendas.models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0036_auto_20191223_1758'),
    ]

    operations = [
        migrations.AddField(
            model_name='timeperiodexceptionsource',
            name='ics_file',
            field=models.FileField(blank=True, null=True, upload_to=chrono.agendas.models.ics_directory_path),
        ),
    ]
