from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0100_event_dml'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='publication_datetime',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Publication date/time'),
        ),
    ]
