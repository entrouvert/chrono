import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0120_check_types'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='new_user_check_type',
            field=models.ForeignKey(
                blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='agendas.CheckType'
            ),
        ),
    ]
