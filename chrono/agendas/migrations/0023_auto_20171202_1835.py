from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0022_auto_20171202_1828'),
    ]

    operations = [
        migrations.AlterField(
            model_name='desk',
            name='timeperiod_exceptions_remote_url',
            field=models.URLField(
                max_length=500, verbose_name='URL to fetch time period exceptions from', blank=True
            ),
        ),
    ]
