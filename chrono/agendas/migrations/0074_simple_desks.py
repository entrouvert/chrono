from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0073_auto_20210125_1800'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='desk_simple_management',
            field=models.BooleanField(default=False, verbose_name='Global desk management'),
        ),
    ]
