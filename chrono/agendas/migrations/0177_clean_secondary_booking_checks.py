from django.db import migrations


def clean_secondary_booking_checks(apps, schema_editor):
    BookingCheck = apps.get_model('agendas', 'BookingCheck')
    BookingCheck.objects.filter(booking__primary_booking__isnull=False).delete()


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0176_minimal_cancellation_delays'),
    ]

    operations = [
        migrations.RunPython(clean_secondary_booking_checks, migrations.RunPython.noop),
    ]
