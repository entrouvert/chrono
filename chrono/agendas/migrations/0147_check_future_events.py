from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0146_event_bigautofield'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='enable_check_for_future_events',
            field=models.BooleanField(
                default=False, verbose_name='Enable the check of bookings when event has not passed'
            ),
        ),
    ]
