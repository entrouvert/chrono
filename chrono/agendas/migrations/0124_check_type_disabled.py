from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0123_user_check_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='checktype',
            name='disabled',
            field=models.BooleanField(default=False, verbose_name='Disabled'),
        ),
    ]
