from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0020_auto_20171102_1021'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='backoffice_url',
            field=models.URLField(blank=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='label',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='user_name',
            field=models.CharField(max_length=250, blank=True),
        ),
    ]
