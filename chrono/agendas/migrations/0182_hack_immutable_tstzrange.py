import django.contrib.postgres.indexes
import django.db.models.expressions
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0181_hack_immutable_tstzrange'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='event',
            index=django.contrib.postgres.indexes.GistIndex(
                django.db.models.expressions.F('agenda'),
                django.db.models.expressions.Func(
                    django.db.models.expressions.F('start_datetime'),
                    django.db.models.expressions.F('duration'),
                    function='hack_immutable_tstzrange',
                ),
                condition=models.Q(('cancelled', False), ('recurrence_days__isnull', True)),
                name='hack_immutable_tstzrange_idx',
            ),
        ),
    ]
