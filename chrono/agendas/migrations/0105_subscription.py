from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0104_subscription'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='extra_data',
            field=models.JSONField(null=True),
        ),
        migrations.AddField(
            model_name='subscription',
            name='user_email',
            field=models.EmailField(blank=True, max_length=254),
        ),
        migrations.AddField(
            model_name='subscription',
            name='user_first_name',
            field=models.CharField(default='', max_length=250),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='subscription',
            name='user_last_name',
            field=models.CharField(default='', max_length=250),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='subscription',
            name='user_phone_number',
            field=models.CharField(blank=True, max_length=16),
        ),
    ]
