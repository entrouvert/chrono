from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0136_remove_check_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agenda',
            name='check_type_group',
        ),
        migrations.DeleteModel(
            name='CheckType',
        ),
        migrations.DeleteModel(
            name='CheckTypeGroup',
        ),
    ]
