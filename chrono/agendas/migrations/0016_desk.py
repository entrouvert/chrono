from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0015_auto_20170628_1137'),
    ]

    operations = [
        migrations.CreateModel(
            name='Desk',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('label', models.CharField(max_length=150, verbose_name='Label')),
                ('slug', models.SlugField(max_length=150, verbose_name='Identifier')),
                ('agenda', models.ForeignKey(to='agendas.Agenda', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['label', 'slug'],
            },
        ),
    ]
