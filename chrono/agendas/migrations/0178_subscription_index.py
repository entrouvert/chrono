from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0177_clean_secondary_booking_checks'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='subscription',
            index=models.Index(fields=['agenda', 'user_external_id'], name='agendas_sub_agenda__a2e21e_idx'),
        ),
    ]
