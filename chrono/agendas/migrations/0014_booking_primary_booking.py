from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0013_auto_20161028_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='primary_booking',
            field=models.ForeignKey(
                related_name='secondary_booking_set',
                to='agendas.Booking',
                null=True,
                on_delete=models.CASCADE,
            ),
        ),
    ]
