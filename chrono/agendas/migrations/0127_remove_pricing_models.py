from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0126_pricing'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='checktype',
            name='pricing',
        ),
        migrations.RemoveField(
            model_name='checktype',
            name='pricing_rate',
        ),
    ]
