from dateutil.rrule import DAILY, WEEKLY
from django.db import migrations, transaction
from django.db.utils import ProgrammingError


def migrate_recurrence_fields(apps, schema_editor):
    Event = apps.get_model('agendas', 'Event')

    for event in Event.objects.filter(recurrence_rule__isnull=False):
        if event.recurrence_rule['freq'] == DAILY:
            event.recurrence_days = list(range(7))
        elif event.recurrence_rule['freq'] == WEEKLY:
            event.recurrence_days = event.recurrence_rule['byweekday']
        event.recurrence_week_interval = event.recurrence_rule.get('interval', 1)
        event.save()


def reverse_migrate_recurrence_fields(apps, schema_editor):
    Event = apps.get_model('agendas', 'Event')

    for event in Event.objects.filter(recurrence_days__isnull=False):
        rrule = {}
        if event.recurrence_days == list(range(7)):
            event.repeat = 'daily'
            rrule['freq'] = DAILY
        else:
            rrule['freq'] = WEEKLY
            rrule['byweekday'] = event.recurrence_days
            if event.recurrence_days == list(range(5)):
                event.repeat = 'weekdays'
            elif event.recurrence_week_interval == 2:
                event.repeat = '2-weeks'
                rrule['interval'] = 2
            else:
                event.repeat = 'weekly'
        event.recurrence_rule = rrule
        event.save()


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0092_change_recurrence_fields'),
    ]

    operations = [
        migrations.RunPython(migrate_recurrence_fields, reverse_migrate_recurrence_fields),
    ]

    def _check_db(self, project_state, schema_editor):
        try:
            with transaction.atomic():
                # check if the column exists
                schema_editor.execute('SELECT recurrence_rule FROM agendas_event')
        except ProgrammingError:
            # if not exists, ignore the migration
            return project_state

    def apply(self, project_state, schema_editor, *args, **kwargs):
        result = self._check_db(project_state, schema_editor)
        return result or super().apply(project_state, schema_editor, *args, **kwargs)
