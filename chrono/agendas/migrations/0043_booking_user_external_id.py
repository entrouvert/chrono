from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0042_auto_20200503_1231'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='user_external_id',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
