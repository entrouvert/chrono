from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0086_booking_user_block_template'),
    ]

    operations = [
        migrations.RenameField(
            model_name='booking',
            old_name='user_name',
            new_name='user_last_name',
        ),
        migrations.AddField(
            model_name='booking',
            name='user_first_name',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
