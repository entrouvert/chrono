from django.db import migrations
from django.utils.text import slugify


def generate_slug(instance, **query_filters):
    base_slug = slugify(instance.label)
    slug = base_slug
    i = 1
    while True:
        queryset = instance._meta.model.objects.filter(slug=slug, **query_filters).exclude(pk=instance.pk)
        if not queryset.exists():
            break
        slug = '%s-%s' % (base_slug, i)
        i += 1
    return slug


def set_slug_on_absence_reasons(apps, schema_editor):
    AbsenceReason = apps.get_model('agendas', 'AbsenceReason')
    for reason in AbsenceReason.objects.all().order_by('-pk'):
        if (
            reason.slug
            and not AbsenceReason.objects.filter(slug=reason.slug, group=reason.group)
            .exclude(pk=reason.pk)
            .exists()
        ):
            continue
        reason.slug = generate_slug(reason, group=reason.group)
        reason.save(update_fields=['slug'])


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0083_reason_slug'),
    ]

    operations = [
        migrations.RunPython(set_slug_on_absence_reasons, migrations.RunPython.noop),
    ]
