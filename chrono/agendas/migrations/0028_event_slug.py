from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0027_event_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='slug',
            field=models.SlugField(
                default=None, null=True, blank=True, max_length=160, verbose_name='Identifier'
            ),
            preserve_default=False,
        ),
    ]
