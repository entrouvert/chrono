from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0082_text_to_jsonb'),
    ]

    operations = [
        migrations.AddField(
            model_name='absencereason',
            name='slug',
            field=models.SlugField(max_length=160, null=True, verbose_name='Identifier'),
        ),
        migrations.AlterUniqueTogether(
            name='absencereason',
            unique_together=['group', 'slug'],
        ),
    ]
