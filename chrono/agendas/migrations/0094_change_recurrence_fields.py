from django.db import migrations, transaction
from django.db.utils import ProgrammingError


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0093_change_recurrence_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='recurrence_rule',
        ),
        migrations.RemoveField(
            model_name='event',
            name='repeat',
        ),
    ]

    def _check_db(self, project_state, schema_editor):
        try:
            with transaction.atomic():
                # check if the column exists
                schema_editor.execute('SELECT recurrence_rule FROM agendas_event')
        except ProgrammingError:
            # if not exists, ignore the migration
            return project_state

    def apply(self, project_state, schema_editor, *args, **kwargs):
        result = self._check_db(project_state, schema_editor)
        return result or super().apply(project_state, schema_editor, *args, **kwargs)
