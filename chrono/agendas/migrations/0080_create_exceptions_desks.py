# Generated by Django 1.11.18 on 2021-01-27 16:46


from django.db import migrations


def create_exceptions_desk(apps, schema_editor):
    Agenda = apps.get_model('agendas', 'Agenda')
    Desk = apps.get_model('agendas', 'Desk')
    desks = []

    for agenda in Agenda.objects.filter(kind='events'):
        desks.append(Desk(agenda=agenda, slug='_exceptions_holder'))
    Desk.objects.bulk_create(desks)


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0079_auto_20210428_1533'),
    ]

    operations = [
        migrations.RunPython(create_exceptions_desk, migrations.RunPython.noop),
    ]
