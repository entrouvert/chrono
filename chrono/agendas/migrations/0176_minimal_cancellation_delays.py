import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0175_agenda_edit_role'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='minimal_cancellation_delay',
            field=models.PositiveIntegerField(
                blank=True,
                default=None,
                null=True,
                validators=[django.core.validators.MaxValueValidator(10000)],
                verbose_name='Minimal cancellation delay (in days)',
            ),
        ),
        migrations.AddField(
            model_name='agenda',
            name='minimal_cancellation_delay_in_working_days',
            field=models.BooleanField(
                default=False, verbose_name='Minimal cancellation delay in working days'
            ),
        ),
    ]
