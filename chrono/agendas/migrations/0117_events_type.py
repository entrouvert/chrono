import django.core.serializers.json
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0116_events_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='custom_fields',
            field=models.JSONField(
                blank=True, default=dict, encoder=django.core.serializers.json.DjangoJSONEncoder
            ),
        ),
    ]
