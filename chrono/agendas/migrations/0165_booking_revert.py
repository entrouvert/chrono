from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0164_alter_bookingcheck_booking'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='previous_state',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='request_uuid',
            field=models.UUIDField(editable=False, null=True),
        ),
    ]
