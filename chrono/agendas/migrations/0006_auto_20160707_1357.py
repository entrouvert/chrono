import datetime

from django.db import migrations, models

from chrono.utils.timezone import utc


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0005_event_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='creation_datetime',
            field=models.DateTimeField(
                default=datetime.datetime(2016, 7, 7, 13, 57, 47, 975893, tzinfo=utc), auto_now_add=True
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='booking',
            name='in_waiting_list',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='event',
            name='full',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='event',
            name='waiting_list_places',
            field=models.PositiveIntegerField(default=0, verbose_name='Places in waiting list'),
        ),
    ]
