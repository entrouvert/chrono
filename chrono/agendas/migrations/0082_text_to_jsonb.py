from django.db import migrations

from chrono.utils.db import EnsureJsonbType


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0081_recurrenceexceptionsreport'),
    ]

    operations = [
        EnsureJsonbType(model_name='booking', field_name='extra_data'),
        EnsureJsonbType(model_name='event', field_name='recurrence_rule'),
        EnsureJsonbType(model_name='eventcancellationreport', field_name='booking_errors'),
    ]
