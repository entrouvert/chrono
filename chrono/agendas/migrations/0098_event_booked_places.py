from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0097_min_booking_delay_working_days'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='booked_places',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='event',
            name='booked_waiting_list_places',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
