import os

from django.db import migrations

with open(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)), '..', 'sql', 'event_booked_places_and_full_triggers.sql'
    )
) as sql_file:
    sql_forwards = sql_file.read()


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0154_partial_booking_fields'),
    ]

    operations = [migrations.RunSQL(sql=sql_forwards, reverse_sql=migrations.RunSQL.noop)]
