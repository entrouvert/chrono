from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0182_hack_immutable_tstzrange'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='start_datetime',
            field=models.DateTimeField(db_index=True, verbose_name='Date/time'),
        ),
        migrations.AddIndex(
            model_name='booking',
            index=models.Index(fields=['user_external_id', 'event'], name='agendas_boo_user_ex_68a702_idx'),
        ),
    ]
