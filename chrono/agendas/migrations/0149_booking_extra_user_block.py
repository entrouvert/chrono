from django.db import migrations, models

import chrono.agendas.models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0148_agenda_minimal_booking_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='booking_extra_user_block_template',
            field=models.TextField(
                blank=True,
                help_text='Displayed on check page',
                validators=[chrono.agendas.models.django_template_validator],
                verbose_name='Extra user block template',
            ),
        ),
    ]
