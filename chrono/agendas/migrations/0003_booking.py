from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0002_event'),
    ]

    operations = [
        migrations.CreateModel(
            name='Booking',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('extra_data', models.JSONField(null=True)),
                ('event', models.ForeignKey(to='agendas.Event', on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]
