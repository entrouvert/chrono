from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0102_publication_datetime'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='publication_date',
        ),
    ]
