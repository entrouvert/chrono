import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0032_auto_20191127_0919'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimePeriodExceptionSource',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('ics_filename', models.CharField(max_length=256, null=True)),
                ('ics_url', models.URLField(null=True, max_length=500)),
                ('desk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='agendas.Desk')),
            ],
        ),
        migrations.AddField(
            model_name='timeperiodexception',
            name='source',
            field=models.ForeignKey(
                null=True, on_delete=django.db.models.deletion.CASCADE, to='agendas.TimePeriodExceptionSource'
            ),
        ),
        migrations.AlterField(
            model_name='desk',
            name='timeperiod_exceptions_remote_url',
            field=models.URLField(
                blank=True, max_length=500, null=True, verbose_name='URL to fetch time period exceptions from'
            ),
        ),
        migrations.AlterField(
            model_name='timeperiodexception',
            name='external_id',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='External ID'),
        ),
    ]
