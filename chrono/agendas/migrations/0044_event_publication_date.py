from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0043_booking_user_external_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='publication_date',
            field=models.DateField(blank=True, null=True, verbose_name='Publication date'),
        ),
    ]
