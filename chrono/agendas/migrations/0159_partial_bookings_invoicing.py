import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0158_partial_booking_check_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='invoicing_tolerance',
            field=models.PositiveSmallIntegerField(
                default=0, validators=[django.core.validators.MaxValueValidator(59)], verbose_name='Tolerance'
            ),
        ),
        migrations.AddField(
            model_name='agenda',
            name='invoicing_unit',
            field=models.CharField(
                choices=[
                    ('hour', 'Per hour'),
                    ('half_hour', 'Per half hour'),
                    ('quarter', 'Per quarter-hour'),
                    ('minute', 'Per minute'),
                ],
                default='hour',
                max_length=10,
                verbose_name='Invoicing',
            ),
        ),
    ]
