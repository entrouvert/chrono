# Generated by Django 1.11.18 on 2020-09-03 08:40

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0058_timeperiodexception_external'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='almost_full',
            field=models.BooleanField(default=False),
        ),
    ]
