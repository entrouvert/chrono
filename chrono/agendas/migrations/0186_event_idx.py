import django.db.models.expressions
import django.db.models.functions.comparison
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0185_agenda_allow_backoffice_booking'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='event',
            index=models.Index(
                django.db.models.expressions.F('agenda'),
                django.db.models.functions.comparison.Least(
                    django.db.models.expressions.F('start_datetime'),
                    django.db.models.functions.comparison.Coalesce(
                        django.db.models.expressions.F('publication_datetime'),
                        django.db.models.functions.comparison.Cast(
                            django.db.models.expressions.Value('-infinity'),
                            output_field=models.DateTimeField(),
                        ),
                    ),
                ),
                condition=models.Q(('cancelled', False), ('recurrence_days__isnull', True)),
                include=('id',),
                name='event_least_dates_idx',
            ),
        ),
    ]
