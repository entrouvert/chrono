from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0018_event_desk'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimePeriodException',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'label',
                    models.CharField(max_length=150, null=True, verbose_name='Optional Label', blank=True),
                ),
                ('start_datetime', models.DateTimeField(verbose_name='Exception start time')),
                ('end_datetime', models.DateTimeField(verbose_name='Exception end time')),
                ('desk', models.ForeignKey(to='agendas.Desk', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['start_datetime'],
            },
        ),
    ]
