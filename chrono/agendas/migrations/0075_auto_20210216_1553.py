# Generated by Django 1.11.18 on 2021-02-16 14:53

import django.db.models.deletion
from django.contrib.postgres.fields import JSONField
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0074_simple_desks'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='primary_event',
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name='recurrences',
                to='agendas.Event',
            ),
        ),
        migrations.AddField(
            model_name='event',
            name='recurrence_rule',
            field=JSONField(null=True, verbose_name='Recurrence rule', blank=True),
        ),
        migrations.AddField(
            model_name='event',
            name='repeat',
            field=models.CharField(
                blank=True,
                choices=[
                    ('daily', 'Daily'),
                    ('weekly', 'Weekly'),
                    ('2-weeks', 'Once every two weeks'),
                    ('weekdays', 'Every weekdays (Monday to Friday)'),
                ],
                max_length=16,
                verbose_name='Repeat',
            ),
        ),
    ]
