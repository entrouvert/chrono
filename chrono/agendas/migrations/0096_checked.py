from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0095_checked'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='disable_check_update',
            field=models.BooleanField(
                default=False, verbose_name='Prevent the check of bookings when event was marked as checked'
            ),
        ),
    ]
