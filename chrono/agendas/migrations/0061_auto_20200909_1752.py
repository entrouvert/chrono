# Generated by Django 1.11.18 on 2020-09-09 15:52

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0060_auto_20200903_1041'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='form_url',
            field=models.URLField(blank=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='user_email',
            field=models.EmailField(blank=True, max_length=254),
        ),
        migrations.AddField(
            model_name='booking',
            name='user_phone_number',
            field=models.CharField(blank=True, max_length=16),
        ),
    ]
