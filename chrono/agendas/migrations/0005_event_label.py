from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0004_booking_cancellation_datetime'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='label',
            field=models.CharField(
                help_text='Optional label to identify this date.',
                max_length=50,
                null=True,
                blank=True,
                verbose_name='Label',
            ),
        ),
    ]
