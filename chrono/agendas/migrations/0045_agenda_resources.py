from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0044_event_publication_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='Resource',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('slug', models.SlugField(max_length=160, unique=True, verbose_name='Identifier')),
                ('label', models.CharField(max_length=150, verbose_name='Label')),
                (
                    'description',
                    models.TextField(
                        blank=True, help_text='Optional description.', verbose_name='Description'
                    ),
                ),
            ],
            options={'ordering': ['label']},
        ),
        migrations.AddField(
            model_name='agenda',
            name='resources',
            field=models.ManyToManyField(to='agendas.Resource'),
        ),
        migrations.AddField(
            model_name='event',
            name='resources',
            field=models.ManyToManyField(to='agendas.Resource'),
        ),
    ]
