from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0084_reason_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='absencereason',
            name='slug',
            field=models.SlugField(max_length=160, verbose_name='Identifier'),
        ),
    ]
