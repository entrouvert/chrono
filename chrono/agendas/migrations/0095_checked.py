from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0094_change_recurrence_fields'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='mark_event_checked_auto',
            field=models.BooleanField(
                default=False,
                verbose_name='Automatically mark event as checked when all bookings have been checked',
            ),
        ),
        migrations.AddField(
            model_name='event',
            name='checked',
            field=models.BooleanField(default=False),
        ),
    ]
