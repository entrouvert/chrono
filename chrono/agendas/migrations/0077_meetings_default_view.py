from django.db import migrations


def set_default_view(apps, schema_editor):
    Agenda = apps.get_model('agendas', 'Agenda')
    Agenda.objects.exclude(kind='events').update(default_view='day')


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0076_event_recurrence_end_date'),
    ]

    operations = [
        migrations.RunPython(set_default_view, migrations.RunPython.noop),
    ]
