from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0050_event_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='default_view',
            field=models.CharField(
                choices=[('day', 'Day view'), ('month', 'Month view'), ('open_events', 'Open events')],
                default='month',
                max_length=20,
                verbose_name='Default view',
            ),
        ),
    ]
