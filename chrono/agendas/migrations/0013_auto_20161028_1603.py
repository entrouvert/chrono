from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0012_manual_set_slugs_on_meeting_types'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meetingtype',
            name='slug',
            field=models.SlugField(verbose_name='Identifier'),
        ),
    ]
