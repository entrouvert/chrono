from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0135_remove_check_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='user_check_type',
        ),
    ]
