from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0150_event_check_locked'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='invoiced',
            field=models.BooleanField(default=False),
        ),
    ]
