from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0089_agenda_event_display_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agenda',
            name='default_view',
            field=models.CharField(
                choices=[
                    ('day', 'Day view'),
                    ('week', 'Week view'),
                    ('month', 'Month view'),
                    ('open_events', 'Open events'),
                ],
                max_length=20,
                verbose_name='Default view',
            ),
        ),
    ]
