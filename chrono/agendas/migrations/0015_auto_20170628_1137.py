from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0014_booking_primary_booking'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agenda',
            name='label',
            field=models.CharField(max_length=150, verbose_name='Label'),
        ),
        migrations.AlterField(
            model_name='event',
            name='label',
            field=models.CharField(
                help_text='Optional label to identify this date.',
                max_length=150,
                null=True,
                verbose_name='Label',
                blank=True,
            ),
        ),
        migrations.AlterField(
            model_name='meetingtype',
            name='label',
            field=models.CharField(max_length=150, verbose_name='Label'),
        ),
    ]
