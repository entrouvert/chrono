from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0006_auto_20160707_1357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agenda',
            name='label',
            field=models.CharField(max_length=100, verbose_name='Label'),
        ),
    ]
