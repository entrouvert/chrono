from django.db import migrations, models

import chrono.agendas.models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0049_event_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='slug',
            field=models.SlugField(
                blank=True,
                max_length=160,
                validators=[chrono.agendas.models.validate_not_digit],
                verbose_name='Identifier',
            ),
            preserve_default=False,
        ),
    ]
