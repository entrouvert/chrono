from collections import defaultdict

from django.db import migrations


def forwards(apps, schema_editor):
    Booking = apps.get_model('agendas', 'Booking')
    for booking in Booking.objects.filter(user_check_type__isnull=False):
        booking.user_check_type_slug = booking.user_check_type.slug
        booking.user_check_type_label = booking.user_check_type.label
        booking.save()


def backwards(apps, schema_editor):
    Booking = apps.get_model('agendas', 'Booking')
    Agenda = apps.get_model('agendas', 'Agenda')
    check_types_by_agenda_id = defaultdict(list)
    for agenda in Agenda.objects.filter(kind='events'):
        if not agenda.check_type_group:
            continue
        for check_type in agenda.check_type_group.check_types.all():
            check_types_by_agenda_id[agenda.pk].append(check_type)
    for booking in Booking.objects.filter(user_check_type_slug__isnull=False, user_was_present__isnull=False):
        if booking.event.agenda_id not in check_types_by_agenda_id:
            # no check_types for this agenda
            continue
        for check_type in check_types_by_agenda_id[booking.event.agenda_id]:
            if check_type.kind == 'absence' and booking.user_was_present is True:
                continue
            if check_type.kind == 'presence' and booking.user_was_present is False:
                continue
            if check_type.slug == booking.user_check_type_slug:
                booking.user_check_type = check_type
                booking.save()
                break


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0134_remove_check_type'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=backwards),
    ]
