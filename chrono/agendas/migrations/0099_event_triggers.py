import os

from django.db import migrations

with open(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)), '..', 'sql', 'event_booked_places_and_full_triggers.sql'
    )
) as sql_file:
    sql_forwards = sql_file.read()

sql_backwards = """
DROP TRIGGER IF EXISTS update_event_full_fields_trigger ON agendas_event;
DROP TRIGGER IF EXISTS update_event_places_fields_trigger ON agendas_booking;

DROP FUNCTION IF EXISTS update_event_full_fields;
DROP FUNCTION IF EXISTS update_event_places_fields;
"""


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0098_event_booked_places'),
    ]

    operations = [migrations.RunSQL(sql=sql_forwards, reverse_sql=sql_backwards)]
