from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0021_auto_20171126_1330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='agenda',
            name='slug',
            field=models.SlugField(max_length=160, verbose_name='Identifier'),
        ),
        migrations.AlterField(
            model_name='desk',
            name='slug',
            field=models.SlugField(max_length=160, verbose_name='Identifier'),
        ),
        migrations.AlterField(
            model_name='meetingtype',
            name='slug',
            field=models.SlugField(max_length=160, verbose_name='Identifier'),
        ),
    ]
