from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Agenda',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('label', models.CharField(max_length=50, verbose_name='Label')),
                ('slug', models.SlugField(verbose_name='Identifier')),
            ],
            options={
                'ordering': ['label'],
            },
            bases=(models.Model,),
        ),
    ]
