# Generated by Django 4.2.15 on 2025-02-19 10:15

import os

from django.db import migrations

sql_forwards = '''
DROP TRIGGER IF EXISTS set_end_datetime_trg ON agendas_event;
DROP FUNCTION IF EXISTS set_end_datetime;
'''

with open(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        '..',
        'sql',
        'event_triggers_for_tstzrange_constraint.sql',
    )
) as sql_file:
    sql_triggers = sql_file.read()

sql_backwards = (
    '''
%s

CREATE OR REPLACE FUNCTION set_end_datetime() RETURNS TRIGGER AS $$
    BEGIN
        IF NEW.meeting_type_id IS NULL THEN
            NEW._end_datetime = NULL;
        ELSE
            NEW._end_datetime = NEW.start_datetime + ((
                SELECT mt.duration FROM agendas_meetingtype mt WHERE mt.id = NEW.meeting_type_id) ||' minutes')::interval;
        END IF;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS set_end_datetime_trg ON agendas_event;
CREATE TRIGGER set_end_datetime_trg
    BEFORE INSERT OR UPDATE ON agendas_event
    FOR EACH ROW
    EXECUTE PROCEDURE set_end_datetime();
'''
    % sql_triggers
)


class Migration(migrations.Migration):

    dependencies = [
        ('agendas', '0188_agenda_minimal_time_between_bookings'),
    ]

    operations = [
        migrations.RunSQL(sql=sql_forwards, reverse_sql=sql_triggers),
    ]
