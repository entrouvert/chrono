# Generated by Django 1.11.18 on 2019-12-09 14:24

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0034_initial_source'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='desk',
            name='timeperiod_exceptions_remote_url',
        ),
        migrations.RemoveField(
            model_name='timeperiodexception',
            name='external_id',
        ),
    ]
