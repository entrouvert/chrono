from django.db import migrations, models

import chrono.agendas.models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0085_reason_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='booking_user_block_template',
            field=models.TextField(
                blank=True,
                verbose_name='User block template',
                validators=[chrono.agendas.models.django_template_validator],
            ),
        ),
    ]
