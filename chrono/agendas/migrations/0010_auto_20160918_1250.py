from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('agendas', '0009_auto_20160911_1640'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['agenda', 'start_datetime', 'label']},
        ),
        migrations.AddField(
            model_name='agenda',
            name='edit_role',
            field=models.ForeignKey(
                related_name='+',
                default=None,
                verbose_name='Edit Role',
                to='auth.Group',
                blank=True,
                null=True,
                on_delete=models.CASCADE,
            ),
        ),
        migrations.AddField(
            model_name='agenda',
            name='view_role',
            field=models.ForeignKey(
                related_name='+',
                default=None,
                verbose_name='View Role',
                to='auth.Group',
                blank=True,
                null=True,
                on_delete=models.CASCADE,
            ),
        ),
    ]
