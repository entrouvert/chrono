from django.db import migrations, models


def set_event_desk(apps, schema_editor):
    Events = apps.get_model('agendas', 'Event')
    Desk = apps.get_model('agendas', 'Desk')
    for event in Events.objects.all():
        if not event.agenda.kind == 'meetings':
            continue

        desk, _ = Desk.objects.get_or_create(label='Guichet 1', slug='guichet-1', agenda=event.agenda)
        event.desk = desk
        event.save()


def unset_event_desk(apps, schema_editor):
    pass


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0017_timeperiod_desk'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='desk',
            field=models.ForeignKey(to='agendas.Desk', null=True, on_delete=models.CASCADE),
        ),
        migrations.RunPython(set_event_desk, unset_event_desk),
    ]
