from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0071_booking_attendance'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='user_absence_reason',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
