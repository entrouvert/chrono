from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0119_check_types'),
    ]

    operations = [
        migrations.AddField(
            model_name='checktype',
            name='kind',
            field=models.CharField(
                choices=[('absence', 'Absence'), ('presence', 'Presence')],
                default='absence',
                max_length=8,
                verbose_name='Kind',
            ),
        ),
        migrations.AddField(
            model_name='checktype',
            name='pricing',
            field=models.DecimalField(
                blank=True,
                decimal_places=2,
                help_text='Fixed pricing',
                max_digits=5,
                null=True,
                verbose_name='Pricing',
            ),
        ),
        migrations.AddField(
            model_name='checktype',
            name='pricing_rate',
            field=models.PositiveIntegerField(
                blank=True, help_text='Percentage rate', null=True, verbose_name='Pricing rate'
            ),
        ),
    ]
