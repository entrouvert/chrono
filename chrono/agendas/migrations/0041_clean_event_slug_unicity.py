from django.db import migrations


def clean_constraint(apps, schema_editor):
    Event = apps.get_model('agendas', 'Event')
    # remove _like index added for unicity if exists
    index_to_remove = schema_editor._create_index_name('agendas_event', ['slug'], suffix='_like')
    index_names = schema_editor._constraint_names(Event, ['slug'], index=True)
    for index_name in index_names:
        if index_name == index_to_remove:
            schema_editor.execute(
                schema_editor._delete_constraint_sql(schema_editor.sql_delete_index, Event, index_name)
            )
    # remove unique constraint if exists
    constraint_names = schema_editor._constraint_names(Event, ['slug'], unique=True)
    for constraint_name in constraint_names:
        schema_editor.execute(
            schema_editor._delete_constraint_sql(schema_editor.sql_delete_unique, Event, constraint_name)
        )


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0040_timeperiod_agenda'),
    ]

    operations = [
        migrations.RunPython(clean_constraint, migrations.RunPython.noop),
    ]
