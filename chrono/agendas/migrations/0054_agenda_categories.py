import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0053_event_date_range_constraint'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('slug', models.SlugField(max_length=160, unique=True, verbose_name='Identifier')),
                ('label', models.CharField(max_length=150, verbose_name='Label')),
            ],
            options={
                'ordering': ['label'],
            },
        ),
        migrations.AddField(
            model_name='agenda',
            name='category',
            field=models.ForeignKey(
                blank=True,
                null=True,
                verbose_name='Category',
                on_delete=django.db.models.deletion.SET_NULL,
                to='agendas.Category',
            ),
        ),
    ]
