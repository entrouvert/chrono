from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('start_datetime', models.DateTimeField(verbose_name='Date/time')),
                ('places', models.PositiveIntegerField(verbose_name='Places')),
                ('agenda', models.ForeignKey(to='agendas.Agenda', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['agenda', 'start_datetime'],
            },
            bases=(models.Model,),
        ),
    ]
