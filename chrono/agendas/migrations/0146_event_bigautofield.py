import os

from django.db import migrations, models

sql_forwards_bigint = """
ALTER SEQUENCE "agendas_event_id_seq" as bigint MAXVALUE 9223372036854775807;
ALTER TABLE "agendas_event" ALTER COLUMN "id" TYPE bigint USING "id"::bigint, ALTER COLUMN "primary_event_id" TYPE bigint USING "primary_event_id"::bigint;
ALTER TABLE "agendas_booking" ALTER COLUMN "event_id" TYPE bigint USING "event_id"::bigint;
ALTER TABLE "agendas_event_resources" ALTER COLUMN "event_id" TYPE bigint USING "event_id"::bigint;
ALTER TABLE "agendas_eventcancellationreport" ALTER COLUMN "event_id" TYPE bigint USING "event_id"::bigint;
ALTER TABLE "agendas_recurrenceexceptionsreport_events" ALTER COLUMN "event_id" TYPE bigint USING "event_id"::bigint;
"""


with open(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)), '..', 'sql', 'event_booked_places_and_full_triggers.sql'
    )
) as sql_file:
    sql_forwards_triggers = sql_file.read()


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0145_user_phone_number'),
    ]

    operations = [
        migrations.RunSQL(
            sql=sql_forwards_bigint,
            reverse_sql=migrations.RunSQL.noop,
            state_operations=[
                migrations.AlterField(
                    model_name='event',
                    name='id',
                    field=models.BigAutoField(primary_key=True, serialize=False),
                ),
            ],
        ),
        migrations.RunSQL(sql=sql_forwards_triggers, reverse_sql=migrations.RunSQL.noop),
    ]
