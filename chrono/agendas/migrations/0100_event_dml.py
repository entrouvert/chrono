from django.db import migrations

# trigger places fields
sql_forwards = """UPDATE agendas_event SET id=id;"""


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0099_event_triggers'),
    ]

    operations = [migrations.RunSQL(sql=sql_forwards, reverse_sql='')]
