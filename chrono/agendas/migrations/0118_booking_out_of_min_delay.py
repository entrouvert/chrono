from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0117_events_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='out_of_min_delay',
            field=models.BooleanField(default=False),
        ),
    ]
