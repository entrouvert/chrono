from django.db import migrations, models

import chrono.agendas.models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0088_booking_check_filters'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='event_display_template',
            field=models.CharField(
                blank=True,
                help_text=(
                    'By default event labels will be displayed to users. '
                    'This allows for a custom template to include additional informations. '
                    'For example, "{{ event.label }} - {{ event.start_datetime }}" will show event datetime after label. '
                    'Available variables: event.label (label), event.start_datetime (start date/time), event.places (places), '
                    'event.remaining_places (remaining places), event.duration (duration), event.pricing (pricing).'
                ),
                max_length=256,
                validators=[chrono.agendas.models.event_template_validator],
                verbose_name='Event display template',
            ),
        ),
    ]
