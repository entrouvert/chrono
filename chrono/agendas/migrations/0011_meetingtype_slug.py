from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0010_auto_20160918_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='meetingtype',
            name='slug',
            field=models.SlugField(null=True, verbose_name='Identifier'),
        ),
    ]
