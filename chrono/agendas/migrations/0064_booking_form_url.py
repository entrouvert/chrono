from django.db import migrations, models

import chrono.agendas.models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0063_auto_20200928_1445'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='booking_form_url',
            field=models.CharField(
                blank=True,
                max_length=200,
                validators=[chrono.agendas.models.django_template_validator],
                verbose_name='Booking form URL',
            ),
        ),
    ]
