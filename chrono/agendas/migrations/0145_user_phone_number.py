from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0144_sharedcustodyagenda_date_end'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='user_phone_number',
            field=models.CharField(blank=True, max_length=30),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='user_phone_number',
            field=models.CharField(blank=True, max_length=30),
        ),
    ]
