from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0008_auto_20160910_1319'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='maximal_booking_delay',
            field=models.PositiveIntegerField(default=56, verbose_name='Maximal booking delay (in days)'),
        ),
        migrations.AddField(
            model_name='agenda',
            name='minimal_booking_delay',
            field=models.PositiveIntegerField(default=1, verbose_name='Minimal booking delay (in days)'),
        ),
    ]
