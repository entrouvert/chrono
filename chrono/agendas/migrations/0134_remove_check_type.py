from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0133_auto_20220628_1600'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='user_check_type_label',
            field=models.CharField(blank=True, null=True, max_length=150),
        ),
        migrations.AddField(
            model_name='booking',
            name='user_check_type_slug',
            field=models.CharField(blank=True, null=True, max_length=160),
        ),
    ]
