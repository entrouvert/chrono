# Generated by Django 1.11.18 on 2021-02-16 15:10

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0075_auto_20210216_1553'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='recurrence_end_date',
            field=models.DateField(
                blank=True,
                null=True,
                verbose_name='Recurrence end date',
                help_text='If left blank, a one-year maximal booking delay will be applied for this event.',
            ),
        ),
    ]
