import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0115_events_type'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eventstype',
            options={'ordering': ['label']},
        ),
        migrations.AddField(
            model_name='agenda',
            name='events_type',
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                verbose_name='Events type',
                related_name='agendas',
                to='agendas.EventsType',
            ),
        ),
    ]
