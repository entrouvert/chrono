from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0096_checked'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='minimal_booking_delay_in_working_days',
            field=models.BooleanField(default=False, verbose_name='Minimal booking delay in working days'),
        ),
    ]
