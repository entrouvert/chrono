from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0149_booking_extra_user_block'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='check_locked',
            field=models.BooleanField(default=False),
        ),
    ]
