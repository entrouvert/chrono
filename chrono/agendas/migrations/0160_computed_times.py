from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0159_partial_bookings_invoicing'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='computed_end_time',
            field=models.TimeField(null=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='computed_start_time',
            field=models.TimeField(null=True),
        ),
    ]
