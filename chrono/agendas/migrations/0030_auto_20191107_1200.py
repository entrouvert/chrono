from django.db import migrations
from django.utils.text import slugify


def generate_slug(instance, **query_filters):
    base_slug = slugify(instance.label)
    slug = base_slug
    i = 1
    while True:
        queryset = instance._meta.model.objects.filter(slug=slug, **query_filters).exclude(pk=instance.pk)
        if not queryset.exists():
            break
        slug = '%s-%s' % (base_slug, i)
        i += 1
    return slug


def set_slug_on_agendas(apps, schema_editor):
    Agenda = apps.get_model('agendas', 'Agenda')
    for agenda in Agenda.objects.all().order_by('-pk'):
        if not Agenda.objects.filter(slug=agenda.slug).exclude(pk=agenda.pk).exists():
            continue
        agenda.slug = generate_slug(agenda)
        agenda.save(update_fields=['slug'])


def set_slug_on_desks(apps, schema_editor):
    Desk = apps.get_model('agendas', 'Desk')
    for desk in Desk.objects.all().order_by('-pk'):
        if not Desk.objects.filter(slug=desk.slug, agenda=desk.agenda).exclude(pk=desk.pk).exists():
            continue
        desk.slug = generate_slug(desk, agenda=desk.agenda)
        desk.save(update_fields=['slug'])


def set_slug_on_meetingtypes(apps, schema_editor):
    MeetingType = apps.get_model('agendas', 'MeetingType')
    for meetingtype in MeetingType.objects.all().order_by('-pk'):
        if (
            not MeetingType.objects.filter(slug=meetingtype.slug, agenda=meetingtype.agenda)
            .exclude(pk=meetingtype.pk)
            .exists()
        ):
            continue
        meetingtype.slug = generate_slug(meetingtype, agenda=meetingtype.agenda)
        meetingtype.save(update_fields=['slug'])


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0029_auto_20191106_1320'),
    ]

    operations = [
        migrations.RunPython(set_slug_on_agendas, lambda x, y: None),
        migrations.RunPython(set_slug_on_desks, lambda x, y: None),
        migrations.RunPython(set_slug_on_meetingtypes, lambda x, y: None),
    ]
