import django.db.models.expressions
import django.db.models.functions.datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0152_auto_20230331_0834'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='event',
            index=models.Index(
                django.db.models.functions.datetime.ExtractWeekDay('start_datetime'),
                django.db.models.expressions.F('start_datetime'),
                condition=models.Q(('cancelled', False)),
                name='start_datetime_dow_index',
            ),
        ),
    ]
