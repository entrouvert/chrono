from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0003_booking'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='cancellation_datetime',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
