import datetime

from django.db import migrations

from chrono.utils.timezone import localtime, make_aware


def forwards(apps, schema_editor):
    Event = apps.get_model('agendas', 'Event')
    for event in Event.objects.filter(publication_date__isnull=False):
        try:
            event.publication_datetime = make_aware(
                datetime.datetime.combine(event.publication_date, datetime.time(0, 0))
            )
        except OverflowError:
            if event.publication_date.year < 2000:
                event.publication_date = None
            else:
                raise
        event.save()


def backwards(apps, schema_editor):
    Event = apps.get_model('agendas', 'Event')
    for event in Event.objects.filter(publication_datetime__isnull=False):
        event.publication_date = localtime(event.publication_datetime).date()
        event.save()


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0101_publication_datetime'),
    ]

    operations = [
        migrations.RunPython(forwards, reverse_code=backwards),
    ]
