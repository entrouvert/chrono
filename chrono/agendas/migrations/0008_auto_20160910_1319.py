import django
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('agendas', '0007_auto_20160722_1135'),
    ]

    operations = [
        migrations.CreateModel(
            name='MeetingType',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                (
                    'duration',
                    models.IntegerField(
                        default=30,
                        validators=[django.core.validators.MinValueValidator(1)],
                        verbose_name='Duration (in minutes)',
                    ),
                ),
            ],
            options={
                'ordering': ['label'],
            },
        ),
        migrations.CreateModel(
            name='TimePeriod',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                (
                    'weekday',
                    models.IntegerField(
                        verbose_name='Week day',
                        choices=[
                            (0, 'Monday'),
                            (1, 'Tuesday'),
                            (2, 'Wednesday'),
                            (3, 'Thursday'),
                            (4, 'Friday'),
                            (5, 'Saturday'),
                            (6, 'Sunday'),
                        ],
                    ),
                ),
                ('start_time', models.TimeField(verbose_name='Start')),
                ('end_time', models.TimeField(verbose_name='End')),
            ],
            options={
                'ordering': ['weekday', 'start_time'],
            },
        ),
        migrations.AddField(
            model_name='agenda',
            name='kind',
            field=models.CharField(
                default='events',
                max_length=20,
                verbose_name='Kind',
                choices=[('events', 'Events'), ('meetings', 'Meetings')],
            ),
        ),
        migrations.AddField(
            model_name='timeperiod',
            name='agenda',
            field=models.ForeignKey(to='agendas.Agenda', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='meetingtype',
            name='agenda',
            field=models.ForeignKey(to='agendas.Agenda', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='event',
            name='meeting_type',
            field=models.ForeignKey(to='agendas.MeetingType', null=True, on_delete=models.CASCADE),
        ),
    ]
