# Generated by Django 1.11.18 on 2021-01-25 17:00

from django.db import migrations


def booking_color_unique_label(apps, schema_editor):
    BookingColor = apps.get_model('agendas', 'BookingColor')

    for color in BookingColor.objects.order_by('label').distinct('label'):
        BookingColor.objects.filter(label=color.label).exclude(pk=color.pk).delete()


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('agendas', '0072_booking_absence_reason'),
    ]

    operations = [
        migrations.RunPython(booking_color_unique_label, migrations.RunPython.noop),
        migrations.AlterUniqueTogether(
            name='bookingcolor',
            unique_together={('label',)},
        ),
        migrations.RemoveField(
            model_name='bookingcolor',
            name='agenda',
        ),
    ]
