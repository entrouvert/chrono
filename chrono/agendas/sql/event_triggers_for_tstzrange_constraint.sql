-- trigger and procedure to maintain _ignore_reason from bookings
CREATE OR REPLACE FUNCTION set_ignore_reason() RETURNS TRIGGER AS $$
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            IF NEW.cancellation_datetime IS NOT NULL THEN
                UPDATE agendas_event SET _ignore_reason = 'cancel' WHERE id = NEW.event_id;
            END IF;
            RETURN NEW;
        ELSIF (TG_OP = 'DELETE') THEN
            UPDATE agendas_event SET _ignore_reason = 'delete' WHERE id = OLD.event_id;
            RETURN OLD;
        ELSE
            PERFORM 1 FROM agendas_booking b WHERE b.event_id = NEW.event_id AND b.cancellation_datetime IS NOT NULL;
            IF FOUND THEN
                UPDATE agendas_event SET _ignore_reason = 'cancel' WHERE id = NEW.event_id;
            ELSE
                UPDATE agendas_event SET _ignore_reason = NULL WHERE id = NEW.event_id;
            END IF;
            RETURN NEW;
        END IF;
    END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS set_ignore_reason_trg ON agendas_booking;
CREATE TRIGGER set_ignore_reason_trg
    AFTER INSERT OR UPDATE OR DELETE ON agendas_booking
    FOR EACH ROW
    EXECUTE PROCEDURE set_ignore_reason();

-- triggers to maintain _end_datetime
CREATE OR REPLACE FUNCTION update_duration() RETURNS TRIGGER AS $$
    BEGIN
        UPDATE agendas_event SET _end_datetime = start_datetime + (NEW.duration ||' minutes')::interval WHERE meeting_type_id = NEW.id;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_duration_trg ON agendas_meetingtype;
CREATE TRIGGER update_duration_trg
    AFTER UPDATE ON agendas_meetingtype
    FOR EACH ROW
    WHEN (OLD.duration != NEW.duration)
    EXECUTE PROCEDURE update_duration();
