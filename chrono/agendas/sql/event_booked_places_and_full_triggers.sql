CREATE OR REPLACE FUNCTION update_event_full_fields() RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        NEW.booked_places = 0;
        NEW.booked_waiting_list_places = 0;
    ELSE
        -- update booked_places and booked_waiting_list_places fields
        SELECT
            COUNT(1) FILTER (WHERE NOT(in_waiting_list)),
            COUNT(1) FILTER (WHERE in_waiting_list)
            INTO NEW.booked_places, NEW.booked_waiting_list_places
            FROM agendas_booking b
            WHERE b.event_id = NEW.id AND b.cancellation_datetime IS NULL;
    END IF;

    -- for events agenda with partial bookings, event is never full
    PERFORM 1 FROM agendas_agenda a WHERE a.id = NEW.agenda_id AND a.partial_bookings IS TRUE;
    IF FOUND THEN
        NEW.almost_full = false;
        NEW.full = false;
        RETURN NEW;
    END IF;

    -- update almost_full field
    IF (NEW.booked_places >= NEW.places * 0.9) THEN
        NEW.almost_full = true;
    ELSE
        NEW.almost_full = false;
    END IF;

    -- update full field
    IF (NEW.booked_places >= NEW.places) AND (NEW.waiting_list_places = 0) THEN
        NEW.full = true;
    ELSIF (NEW.waiting_list_places > 0) AND (NEW.booked_waiting_list_places >= NEW.waiting_list_places) THEN
        NEW.full = true;
    ELSE
        NEW.full = false;
    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_event_places_fields() RETURNS TRIGGER AS $$
DECLARE
    e_id bigint;
BEGIN
    IF (TG_OP = 'DELETE') THEN
        e_id = OLD.event_id;
    ELSE
        e_id = NEW.event_id;
    END IF;
    -- update booked_places and booked_waiting_list_places fields on agendas_event
    WITH new_stats AS (
        SELECT
            COUNT(1) FILTER (WHERE NOT(in_waiting_list)) AS booked,
            COUNT(1) FILTER (WHERE in_waiting_list) AS waiting
            FROM agendas_booking b
            WHERE b.event_id = e_id AND b.cancellation_datetime IS NULL
    )
    UPDATE agendas_event
        SET booked_places = new_stats.booked,
            booked_waiting_list_places = new_stats.waiting
        FROM new_stats
        WHERE id = e_id AND (agendas_event.booked_places <> new_stats.booked
                             OR agendas_event.booked_waiting_list_places <> new_stats.waiting);

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS update_event_full_fields_trigger ON agendas_event;
CREATE TRIGGER update_event_full_fields_trigger BEFORE INSERT OR UPDATE ON agendas_event
    FOR EACH ROW EXECUTE PROCEDURE update_event_full_fields();

DROP TRIGGER IF EXISTS update_event_places_fields_trigger ON agendas_booking;
CREATE TRIGGER update_event_places_fields_trigger AFTER INSERT OR UPDATE OR DELETE ON agendas_booking
    FOR EACH ROW EXECUTE PROCEDURE update_event_places_fields();
