CREATE FUNCTION hack_immutable_tstzrange (start timestamptz, duration integer)
    RETURNS tstzrange
    LANGUAGE 'sql'
IMMUTABLE  -- common options:  IMMUTABLE  STABLE  STRICT  SECURITY DEFINER
AS $function$
    SELECT tstzrange($1, $1 + '60 seconds'::interval * COALESCE($2, 0), '[)');
$function$;
