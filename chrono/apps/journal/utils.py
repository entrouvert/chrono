# chrono - agendas system
# Copyright (C) 2016-2024  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth import get_user_model

from .models import AuditEntry

User = get_user_model()


def audit(action, request=None, user=None, agenda=None, extra_data=None, bulk=False):
    action_type, action_code = action.split(':', 1)
    extra_data = extra_data or {}
    if 'booking' in extra_data:
        extra_data['booking_id'] = extra_data['booking'].id
        extra_data['user_external_id'] = extra_data['booking'].user_external_id
        extra_data['user_name'] = extra_data['booking'].user_name
        if 'event' not in extra_data:
            extra_data['event'] = extra_data['booking'].event
        extra_data['booking'] = extra_data['booking'].get_journal_label()
    if 'event' in extra_data:
        extra_data['event_id'] = extra_data['event'].id
        extra_data['event'] = extra_data['event'].get_journal_label()
    ae = AuditEntry(
        user=request.user if request and isinstance(request.user, User) else user,
        action_type=action_type,
        action_code=action_code,
        agenda=agenda,
        extra_data=extra_data,
    )
    if not bulk:
        ae.save()
    return ae


def bulk_audit(audit_entries):
    AuditEntry.objects.bulk_create(audit_entries)
