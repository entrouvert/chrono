# chrono - agendas system
# Copyright (C) 2016-2024  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

import django_filters
from django.forms.widgets import DateInput
from django.utils.translation import gettext_lazy as _

from chrono.agendas.models import Agenda

from .models import AuditEntry


class DateWidget(DateInput):
    input_type = 'date'

    def __init__(self, *args, **kwargs):
        kwargs['format'] = '%Y-%m-%d'
        super().__init__(*args, **kwargs)


class DayFilter(django_filters.DateFilter):
    def filter(self, qs, value):
        if value:
            qs = qs.filter(timestamp__gte=value, timestamp__lt=value + datetime.timedelta(days=1))
        return qs


class JournalFilterSet(django_filters.FilterSet):
    timestamp = DayFilter(widget=DateWidget())
    agenda = django_filters.ModelChoiceFilter(queryset=Agenda.objects.all())
    action_type = django_filters.ChoiceFilter(
        choices=(
            ('booking', _('Booking')),
            ('check', _('Checking')),
            ('invoice', _('Invoicing')),
        )
    )
    user_external_id = django_filters.CharFilter(
        label=_('User (external ID)'),
        method='filter_user_external_id',
    )

    class Meta:
        model = AuditEntry
        fields = []

    def filter_user_external_id(self, queryset, name, value):
        if not value:
            return queryset
        return queryset.filter(extra_data__user_external_id=value)
