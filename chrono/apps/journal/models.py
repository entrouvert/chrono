# chrono - agendas system
# Copyright (C) 2016-2024  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

MESSAGES = {
    'booking:accept': _('acceptation of booking (%(booking_id)s) in event "%(event)s"'),
    'booking:cancel': _('cancellation of booking (%(booking_id)s) in event "%(event)s"'),
    'booking:create': _('created booking (%(booking_id)s) for event %(event)s'),
    'booking:create-from-subscription': _(
        'created booking (%(booking_id)s) from subscription of %(user_name)s for event %(event)s'
    ),
    'booking:suspend': _('suspension of booking (%(booking_id)s) in event "%(event)s"'),
    'booking:delete': _('deletion of booking (%(booking_id)s) in event "%(event)s"'),
    'check:mark': _('marked event %(event)s as checked'),
    'check:mark-unchecked-absent': _('marked unchecked users as absent in %(event)s'),
    'check:mark-unchecked-present': _('marked unchecked users as present in %(event)s'),
    'check:reset': _('reset check of %(user_name)s in %(event)s'),
    'check:lock': _('marked event %(event)s as locked for checks'),
    'check:unlock': _('unmarked event %(event)s as locked for checks'),
    'check:absence': _('marked absence of %(user_name)s in %(event)s'),
    'check:presence': _('marked presence of %(user_name)s in %(event)s'),
    'invoice:mark': _('marked event %(event)s as invoiced'),
    'invoice:unmark': _('unmarked event %(event)s as invoiced'),
}


class AuditEntry(models.Model):
    timestamp = models.DateTimeField(verbose_name=_('Date'), auto_now_add=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('User'), on_delete=models.SET_NULL, null=True
    )
    action_type = models.CharField(verbose_name=_('Action type'), max_length=100)
    action_code = models.CharField(verbose_name=_('Action code'), max_length=100)
    agenda = models.ForeignKey(
        'agendas.Agenda', on_delete=models.SET_NULL, null=True, related_name='audit_entries'
    )
    extra_data = models.JSONField(blank=True, default=dict)

    class Meta:
        ordering = ('-timestamp',)

    @property
    def message_type(self):
        return f'{self.action_type}:{self.action_code}'

    def get_action_text(self):
        try:
            return MESSAGES[self.message_type] % self.extra_data
        except KeyError:
            return _('Unknown entry (%(type)s:%(code)s)') % {
                'type': self.action_type,
                'code': self.action_code,
            }
