import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('agendas', '0170_alter_agenda_events_type'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('snapshot', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UnavailabilityCalendarSnapshot',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(blank=True, null=True)),
                ('serialization', models.JSONField(blank=True, default=dict)),
                ('label', models.CharField(blank=True, max_length=150, verbose_name='Label')),
                ('application_slug', models.CharField(max_length=100, null=True)),
                ('application_version', models.CharField(max_length=100, null=True)),
                (
                    'instance',
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to='agendas.unavailabilitycalendar',
                        related_name='instance_snapshots',
                    ),
                ),
                (
                    'user',
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL
                    ),
                ),
            ],
            options={
                'ordering': ('-timestamp',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResourceSnapshot',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(blank=True, null=True)),
                ('serialization', models.JSONField(blank=True, default=dict)),
                ('label', models.CharField(blank=True, max_length=150, verbose_name='Label')),
                ('application_slug', models.CharField(max_length=100, null=True)),
                ('application_version', models.CharField(max_length=100, null=True)),
                (
                    'instance',
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to='agendas.resource',
                        related_name='instance_snapshots',
                    ),
                ),
                (
                    'user',
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL
                    ),
                ),
            ],
            options={
                'ordering': ('-timestamp',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EventsTypeSnapshot',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(blank=True, null=True)),
                ('serialization', models.JSONField(blank=True, default=dict)),
                ('label', models.CharField(blank=True, max_length=150, verbose_name='Label')),
                ('application_slug', models.CharField(max_length=100, null=True)),
                ('application_version', models.CharField(max_length=100, null=True)),
                (
                    'instance',
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to='agendas.eventstype',
                        related_name='instance_snapshots',
                    ),
                ),
                (
                    'user',
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL
                    ),
                ),
            ],
            options={
                'ordering': ('-timestamp',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CategorySnapshot',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(blank=True, null=True)),
                ('serialization', models.JSONField(blank=True, default=dict)),
                ('label', models.CharField(blank=True, max_length=150, verbose_name='Label')),
                ('application_slug', models.CharField(max_length=100, null=True)),
                ('application_version', models.CharField(max_length=100, null=True)),
                (
                    'instance',
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to='agendas.category',
                        related_name='instance_snapshots',
                    ),
                ),
                (
                    'user',
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL
                    ),
                ),
            ],
            options={
                'ordering': ('-timestamp',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AgendaSnapshot',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(blank=True, null=True)),
                ('serialization', models.JSONField(blank=True, default=dict)),
                ('label', models.CharField(blank=True, max_length=150, verbose_name='Label')),
                ('application_slug', models.CharField(max_length=100, null=True)),
                ('application_version', models.CharField(max_length=100, null=True)),
                (
                    'instance',
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to='agendas.agenda',
                        related_name='instance_snapshots',
                    ),
                ),
                (
                    'user',
                    models.ForeignKey(
                        null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL
                    ),
                ),
            ],
            options={
                'ordering': ('-timestamp',),
                'abstract': False,
            },
        ),
    ]
