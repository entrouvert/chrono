# chrono - agendas system
# Copyright (C) 2016-2024  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.core.management.base import BaseCommand
from django.utils.timezone import now

from chrono.agendas.models import Agenda, Category, EventsType, Resource, UnavailabilityCalendar


class Command(BaseCommand):
    help = 'Clear obsolete snapshot instances'

    def handle(self, **options):
        for model in [Agenda, Category, EventsType, Resource, UnavailabilityCalendar]:
            model.snapshots.filter(updated_at__lte=now() - datetime.timedelta(days=1)).delete()
