# chrono - agendas system
# Copyright (C) 2024  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt

from . import models


@csrf_exempt
@login_required
def save_preference(request):
    user_pref, dummy = models.UserPreferences.objects.get_or_create(user=request.user)

    if len(request.body) > 1000:
        return HttpResponseBadRequest(_('Payload is too large'))
    try:
        prefs = json.loads(request.body)
    except json.JSONDecodeError:
        return HttpResponseBadRequest(_('Bad format'))
    if not isinstance(prefs, dict) or len(prefs) != 1:
        return HttpResponseBadRequest(_('Bad format'))

    user_pref.preferences.update(prefs)
    user_pref.save()
    return HttpResponse('', status=204)
