from django.db import migrations
from django.db.models import Count


def forward(apps, schema_editor):
    UserPreferences = apps.get_model('user_preferences', 'UserPreferences')
    duplicates = (
        UserPreferences.objects.values('user').annotate(Count('user')).order_by().filter(user__count__gt=1)
    )
    for values in duplicates:
        for user_prefs in UserPreferences.objects.filter(user=values['user']).order_by('pk')[1:]:
            user_prefs.delete()


class Migration(migrations.Migration):
    dependencies = [
        ('user_preferences', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forward, reverse_code=migrations.RunPython.noop),
    ]
