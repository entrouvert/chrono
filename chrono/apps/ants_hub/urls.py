# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import functools

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from django.urls import path
from django.utils.translation import gettext as _

from . import views


def view_decorator(func):
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied
        if not settings.CHRONO_ANTS_HUB_URL:
            messages.info(
                request, _('Configure CHRONO_ANTS_HUB_URL to get access to ANTS-Hub configuration panel.')
            )
            return redirect('chrono-manager-homepage')
        return func(request, *args, **kwargs)

    return wrapper


urlpatterns = [
    path('', views.Homepage.as_view(), name='chrono-manager-ants-hub'),
    path('synchronize/', views.Synchronize.as_view(), name='chrono-manager-ants-hub-synchronize'),
    path('city/add/', views.CityAddView.as_view(), name='chrono-manager-ants-hub-city-add'),
    path('city/<int:pk>/edit/', views.CityEditView.as_view(), name='chrono-manager-ants-hub-city-edit'),
    path('city/<int:pk>/delete/', views.CityDeleteView.as_view(), name='chrono-manager-ants-hub-city-delete'),
    path('city/<int:pk>/place/add/', views.PlaceAddView.as_view(), name='chrono-manager-ants-hub-place-add'),
    path(
        'city/<int:city_pk>/place/<int:pk>/', views.PlaceView.as_view(), name='chrono-manager-ants-hub-place'
    ),
    path(
        'city/<int:city_pk>/place/<int:pk>/edit/',
        views.PlaceEditView.as_view(),
        name='chrono-manager-ants-hub-place-edit',
    ),
    path(
        'city/<int:city_pk>/place/<int:pk>/url/',
        views.PlaceUrlEditView.as_view(),
        name='chrono-manager-ants-hub-place-url',
    ),
    path(
        'city/<int:city_pk>/place/<int:pk>/delete/',
        views.PlaceDeleteView.as_view(),
        name='chrono-manager-ants-hub-place-delete',
    ),
    path(
        'city/<int:city_pk>/place/<int:pk>/agenda/add/',
        views.PlaceAgendaAddView.as_view(),
        name='chrono-manager-ants-hub-agenda-add',
    ),
    path(
        'city/<int:city_pk>/place/<int:place_pk>/agenda/<int:pk>/edit/',
        views.PlaceAgendaEditView.as_view(),
        name='chrono-manager-ants-hub-agenda-edit',
    ),
    path(
        'city/<int:city_pk>/place/<int:place_pk>/agenda/<int:pk>/delete/',
        views.PlaceAgendaDeleteView.as_view(),
        name='chrono-manager-ants-hub-agenda-delete',
    ),
]

for pattern in urlpatterns:
    pattern.callback = view_decorator(pattern.callback)
