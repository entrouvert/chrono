# chrono - agendas system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path

from chrono.apps.export_import import urls as export_import_urls

from .api.urls import urlpatterns as chrono_api_urls
from .manager.urls import urlpatterns as chrono_manager_urls
from .urls_utils import decorated_includes
from .views import LoginView, LogoutView, homepage

urlpatterns = [
    path('', homepage, name='home'),
    re_path(r'^manage/', decorated_includes(login_required, include(chrono_manager_urls))),
    path('api/', include(chrono_api_urls)),
    path('api/', include(export_import_urls)),
    path('logout/', LogoutView.as_view(), name='auth_logout'),
    path('login/', LoginView.as_view(), name='auth_login'),
]

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns.append(
        path(
            'accounts/mellon/',
            include('mellon.urls'),
            kwargs={
                'template_base': 'chrono/manager_base.html',
            },
        )
    )

if settings.DEBUG and 'debug_toolbar' in settings.INSTALLED_APPS:
    import debug_toolbar  # pylint: disable=import-error

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

# static and media files
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
