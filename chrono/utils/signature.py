# combo - content management system
# Copyright (C) 2015-2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import datetime
import hashlib
import hmac
import random
from urllib import parse

from django.conf import settings
from django.utils.encoding import smart_bytes
from django.utils.http import quote, urlencode

# Simple signature scheme for query strings


def sign_url(url, key, algo='sha256', timestamp=None, nonce=None):
    parsed = parse.urlparse(url)
    new_query = sign_query(parsed.query, key, algo, timestamp, nonce)
    return parse.urlunparse(parsed[:4] + (new_query,) + parsed[5:])


def sign_query(query, key, algo='sha256', timestamp=None, nonce=None):
    if timestamp is None:
        timestamp = datetime.datetime.utcnow()
    timestamp = timestamp.strftime('%Y-%m-%dT%H:%M:%SZ')
    if nonce is None:
        nonce = hex(random.getrandbits(128))[2:]
    new_query = query
    if new_query:
        new_query += '&'
    new_query += urlencode((('algo', algo), ('timestamp', timestamp), ('nonce', nonce)))
    signature = base64.b64encode(sign_string(new_query, key, algo=algo))
    new_query += '&signature=' + quote(signature)
    return new_query


def sign_string(s, key, algo='sha256', timedelta=30):
    digestmod = getattr(hashlib, algo)
    hash = hmac.HMAC(smart_bytes(key), digestmod=digestmod, msg=smart_bytes(s))
    return hash.digest()


def check_request_signature(django_request, keys=None):
    keys = keys or []
    query_string = django_request.META['QUERY_STRING']
    if not query_string:
        return False
    orig = django_request.GET.get('orig', '')
    known_services = getattr(settings, 'KNOWN_SERVICES', None)
    if known_services and orig:
        for services in known_services.values():
            for service in services.values():
                if 'verif_orig' in service and service['verif_orig'] == orig:
                    keys.append(service['secret'])
                    break
    return check_query(query_string, keys)


def check_query(query, keys, known_nonce=None, timedelta=30):
    parsed = parse.parse_qs(query)
    if not ('signature' in parsed and 'algo' in parsed and 'timestamp' in parsed and 'nonce' in parsed):
        return False
    unsigned_query, signature_content = query.split('&signature=', 1)
    if '&' in signature_content:
        return False  # signature must be the last parameter
    signature = base64.b64decode(parsed['signature'][0])
    algo = parsed['algo'][0]
    timestamp = parsed['timestamp'][0]
    timestamp = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ')
    nonce = parsed['nonce']
    if known_nonce is not None and known_nonce(nonce):
        return False
    if abs(datetime.datetime.utcnow() - timestamp) > datetime.timedelta(seconds=timedelta):
        return False
    return check_string(unsigned_query, signature, keys, algo=algo)


def check_string(s, signature, keys, algo='sha256'):
    if not isinstance(keys, list):
        keys = [keys]
    for key in keys:
        signature2 = sign_string(s, key, algo=algo)
        if len(signature2) != len(signature):
            continue
        res = 0
        # constant time compare
        for a, b in zip(signature, signature2):
            res |= a ^ b
        if res == 0:
            return True
    return False
