# chrono - agendas system
# Copyright (C) 2024  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from rest_framework import permissions

try:
    from hobo.rest_permissions import IsAdminUser, IsAPIClient

    has_hobo = True
except (ImportError, NameError):
    has_hobo = False

if has_hobo:
    APIAdminOrAuthUser = (IsAPIClient & IsAdminUser) | ((~IsAPIClient) & permissions.IsAuthenticated)
else:
    APIAdminOrAuthUser = permissions.IsAuthenticated


class ReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS


APIAdminOrAuthUserOrReadOnly = APIAdminOrAuthUser | ReadOnly
