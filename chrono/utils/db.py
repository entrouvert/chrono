# chrono - agendas system
# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db.migrations.operations.base import Operation
from django.db.models import Aggregate


class SumCardinality(Aggregate):
    template = 'SUM(CARDINALITY(%(expressions)s))'


class EnsureJsonbType(Operation):
    reversible = True

    def __init__(self, model_name, field_name):
        self.model_name = model_name
        self.field_name = field_name

    def state_forwards(self, app_label, state):
        pass

    def database_forwards(self, app_label, schema_editor, from_state, to_state):
        model = from_state.apps.get_model(app_label, self.model_name)
        table_name = model._meta.db_table
        field = model._meta.get_field(self.field_name)
        _, column_name = field.get_attname_column()
        with schema_editor.connection.cursor() as cursor:
            cursor.execute(
                'ALTER TABLE {table} ALTER COLUMN {col} TYPE jsonb USING {col}::jsonb;'.format(
                    table=table_name, col=column_name
                )
            )

    def database_backwards(self, app_label, schema_editor, from_state, to_state):
        pass

    def describe(self):
        return 'Migrate to postgres jsonb type'
