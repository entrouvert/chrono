# chrono - agendas system
# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib.parse

from django.conf import settings


def translate_from_publik_url(url):
    if not url:
        return ''
    source_url = urllib.parse.urlparse(url)

    if source_url.scheme != 'publik':
        # search in legacy urls
        legacy_urls_mapping = getattr(settings, 'LEGACY_URLS_MAPPING', None)
        if not legacy_urls_mapping:
            return url
        splitted_url = urllib.parse.urlparse(url)
        hostname = splitted_url.netloc
        if hostname not in legacy_urls_mapping:
            return url
        return splitted_url._replace(netloc=legacy_urls_mapping[hostname]).geturl()

    known_services = getattr(settings, 'KNOWN_SERVICES', None)
    if not known_services:
        return url
    for data in known_services.values():
        for slug, service_data in data.items():
            if slug == source_url.netloc:
                service_url = urllib.parse.urlparse(service_data['url'])
                return urllib.parse.urlunparse(
                    (service_url.scheme, service_url.netloc, source_url.path, '', '', None)
                )
    return ''


def translate_to_publik_url(url):
    if not url:
        return ''
    known_services = getattr(settings, 'KNOWN_SERVICES', None)
    if not known_services:
        return url
    source_url = urllib.parse.urlparse(url)
    for data in known_services.values():
        for slug, service_data in data.items():
            service_url = urllib.parse.urlparse(service_data['url'])
            if source_url.netloc == service_url.netloc and source_url.scheme == service_url.scheme:
                return str(urllib.parse.urlunparse(('publik', slug, source_url.path, '', '', None)))
    return url
