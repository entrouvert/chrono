# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import functools
import zoneinfo

import django.utils.timezone
from django.conf import settings

utc = zoneinfo.ZoneInfo('UTC')


@functools.lru_cache
def get_default_timezone():
    """
    Return the default time zone as a tzinfo instance.

    This is the time zone defined by settings.TIME_ZONE.
    """
    return zoneinfo.ZoneInfo(settings.TIME_ZONE)


def localtime(value=None, timezone=None):
    if timezone is None:
        timezone = get_default_timezone()
    return django.utils.timezone.localtime(value=value, timezone=timezone)


is_aware = django.utils.timezone.is_aware


def make_aware(value, timezone=None, is_dst=None):
    if timezone is None:
        timezone = get_default_timezone()
    return django.utils.timezone.make_aware(value, timezone=timezone, is_dst=is_dst)


def make_naive(value, timezone=None):
    if timezone is None:
        timezone = get_default_timezone()
    return django.utils.timezone.make_naive(value, timezone=timezone)


def now():
    """
    Return an aware or naive datetime.datetime, depending on settings.USE_TZ.
    """
    if settings.USE_TZ:
        # timeit shows that datetime.now(tz=utc) is 24% slower
        return datetime.datetime.utcnow().replace(tzinfo=utc)
    else:
        return datetime.datetime.now()
