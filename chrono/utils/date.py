def get_weekday_index(datetime):
    return (datetime.day - 1) // 7 + 1
