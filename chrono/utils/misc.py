# chrono - agendas system
# Copyright (C) 2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import copy

from django.core.exceptions import FieldDoesNotExist, ValidationError
from django.utils.translation import gettext_lazy as _


class ICSError(Exception):
    pass


class AgendaImportError(Exception):
    pass


def generate_slug(instance, seen_slugs=None, base_slug=None, **query_filters):
    if base_slug is None:
        base_slug = instance.base_slug
    slug = base_slug
    i = 1

    if seen_slugs is None:
        # no optimization: check slug in DB each time
        while instance._meta.model.objects.filter(slug=slug, **query_filters).exists():
            slug = '%s-%s' % (base_slug, i)
            i += 1
        return slug

    # seen_slugs is filled
    while True:
        if slug not in seen_slugs:
            # check in DB to be sure, but only if not seen
            queryset = instance._meta.model.objects.filter(slug=slug, **query_filters)
            if not queryset.exists():
                break
        slug = '%s-%s' % (base_slug, i)
        i += 1
    seen_slugs.add(slug)
    return slug


def clean_import_data(cls, data):
    cleaned_data = copy.deepcopy(data)
    for param in data:
        try:
            field = cls._meta.get_field(param)
        except FieldDoesNotExist:
            # remove unknown fields
            cleaned_data.pop(param)
            continue
        if field.many_to_many:
            # remove many to many fields, they have to be managed after update_or_create
            cleaned_data.pop(param)
            continue
        if param == 'slug':
            value = cleaned_data[param]
            try:
                field.run_validators(value)
            except ValidationError:
                raise AgendaImportError(_('Bad slug format "%s"') % value)
    return cleaned_data
