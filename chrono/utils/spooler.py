# chrono - agendas system
# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import connection
from uwsgidecorators import spool  # pylint: disable=import-error

from chrono.agendas.models import Agenda, Event, ICSError, TimePeriodExceptionSource


def set_connection(domain):
    from hobo.multitenant.middleware import TenantMiddleware  # pylint: disable=import-error

    tenant = TenantMiddleware.get_tenant_by_hostname(domain)
    connection.set_tenant(tenant)


@spool
def refresh_exception_source(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    try:
        source = TimePeriodExceptionSource.objects.get(pk=args['source_id'])
    except TimePeriodExceptionSource.DoesNotExist:
        return
    try:
        source.refresh_timeperiod_exceptions_from_ics()
    except ICSError:
        pass


@spool
def refresh_exceptions_from_settings(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    try:
        source = TimePeriodExceptionSource.objects.get(pk=args['source_id'])
    except TimePeriodExceptionSource.DoesNotExist:
        return

    source.refresh_from_settings()


@spool
def event_notify_checked(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    try:
        event = Event.objects.get(pk=args['event_id'])
    except Event.DoesNotExist:
        return

    event.notify_checked()


@spool
def ants_hub_city_push(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    from chrono.apps.ants_hub.models import City

    try:
        City.push()
    except Exception:  # noqa pylint: disable=broad-except
        pass


@spool
def refresh_booking_computed_times_from_agenda(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    try:
        agenda = Agenda.objects.get(pk=args['agenda_id'])
    except Agenda.DoesNotExist:
        return

    agenda.refresh_booking_computed_times()


@spool
def refresh_booking_computed_times_from_event(args):
    if args.get('domain'):
        # multitenant installation
        set_connection(args['domain'])

    try:
        event = Event.objects.get(pk=args['event_id'])
    except Event.DoesNotExist:
        return

    event.refresh_booking_computed_times()
