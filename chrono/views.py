# chrono - agendas system
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from urllib.parse import quote

from django.conf import settings
from django.contrib.auth import views as auth_views
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache

if 'mellon' in settings.INSTALLED_APPS:
    from mellon.utils import get_idps
else:
    get_idps = lambda: []


class LoginView(auth_views.LoginView):
    def get(self, request, *args, **kwargs):
        if any(get_idps()):
            if not 'next' in request.GET:
                return HttpResponseRedirect(resolve_url('mellon_login'))
            return HttpResponseRedirect(
                resolve_url('mellon_login') + '?next=' + quote(request.GET.get('next'))
            )
        return super().get(request, *args, **kwargs)


class LogoutView(auth_views.LogoutView):
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if any(get_idps()):
            return HttpResponseRedirect(resolve_url('mellon_logout'))
        return super().dispatch(request, *args, **kwargs)


def homepage(request, *args, **kwargs):
    return HttpResponseRedirect(resolve_url('chrono-manager-homepage'))
