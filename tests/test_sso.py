import pytest
from django.test import override_settings

pytestmark = pytest.mark.django_db


def test_sso(app):
    with override_settings(MELLON_IDENTITY_PROVIDERS=[{'METADATA': 'x', 'ENTITY_ID': 'x'}]):
        resp = app.get('/login/')
        assert resp.location.endswith('/accounts/mellon/login/')

        resp = app.get('/login/?next=/manage/')
        assert resp.location.endswith('/accounts/mellon/login/?next=/manage/')


def test_slo(app):
    with override_settings(MELLON_IDENTITY_PROVIDERS=[{'METADATA': 'x', 'ENTITY_ID': 'x'}]):
        resp = app.get('/logout/')
        assert resp.location.endswith('/accounts/mellon/logout/')
