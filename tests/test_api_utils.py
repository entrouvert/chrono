import pytest

from chrono.agendas.models import Agenda, MeetingType
from chrono.api.utils import Response


@pytest.mark.parametrize(
    'data, expected',
    [
        (None, None),
        ({}, {}),
        ({'reason': 'foo'}, {'reason': 'foo'}),
        ({'err_class': 'foo'}, {'err_class': 'foo', 'reason': 'foo'}),
        ({'bar': 'foo'}, {'bar': 'foo'}),
    ],
)
def test_response_data(data, expected):
    resp = Response(data=data)
    assert resp.data == expected


def test_err_desc_translation(db, app, settings):
    settings.LANGUAGE_CODE = 'fr-fr'
    agenda = Agenda.objects.create(label='Foo bar', kind='events')

    resp = app.get(
        '/api/agenda/%s/datetimes/' % agenda.slug,
        params={'user_external_id': '42', 'exclude_user_external_id': '35'},
        status=400,
    )
    assert resp.json['err_desc'] == 'contenu de requête invalide'
    assert resp.json['err_class'] == 'invalid payload'

    agenda = Agenda.objects.create(label='Foo bar', kind='meetings')
    meeting_type = MeetingType.objects.create(agenda=agenda, slug='foo', duration=60)
    resp = app.get(
        '/api/agenda/%s/meetings/%s/datetimes/?resources=hop' % (agenda.slug, meeting_type.slug), status=400
    )
    assert resp.json['err_desc'] == 'slugs invalides : hop'
    assert resp.json['err_class'] == 'invalid slugs: hop'
