import pytest

from chrono.utils.interval import Interval, IntervalSet


def test_interval_union():
    assert Interval(1, 2).union((2, 3)) == (1, 3)
    assert Interval(1, 2).union((2, 3)) == Interval(1, 3)


def test_interval_set_merge_adjacent():
    """
    Test that adjacent intervals are merged
    """
    s = IntervalSet()
    for i in range(0, 100, 2):
        s.add(i, i + 1)
    for i in range(1, 100, 2):
        s.add(Interval(i, i + 1))
    s.add((11, 12))
    assert list(s) == [(0, 100)]


def test_interval_set_from_ordered():
    assert IntervalSet.from_ordered([(0, 10), (1, 9), (2, 13), (13, 20)]) == [(0, 20)]
    with pytest.raises(ValueError):
        IntervalSet.from_ordered([(3, 4), (1, 2)])


def test_interval_set_overlaps():
    """
    Test overlaps() works.
    """
    s = IntervalSet()
    assert not s.overlaps(0, 1)
    for i in range(0, 100, 2):
        s.add(i, i + 1)
    i = -0.5
    while i < 99:
        assert s.overlaps(i, i + 1)
        assert s.overlaps(i + 1, i + 2)
        i += 2


def test_interval_set_add_and_from_ordered_equivalence():
    s1 = IntervalSet()
    for i in range(0, 100, 2):
        s1.add(i, i + 1)
    s2 = IntervalSet.from_ordered((i, i + 1) for i in range(0, 100, 2))
    assert s1 == s2


def test_interval_set_from_ordered_merge_adjacent():
    s = IntervalSet.from_ordered((i, i + 1) for i in range(0, 100))
    assert list(s) == [(0, 100)]


def test_invterval_cast():
    s = IntervalSet([(1, 2)])
    assert IntervalSet.cast(s) == [(1, 2)]
    assert IntervalSet.cast(((1, 2),)) == [(1, 2)]
    assert IntervalSet.cast([[1, 2]]) == [(1, 2)]
    assert IntervalSet.cast((Interval(1, 2),)) == [(1, 2)]


def test_interval_set_sub():
    s = IntervalSet([(0, 3), (4, 7), (8, 11), (12, 15)])

    assert (s - [(-2, -1), (1, 2), (3, 5), (10, 11.5), (11.5, 15.5)]) == [(0, 1), (2, 3), (5, 7), (8, 10)]
    assert (s - [(-2, -1), (1, 2), (3, 5), (10, 11.5), (11.5, 15.5)]) == [(0, 1), (2, 3), (5, 7), (8, 10)]

    assert (s - []) == s
    assert (IntervalSet([(0, 2)]) - [(1, 2)]) == [(0, 1)]

    assert ([] - s) == []


def test_interval_set_min_max():
    assert IntervalSet().min() is None
    assert IntervalSet().max() is None

    assert IntervalSet.simple(1, 2).min() == 1
    assert IntervalSet.simple(1, 2).max() == 2


def test_interval_set_repr():
    assert repr(IntervalSet([(1, 2)])) == repr([(1, 2)])


def test_interval_set_eq():
    assert IntervalSet([(1, 2)]) == [(1, 2)]
    assert [(1, 2)] == IntervalSet([(1, 2)])
    assert not IntervalSet([(1, 2)]) == None  # noqa pylint: disable=singleton-comparison
    # noqa pylint: disable=singleton-comparison
    assert not None == IntervalSet([(1, 2)])


def test_interval_set_add():
    s = IntervalSet([(0, 3), (4, 7), (8, 11), (12, 15)])
    t = IntervalSet([(3, 4), (7, 8), (11, 12)])

    assert s + t == [(0, 15)]
    assert t + s == [(0, 15)]
    assert [(3, 4), (7, 8), (11, 12)] + s == [(0, 15)]

    t = IntervalSet([(3, 4), (11, 12)])
    assert s + t == [(0, 7), (8, 15)]
    assert t + s == [(0, 7), (8, 15)]

    t = IntervalSet([(2, 5), (10, 13)])
    assert s + t == [(0, 7), (8, 15)]
    assert t + s == [(0, 7), (8, 15)]

    assert s + [] == s
    assert [] + s == s

    assert IntervalSet([(1, 3), (4, 6)]) + [(3, 4), (4, 5)] == [(1, 6)]
