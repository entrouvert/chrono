# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

import responses
from django.core.management import call_command


def test_sync_ants_hub(db, hub, place_agenda, freezer):
    freezer.move_to('2023-06-01T17:12:00+02:00')
    response = hub.add(
        responses.POST,
        'https://toto:@ants-hub.example.com/api/chrono/rendez-vous-disponibles/',
        json={'err': 0},
    )
    call_command('sync-ants-hub')
    assert response.call_count == 1
    payload = json.loads(hub.calls[-1].request.body)
    assert len(payload['collectivites']) == 1
    assert {**payload['collectivites'][0], 'lieux': None} == {
        'annulation_url': '',
        'full': True,
        'gestion_url': '',
        'id': '1',
        'lieux': None,
        'logo_url': 'https://newcity.com/logo.png',
        'nom': 'Newcity',
        'rdv_url': '',
        'url': 'https://newcity.com/rdv/',
    }
    assert len(payload['collectivites'][0]['lieux']) == 1
    assert {**payload['collectivites'][0]['lieux'][0], 'plages': None} == {
        'annulation_url': '',
        'code_postal': '13260',
        'full': True,
        'gestion_url': '',
        'id': '1',
        'latitude': -40.3,
        'longitude': 2.3,
        'nom': 'Townhall',
        'numero_rue': '221B Baker Street',
        'plages': None,
        'rdv_url': '',
        'rdvs': [],
        'url': '',
        'ville': 'Newcity',
        'logo_url': '',
    }
    assert len(payload['collectivites'][0]['lieux'][0]['plages']) == 39
    assert payload['collectivites'][0]['lieux'][0]['plages'][0] == {
        'date': '2023-06-02',
        'duree': 15,
        'heure_debut': '08:00:00',
        'heure_fin': '17:00:00',
        'personnes': 1,
        'types_rdv': ['CNI'],
    }
