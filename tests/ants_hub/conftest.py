# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import responses
from django.core.cache import cache

from chrono.apps.ants_hub.models import City, Place, PlaceAgenda
from tests.manager.conftest import admin_user, simple_user  # noqa pylint: disabled=unused-import
from tests.utils import build_agendas


@pytest.fixture
def ants_settings(settings):
    settings.CHRONO_ANTS_HUB_URL = 'https://toto:@ants-hub.example.com/api/chrono/'


@pytest.fixture
def hub(ants_settings):
    cache.delete('ants-hub-ok')
    with responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
        rsps.add(responses.GET, 'https://toto:@ants-hub.example.com/api/chrono/ping/', json={'err': 0})
        yield rsps


@pytest.fixture
def city():
    return City.objects.create(
        id=1, name='Newcity', logo_url='https://newcity.com/logo.png', url='https://newcity.com/rdv/'
    )


@pytest.fixture
def place(city):
    return Place.objects.create(
        id=1,
        city=city,
        name='Townhall',
        address='221B Baker Street',
        zipcode='13260',
        city_name='Newcity',
        longitude='2.3',
        latitude='-40.3',
    )


@pytest.fixture
def agenda():
    return build_agendas(
        '''
meetings CNI
   desk "Desk1"
     timeperiod monday-friday 08:00-17:00
   meeting-type "Rdv CNI" mt-15 15
   meeting-type "Rdv CNI" mt-30 30
   meeting-type "Rdv CNI" mt-45 45
   meeting-type "Rdv CNI" mt-60 60
'''
    )


@pytest.fixture
def virtual_agenda():
    return build_agendas(
        '''
virtual CNI
    meetings CNI1
       desk "Desk1"
         timeperiod monday-friday 08:00-12:00
       meeting-type "Rdv CNI" mt-15 15
       meeting-type "Rdv CNI" mt-30 30
       meeting-type "Rdv CNI" mt-45 45
       meeting-type "Rdv CNI" mt-60 60
    meetings CNI2
       desk "Desk1"
         timeperiod monday-friday 14:00-17:00
       meeting-type "Rdv CNI" mt-15 15
       meeting-type "Rdv CNI" mt-30 30
       meeting-type "Rdv CNI" mt-45 45
       meeting-type "Rdv CNI" mt-60 60
'''
    )


@pytest.fixture
def place_agenda(place, agenda):
    return PlaceAgenda.objects.create(
        id=1,
        place=place,
        agenda=agenda,
        setting={
            'meeting-types': {
                'mt-15': {
                    'ants_meeting_type': [1],
                    'ants_persons_number': [1],
                }
            }
        },
    )
