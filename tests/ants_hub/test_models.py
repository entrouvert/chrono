# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from chrono.apps.ants_hub.models import City, Place, PlaceAgenda
from tests.utils import add_meeting, build_meetings_agenda, build_virtual_agenda, paris


@pytest.fixture(autouse=True)
def ants_setup(db, freezer):
    freezer.move_to(paris('2023-04-07 17:32'))

    class Namespace:
        durations = [15, 30, 45, 60]
        mairie_agenda = build_meetings_agenda(
            label='cni_passport_mairie',
            meeting_types=durations,
            desks=('desk1', 'monday-friday 9:00-12:00 14:00-17:00'),
            maximal_booking_delay=10,
        )
        annexe_agenda = build_virtual_agenda(
            label='annexe',
            agendas={
                'Agenda 1': {
                    'desks': ('Bureau 1', 'monday-wednesday 08:00-12:00'),
                    'meeting_types': durations,
                },
                'Agenda 2': {
                    'desks': ('Bureau 2', 'thursday-friday 14:00-17:00'),
                    'meeting_types': durations,
                },
            },
            maximal_booking_delay=5,
        )
        city = City.objects.create(
            id=1,
            name='Saint-Didier',
            url='https://saint-didier.fr/',
        )
        mairie = Place.objects.create(
            id=1,
            city=city,
            name='Mairie',
            address='2 rue du four',
            zipcode='99999',
            city_name='Saint-Didier',
            logo_url='https://saint-didier.fr/logo.png',
        )
        annexe = Place.objects.create(
            id=2,
            city=city,
            name='Mairie annexe',
            address='3 rue du four',
            zipcode='99999',
            city_name='Saint-Didier',
        )
        PlaceAgenda.objects.create(
            place=mairie,
            agenda=mairie_agenda,
            setting={
                'meeting-types': {
                    'mt-15': {
                        'ants_meeting_type': [1, 2],
                        'ants_persons_number': [1],
                    },
                    'mt-30': {
                        'ants_meeting_type': [1, 2],
                        'ants_persons_number': [2],
                    },
                    'mt-45': {
                        'ants_meeting_type': [1, 2],
                        'ants_persons_number': [3],
                    },
                    'mt-60': {
                        'ants_meeting_type': [1, 2],
                        'ants_persons_number': [4],
                    },
                }
            },
        )
        PlaceAgenda.objects.create(
            place=annexe,
            agenda=annexe_agenda,
            setting={
                'meeting-types': {
                    'mt-15': {
                        'ants_meeting_type': [1, 2],
                        'ants_persons_number': [1],
                    },
                    'mt-30': {
                        'ants_meeting_type': [1, 2],
                        'ants_persons_number': [2],
                    },
                    'mt-45': {
                        'ants_meeting_type': [1, 2],
                        'ants_persons_number': [3],
                    },
                    'mt-60': {
                        'ants_meeting_type': [1, 2],
                        'ants_persons_number': [4],
                    },
                }
            },
        )
        add_meeting(
            mairie_agenda,
            paris('2023-04-10 09:00'),
            cancellation_datetime=paris('2023-04-08 13:15'),
            meeting_type='mt-15',
            extra_data={'ants_identifiant_predemande': '12345678'},
        )
        add_meeting(
            mairie_agenda,
            paris('2023-04-11 11:00'),
            meeting_type='mt-30',
            extra_data={'ants_identifiant_predemande': 'ABCDEFGH , IJKLMNOP'},
        )

        add_meeting(
            annexe_agenda._agenda_1,
            paris('2023-04-10 10:00'),
            cancellation_datetime=paris('2023-04-08 13:15'),
            meeting_type='mt-45',
            extra_data={'ants_identifiant_predemande': '1234ABCD'},
        )
        add_meeting(
            annexe_agenda._agenda_1,
            paris('2023-04-10 11:00'),
            meeting_type='mt-45',
            extra_data={'ants_identifiant_predemande': 'XYZ12JKL'},
        )
        add_meeting(
            annexe_agenda._agenda_2,
            paris('2023-04-11 11:00'),
            meeting_type='mt-60',
            extra_data={'ants_identifiant_predemande': 'ABCD1234'},
        )
        add_meeting(
            annexe_agenda._agenda_2,
            paris('2023-04-11 12:00'),
            meeting_type='mt-60',
            extra_data={'ants_identifiant_predemande': ''},
        )

    return Namespace


def test_export_to_push(ants_setup):
    assert ants_setup.city.export_to_push() == {
        'full': True,
        'id': '1',
        'nom': 'Saint-Didier',
        'url': 'https://saint-didier.fr/',
        'logo_url': '',
        'rdv_url': '',
        'annulation_url': '',
        'gestion_url': '',
        'lieux': [
            {
                'full': True,
                'id': '1',
                'nom': 'Mairie',
                'numero_rue': '2 rue du four',
                'code_postal': '99999',
                'ville': 'Saint-Didier',
                'latitude': 46.596,
                'longitude': 2.476,
                'url': '',
                'rdv_url': '',
                'gestion_url': '',
                'annulation_url': '',
                'rdvs': [
                    {'annule': True, 'date': '2023-04-10T07:00:00+00:00', 'id': '12345678'},
                    {'date': '2023-04-11T09:00:00+00:00', 'id': 'ABCDEFGH'},
                    {'date': '2023-04-11T09:00:00+00:00', 'id': 'IJKLMNOP'},
                ],
                'plages': [
                    {
                        'date': '2023-04-10',
                        'duree': 15,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 30,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 45,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 60,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 15,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 30,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 45,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 60,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 15,
                        'heure_debut': '09:00:00',
                        'heure_fin': '11:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 30,
                        'heure_debut': '09:00:00',
                        'heure_fin': '11:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 45,
                        'heure_debut': '09:00:00',
                        'heure_fin': '11:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 60,
                        'heure_debut': '09:00:00',
                        'heure_fin': '11:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 15,
                        'heure_debut': '11:30:00',
                        'heure_fin': '12:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 30,
                        'heure_debut': '11:30:00',
                        'heure_fin': '12:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 15,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 30,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 45,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 60,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-12',
                        'duree': 15,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-12',
                        'duree': 30,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-12',
                        'duree': 45,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-12',
                        'duree': 60,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-12',
                        'duree': 15,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-12',
                        'duree': 30,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-12',
                        'duree': 45,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-12',
                        'duree': 60,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-13',
                        'duree': 15,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-13',
                        'duree': 30,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-13',
                        'duree': 45,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-13',
                        'duree': 60,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-13',
                        'duree': 15,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-13',
                        'duree': 30,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-13',
                        'duree': 45,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-13',
                        'duree': 60,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-14',
                        'duree': 15,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-14',
                        'duree': 30,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-14',
                        'duree': 45,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-14',
                        'duree': 60,
                        'heure_debut': '09:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-14',
                        'duree': 15,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-14',
                        'duree': 30,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-14',
                        'duree': 45,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-14',
                        'duree': 60,
                        'heure_debut': '14:00:00',
                        'heure_fin': '17:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                ],
                'logo_url': 'https://saint-didier.fr/logo.png',
            },
            {
                'full': True,
                'id': '2',
                'nom': 'Mairie annexe',
                'numero_rue': '3 rue du four',
                'code_postal': '99999',
                'ville': 'Saint-Didier',
                'latitude': 46.596,
                'longitude': 2.476,
                'url': '',
                'rdv_url': '',
                'gestion_url': '',
                'annulation_url': '',
                'rdvs': [
                    {'annule': True, 'date': '2023-04-10T08:00:00+00:00', 'id': '1234ABCD'},
                    {'date': '2023-04-10T09:00:00+00:00', 'id': 'XYZ12JKL'},
                    {'date': '2023-04-11T09:00:00+00:00', 'id': 'ABCD1234'},
                ],
                'plages': [
                    {
                        'date': '2023-04-10',
                        'duree': 15,
                        'heure_debut': '08:00:00',
                        'heure_fin': '11:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 30,
                        'heure_debut': '08:00:00',
                        'heure_fin': '11:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 45,
                        'heure_debut': '08:00:00',
                        'heure_fin': '11:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 60,
                        'heure_debut': '08:00:00',
                        'heure_fin': '11:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-10',
                        'duree': 15,
                        'heure_debut': '11:45:00',
                        'heure_fin': '12:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 15,
                        'heure_debut': '08:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 1,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 30,
                        'heure_debut': '08:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 2,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 45,
                        'heure_debut': '08:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 3,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                    {
                        'date': '2023-04-11',
                        'duree': 60,
                        'heure_debut': '08:00:00',
                        'heure_fin': '12:00:00',
                        'personnes': 4,
                        'types_rdv': ['CNI', 'PASSPORT'],
                    },
                ],
                'logo_url': '',
            },
        ],
    }
