# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import responses

from chrono.apps.ants_hub.models import ANTSMeetingType, ANTSPersonsNumber, City, PlaceAgenda
from tests.utils import login

pytestmark = pytest.mark.django_db


def test_unconfigured(app, admin_user):
    login(app)
    resp = app.get('/manage/', status=200)
    assert 'ANTS' not in resp


def test_unlogged(ants_settings, app):
    app.get('/manage/ants/', status=302)


def test_configured(hub, app, admin_user):
    login(app)
    resp = app.get('/manage/', status=200)
    resp = resp.click('ANTS')
    assert 'ANTS Hub is responding' in resp
    assert 'New city' in resp


def test_hub_is_down(hub, app, admin_user):
    hub.replace(responses.GET, 'https://toto:@ants-hub.example.com/api/chrono/ping/', status=500)
    login(app)
    resp = app.get('/manage/', status=200)
    resp = resp.click('ANTS')
    assert 'ANTS Hub is down' in resp


def test_city_add(hub, app, admin_user):
    login(app)
    resp = app.get('/manage/ants/', status=200)
    assert 'Newcity' not in resp
    resp = resp.click('New city')
    resp.form.set('name', 'Newcity')
    resp.form.set('url', 'https://newcity.com/rdv/')
    resp.form.set('logo_url', 'https://newcity.com/logo.png')
    resp = resp.form.submit().follow()
    assert 'Newcity' in resp


def test_city_edit(hub, app, admin_user, city):
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click(href='edit')
    assert 'logo.png' in resp


def test_city_delete(hub, app, admin_user, city):
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click(href='delete')
    resp = resp.form.submit(status=302)
    assert City.objects.count() == 0


def test_add_place(hub, app, admin_user, city):
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click('Add place')
    resp.form.set('name', 'Townhall')
    resp.form.set('address', '221B Baker Street')
    resp.form.set('zipcode', '13260')
    resp.form.set('city_name', 'Newcity')
    resp.form.set('longitude', '2.3')
    resp.form.set('latitude', '-40.3')
    assert city.places.count() == 0
    resp = resp.form.submit().follow()
    assert city.places.count() == 1
    assert 'Newcity' in resp
    assert 'Townhall' in resp


def test_edit_place(hub, app, admin_user, city, place):
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click('Townhall')
    resp = resp.click('Edit', href='edit')
    assert 'Baker Street' in resp
    resp.form.set('address', 'Downing Street')
    resp = resp.form.submit().follow()
    assert 'Downing Street' in resp
    place.refresh_from_db()
    assert place.address == 'Downing Street'


def test_url_edit_place(hub, app, admin_user, city, place):
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click('Townhall')
    assert 'https://townhall.example.com/rdv/' not in resp
    resp = resp.click('Edit', href='url')
    resp.form.set('url', 'https://townhall.example.com/rdv/')
    resp = resp.form.submit().follow()
    assert 'https://townhall.example.com/rdv/' in resp
    place.refresh_from_db()
    assert place.url == 'https://townhall.example.com/rdv/'


def test_delete_place(hub, app, admin_user, city, place):
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click('Townhall')
    resp = resp.click('Remove')
    assert city.places.count() == 1
    resp = resp.form.submit().follow()
    assert city.places.count() == 0


def test_add_agenda(hub, app, admin_user, city, place, agenda, freezer):
    freezer.move_to('2023-06-01T17:12:00+02:00')
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click('Townhall')
    resp = resp.click('Add')
    resp.form.set('agenda', str(agenda.pk))
    assert not place.agendas.exists()
    resp = resp.form.submit().follow()
    place_agenda = place.agendas.get()
    assert list(place_agenda.iter_open_dates()) == []
    assert not place_agenda.setting.get('meeting-types')
    assert '(not configured)' in resp
    assert '1 person' not in resp
    resp = resp.click(href='agenda.*edit')
    # make the meeting-type of 15 minutes correpond to a meeting to get a CNI
    # for 1 person
    assert [item.text() for item in resp.pyquery('table td.meeting-type').items()] == [
        'Rdv CNI (15 minutes)',
        'Rdv CNI (30 minutes)',
        'Rdv CNI (45 minutes)',
        'Rdv CNI (60 minutes)',
    ]
    # check possible checkbox values
    assert {item.text() for item in resp.pyquery('table td label').items()} == {
        '1 person',
        '2 persons',
        '3 persons',
        '4 persons',
        '5 persons',
        'CNI',
        'CNI and passport',
        'Passport',
    }

    resp.form.set('mt_mt-15_1', [str(ANTSMeetingType.CNI)])
    resp.form.set('mt_mt-15_2', [str(ANTSPersonsNumber.ONE)])
    resp = resp.form.submit().follow()
    place_agenda.refresh_from_db()
    assert place_agenda.setting.get('meeting-types')
    assert '(not configured)' not in resp
    assert '(CNI, 1 person)' in resp
    assert place_agenda.setting == {
        'meeting-types': {
            'mt-15': {'ants_meeting_type': [1], 'ants_persons_number': [1]},
            'mt-30': {'ants_meeting_type': [], 'ants_persons_number': []},
            'mt-45': {'ants_meeting_type': [], 'ants_persons_number': []},
            'mt-60': {'ants_meeting_type': [], 'ants_persons_number': []},
        }
    }

    open_dates = list(place_agenda.iter_open_dates())
    assert len(open_dates) == 39
    assert open_dates[0] == {
        'date': '2023-06-02',
        'heure_debut': '08:00:00',
        'heure_fin': '17:00:00',
        'duree': 15,
        'personnes': 1,
        'types_rdv': ['CNI'],
    }


def test_delete_agenda(hub, app, admin_user, city, place, agenda, place_agenda):
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click('Townhall')
    resp = resp.click(href='agenda.*delete')
    resp = resp.form.submit().follow()
    assert 'CNI' not in resp
    assert PlaceAgenda.objects.count() == 0


def test_synchronize(hub, app, admin_user, city, place, agenda, place_agenda, freezer):
    hub.add(
        responses.POST,
        'https://toto:@ants-hub.example.com/api/chrono/rendez-vous-disponibles/',
        json={'err': 0},
    )
    freezer.move_to('2023-06-01T17:12:00+02:00')
    login(app)
    resp = app.get('/manage/', status=200)
    resp = resp.click('ANTS')
    resp = resp.click('Synchronize')
    resp = resp.form.submit().follow()


def test_add_virtual_agenda(hub, app, admin_user, city, place, virtual_agenda, freezer):
    agenda = virtual_agenda
    freezer.move_to('2023-06-01T17:12:00+02:00')
    login(app)
    resp = app.get('/manage/ants/')
    resp = resp.click('Townhall')
    resp = resp.click('Add')
    resp.form.set('agenda', str(agenda.pk))
    assert not place.agendas.exists()
    resp = resp.form.submit().follow()
    place_agenda = place.agendas.get()
    assert list(place_agenda.iter_open_dates()) == []
    assert not place_agenda.setting.get('meeting-types')
    assert '(not configured)' in resp
    assert '1 person' not in resp
    resp = resp.click(href='agenda.*edit')
    # make the meeting-type of 15 minutes correpond to a meeting to get a CNI
    # for 1 person
    assert [item.text() for item in resp.pyquery('table td.meeting-type').items()] == [
        'Rdv CNI (15 minutes)',
        'Rdv CNI (30 minutes)',
        'Rdv CNI (45 minutes)',
        'Rdv CNI (60 minutes)',
    ]
    # check possible checkbox values
    assert {item.text() for item in resp.pyquery('table td label').items()} == {
        '1 person',
        '2 persons',
        '3 persons',
        '4 persons',
        '5 persons',
        'CNI',
        'CNI and passport',
        'Passport',
    }

    resp.form.set('mt_mt-15_1', [str(ANTSMeetingType.CNI)])
    resp.form.set('mt_mt-15_2', [str(ANTSPersonsNumber.ONE)])
    resp = resp.form.submit().follow()
    place_agenda.refresh_from_db()
    assert place_agenda.setting.get('meeting-types')
    assert '(not configured)' not in resp
    assert '(CNI, 1 person)' in resp
    assert place_agenda.setting == {
        'meeting-types': {
            'mt-15': {'ants_meeting_type': [1], 'ants_persons_number': [1]},
            'mt-30': {'ants_meeting_type': [], 'ants_persons_number': []},
            'mt-45': {'ants_meeting_type': [], 'ants_persons_number': []},
            'mt-60': {'ants_meeting_type': [], 'ants_persons_number': []},
        }
    }

    open_dates = list(place_agenda.iter_open_dates())
    assert len(open_dates) == 78
    assert open_dates[0] == {
        'date': '2023-06-02',
        'heure_debut': '08:00:00',
        'heure_fin': '12:00:00',
        'duree': 15,
        'personnes': 1,
        'types_rdv': ['CNI'],
    }
