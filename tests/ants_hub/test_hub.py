# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import requests
import responses

from chrono.apps.ants_hub.hub import AntsHubException, check_duplicate, ping, push_rendez_vous_disponibles


def test_ping_timeout(hub):
    hub.replace(
        responses.GET, 'https://toto:@ants-hub.example.com/api/chrono/ping/', body=requests.Timeout('boom!')
    )
    ping()


def test_push_rendez_vous_disponibles_timeout(hub):
    hub.add(
        responses.POST,
        'https://toto:@ants-hub.example.com/api/chrono/rendez-vous-disponibles/',
        body=requests.Timeout('boom!'),
    )
    push_rendez_vous_disponibles({})


def test_ping_internal_server_error(hub):
    hub.replace(responses.GET, 'https://toto:@ants-hub.example.com/api/chrono/ping/', status=500)
    with pytest.raises(AntsHubException):
        ping()


def test_push_rendez_vous_disponibles_internal_server_error(hub):
    hub.add(
        responses.POST, 'https://toto:@ants-hub.example.com/api/chrono/rendez-vous-disponibles/', status=500
    )
    with pytest.raises(AntsHubException):
        push_rendez_vous_disponibles({})


def test_ping_application_error(hub):
    hub.replace(
        responses.GET, 'https://toto:@ants-hub.example.com/api/chrono/ping/', json={'err': 'overload'}
    )
    with pytest.raises(AntsHubException, match='overload'):
        ping()


def test_push_rendez_vous_disponibles_application_error(hub):
    hub.add(
        responses.POST,
        'https://toto:@ants-hub.example.com/api/chrono/rendez-vous-disponibles/',
        json={'err': 'overload'},
    )
    with pytest.raises(AntsHubException, match='overload'):
        push_rendez_vous_disponibles({})


class TestCheckDuplicate:
    def test_status_500(self, hub):
        hub.add(responses.GET, 'https://toto:@ants-hub.example.com/api/chrono/rdv-status/', status=500)
        assert check_duplicate(['A' * 10, '1' * 10]) == {
            'err': 1,
            'err_desc': "ANTS hub is unavailable: HTTPError('500 Server Error: Internal Server Error for url: https://toto:@ants-hub.example.com/api/chrono/rdv-status/?identifiant_predemande=AAAAAAAAAA&identifiant_predemande=1111111111')",
        }

    def test_timeout(self, hub):
        hub.add(
            responses.GET,
            'https://toto:@ants-hub.example.com/api/chrono/rdv-status/',
            body=requests.Timeout('boom!'),
        )
        assert check_duplicate(['A' * 10, '1' * 10]) == {
            'err': 1,
            'err_desc': "ANTS hub is unavailable: Timeout('boom!')",
        }

    def test_ok(self, hub):
        hub.add(
            responses.GET,
            'https://toto:@ants-hub.example.com/api/chrono/rdv-status/?identifiant_predemande=AAAAAAAAAA&identifiant_predemande=1111111111',
            json={
                'err': 0,
                'data': {},
            },
        )
        assert check_duplicate(['A' * 10, '1' * 10]) == {
            'err': 0,
            'data': {},
        }
