# chrono - agendas system
# Copyright (C) 2023  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from unittest import mock

import pytest
import requests
import responses
from django.contrib.auth import get_user_model

from chrono.apps.ants_hub.hub import AntsHubException, check_duplicate, ping, push_rendez_vous_disponibles

User = get_user_model()


def test_authorization(app, user):
    app.post('/api/ants/check-duplicate/', status=401)


@pytest.fixture
def user(db):
    user = User(username='john.doe', first_name='John', last_name='Doe', email='john.doe@example.net')
    user.set_password('password')
    user.save()
    return user


@pytest.fixture
def auth_app(user, app):
    app.authorization = ('Basic', ('john.doe', 'password'))
    return app


class TestCheckDuplicateAPI:
    def test_not_configured(self, auth_app):
        resp = auth_app.post('/api/ants/check-duplicate/', status=400)
        assert resp.json == {
            'err': 1,
            'err_class': 'CHRONO_ANTS_HUB_URL is not configured',
            'err_desc': 'CHRONO_ANTS_HUB_URL is not configured',
            'reason': 'CHRONO_ANTS_HUB_URL is not configured',
        }

    def test_input_empty(self, hub, auth_app):
        resp = auth_app.post('/api/ants/check-duplicate/')
        assert resp.json == {'data': {'accept_rdv': True}, 'err': 0}

    @mock.patch('chrono.apps.ants_hub.hub.check_duplicate')
    def test_proxy(self, check_duplicate_mock, hub, auth_app):
        # do not care about output
        check_duplicate_mock.return_value = {'err': 0, 'data': {'xyz': '1234'}}

        # GET param
        resp = auth_app.post('/api/ants/check-duplicate/?identifiant_predemande= ABCdE12345, ,1234567890 ')
        assert resp.json == {'err': 0, 'data': {'xyz': '1234'}}
        assert check_duplicate_mock.call_args[0][0] == ['ABCDE12345', '1234567890']

        # JSON payload as string
        resp = auth_app.post_json(
            '/api/ants/check-duplicate/?identifiant_predemande=XYZ',
            params={'identifiant_predemande': ' XBCdE12345, ,1234567890 '},
        )
        assert resp.json == {'err': 0, 'data': {'xyz': '1234'}}
        assert check_duplicate_mock.call_args[0][0] == ['XBCDE12345', '1234567890']

        # JSON payload as list
        resp = auth_app.post_json(
            '/api/ants/check-duplicate/?identifiant_predemande=XYZ',
            params={'identifiant_predemande': [' YBCdE12345', ' ', '1234567890 ']},
        )
        assert resp.json == {'err': 0, 'data': {'xyz': '1234'}}
        assert check_duplicate_mock.call_args[0][0] == ['YBCDE12345', '1234567890']
