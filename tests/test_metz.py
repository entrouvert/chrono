import pytest
from django.core.management import call_command


@pytest.fixture
def metz_data(db):
    call_command('import_site', 'tests/metz.json')


def test_get_all_slots(db, app, metz_data, freezer):
    freezer.move_to('2020-04-29 22:04:00')

    response = app.get('/api/agenda/vdmvirtuel-retraitmasque/meetings/un-retrait/datetimes/')
    assert len(response.json['data']) == 324
