import django_webtest
import pytest
from django.core.signals import setting_changed
from django.dispatch import receiver

from chrono.utils.timezone import get_default_timezone


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm.setup_auth = False
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture(autouse=True)
def media_root(settings, tmpdir):
    settings.MEDIA_ROOT = str(tmpdir.mkdir('media_root'))


@pytest.fixture
def nocache(settings):
    settings.CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        }
    }


@receiver(setting_changed)
def update_connections_time_zone(**kwargs):
    if kwargs['setting'] == 'TIME_ZONE':
        # Reset local time zone lru cache
        get_default_timezone.cache_clear()
