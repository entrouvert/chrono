import pytest
from django.db import connection
from django.test.utils import CaptureQueriesContext

from chrono.agendas.models import Agenda, EventsType
from chrono.apps.snapshot.models import EventsTypeSnapshot
from tests.utils import login

pytestmark = pytest.mark.django_db


def test_list_events_type_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    app = login(app, username='manager', password='manager')
    app.get('/manage/events-types/', status=403)

    resp = app.get('/manage/', status=200)
    assert 'Events types' not in resp.text


def test_add_events_type(app, admin_user):
    app = login(app)
    resp = app.get('/manage/', status=200)
    resp = resp.click('Events types')
    resp = resp.click('New')
    resp.form['label'] = 'Foo bar'
    resp = resp.form.submit()
    events_type = EventsType.objects.latest('pk')
    assert resp.location.endswith('/manage/events-type/%s/' % events_type.pk)
    assert events_type.label == 'Foo bar'
    assert events_type.slug == 'foo-bar'
    assert events_type.custom_fields == []
    assert EventsTypeSnapshot.objects.count() == 1


def test_add_events_type_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    app = login(app, username='manager', password='manager')
    app.get('/manage/events-type/add/', status=403)


def test_view_events_type(app, admin_user):
    agenda = Agenda.objects.create(label='Foo Bar', kind='meetings')
    events_type = EventsType.objects.create(label='Foo bar')

    app = login(app)
    resp = app.get('/manage/events-type/%s/' % events_type.pk, status=200)
    assert '/manage/agendas/%s/settings' % agenda.pk not in resp.text

    agenda.events_type = events_type
    agenda.save()
    resp = app.get('/manage/events-type/%s/' % events_type.pk, status=200)
    assert '/manage/agendas/%s/settings' % agenda.pk in resp.text


def test_view_events_type_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    events_type = EventsType.objects.create(label='Foo bar')
    app = login(app, username='manager', password='manager')
    app.get('/manage/events-type/%s/' % events_type.pk, status=403)


def test_edit_events_type(app, admin_user):
    events_type = EventsType.objects.create(label='Foo bar')
    events_type2 = EventsType.objects.create(label='baz')

    app = login(app)
    resp = app.get('/manage/events-type/%s/' % events_type.pk, status=200)
    resp = resp.click(href='/manage/events-type/%s/edit/' % events_type.pk)
    resp.form['label'] = 'Foo bar baz'
    resp.form['slug'] = events_type2.slug
    resp = resp.form.submit()
    assert resp.context['form'].errors['slug'] == ['Events type with this Identifier already exists.']

    resp.form['slug'] = 'baz2'
    resp = resp.form.submit()
    assert resp.location.endswith('/manage/events-type/%s/' % events_type.pk)
    events_type.refresh_from_db()
    assert events_type.label == 'Foo bar baz'
    assert events_type.slug == 'baz2'
    assert events_type.custom_fields == []
    assert EventsTypeSnapshot.objects.count() == 1


def test_edit_events_type_custom_fields(app, admin_user):
    events_type = EventsType.objects.create(label='Foo bar')

    app = login(app)
    resp = app.get('/manage/events-type/%s/edit/' % events_type.pk)
    assert [v[0] for v in resp.form['form-0-field_type'].options] == ['', 'text', 'textarea', 'bool', 'date']
    resp.form['form-0-varname'] = 'foo-foo'
    resp.form['form-0-label'] = 'Foo'
    resp = resp.form.submit()
    assert resp.context['formset'][0].errors['varname'] == [
        'Enter a valid “slug” consisting of letters, numbers, or underscores.'
    ]
    resp.form['form-0-label'] = ''
    resp.form['form-0-varname'] = 'foo'
    resp = resp.form.submit()
    assert resp.context['formset'][0].errors['field_type'] == ['This field is required.']
    assert resp.context['formset'][0].errors['label'] == ['This field is required.']
    resp.form['form-0-label'] = 'Foo'
    resp.form['form-0-field_type'] = 'text'
    resp = resp.form.submit()
    assert resp.status_code == 302
    events_type.refresh_from_db()
    assert events_type.custom_fields == [
        {'varname': 'foo', 'label': 'Foo', 'field_type': 'text', 'booking_limit_period': ''},
    ]

    resp = app.get('/manage/events-type/%s/edit/' % events_type.pk)
    assert resp.form['form-TOTAL_FORMS'].value == '2'
    assert resp.form['form-0-varname'].value == 'foo'
    assert resp.form['form-0-label'].value == 'Foo'
    assert resp.form['form-0-field_type'].value == 'text'
    assert resp.form['form-1-varname'].value == ''
    assert resp.form['form-1-label'].value == ''
    assert resp.form['form-1-field_type'].value == ''
    resp.form['form-0-label'] = 'Foo-bis'
    resp.form['form-1-varname'] = 'blah'
    resp.form['form-1-label'] = 'Blah'
    resp.form['form-1-field_type'] = 'bool'
    resp.form['form-1-booking_limit_period'].select(text='By month')
    resp = resp.form.submit()
    assert resp.status_code == 302
    events_type.refresh_from_db()
    assert events_type.custom_fields == [
        {'varname': 'foo', 'label': 'Foo-bis', 'field_type': 'text', 'booking_limit_period': ''},
        {'varname': 'blah', 'label': 'Blah', 'field_type': 'bool', 'booking_limit_period': 'month'},
    ]

    resp = app.get('/manage/events-type/%s/edit/' % events_type.pk)
    assert resp.form['form-TOTAL_FORMS'].value == '3'
    assert resp.form['form-0-varname'].value == 'foo'
    assert resp.form['form-0-label'].value == 'Foo-bis'
    assert resp.form['form-0-field_type'].value == 'text'
    assert resp.form['form-0-booking_limit_period'].value == ''
    assert resp.form['form-1-varname'].value == 'blah'
    assert resp.form['form-1-label'].value == 'Blah'
    assert resp.form['form-1-field_type'].value == 'bool'
    assert resp.form['form-1-booking_limit_period'].value == 'month'
    assert resp.form['form-2-varname'].value == ''
    assert resp.form['form-2-label'].value == ''
    assert resp.form['form-2-field_type'].value == ''
    assert resp.form['form-2-booking_limit_period'].value == ''
    resp.form['form-0-varname'] = 'foo'
    resp.form['form-0-label'] = 'Foo'
    resp.form['form-1-varname'] = ''
    resp = resp.form.submit()
    assert resp.status_code == 302
    events_type.refresh_from_db()
    assert events_type.custom_fields == [
        {'varname': 'foo', 'label': 'Foo', 'field_type': 'text', 'booking_limit_period': ''},
    ]

    # custom_fields contains anything (bad format or incomplete)
    bad_values = [
        {'foo': 'bar'},
        ['foo', 'bar'],
        [['foo', 'bar']],
        [{'foo': 'bar'}],
        [{'varname': '', 'label': 'Foo', 'field_type': 'text'}],
        [{'varname': 'foo', 'label': '', 'field_type': 'text'}],
        [{'varname': 'foo', 'label': 'Foo', 'field_type': ''}],
    ]
    for bad_value in bad_values:
        events_type.custom_fields = bad_value
        events_type.save()
        resp = app.get('/manage/events-type/%s/edit/' % events_type.pk)
        assert resp.form['form-TOTAL_FORMS'].value == '1'
        assert resp.form['form-0-varname'].value == ''
        assert resp.form['form-0-label'].value == ''
        assert resp.form['form-0-field_type'].value == ''


def test_edit_events_type_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    events_type = EventsType.objects.create(label='Foo bar')
    app = login(app, username='manager', password='manager')
    app.get('/manage/events-type/%s/edit/' % events_type.pk, status=403)


def test_delete_events_type(app, admin_user):
    events_type = EventsType.objects.create(label='Foo bar')

    app = login(app)
    resp = app.get('/manage/events-type/%s/' % events_type.pk, status=200)
    resp = resp.click(href='/manage/events-type/%s/delete/' % events_type.pk)
    resp = resp.form.submit()
    assert resp.location.endswith('/manage/events-types/')
    assert EventsType.objects.exists() is False
    assert EventsTypeSnapshot.objects.count() == 1


def test_delete_referenced_events_type(app):
    events_type = EventsType.objects.create(label='Foobar')
    agenda = Agenda.objects.create(label='Foo Bar', events_type=events_type)

    events_type.delete()

    agenda = Agenda.objects.get(pk=agenda.pk)
    assert agenda
    assert agenda.events_type is None


def test_delete_referenced_events_type_error(app, admin_user):
    events_type = EventsType.objects.create(label='Foobar')
    agenda = Agenda.objects.create(label='Foo Bar', events_type=events_type)

    app = login(app)

    resp = app.get('/manage/events-type/%s/' % events_type.pk, status=200)
    resp = resp.click(href='/manage/events-type/%s/delete/' % events_type.pk)

    resp.form.submit(status=403)

    assert EventsType.objects.get(pk=events_type.pk)
    assert 'Cannot delete event type "' in resp
    assert '<a href="/manage/agendas/%s/">%s</a>' % (agenda.pk, agenda) in resp

    agenda.delete()

    # Now deletion should be OK
    resp = app.get('/manage/events-type/%s/' % events_type.pk, status=200)
    resp = resp.click(href='/manage/events-type/%s/delete/' % events_type.pk)

    assert 'Cannot delete event type "' not in resp
    assert '<a href="/manage/agendas/%s/">%s</a>' % (agenda.pk, agenda) not in resp

    resp.form.submit()
    assert EventsType.objects.exists() is False


def test_delete_events_type_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    events_type = EventsType.objects.create(label='Foo bar')
    app = login(app, username='manager', password='manager')
    app.get('/manage/events-type/%s/delete/' % events_type.pk, status=403)


def test_inspect_events_type(app, admin_user):
    events_type = EventsType.objects.create(label='Foo bar')

    app = login(app)

    resp = app.get('/manage/events-type/%s/' % events_type.pk)
    with CaptureQueriesContext(connection) as ctx:
        resp = resp.click('Inspect')
        assert len(ctx.captured_queries) == 3
