import datetime

import pytest

from chrono.agendas.models import Agenda, AgendaSnapshot, Desk, MeetingType, TimePeriod
from tests.utils import login

pytestmark = pytest.mark.django_db


def test_manager_partial_bookings_meetings_add_agenda(app, admin_user, settings):
    app = login(app)
    resp = app.get('/manage/agendas/add/')
    assert 'partial-bookings' not in resp.text

    settings.PARTIAL_BOOKINGS_MEETINGS_ENABLED = True

    resp = app.get('/manage/agendas/add/')
    resp.form['label'] = 'Foo bar'
    resp.form['kind'] = 'partial-bookings'
    resp = resp.form.submit().follow()

    agenda = Agenda.objects.get(label='Foo bar')
    assert agenda.kind == 'meetings'
    assert agenda.partial_bookings is True
    assert agenda.meetingtype_set.count() == 1
    assert AgendaSnapshot.objects.count() == 1

    default_desk = agenda.desk_set.get()
    assert default_desk.label == 'Resource 1'

    resp = resp.click('Settings')

    assert 'Meeting Type' not in resp.text
    assert 'Backoffice booking' not in resp.text
    assert 'Desk' not in resp.text

    assert 'Booking settings' in resp.text
    assert 'Step between bookable times: 30 minutes' in resp.text

    resp = resp.click('Switch to invididual resource management').follow()
    assert 'Switch to global resource management' in resp.text

    resp = resp.click('New resource')

    assert 'New resource' in resp.text
    assert 'Copy settings of resource' in resp.text

    resp.form['label'] = 'Resource 2'
    resp = resp.form.submit().follow()

    assert AgendaSnapshot.objects.count() == 3

    snapshot = AgendaSnapshot.objects.first()
    assert snapshot.comment == 'added resource (Resource 2)'

    resp = resp.click('Configure', href='partial-bookings-settings')
    resp.form['booking_step'] = 15
    resp = resp.form.submit().follow()

    assert AgendaSnapshot.objects.count() == 4
    assert 'Step between bookable times: 15 minutes' in resp.text
    assert 'Minimal booking duration' not in resp.text
    assert 'Maximal booking duration' not in resp.text
    assert 'Minimal time between bookings' not in resp.text

    resp = resp.click('Configure', href='partial-bookings-settings')
    resp.form['minimal_booking_duration'] = 30
    resp.form['maximal_booking_duration'] = 60
    resp.form['minimal_time_between_bookings'] = 15
    resp = resp.form.submit().follow()

    assert 'Minimal booking duration: 30 minutes' in resp.text
    assert 'Maximal booking duration: 60 minutes' in resp.text
    assert 'Minimal time between bookings: 15 minutes' in resp.text

    resp = resp.click('Inspect')

    assert 'Meeting Type' not in resp.text
    assert 'Backoffice booking' not in resp.text
    assert 'Desk' not in resp.text
    assert (
        resp.pyquery('.parameter-step-between-bookable-times-in-minutes').text()
        == 'Step between bookable times (in minutes): 15'
    )
    assert (
        resp.pyquery('.parameter-minimal-booking-duration-in-minutes').text()
        == 'Minimal booking duration (in minutes): 30'
    )
    assert (
        resp.pyquery('.parameter-maximal-booking-duration-in-minutes').text()
        == 'Maximal booking duration (in minutes): 60'
    )
    assert (
        resp.pyquery('.parameter-minimal-time-between-bookings-in-minutes').text()
        == 'Minimal time between bookings (in minutes): 15'
    )


@pytest.mark.freeze_time('2025-02-06 14:00')
def test_manager_partial_bookings_meetings_calendar_views(app, admin_user, api_user):
    agenda = Agenda.objects.create(
        label='Foo bar',
        kind='meetings',
        partial_bookings=True,
        minimal_booking_delay=0,
    )
    MeetingType.objects.create(agenda=agenda, slug='booking-step', duration=30)
    desk = Desk.objects.create(agenda=agenda, slug='desk')

    today = datetime.date.today()
    TimePeriod.objects.create(
        weekday=today.weekday(), start_time=datetime.time(9, 0), end_time=datetime.time(17, 00), desk=desk
    )

    login(app)
    resp = app.get('/manage/agendas/%s/day/%s/%s/%s/' % (agenda.pk, today.year, today.month, today.day))

    assert len(resp.pyquery('th.hour')) == 8
    assert len(resp.pyquery('div.booking')) == 0

    # book some slots
    app.reset()
    app.authorization = ('Basic', ('john.doe', 'password'))
    # 1 hour
    app.post(
        '/api/agenda/%s/free-range/fillslot/' % agenda.slug,
        params={'start_datetime': '2025-02-06-1000', 'end_datetime': '2025-02-06-1100'},
    )
    # 3 hours
    app.post(
        '/api/agenda/%s/free-range/fillslot/' % agenda.slug,
        params={'start_datetime': '2025-02-06-1230', 'end_datetime': '2025-02-06-1530'},
    )

    app.reset()
    login(app)
    resp = app.get('/manage/agendas/%s/day/%s/%s/%s/' % (agenda.pk, today.year, today.month, today.day))

    assert len(resp.pyquery('div.booking')) == 2
    assert 'height: 100%;' in resp.pyquery('div.booking')[0].attrib['style']
    assert 'top: 0%;' in resp.pyquery('div.booking')[0].attrib['style']
    assert 'height: 300%;' in resp.pyquery('div.booking')[1].attrib['style']
    assert 'top: 50%;' in resp.pyquery('div.booking')[1].attrib['style']

    resp = app.get('/manage/agendas/%s/week/%s/%s/%s/' % (agenda.pk, today.year, today.month, today.day))
    assert len(resp.pyquery('div.booking')) == 2
    assert 'height:100.0%;' in resp.pyquery('div.booking')[0].attrib['style']
    assert 'top:100.0%;' in resp.pyquery('div.booking')[0].attrib['style']
    assert 'height:300.0%;' in resp.pyquery('div.booking')[1].attrib['style']
    assert 'top:350.0%;' in resp.pyquery('div.booking')[1].attrib['style']

    resp = app.get('/manage/agendas/%s/month/%s/%s/%s/' % (agenda.pk, today.year, today.month, today.day))
    assert len(resp.pyquery('div.booking')) == 2
    assert 'height:100.0%;' in resp.pyquery('div.booking')[0].attrib['style']
    assert 'top:100.0%;' in resp.pyquery('div.booking')[0].attrib['style']
    assert 'height:300.0%;' in resp.pyquery('div.booking')[1].attrib['style']
    assert 'top:350.0%;' in resp.pyquery('div.booking')[1].attrib['style']
