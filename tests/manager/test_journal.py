import datetime

import pytest
from pyquery import PyQuery

from chrono.agendas.models import Agenda, Booking, Event
from chrono.apps.journal.models import AuditEntry
from chrono.apps.journal.utils import audit
from chrono.utils.timezone import make_aware
from tests.utils import login

pytestmark = pytest.mark.django_db


def test_journal_permission(app, admin_user, manager_user):
    app = login(app, username='manager', password='manager')
    resp = app.get('/manage/', status=403)
    assert 'Audit journal' not in resp.text
    app.get('/manage/journal/', status=403)
    app = login(app)
    resp = app.get('/manage/')
    assert 'Audit journal' in resp.text
    app.get('/manage/journal/', status=200)


def test_journal_browse(app, admin_user, manager_user, settings):
    admin_user.first_name = 'Admin'
    admin_user.save()
    manager_user.first_name = 'Manager'
    manager_user.save()

    # some audit events
    agendas = [
        Agenda.objects.create(label='Foo', kind='events'),
        Agenda.objects.create(label='Bar', kind='events'),
        Agenda.objects.create(label='Baz', kind='events'),
    ]

    event = Event.objects.create(
        start_datetime=make_aware(datetime.datetime(2024, 1, 2, 3, 4)), places=20, agenda=agendas[0]
    )
    booking = Booking.objects.create(event=event, user_external_id='user:01')
    event2 = Event.objects.create(
        start_datetime=make_aware(datetime.datetime(2024, 1, 2, 3, 4)),
        places=20,
        label='foobar',
        agenda=agendas[0],
    )

    for i in range(20):
        user = admin_user if i % 3 else manager_user
        agenda = agendas[i % 3]

        entry = audit(
            'booking:cancel',
            user=user,
            agenda=agenda,
            extra_data={'booking': booking},
        )
        entry.timestamp = make_aware(
            datetime.datetime(2024, 1, 1) + datetime.timedelta(days=i, hours=i, minutes=i)
        )
        entry.save()

        entry = audit(
            'check:absence', user=user, agenda=agenda, extra_data={'user_name': 'User', 'event': event2}
        )
        entry.timestamp = make_aware(
            datetime.datetime(2024, 1, 2) + datetime.timedelta(days=i, hours=i, minutes=i)
        )
        entry.save()

    app = login(app)
    resp = app.get('/manage/journal/')

    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == [
        [
            'Jan. 21, 2024, 7:19 p.m.',
            'Admin',
            'Bar',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 20, 2024, 7:19 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 20, 2024, 6:18 p.m.',
            'Manager',
            'Foo',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 19, 2024, 6:18 p.m.',
            'Manager',
            'Foo',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 19, 2024, 5:17 p.m.',
            'Admin',
            'Baz',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 18, 2024, 5:17 p.m.',
            'Admin',
            'Baz',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 18, 2024, 4:16 p.m.',
            'Admin',
            'Bar',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 17, 2024, 4:16 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 17, 2024, 3:15 p.m.',
            'Manager',
            'Foo',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 16, 2024, 3:15 p.m.',
            'Manager',
            'Foo',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
    ]

    resp = resp.click('2')  # pagination

    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == [
        [
            'Jan. 16, 2024, 2:14 p.m.',
            'Admin',
            'Baz',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 15, 2024, 2:14 p.m.',
            'Admin',
            'Baz',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 15, 2024, 1:13 p.m.',
            'Admin',
            'Bar',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 14, 2024, 1:13 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 14, 2024, 12:12 p.m.',
            'Manager',
            'Foo',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 13, 2024, 12:12 p.m.',
            'Manager',
            'Foo',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 13, 2024, 11:11 a.m.',
            'Admin',
            'Baz',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 12, 2024, 11:11 a.m.',
            'Admin',
            'Baz',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 12, 2024, 10:10 a.m.',
            'Admin',
            'Bar',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
        [
            'Jan. 11, 2024, 10:10 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
    ]

    # filters
    assert resp.form['timestamp'].attrs == {'type': 'date'}
    resp.form['timestamp'].value = '2024-01-19'
    resp = resp.form.submit()
    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == [
        [
            'Jan. 19, 2024, 6:18 p.m.',
            'Manager',
            'Foo',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 19, 2024, 5:17 p.m.',
            'Admin',
            'Baz',
            'marked absence of User in foobar (01/02/2024 3:04 a.m.)',
        ],
    ]

    assert resp.form['timestamp'].value == '2024-01-19'
    resp.form['agenda'].value = agendas[0].id
    resp = resp.form.submit()
    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == [
        [
            'Jan. 19, 2024, 6:18 p.m.',
            'Manager',
            'Foo',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ]
    ]

    resp.form['agenda'].value = agendas[1].id
    resp = resp.form.submit()
    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == []

    resp.form['timestamp'].value = ''
    resp.form['action_type'].value = 'booking'
    resp = resp.form.submit()
    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == [
        [
            'Jan. 20, 2024, 7:19 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 17, 2024, 4:16 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 14, 2024, 1:13 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 11, 2024, 10:10 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 8, 2024, 7:07 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 5, 2024, 4:04 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 2, 2024, 1:01 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
    ]

    resp.form['action_type'].value = ''
    resp.form['user_external_id'].value = 'user:01'
    resp = resp.form.submit()
    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == [
        [
            'Jan. 20, 2024, 7:19 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 17, 2024, 4:16 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 14, 2024, 1:13 p.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 11, 2024, 10:10 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 8, 2024, 7:07 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 5, 2024, 4:04 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
        [
            'Jan. 2, 2024, 1:01 a.m.',
            'Admin',
            'Bar',
            f'cancellation of booking ({booking.id}) in event "01/02/2024 3:04 a.m."',
        ],
    ]


def test_journal_audit_booking():
    agenda = Agenda.objects.create(label='Bar', kind='events')
    event = Event.objects.create(
        start_datetime=make_aware(datetime.datetime(2024, 1, 2, 3, 4)), places=20, agenda=agenda
    )
    booking = Booking.objects.create(
        event=event,
        in_waiting_list=True,
        cancellation_datetime=make_aware(datetime.datetime(2024, 1, 2, 3, 4)),
    )

    entry = audit(
        'booking:cancel',
        agenda=agenda,
        extra_data={'booking': booking},
    )
    assert (
        entry.extra_data['booking']
        == f'ID: {booking.id} / in waiting list / cancelled at 01/02/2024 3:04 a.m.'
    )

    booking.user_first_name = 'first'
    booking.user_last_name = 'last'
    booking.in_waiting_list = False
    booking.save()
    entry = audit(
        'booking:cancel',
        agenda=agenda,
        extra_data={'booking': booking},
    )
    assert (
        entry.extra_data['booking']
        == f'ID: {booking.id} / user: first last / cancelled at 01/02/2024 3:04 a.m.'
    )

    booking.cancellation_datetime = None
    booking.start_time = datetime.time(10, 0)
    booking.save()
    entry = audit(
        'booking:cancel',
        agenda=agenda,
        extra_data={'booking': booking},
    )
    assert entry.extra_data['booking'] == f'ID: {booking.id} / user: first last / 10 a.m. → ?'

    booking.end_time = datetime.time(11, 0)
    booking.save()
    entry = audit(
        'booking:cancel',
        agenda=agenda,
        extra_data={'booking': booking},
    )
    assert entry.extra_data['booking'] == f'ID: {booking.id} / user: first last / 10 a.m. → 11 a.m.'

    booking.start_time = None
    booking.save()
    entry = audit(
        'booking:cancel',
        agenda=agenda,
        extra_data={'booking': booking},
    )
    assert entry.extra_data['booking'] == f'ID: {booking.id} / user: first last / ? → 11 a.m.'


def test_journal_browse_invalid_or_unknown_event(app, admin_user, settings):
    admin_user.first_name = 'Admin'
    admin_user.save()
    AuditEntry.objects.all().delete()

    agenda = Agenda.objects.create(label='Foo', kind='events')
    event = Event.objects.create(
        start_datetime=make_aware(datetime.datetime(2024, 1, 2, 3, 4)), places=20, agenda=agenda
    )
    entry = audit(
        'booking:cancel',
        user=admin_user,
        agenda=agenda,
        extra_data={'event': event},  # missing booking_id
    )
    entry.timestamp = make_aware(datetime.datetime(2024, 1, 1))
    entry.save()
    resp = login(app).get('/manage/journal/')
    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == [
        ['Jan. 1, 2024, midnight', 'Admin', 'Foo', 'Unknown entry (booking:cancel)']
    ]

    AuditEntry.objects.all().delete()
    entry = audit(
        'foo:bar',
        user=admin_user,
        agenda=agenda,
    )
    entry.timestamp = make_aware(datetime.datetime(2024, 1, 1))
    entry.save()
    resp = login(app).get('/manage/journal/')
    assert [[x.text for x in PyQuery(x).find('td')] for x in resp.pyquery('tbody tr')] == [
        ['Jan. 1, 2024, midnight', 'Admin', 'Foo', 'Unknown entry (foo:bar)']
    ]
