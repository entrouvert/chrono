import copy
import datetime
from unittest import mock

import pytest

from chrono.agendas.models import Agenda, Booking, BookingCheck, Category, Event
from chrono.utils.lingo import CheckType, LingoError
from chrono.utils.timezone import make_aware, now
from tests.utils import login

pytestmark = pytest.mark.django_db


def test_events_report_form(app, admin_user):
    login(app)
    resp = app.get('/manage/agendas/events/report/')
    resp.form['date_start'] = '2022-01-01'
    resp.form['date_end'] = '2021-12-31'
    resp = resp.form.submit()
    assert resp.context['form'].errors['date_end'] == ['End date must be greater than start date.']

    resp.form['date_end'] = '2022-04-02'
    resp = resp.form.submit()
    assert resp.context['form'].errors['date_end'] == ['Please select an interval of no more than 3 months.']

    resp.form['date_end'] = '2022-04-01'
    resp = resp.form.submit()
    assert resp.context['form'].errors == {
        'agendas': ['This field is required.'],
        'status': ['This field is required.'],
    }


@pytest.mark.freeze_time('2022-02-15')
def test_events_report(app, admin_user):
    start, end = (
        now() - datetime.timedelta(days=15),
        now() + datetime.timedelta(days=14),
    )  # 2022-02-31, 2022-03-01
    agenda1 = Agenda.objects.create(label='Events 1', kind='events')
    agenda2 = Agenda.objects.create(label='Events 2', kind='events')
    Event.objects.create(
        label='event 0',
        start_datetime=start,
        places=10,
        agenda=agenda1,
        checked=False,
        invoiced=False,
        check_locked=False,
    )
    event1 = Event.objects.create(
        label='event 1',
        start_datetime=start + datetime.timedelta(days=1),
        places=10,
        agenda=agenda1,
        checked=False,
        invoiced=False,
        check_locked=False,
    )
    Booking.objects.create(event=event1)
    event2 = Event.objects.create(
        label='event 2',
        start_datetime=start + datetime.timedelta(days=2),
        places=10,
        agenda=agenda1,
        checked=True,
        invoiced=False,
        check_locked=True,
    )
    Booking.objects.create(event=event2, cancellation_datetime=now())
    event3 = Event.objects.create(
        label='event 3',
        start_datetime=now(),
        places=10,
        agenda=agenda2,
        checked=False,
        invoiced=True,
        check_locked=True,
    )
    booking = Booking.objects.create(event=event3)
    BookingCheck.objects.create(booking=booking, presence=True)
    event4 = Event.objects.create(
        label='event 4',
        start_datetime=end - datetime.timedelta(days=1),
        places=10,
        agenda=agenda2,
        checked=True,
        invoiced=True,
        check_locked=False,
    )
    Event.objects.create(
        label='event 5',
        start_datetime=end,
        places=10,
        agenda=agenda2,
        checked=False,
        invoiced=False,
        check_locked=False,
    )
    Event.objects.create(
        label='event cancelled',
        start_datetime=now() + datetime.timedelta(days=4),
        places=10,
        agenda=agenda1,
        cancelled=True,
        checked=False,
        invoiced=False,
        check_locked=False,
    )
    recurring_event = Event.objects.create(
        label='recurring',
        start_datetime=start + datetime.timedelta(days=1),
        places=10,
        agenda=agenda2,
        recurrence_days=[1, 2],
        recurrence_end_date=now(),
        checked=False,
        invoiced=False,
        check_locked=False,
    )
    recurring_event.create_all_recurrences()
    recurrences = recurring_event.recurrences.all()
    assert recurrences.count() == 4
    recurrent_event1 = recurrences[0]
    recurrent_event1.checked = False
    recurrent_event1.invoiced = False
    recurrent_event1.check_locked = False
    recurrent_event1.save()
    Booking.objects.create(event=recurrent_event1)
    recurrent_event2 = recurrences[1]
    recurrent_event2.checked = True
    recurrent_event2.invoiced = False
    recurrent_event2.check_locked = True
    recurrent_event2.save()
    Booking.objects.create(event=recurrent_event2, cancellation_datetime=now())
    recurrent_event3 = recurrences[2]
    recurrent_event3.checked = False
    recurrent_event3.invoiced = True
    recurrent_event3.check_locked = True
    recurrent_event3.save()
    booking = Booking.objects.create(event=recurrent_event3)
    BookingCheck.objects.create(booking=booking, presence=True)
    recurrent_event4 = recurrences[3]
    recurrent_event4.checked = True
    recurrent_event4.invoiced = True
    recurrent_event4.check_locked = False
    recurrent_event4.save()

    def add_agenda(resp):
        select = copy.copy(resp.form.fields['agendas'][0])
        select.id = 'id_agendas_1'
        resp.form.fields['agendas'].append(select)
        resp.form.field_order.append(('agendas', select))

    login(app)
    resp = app.get('/manage/agendas/events/report/')
    assert resp.form['agendas'].options == [
        ('', True, '---------'),
        (agenda1.slug, False, 'Events 1'),
        (agenda2.slug, False, 'Events 2'),
    ]
    add_agenda(resp)
    resp.form['date_start'] = '2022-02-01'
    resp.form['date_end'] = '2022-02-28'
    resp.form.get('agendas', 0).value = agenda1.slug
    resp.form.get('agendas', 1).value = agenda2.slug
    resp.form['status'] = ['bookings-not-checked', 'not-checked', 'not-invoiced', 'locked', 'not-locked']
    resp = resp.form.submit()
    events = resp.context['form'].get_events()
    assert list(events['bookings_not_checked']) == [event1, recurrent_event1]
    assert list(events['not_checked']) == [event1, recurrent_event1, recurrent_event3, event3]
    assert list(events['not_invoiced']) == [event1, recurrent_event1, event2, recurrent_event2]
    assert list(events['locked']) == [event2, recurrent_event2, recurrent_event3, event3]
    assert list(events['not_locked']) == [event1, recurrent_event1, recurrent_event4, event4]
    assert [label.text.strip() for label in resp.pyquery.find('ul.objects-list li a')] == [
        'Events 1 / event 1 /  Feb. 1, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 1, 2022, 1 a.m.',
        'Events 1 / event 1 /  Feb. 1, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 1, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 8, 2022, 1 a.m.',
        'Events 2 / event 3 /  Feb. 15, 2022, 1 a.m.',
        'Events 1 / event 1 /  Feb. 1, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 1, 2022, 1 a.m.',
        'Events 1 / event 2 /  Feb. 2, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 7, 2022, 1 a.m.',
        'Events 1 / event 2 /  Feb. 2, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 7, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 8, 2022, 1 a.m.',
        'Events 2 / event 3 /  Feb. 15, 2022, 1 a.m.',
        'Events 1 / event 1 /  Feb. 1, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 1, 2022, 1 a.m.',
        'Events 2 / recurring /  Feb. 14, 2022, 1 a.m.',
        'Events 2 / event 4 /  Feb. 28, 2022, 1 a.m.',
    ]

    resp = app.get('/manage/agendas/events/report/')
    add_agenda(resp)
    resp.form['date_start'] = '2022-02-01'
    resp.form['date_end'] = '2022-02-28'
    resp.form.get('agendas', 0).value = agenda1.slug
    resp.form.get('agendas', 1).value = agenda2.slug
    resp.form['status'] = ['not-checked', 'locked']
    resp = resp.form.submit()
    events = resp.context['form'].get_events()
    assert list(events['not_checked']) == [event1, recurrent_event1, recurrent_event3, event3]
    assert 'not_invoiced' not in events
    assert list(events['locked']) == [event2, recurrent_event2, recurrent_event3, event3]
    assert 'not-locked' not in events

    resp = app.get('/manage/agendas/events/report/')
    add_agenda(resp)
    resp.form['date_start'] = '2022-02-01'
    resp.form['date_end'] = '2022-02-28'
    resp.form.get('agendas', 0).value = agenda1.slug
    resp.form.get('agendas', 1).value = agenda2.slug
    resp.form['status'] = ['not-invoiced', 'not-locked']
    resp = resp.form.submit()
    events = resp.context['form'].get_events()
    assert 'not_checked' not in events
    assert list(events['not_invoiced']) == [event1, recurrent_event1, event2, recurrent_event2]
    assert 'locked' not in events
    assert list(events['not_locked']) == [event1, recurrent_event1, recurrent_event4, event4]

    resp = app.get('/manage/agendas/events/report/')
    resp.form['date_start'] = '2022-02-01'
    resp.form['date_end'] = '2022-02-28'
    resp.form.get('agendas', 0).value = agenda2.slug
    resp.form['status'] = ['not-invoiced', 'not-locked']
    resp = resp.form.submit()
    events = resp.context['form'].get_events()
    assert 'not_checked' not in events
    assert list(events['not_invoiced']) == [recurrent_event1, recurrent_event2]
    assert 'locked' not in events
    assert list(events['not_locked']) == [recurrent_event1, recurrent_event4, event4]

    resp = app.get('/manage/agendas/events/report/')
    resp.form['date_start'] = '2022-02-01'
    resp.form['date_end'] = '2022-02-28'
    resp.form.get('agendas', 0).value = agenda2.slug
    resp.form['status'] = ['bookings-not-checked', 'not-checked', 'not-invoiced', 'locked', 'not-locked']
    resp = resp.form.submit()
    events = resp.context['form'].get_events()
    assert list(events['bookings_not_checked']) == [recurrent_event1]
    assert list(events['not_checked']) == [recurrent_event1, recurrent_event3, event3]
    assert list(events['not_invoiced']) == [recurrent_event1, recurrent_event2]
    assert list(events['locked']) == [recurrent_event2, recurrent_event3, event3]
    assert list(events['not_locked']) == [recurrent_event1, recurrent_event4, event4]

    category = Category.objects.create(label='cat')
    agenda1.category = category
    agenda1.save()
    resp = app.get('/manage/agendas/events/report/?agendas=%s&category=%s' % (agenda1.slug, category.slug))
    assert list(resp.context['form'].cleaned_data['agendas']) == [agenda1]
    assert resp.context['form'].cleaned_data['category'] == category


def test_events_report_csv(app, admin_user):
    agenda = Agenda.objects.create(label='Events', kind='events')
    Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda,
        checked=False,
        invoiced=False,
        check_locked=False,
    )
    recurring_event = Event.objects.create(
        label='recurring',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda,
        recurrence_days=[2],
        recurrence_end_date=datetime.date(2022, 2, 2),
        checked=False,
        invoiced=False,
    )
    recurring_event.create_all_recurrences()
    Event.objects.filter(primary_event=recurring_event).update(check_locked=True)

    login(app)
    resp = app.get(
        '/manage/agendas/events/report/?csv=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s'
        '&status=not-checked&status=not-invoiced&status=locked&status=not-locked' % agenda.slug
    )
    assert resp.headers['Content-Type'] == 'text/csv'
    assert (
        resp.headers['Content-Disposition']
        == 'attachment; filename="events_report_2022-02-01_2022-02-28.csv"'
    )
    assert resp.text == (
        '\ufeff"Not checked"\r\n'
        '"agenda";"label";"date";"time"\r\n'
        '"Events";"event";"2022-02-01";"11:00"\r\n'
        '"Events";"recurring";"2022-02-01";"11:00"\r\n'
        '"Not invoiced"\r\n'
        '"agenda";"label";"date";"time"\r\n'
        '"Events";"event";"2022-02-01";"11:00"\r\n'
        '"Events";"recurring";"2022-02-01";"11:00"\r\n'
        '"Check locked"\r\n'
        '"agenda";"label";"date";"time"\r\n'
        '"Events";"recurring";"2022-02-01";"11:00"\r\n'
        '"Check not locked"\r\n'
        '"agenda";"label";"date";"time"\r\n'
        '"Events";"event";"2022-02-01";"11:00"\r\n'
    )

    resp = app.get(
        '/manage/agendas/events/report/?csv=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&status=not-checked'
        % agenda.slug
    )
    assert resp.text == (
        '\ufeff"Not checked"\r\n'
        '"agenda";"label";"date";"time"\r\n'
        '"Events";"event";"2022-02-01";"11:00"\r\n'
        '"Events";"recurring";"2022-02-01";"11:00"\r\n'
    )

    resp = app.get(
        '/manage/agendas/events/report/?csv=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&status=not-invoiced'
        % agenda.slug
    )
    assert resp.text == (
        '\ufeff"Not invoiced"\r\n'
        '"agenda";"label";"date";"time"\r\n'
        '"Events";"event";"2022-02-01";"11:00"\r\n'
        '"Events";"recurring";"2022-02-01";"11:00"\r\n'
    )

    resp = app.get(
        '/manage/agendas/events/report/?csv=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&status=locked'
        % agenda.slug
    )
    assert resp.text == (
        '\ufeff"Check locked"\r\n'
        '"agenda";"label";"date";"time"\r\n'
        '"Events";"recurring";"2022-02-01";"11:00"\r\n'
    )

    resp = app.get(
        '/manage/agendas/events/report/?csv=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&status=not-locked'
        % agenda.slug
    )
    assert resp.text == (
        '\ufeff"Check not locked"\r\n'
        '"agenda";"label";"date";"time"\r\n'
        '"Events";"event";"2022-02-01";"11:00"\r\n'
    )

    # form invalid
    resp = app.get(
        '/manage/agendas/events/report/?csv=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s'
        % agenda.slug
    )
    assert resp.context['form'].errors['status'] == ['This field is required.']


@mock.patch('chrono.manager.views.get_agenda_check_types')
@pytest.mark.freeze_time('2022-02-15 14:00')
def test_events_report_check_absences(check_types, app, admin_user):
    agenda1 = Agenda.objects.create(label='Events', kind='events', enable_check_for_future_events=True)
    event = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda1,
        checked=False,
    )
    booking = Booking.objects.create(event=event)
    event_future1 = Event.objects.create(
        label='event future1',
        start_datetime=make_aware(datetime.datetime(2022, 2, 28, 12, 0)),
        places=10,
        agenda=agenda1,
        checked=False,
    )
    booking_future1 = Booking.objects.create(event=event_future1)
    booking_cancelled = Booking.objects.create(event=event, cancellation_datetime=now())
    booking_secondary = Booking.objects.create(event=event, primary_booking=booking)
    booking_waiting = Booking.objects.create(event=event, in_waiting_list=True)
    booking_checked_presence = Booking.objects.create(event=event)
    booking_checked_presence.mark_user_presence()
    booking_checked_absence = Booking.objects.create(event=event)
    booking_checked_absence.mark_user_absence()
    event_locked = Event.objects.create(
        label='event locked',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda1,
        checked=False,
        check_locked=True,
    )
    booking_locked = Booking.objects.create(event=event_locked)
    agenda2 = Agenda.objects.create(label='Events', kind='events', enable_check_for_future_events=False)
    event_future2 = Event.objects.create(
        label='event future2',
        start_datetime=make_aware(datetime.datetime(2022, 2, 28, 12, 0)),
        places=10,
        agenda=agenda2,
        checked=False,
    )
    booking_future2 = Booking.objects.create(event=event_future2)

    login(app)

    # no check types, no button
    check_types.return_value = []
    resp = app.get(
        '/manage/agendas/events/report/?date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&status=not-checked'
        % (agenda1.pk, agenda2.pk)
    )
    assert 'check-absences' not in resp
    # no check types, nothing changed
    resp = app.get(
        '/manage/agendas/events/report/?check-absences=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&status=not-checked'
        % (agenda1.pk, agenda2.pk)
    )
    booking.refresh_from_db()
    booking_future1.refresh_from_db()
    booking_cancelled.refresh_from_db()
    booking_secondary.refresh_from_db()
    booking_waiting.refresh_from_db()
    booking_checked_presence.refresh_from_db()
    booking_checked_absence.refresh_from_db()
    booking_locked.refresh_from_db()
    booking_future2.refresh_from_db()
    assert booking.user_checks.count() == 0
    assert booking_future1.user_checks.count() == 0
    assert booking_cancelled.user_checks.count() == 0
    assert booking_secondary.user_checks.count() == 0
    assert booking_waiting.user_checks.count() == 0
    assert booking_checked_presence.user_checks.count() == 1
    assert booking_checked_presence.user_checks.first().presence is True
    assert booking_checked_absence.user_checks.count() == 1
    assert booking_checked_absence.user_checks.first().presence is False
    assert booking_locked.user_checks.count() == 0
    assert booking_future2.user_checks.count() == 0

    # check types, but no unjustified_absence
    check_types.return_value = [
        CheckType(slug='foo-reason', label='Foo reason', kind='absence'),
        CheckType(slug='bar-reason', label='Bar reason', kind='presence'),
    ]
    resp = app.get(
        '/manage/agendas/events/report/?date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&status=not-checked'
        % (agenda1.pk, agenda2.pk)
    )
    assert 'check-absences' not in resp

    # should not happen, but unjustified_absence has wrong kind
    check_types.return_value = [
        CheckType(slug='foo-reason', label='Foo reason', kind='absence'),
        CheckType(slug='bar-reason', label='Bar reason', kind='presence', unjustified_absence=True),
    ]
    resp = app.get(
        '/manage/agendas/events/report/?date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&status=not-checked'
        % (agenda1.pk, agenda2.pk)
    )
    assert 'check-absences' not in resp
    resp = app.get(
        '/manage/agendas/events/report/?check-absences=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&status=not-checked'
        % (agenda1.pk, agenda2.pk)
    )
    booking.refresh_from_db()
    booking_future1.refresh_from_db()
    booking_cancelled.refresh_from_db()
    booking_secondary.refresh_from_db()
    booking_waiting.refresh_from_db()
    booking_checked_presence.refresh_from_db()
    booking_checked_absence.refresh_from_db()
    booking_locked.refresh_from_db()
    booking_future2.refresh_from_db()
    assert booking.user_checks.count() == 0
    assert booking_future1.user_checks.count() == 0
    assert booking_cancelled.user_checks.count() == 0
    assert booking_secondary.user_checks.count() == 0
    assert booking_waiting.user_checks.count() == 0
    assert booking_checked_presence.user_checks.count() == 1
    assert booking_checked_presence.user_checks.first().presence is True
    assert booking_checked_absence.user_checks.count() == 1
    assert booking_checked_absence.user_checks.first().presence is False
    assert booking_locked.user_checks.count() == 0
    assert booking_future2.user_checks.count() == 0

    # with unjustified_absence
    check_types.return_value = [
        CheckType(slug='foo-reason', label='Foo reason', kind='absence', unjustified_absence=True),
        CheckType(slug='bar-reason', label='Bar reason', kind='presence'),
    ]
    resp = app.get(
        '/manage/agendas/events/report/?date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&status=not-checked'
        % (agenda1.slug, agenda2.slug)
    )
    assert 'check-absences' in resp
    resp = app.get(
        '/manage/agendas/events/report/?check-absences=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&status=not-checked'
        % (agenda1.slug, agenda2.slug)
    )
    booking.refresh_from_db()
    booking_future1.refresh_from_db()
    booking_cancelled.refresh_from_db()
    booking_secondary.refresh_from_db()
    booking_waiting.refresh_from_db()
    booking_checked_presence.refresh_from_db()
    booking_checked_absence.refresh_from_db()
    booking_locked.refresh_from_db()
    booking_future2.refresh_from_db()
    assert booking.user_checks.count() == 1
    assert booking.user_checks.first().presence is False
    assert booking.user_checks.first().type_slug == 'foo-reason'
    assert booking.user_checks.first().type_label == 'Foo reason'
    assert booking_future1.user_checks.count() == 1
    assert booking_future1.user_checks.first().presence is False
    assert booking_future1.user_checks.first().type_slug == 'foo-reason'
    assert booking_future1.user_checks.first().type_label == 'Foo reason'
    assert booking_cancelled.user_checks.count() == 0
    assert booking_secondary.user_checks.count() == 0
    assert booking_waiting.user_checks.count() == 0
    assert booking_checked_presence.user_checks.count() == 1
    assert booking_checked_presence.user_checks.first().presence is True
    assert booking_checked_absence.user_checks.count() == 1
    assert booking_checked_absence.user_checks.first().presence is False
    assert booking_locked.user_checks.count() == 0
    assert booking_future2.user_checks.count() == 0

    # form invalid
    resp = app.get(
        '/manage/agendas/events/report/?check-absences=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s'
        % agenda1.slug
    )
    assert resp.context['form'].errors['status'] == ['This field is required.']


@mock.patch('chrono.manager.views.unlock_agendas')
def test_events_report_unlock_events(unlock_agendas, app, admin_user):
    agenda1 = Agenda.objects.create(label='Events 1', kind='events')
    agenda2 = Agenda.objects.create(label='Events 2', kind='events')
    partial_bookings_agenda = Agenda.objects.create(label='Events', kind='events', partial_bookings=True)
    event1 = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda1,
        check_locked=True,
    )
    not_locked_event = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda1,
        check_locked=False,
    )
    event2 = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda2,
        check_locked=True,
    )
    partial_bookings_event = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=partial_bookings_agenda,
        check_locked=True,
    )
    booking = Booking.objects.create(
        event=partial_bookings_event,
        user_external_id='child:42',
        start_time=datetime.time(8, 0),
        end_time=datetime.time(17, 0),
    )
    booking.mark_user_presence(
        check_type_slug='foo-reason',
        start_time=datetime.time(7, 55),
        end_time=datetime.time(17, 15),
    )
    # computed times are None, because refresh_computed_times was not called in this test
    assert booking.user_check.computed_start_time is None
    assert booking.user_check.computed_end_time is None

    login(app)

    resp = app.get(
        '/manage/agendas/events/report/?date_start=2022-02-01&date_end=2022-02-28&agendas=%s&status=locked'
        % agenda1.slug
    )
    assert '<button class="submit-button" name="unlock">' in resp
    app.get(
        '/manage/agendas/events/report/?unlock=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&status=locked'
        % agenda1.slug
    )
    event1.refresh_from_db()
    assert event1.check_locked is False
    not_locked_event.refresh_from_db()
    assert not_locked_event.check_locked is False
    event2.refresh_from_db()
    assert event2.check_locked is True
    partial_bookings_event.refresh_from_db()
    assert partial_bookings_event.check_locked is True
    assert unlock_agendas.call_args_list == [
        mock.call(
            agenda_slugs=['events-1'],
            date_start=datetime.date(2022, 2, 1),
            date_end=datetime.date(2022, 3, 1),
        )
    ]

    event1.check_locked = True
    event1.save()
    unlock_agendas.reset_mock()
    app.get(
        '/manage/agendas/events/report/?unlock=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&agendas=%s&status=locked'
        % (agenda1.slug, agenda2.slug, partial_bookings_agenda.slug)
    )
    event1.refresh_from_db()
    assert event1.check_locked is False
    not_locked_event.refresh_from_db()
    assert not_locked_event.check_locked is False
    event2.refresh_from_db()
    assert event2.check_locked is False
    partial_bookings_event.refresh_from_db()
    assert partial_bookings_event.check_locked is False
    assert unlock_agendas.call_args_list == [
        mock.call(
            agenda_slugs=['events-1', 'events-2'],
            date_start=datetime.date(2022, 2, 1),
            date_end=datetime.date(2022, 3, 1),
        )
    ]
    booking.refresh_from_db()
    assert booking.user_check.computed_start_time == datetime.time(7, 0)
    assert booking.user_check.computed_end_time == datetime.time(18, 0)

    event1.check_locked = True
    event1.save()
    unlock_agendas.side_effect = LingoError('oups')
    resp = app.get(
        '/manage/agendas/events/report/?unlock=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&agendas=%s&status=locked'
        % (agenda1.slug, agenda2.slug, partial_bookings_agenda.slug)
    )
    assert '<li class="error">oups</li>' in resp
    event1.refresh_from_db()
    assert event1.check_locked is True

    # form invalid
    resp = app.get(
        '/manage/agendas/events/report/?unlock=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&agendas=%s'
        % (agenda1.slug, agenda2.slug, partial_bookings_agenda.slug)
    )
    assert resp.context['form'].errors['status'] == ['This field is required.']


@mock.patch('chrono.manager.views.unlock_agendas')
def test_events_report_lock_events(unlock_agendas, app, admin_user):
    agenda1 = Agenda.objects.create(label='Events 1', kind='events')
    agenda2 = Agenda.objects.create(label='Events 2', kind='events')
    partial_bookings_agenda = Agenda.objects.create(label='Events', kind='events', partial_bookings=True)
    event1 = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda1,
        check_locked=False,
    )
    locked_event = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda1,
        check_locked=True,
    )
    event2 = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=agenda2,
        check_locked=False,
    )
    partial_bookings_event = Event.objects.create(
        label='event',
        start_datetime=make_aware(datetime.datetime(2022, 2, 1, 12, 0)),
        places=10,
        agenda=partial_bookings_agenda,
        check_locked=False,
    )
    booking = Booking.objects.create(
        event=partial_bookings_event,
        user_external_id='child:42',
        start_time=datetime.time(8, 0),
        end_time=datetime.time(17, 0),
    )
    booking.mark_user_presence(
        check_type_slug='foo-reason',
        start_time=datetime.time(7, 55),
        end_time=datetime.time(17, 15),
    )
    # computed times are None, because refresh_computed_times was not called in this test
    assert booking.user_check.computed_start_time is None
    assert booking.user_check.computed_end_time is None

    login(app)

    resp = app.get(
        '/manage/agendas/events/report/?date_start=2022-02-01&date_end=2022-02-28&agendas=%s&status=not-locked'
        % agenda1.slug
    )
    assert '<button class="submit-button" name="lock">' in resp
    app.get(
        '/manage/agendas/events/report/?lock=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&status=not-locked'
        % agenda1.slug
    )
    event1.refresh_from_db()
    assert event1.check_locked is True
    locked_event.refresh_from_db()
    assert locked_event.check_locked is True
    event2.refresh_from_db()
    assert event2.check_locked is False
    partial_bookings_event.refresh_from_db()
    assert partial_bookings_event.check_locked is False
    assert unlock_agendas.call_args_list == []

    event1.check_locked = False
    event1.save()
    app.get(
        '/manage/agendas/events/report/?lock=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&agendas=%s&status=not-locked'
        % (agenda1.slug, agenda2.slug, partial_bookings_agenda.slug)
    )
    event1.refresh_from_db()
    assert event1.check_locked is True
    locked_event.refresh_from_db()
    assert locked_event.check_locked is True
    event2.refresh_from_db()
    assert event2.check_locked is True
    partial_bookings_event.refresh_from_db()
    assert partial_bookings_event.check_locked is True
    assert unlock_agendas.call_args_list == []
    booking.refresh_from_db()
    # computed times are still None, refresh_computed_times is not called on lock
    assert booking.user_check.computed_start_time is None
    assert booking.user_check.computed_end_time is None

    # form invalid
    resp = app.get(
        '/manage/agendas/events/report/?lock=&date_start=2022-02-01&date_end=2022-02-28&agendas=%s&agendas=%s&agendas=%s'
        % (agenda1.slug, agenda2.slug, partial_bookings_agenda.slug)
    )
    assert resp.context['form'].errors['status'] == ['This field is required.']


def test_events_report_as_manager(app, manager_user):
    agenda = Agenda(label='Foo bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    app = login(app, username='manager', password='manager')
    resp = app.get('/manage/')
    assert '/manage/agendas/events/report/' not in resp
    app.get('/manage/agendas/events/report/', status=403)
