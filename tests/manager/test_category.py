import pytest
from django.db import connection
from django.test.utils import CaptureQueriesContext

from chrono.agendas.models import Agenda, Category
from chrono.apps.snapshot.models import CategorySnapshot
from chrono.apps.user_preferences.models import UserPreferences
from tests.utils import login

pytestmark = pytest.mark.django_db


def update_preference(user_preference, name, value):
    user_preference.preferences.update({name: value})
    user_preference.save()


def test_list_categories_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    app = login(app, username='manager', password='manager')
    app.get('/manage/categories/', status=403)

    resp = app.get('/manage/', status=200)
    assert 'Categories' not in resp.text


def test_add_category(app, admin_user):
    app = login(app)
    resp = app.get('/manage/', status=200)
    resp = resp.click('Categories')
    resp = resp.click('New')
    resp.form['label'] = 'Foo bar'
    resp = resp.form.submit()
    category = Category.objects.latest('pk')
    assert resp.location.endswith('/manage/categories/')
    assert category.label == 'Foo bar'
    assert category.slug == 'foo-bar'
    assert CategorySnapshot.objects.count() == 1


def test_add_category_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    app = login(app, username='manager', password='manager')
    app.get('/manage/category/add/', status=403)


def test_edit_category(app, admin_user):
    category = Category.objects.create(label='Foo bar')

    app = login(app)
    resp = app.get('/manage/categories/', status=200)
    resp = resp.click(href='/manage/category/%s/edit/' % category.pk)
    resp.form['label'] = 'Foo bar baz'
    resp.form['slug'] = 'baz'
    resp = resp.form.submit()
    assert resp.location.endswith('/manage/categories/')
    category.refresh_from_db()
    assert category.label == 'Foo bar baz'
    assert category.slug == 'baz'
    assert CategorySnapshot.objects.count() == 1


def test_edit_category_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    category = Category.objects.create(label='Foo bar')
    app = login(app, username='manager', password='manager')
    app.get('/manage/category/%s/edit/' % category.pk, status=403)


def test_delete_category(app, admin_user):
    category = Category.objects.create(label='Foo bar')

    app = login(app)
    resp = app.get('/manage/categories/', status=200)
    resp = resp.click(href='/manage/category/%s/delete/' % category.pk)
    resp = resp.form.submit()
    assert resp.location.endswith('/manage/categories/')
    assert Category.objects.exists() is False
    assert CategorySnapshot.objects.count() == 1


def test_delete_category_as_manager(app, manager_user):
    agenda = Agenda(label='Foo Bar')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    category = Category.objects.create(label='Foo bar')
    app = login(app, username='manager', password='manager')
    app.get('/manage/category/%s/delete/' % category.pk, status=403)


def test_inspect_category(app, admin_user):
    category = Category.objects.create(label='Foo bar')

    app = login(app)

    resp = app.get('/manage/category/%s/edit/' % category.pk)
    with CaptureQueriesContext(connection) as ctx:
        resp = resp.click('Inspect')
        assert len(ctx.captured_queries) == 3


def test_category_fold_preferences(app, admin_user):
    category1 = Category.objects.create(label='Foo bar')
    category2 = Category.objects.create(label='Toto')
    pref_name1 = f'foldable-manager-category-group-{category1.id}'
    pref_name2 = f'foldable-manager-category-group-{category2.id}'

    Agenda.objects.create(label='Foo bar', category=category1)
    agenda2 = Agenda.objects.create(label='Titi', category=category2)

    app = login(app)

    resp = app.get('/manage/')

    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name1}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' not in elt[0].classes

    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name2}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' not in elt[0].classes

    user_prefs = UserPreferences.objects.get(user=admin_user)
    update_preference(user_prefs, pref_name1, True)
    resp = app.get('/manage/')

    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name1}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' in elt[0].classes

    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name2}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' not in elt[0].classes

    # Order is preserved when adding a new category : preferences are preserved
    category_temp = Category.objects.create(label='Tata0')
    category3 = Category.objects.create(label='Tata')
    pref_name3 = f'foldable-manager-category-group-{category3.id}'
    category_temp.delete()
    Agenda.objects.create(label='Titi', category=category3)

    update_preference(user_prefs, pref_name1, False)
    update_preference(user_prefs, pref_name2, True)
    resp = app.get('/manage/')

    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name1}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' not in elt[0].classes

    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name2}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' in elt[0].classes

    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name3}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' not in elt[0].classes

    # Preferences are not "shifted" when a category is deleted
    agenda2.delete()
    resp = app.get('/manage/')
    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name1}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' not in elt[0].classes

    elt = resp.pyquery.find(f'div[data-section-folded-pref-name={pref_name3}]')
    assert len(elt) == 1
    assert 'foldable' in elt[0].classes
    assert 'folded' not in elt[0].classes
