import os

import pytest
from django.core.files import File
from pyquery import PyQuery

from chrono.agendas.models import Agenda, Category, EventsType, Resource, UnavailabilityCalendar
from chrono.apps.export_import.models import Application, ApplicationElement
from tests.utils import login

pytestmark = pytest.mark.django_db

TESTS_DATA_DIR = os.path.join(os.path.dirname(__file__), '..', 'data')


@pytest.fixture
def application_with_icon():
    application = Application.objects.create(
        name='App 1',
        slug='app-1',
        version_number='1',
    )
    with open(os.path.join(TESTS_DATA_DIR, 'black.jpeg'), mode='rb') as fd:
        application.icon.save('black.jpeg', File(fd), save=True)
    return application


@pytest.fixture
def application_without_icon():
    application = Application.objects.create(
        name='App 2',
        slug='app-2',
        version_number='1',
    )
    return application


@pytest.mark.parametrize('icon', [True, False])
def test_agenda(app, admin_user, application_with_icon, application_without_icon, icon):
    if icon:
        application = application_with_icon
    else:
        application = application_without_icon

    agenda1 = Agenda.objects.create(label='Agenda 1')
    agenda2 = Agenda.objects.create(label='Agenda 2')
    ApplicationElement.objects.create(content_object=agenda2, application=application)
    agenda3 = Agenda.objects.create(label='Agenda 3')
    ApplicationElement.objects.create(content_object=agenda3, application=application)

    app = login(app)

    # no categories
    resp = app.get('/manage/')
    assert len(resp.pyquery('.section')) == 1
    assert len(resp.pyquery('.section h3')) == 0
    assert len(resp.pyquery('.section ul.objects-list li')) == 3
    assert (
        resp.pyquery('.section ul.objects-list li:nth-child(1)').text()
        == 'Events Agenda 1 [identifier: agenda-1]'
    )
    assert (
        resp.pyquery('.section ul.objects-list li:nth-child(2)').text()
        == 'Events Agenda 2 [identifier: agenda-2]'
    )
    assert (
        resp.pyquery('.section ul.objects-list li:nth-child(3)').text()
        == 'Events Agenda 3 [identifier: agenda-3]'
    )
    if icon:
        assert len(resp.pyquery('.section ul.objects-list img')) == 2
        assert len(resp.pyquery('.section ul.objects-list li:nth-child(1) img')) == 0
        assert len(resp.pyquery('.section ul.objects-list li:nth-child(2) img.application-icon')) == 1
        assert len(resp.pyquery('.section ul.objects-list li:nth-child(3) img.application-icon')) == 1
    else:
        assert len(resp.pyquery('.section ul.objects-list img')) == 0
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0
    assert 'Agendas outside applications' in resp

    # check application view
    resp = resp.click(application.name)
    assert resp.pyquery('h2').text() == application.name
    if icon:
        assert len(resp.pyquery('h2 img.application-logo')) == 1
    else:
        assert len(resp.pyquery('h2 img')) == 0
    assert len(resp.pyquery('.section ul.objects-list li')) == 2
    assert (
        resp.pyquery('.section ul.objects-list li:nth-child(1)').text()
        == 'Events Agenda 2 [identifier: agenda-2]'
    )
    assert (
        resp.pyquery('.section ul.objects-list li:nth-child(2)').text()
        == 'Events Agenda 3 [identifier: agenda-3]'
    )
    assert len(resp.pyquery('.section ul.objects-list li img')) == 0

    # check elements outside applications
    resp = app.get('/manage/')
    resp = resp.click('Agendas outside applications')
    assert resp.pyquery('h2').text() == 'Agendas outside applications'
    assert len(resp.pyquery('.section ul.objects-list li')) == 1
    assert (
        resp.pyquery('.section ul.objects-list li:nth-child(1)').text()
        == 'Events Agenda 1 [identifier: agenda-1]'
    )

    # with category
    cat = Category.objects.create(label='Cat')
    ApplicationElement.objects.create(content_object=cat, application=application)
    agenda2.category = cat
    agenda2.save()
    resp = app.get('/manage/')
    assert len(resp.pyquery('.section')) == 2
    assert PyQuery(resp.pyquery('.section')[0]).find('h3').text() == 'Cat'
    assert len(PyQuery(resp.pyquery('.section')[0]).find('ul.objects-list li')) == 1
    assert (
        PyQuery(resp.pyquery('.section')[0]).find('ul.objects-list li').text()
        == 'Events Agenda 2 [identifier: agenda-2]'
    )
    assert PyQuery(resp.pyquery('.section')[1]).find('h3').text() == 'Misc'
    assert len(PyQuery(resp.pyquery('.section')[1]).find('ul.objects-list li')) == 2
    assert (
        PyQuery(resp.pyquery('.section')[1]).find('ul.objects-list li:nth-child(1)').text()
        == 'Events Agenda 1 [identifier: agenda-1]'
    )
    assert (
        PyQuery(resp.pyquery('.section')[1]).find('ul.objects-list li:nth-child(2)').text()
        == 'Events Agenda 3 [identifier: agenda-3]'
    )
    if icon:
        assert len(PyQuery(resp.pyquery('.section')[0]).find('ul.objects-list img')) == 1
        assert (
            len(
                PyQuery(resp.pyquery('.section')[0]).find(
                    'ul.objects-list li:nth-child(1) img.application-icon'
                )
            )
            == 1
        )
        assert len(PyQuery(resp.pyquery('.section')[1]).find('ul.objects-list img')) == 1
        assert len(PyQuery(resp.pyquery('.section')[1]).find('ul.objects-list li:nth-child(1) img')) == 0
        assert (
            len(
                PyQuery(resp.pyquery('.section')[1]).find(
                    'ul.objects-list li:nth-child(2) img.application-icon'
                )
            )
            == 1
        )

    # check application view
    resp = resp.click(application.name)
    assert len(resp.pyquery('.section')) == 2
    assert PyQuery(resp.pyquery('.section')[0]).find('h3').text() == 'Cat'
    assert len(PyQuery(resp.pyquery('.section')[0]).find('ul.objects-list li')) == 1
    assert (
        PyQuery(resp.pyquery('.section')[0]).find('ul.objects-list li:nth-child(1)').text()
        == 'Events Agenda 2 [identifier: agenda-2]'
    )
    assert PyQuery(resp.pyquery('.section')[1]).find('h3').text() == 'Misc'
    assert len(PyQuery(resp.pyquery('.section')[1]).find('ul.objects-list li')) == 1
    assert (
        PyQuery(resp.pyquery('.section')[1]).find('ul.objects-list li:nth-child(1)').text()
        == 'Events Agenda 3 [identifier: agenda-3]'
    )

    # check categories
    Category.objects.create(label='Cat2')
    resp = app.get('/manage/categories/')
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    assert len(resp.pyquery('ul.objects-list li')) == 2
    resp = resp.click(application.name)
    assert len(resp.pyquery('ul.objects-list li')) == 1

    # check detail page
    resp = app.get('/manage/agendas/%s/settings' % agenda1.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0
    resp = app.get('/manage/agendas/%s/settings' % agenda2.pk)
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0

    # check visible flag
    application.visible = False
    application.save()
    resp = app.get('/manage/')
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('.section ul.objects-list img')) == 0
    app.get('/manage/?application=%s' % application.slug, status=404)
    resp = app.get('/manage/agendas/%s/settings' % agenda2.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0


@pytest.mark.parametrize('role_field', ('view_role', 'edit_role', 'admin_role'))
def test_agenda_permissions(app, manager_user, application_without_icon, role_field):
    application = application_without_icon

    agenda1 = Agenda.objects.create(label='Agenda 1')
    agenda2 = Agenda.objects.create(label='Agenda 2')
    agenda3 = Agenda.objects.create(label='Agenda 3')

    app = login(app, username='manager', password='manager')
    app.get('/manage/', status=403)

    # No agenda in application
    setattr(agenda1, role_field, manager_user.groups.all()[0])
    agenda1.save()
    resp = app.get('/manage/', status=200)
    assert 'Agendas outside applications' not in resp
    assert not resp.pyquery('h3:contains("Applications")')

    # No agenda we can access in application
    ApplicationElement.objects.create(content_object=agenda2, application=application)
    resp = app.get('/manage/', status=200)
    assert 'Agendas outside applications' not in resp
    assert not resp.pyquery('h3:contains("Applications")')

    # Agenda in application
    ApplicationElement.objects.create(content_object=agenda1, application=application)
    resp = app.get('/manage/', status=200)
    assert 'Agendas outside applications' not in resp
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name

    # Agenda in application & agenda outside application
    setattr(agenda3, role_field, manager_user.groups.all()[0])
    agenda3.save()
    resp = app.get('/manage/', status=200)
    assert 'Agendas outside applications' in resp
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name


@pytest.mark.parametrize('icon', [True, False])
def test_events_type(app, admin_user, application_with_icon, application_without_icon, icon):
    if icon:
        application = application_with_icon
    else:
        application = application_without_icon

    events_type1 = EventsType.objects.create(label='EventsType 1')
    events_type2 = EventsType.objects.create(label='EventsType 2')
    ApplicationElement.objects.create(content_object=events_type2, application=application)
    events_type3 = EventsType.objects.create(label='EventsType 3')
    ApplicationElement.objects.create(content_object=events_type3, application=application)

    app = login(app)

    resp = app.get('/manage/events-types/')
    assert len(resp.pyquery('ul.objects-list li')) == 3
    assert resp.pyquery('ul.objects-list li:nth-child(1)').text() == 'EventsType 1 [identifier: eventstype-1]'
    assert resp.pyquery('ul.objects-list li:nth-child(2)').text() == 'EventsType 2 [identifier: eventstype-2]'
    assert resp.pyquery('ul.objects-list li:nth-child(3)').text() == 'EventsType 3 [identifier: eventstype-3]'
    if icon:
        assert len(resp.pyquery('ul.objects-list img')) == 2
        assert len(resp.pyquery('ul.objects-list li:nth-child(1) img')) == 0
        assert len(resp.pyquery('ul.objects-list li:nth-child(2) img.application-icon')) == 1
        assert len(resp.pyquery('ul.objects-list li:nth-child(3) img.application-icon')) == 1
    else:
        assert len(resp.pyquery('ul.objects-list img')) == 0
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0
    assert 'Events types outside applications' in resp

    # check application view
    resp = resp.click(application.name)
    assert resp.pyquery('h2').text() == application.name
    if icon:
        assert len(resp.pyquery('h2 img.application-logo')) == 1
    else:
        assert len(resp.pyquery('h2 img')) == 0
    assert len(resp.pyquery('ul.objects-list li')) == 2
    assert resp.pyquery('ul.objects-list li:nth-child(1)').text() == 'EventsType 2 [identifier: eventstype-2]'
    assert resp.pyquery('ul.objects-list li:nth-child(2)').text() == 'EventsType 3 [identifier: eventstype-3]'
    assert len(resp.pyquery('ul.objects-list li img')) == 0

    # check elements outside applications
    resp = app.get('/manage/events-types/')
    resp = resp.click('Events types outside applications')
    assert resp.pyquery('h2').text() == 'Events types outside applications'
    assert len(resp.pyquery('ul.objects-list li')) == 1
    assert resp.pyquery('ul.objects-list li:nth-child(1)').text() == 'EventsType 1 [identifier: eventstype-1]'

    # check detail page
    resp = app.get('/manage/events-type/%s/' % events_type1.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0
    resp = app.get('/manage/events-type/%s/' % events_type2.pk)
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0

    # check visible flag
    application.visible = False
    application.save()
    resp = app.get('/manage/events-types/')
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('ul.objects-list img')) == 0
    app.get('/manage/events-types/?application=%s' % application.slug, status=404)
    resp = app.get('/manage/events-type/%s/' % events_type2.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0

    # Events types outside applications conditionnal display
    ApplicationElement.objects.create(content_object=events_type1, application=application)
    resp = app.get('/manage/events-types/')
    assert 'Events types outside applications' not in resp


@pytest.mark.parametrize('icon', [True, False])
def test_resource(app, admin_user, application_with_icon, application_without_icon, icon):
    if icon:
        application = application_with_icon
    else:
        application = application_without_icon

    resource1 = Resource.objects.create(label='Resource 1')
    resource2 = Resource.objects.create(label='Resource 2')
    ApplicationElement.objects.create(content_object=resource2, application=application)
    resource3 = Resource.objects.create(label='Resource 3')
    ApplicationElement.objects.create(content_object=resource3, application=application)

    app = login(app)

    resp = app.get('/manage/resources/')
    assert len(resp.pyquery('ul.objects-list li')) == 3
    assert resp.pyquery('ul.objects-list li:nth-child(1)').text() == 'Resource 1 (resource-1)'
    assert resp.pyquery('ul.objects-list li:nth-child(2)').text() == 'Resource 2 (resource-2)'
    assert resp.pyquery('ul.objects-list li:nth-child(3)').text() == 'Resource 3 (resource-3)'
    if icon:
        assert len(resp.pyquery('ul.objects-list img')) == 2
        assert len(resp.pyquery('ul.objects-list li:nth-child(1) img')) == 0
        assert len(resp.pyquery('ul.objects-list li:nth-child(2) img.application-icon')) == 1
        assert len(resp.pyquery('ul.objects-list li:nth-child(3) img.application-icon')) == 1
    else:
        assert len(resp.pyquery('ul.objects-list img')) == 0
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0
    assert 'Shared resources outside applications' in resp

    # check application view
    resp = resp.click(application.name)
    assert resp.pyquery('h2').text() == application.name
    if icon:
        assert len(resp.pyquery('h2 img.application-logo')) == 1
    else:
        assert len(resp.pyquery('h2 img')) == 0
    assert len(resp.pyquery('ul.objects-list li')) == 2
    assert resp.pyquery('ul.objects-list li:nth-child(1)').text() == 'Resource 2 (resource-2)'
    assert resp.pyquery('ul.objects-list li:nth-child(2)').text() == 'Resource 3 (resource-3)'
    assert len(resp.pyquery('ul.objects-list li img')) == 0

    # check elements outside applications
    resp = app.get('/manage/resources/')
    resp = resp.click('Shared resources outside applications')
    assert resp.pyquery('h2').text() == 'Shared resources outside applications'
    assert len(resp.pyquery('ul.objects-list li')) == 1
    assert resp.pyquery('ul.objects-list li:nth-child(1)').text() == 'Resource 1 (resource-1)'

    # check detail page
    resp = app.get('/manage/resource/%s/' % resource1.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0
    resp = app.get('/manage/resource/%s/' % resource2.pk)
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0

    # check visible flag
    application.visible = False
    application.save()
    resp = app.get('/manage/resources/')
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('ul.objects-list img')) == 0
    app.get('/manage/resources/?application=%s' % application.slug, status=404)
    resp = app.get('/manage/resource/%s/' % resource2.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0


@pytest.mark.parametrize('icon', [True, False])
def test_unavailability_calendar(app, admin_user, application_with_icon, application_without_icon, icon):
    if icon:
        application = application_with_icon
    else:
        application = application_without_icon

    unavailability_calendar1 = UnavailabilityCalendar.objects.create(label='UnavailabilityCalendar 1')
    unavailability_calendar2 = UnavailabilityCalendar.objects.create(label='UnavailabilityCalendar 2')
    ApplicationElement.objects.create(content_object=unavailability_calendar2, application=application)
    unavailability_calendar3 = UnavailabilityCalendar.objects.create(label='UnavailabilityCalendar 3')
    ApplicationElement.objects.create(content_object=unavailability_calendar3, application=application)

    app = login(app)

    resp = app.get('/manage/unavailability-calendars/')
    assert len(resp.pyquery('ul.objects-list li')) == 3
    assert (
        resp.pyquery('ul.objects-list li:nth-child(1)').text()
        == 'UnavailabilityCalendar 1 (unavailabilitycalendar-1)'
    )
    assert (
        resp.pyquery('ul.objects-list li:nth-child(2)').text()
        == 'UnavailabilityCalendar 2 (unavailabilitycalendar-2)'
    )
    assert (
        resp.pyquery('ul.objects-list li:nth-child(3)').text()
        == 'UnavailabilityCalendar 3 (unavailabilitycalendar-3)'
    )
    if icon:
        assert len(resp.pyquery('ul.objects-list img')) == 2
        assert len(resp.pyquery('ul.objects-list li:nth-child(1) img')) == 0
        assert len(resp.pyquery('ul.objects-list li:nth-child(2) img.application-icon')) == 1
        assert len(resp.pyquery('ul.objects-list li:nth-child(3) img.application-icon')) == 1
    else:
        assert len(resp.pyquery('ul.objects-list img')) == 0
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0
    assert 'Unavailability Calendars outside applications' in resp

    # check application view
    resp = resp.click(application.name)
    assert resp.pyquery('h2').text() == application.name
    if icon:
        assert len(resp.pyquery('h2 img.application-logo')) == 1
    else:
        assert len(resp.pyquery('h2 img')) == 0
    assert len(resp.pyquery('ul.objects-list li')) == 2
    assert (
        resp.pyquery('ul.objects-list li:nth-child(1)').text()
        == 'UnavailabilityCalendar 2 (unavailabilitycalendar-2)'
    )
    assert (
        resp.pyquery('ul.objects-list li:nth-child(2)').text()
        == 'UnavailabilityCalendar 3 (unavailabilitycalendar-3)'
    )
    assert len(resp.pyquery('ul.objects-list li img')) == 0

    # check elements outside applications
    resp = app.get('/manage/unavailability-calendars/')
    resp = resp.click('Unavailability Calendars outside applications')
    assert resp.pyquery('h2').text() == 'Unavailability Calendars outside applications'
    assert len(resp.pyquery('ul.objects-list li')) == 1
    assert (
        resp.pyquery('ul.objects-list li:nth-child(1)').text()
        == 'UnavailabilityCalendar 1 (unavailabilitycalendar-1)'
    )

    # check detail page
    resp = app.get('/manage/unavailability-calendar/%s/settings' % unavailability_calendar1.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0
    resp = app.get('/manage/unavailability-calendar/%s/settings' % unavailability_calendar2.pk)
    assert resp.pyquery('h3:contains("Applications") + .button-paragraph').text() == application.name
    if icon:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img.application-icon')) == 1
    else:
        assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph img')) == 0

    # check visible flag
    application.visible = False
    application.save()
    resp = app.get('/manage/unavailability-calendars/')
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('ul.objects-list img')) == 0
    app.get('/manage/unavailability-calendars/?application=%s' % application.slug, status=404)
    resp = app.get('/manage/unavailability-calendar/%s/settings' % unavailability_calendar2.pk)
    assert len(resp.pyquery('h3:contains("Applications")')) == 0
    assert len(resp.pyquery('h3:contains("Applications") + .button-paragraph')) == 0
