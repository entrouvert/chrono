import datetime

import pytest
from django.utils.timezone import now

from chrono.agendas.models import Agenda, Category, Desk, Event, EventsType, Resource, UnavailabilityCalendar
from chrono.apps.snapshot.models import (
    AgendaSnapshot,
    CategorySnapshot,
    EventsTypeSnapshot,
    ResourceSnapshot,
    UnavailabilityCalendarSnapshot,
)
from tests.utils import login

pytestmark = pytest.mark.django_db


def test_agenda_history(settings, app, admin_user):
    agenda = Agenda.objects.create(slug='foo', label='Foo')
    Desk.objects.create(agenda=agenda, slug='_exceptions_holder')
    snapshot1 = agenda.take_snapshot()
    Event.objects.create(
        agenda=agenda,
        places=1,
        start_datetime=now() - datetime.timedelta(days=60),
    )
    agenda.description = 'Foo Bar'
    agenda.save()
    snapshot2 = agenda.take_snapshot()
    snapshot2.application_slug = 'foobar'
    snapshot2.application_version = '42.0'
    snapshot2.save()
    assert AgendaSnapshot.objects.count() == 2

    app = login(app)
    resp = app.get('/manage/agendas/%s/settings' % agenda.pk)
    resp = resp.click('History')
    assert [x.attrib['class'] for x in resp.pyquery.find('.snapshots-list tr')] == [
        'new-day',
        'collapsed',
    ]
    assert '(Version 42.0)' in resp.pyquery('tr:nth-child(1)').text()

    for mode in ['json', 'inspect', '']:
        resp = app.get(
            '/manage/agendas/%s/history/compare/?version1=%s&version2=%s&mode=%s'
            % (agenda.pk, snapshot1.pk, snapshot2.pk, mode)
        )
        assert 'Snapshot (%s)' % (snapshot1.pk) in resp
        assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
        if mode == 'inspect':
            assert resp.text.count('<ins>') == 6
            assert resp.text.count('<del>') == 0
        else:
            assert resp.text.count('diff_sub') == 1
            assert resp.text.count('diff_add') == 18
            assert resp.text.count('diff_chg') == 0
    resp = app.get(
        '/manage/agendas/%s/history/compare/?version1=%s&version2=%s'
        % (agenda.pk, snapshot2.pk, snapshot1.pk)
    )
    assert 'Snapshot (%s)' % (snapshot1.pk) in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
    assert resp.text.count('diff_sub') == 1
    assert resp.text.count('diff_add') == 18
    assert resp.text.count('diff_chg') == 0

    # check compare on application version number
    snapshot1.application_slug = 'foobar'
    snapshot1.application_version = '41.0'
    snapshot1.save()
    # application not found
    resp = app.get(
        '/manage/agendas/%s/history/compare/?application=foobaz&version1=41.0&version2=42.0' % agenda.pk
    )
    assert resp.location == '/manage/agendas/%s/history/' % agenda.pk
    # version1 not found
    resp = app.get(
        '/manage/agendas/%s/history/compare/?application=foobar&version1=40.0&version2=42.0' % agenda.pk
    )
    assert resp.location == '/manage/agendas/%s/history/' % agenda.pk
    # version2 not found
    resp = app.get(
        '/manage/agendas/%s/history/compare/?application=foobar&version1=41.0&version2=43.0' % agenda.pk
    )
    assert resp.location == '/manage/agendas/%s/history/' % agenda.pk
    # ok
    resp = app.get(
        '/manage/agendas/%s/history/compare/?application=foobar&version1=41.0&version2=42.0' % agenda.pk
    )
    assert 'Snapshot (%s) -  (Version 41.0)' % snapshot1.pk in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % snapshot2.pk in resp

    assert AgendaSnapshot.objects.update(user=admin_user)
    admin_user.delete()
    assert AgendaSnapshot.objects.count() == 2
    assert AgendaSnapshot.objects.filter(user__isnull=True).count() == 2


def test_agenda_history_as_manager(app, manager_user):
    agenda = Agenda.objects.create(slug='foo', label='Foo')
    Desk.objects.create(agenda=agenda, slug='_exceptions_holder')
    snapshot1 = agenda.take_snapshot()
    snapshot2 = agenda.take_snapshot()

    app = login(app, username='manager', password='manager')
    agenda.view_role = manager_user.groups.all()[0]
    agenda.save()
    app.get('/manage/agendas/%s/history/' % agenda.pk, status=403)
    app.get(
        '/manage/agendas/%s/history/compare/?version1=%s&version2=%s'
        % (agenda.pk, snapshot2.pk, snapshot1.pk),
        status=403,
    )

    agenda.admin_role = manager_user.groups.all()[0]
    agenda.save()
    app.get('/manage/agendas/%s/history/' % agenda.pk, status=200)
    app.get(
        '/manage/agendas/%s/history/compare/?version1=%s&version2=%s'
        % (agenda.pk, snapshot2.pk, snapshot1.pk),
        status=200,
    )


def test_category_history(settings, app, admin_user):
    category = Category.objects.create(slug='foo', label='Foo')
    snapshot1 = category.take_snapshot()
    category.label = 'Bar'
    category.save()
    snapshot2 = category.take_snapshot()
    snapshot2.application_slug = 'foobar'
    snapshot2.application_version = '42.0'
    snapshot2.save()
    assert CategorySnapshot.objects.count() == 2

    app = login(app)
    resp = app.get('/manage/category/%s/edit/' % category.pk)
    resp = resp.click('History')
    assert [x.attrib['class'] for x in resp.pyquery.find('.snapshots-list tr')] == [
        'new-day',
        'collapsed',
    ]
    assert '(Version 42.0)' in resp.pyquery('tr:nth-child(1)').text()

    for mode in ['json', 'inspect', '']:
        resp = app.get(
            '/manage/category/%s/history/compare/?version1=%s&version2=%s&mode=%s'
            % (category.pk, snapshot1.pk, snapshot2.pk, mode)
        )
        assert 'Snapshot (%s)' % (snapshot1.pk) in resp
        assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
        if mode == 'inspect':
            assert resp.text.count('<ins>') == 1
            assert resp.text.count('<del>') == 1
        else:
            assert resp.text.count('diff_sub') == 0
            assert resp.text.count('diff_add') == 0
            assert resp.text.count('diff_chg') == 2
    resp = app.get(
        '/manage/category/%s/history/compare/?version1=%s&version2=%s'
        % (category.pk, snapshot2.pk, snapshot1.pk)
    )
    assert 'Snapshot (%s)' % (snapshot1.pk) in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
    assert resp.text.count('diff_sub') == 0
    assert resp.text.count('diff_add') == 0
    assert resp.text.count('diff_chg') == 2

    # check compare on application version number
    snapshot1.application_slug = 'foobar'
    snapshot1.application_version = '41.0'
    snapshot1.save()
    # application not found
    resp = app.get(
        '/manage/category/%s/history/compare/?application=foobaz&version1=41.0&version2=42.0' % category.pk
    )
    assert resp.location == '/manage/category/%s/history/' % category.pk
    # version1 not found
    resp = app.get(
        '/manage/category/%s/history/compare/?application=foobar&version1=40.0&version2=42.0' % category.pk
    )
    assert resp.location == '/manage/category/%s/history/' % category.pk
    # version2 not found
    resp = app.get(
        '/manage/category/%s/history/compare/?application=foobar&version1=41.0&version2=43.0' % category.pk
    )
    assert resp.location == '/manage/category/%s/history/' % category.pk
    # ok
    resp = app.get(
        '/manage/category/%s/history/compare/?application=foobar&version1=41.0&version2=42.0' % category.pk
    )
    assert 'Snapshot (%s) -  (Version 41.0)' % snapshot1.pk in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % snapshot2.pk in resp

    assert CategorySnapshot.objects.update(user=admin_user)
    admin_user.delete()
    assert CategorySnapshot.objects.count() == 2
    assert CategorySnapshot.objects.filter(user__isnull=True).count() == 2


def test_events_type_history(settings, app, admin_user):
    events_type = EventsType.objects.create(slug='foo', label='Foo')
    snapshot1 = events_type.take_snapshot()
    events_type.label = 'Bar'
    events_type.save()
    snapshot2 = events_type.take_snapshot()
    snapshot2.application_slug = 'foobar'
    snapshot2.application_version = '42.0'
    snapshot2.save()
    assert EventsTypeSnapshot.objects.count() == 2

    app = login(app)
    resp = app.get('/manage/events-type/%s/' % events_type.pk)
    resp = resp.click('History')
    assert [x.attrib['class'] for x in resp.pyquery.find('.snapshots-list tr')] == [
        'new-day',
        'collapsed',
    ]
    assert '(Version 42.0)' in resp.pyquery('tr:nth-child(1)').text()

    for mode in ['json', 'inspect', '']:
        resp = app.get(
            '/manage/events-type/%s/history/compare/?version1=%s&version2=%s&mode=%s'
            % (events_type.pk, snapshot1.pk, snapshot2.pk, mode)
        )
        assert 'Snapshot (%s)' % (snapshot1.pk) in resp
        assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
        if mode == 'inspect':
            assert resp.text.count('<ins>') == 1
            assert resp.text.count('<del>') == 1
        else:
            assert resp.text.count('diff_sub') == 0
            assert resp.text.count('diff_add') == 0
            assert resp.text.count('diff_chg') == 2
    resp = app.get(
        '/manage/events-type/%s/history/compare/?version1=%s&version2=%s'
        % (events_type.pk, snapshot2.pk, snapshot1.pk)
    )
    assert 'Snapshot (%s)' % (snapshot1.pk) in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
    assert resp.text.count('diff_sub') == 0
    assert resp.text.count('diff_add') == 0
    assert resp.text.count('diff_chg') == 2

    # check compare on application version number
    snapshot1.application_slug = 'foobar'
    snapshot1.application_version = '41.0'
    snapshot1.save()
    # application not found
    resp = app.get(
        '/manage/events-type/%s/history/compare/?application=foobaz&version1=41.0&version2=42.0'
        % events_type.pk
    )
    assert resp.location == '/manage/events-type/%s/history/' % events_type.pk
    # version1 not found
    resp = app.get(
        '/manage/events-type/%s/history/compare/?application=foobar&version1=40.0&version2=42.0'
        % events_type.pk
    )
    assert resp.location == '/manage/events-type/%s/history/' % events_type.pk
    # version2 not found
    resp = app.get(
        '/manage/events-type/%s/history/compare/?application=foobar&version1=41.0&version2=43.0'
        % events_type.pk
    )
    assert resp.location == '/manage/events-type/%s/history/' % events_type.pk
    # ok
    resp = app.get(
        '/manage/events-type/%s/history/compare/?application=foobar&version1=41.0&version2=42.0'
        % events_type.pk
    )
    assert 'Snapshot (%s) -  (Version 41.0)' % snapshot1.pk in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % snapshot2.pk in resp

    assert EventsTypeSnapshot.objects.update(user=admin_user)
    admin_user.delete()
    assert EventsTypeSnapshot.objects.count() == 2
    assert EventsTypeSnapshot.objects.filter(user__isnull=True).count() == 2


def test_resource_history(settings, app, admin_user):
    resource = Resource.objects.create(slug='foo', label='Foo')
    snapshot1 = resource.take_snapshot()
    resource.label = 'Bar'
    resource.save()
    snapshot2 = resource.take_snapshot()
    snapshot2.application_slug = 'foobar'
    snapshot2.application_version = '42.0'
    snapshot2.save()
    assert ResourceSnapshot.objects.count() == 2

    app = login(app)
    resp = app.get('/manage/resource/%s/' % resource.pk)
    resp = resp.click('History')
    assert [x.attrib['class'] for x in resp.pyquery.find('.snapshots-list tr')] == [
        'new-day',
        'collapsed',
    ]
    assert '(Version 42.0)' in resp.pyquery('tr:nth-child(1)').text()

    for mode in ['json', 'inspect', '']:
        resp = app.get(
            '/manage/resource/%s/history/compare/?version1=%s&version2=%s&mode=%s'
            % (resource.pk, snapshot1.pk, snapshot2.pk, mode)
        )
        assert 'Snapshot (%s)' % (snapshot1.pk) in resp
        assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
        if mode == 'inspect':
            assert resp.text.count('<ins>') == 1
            assert resp.text.count('<del>') == 1
        else:
            assert resp.text.count('diff_sub') == 0
            assert resp.text.count('diff_add') == 0
            assert resp.text.count('diff_chg') == 2
    resp = app.get(
        '/manage/resource/%s/history/compare/?version1=%s&version2=%s'
        % (resource.pk, snapshot2.pk, snapshot1.pk)
    )
    assert 'Snapshot (%s)' % (snapshot1.pk) in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
    assert resp.text.count('diff_sub') == 0
    assert resp.text.count('diff_add') == 0
    assert resp.text.count('diff_chg') == 2

    # check compare on application version number
    snapshot1.application_slug = 'foobar'
    snapshot1.application_version = '41.0'
    snapshot1.save()
    # application not found
    resp = app.get(
        '/manage/resource/%s/history/compare/?application=foobaz&version1=41.0&version2=42.0' % resource.pk
    )
    assert resp.location == '/manage/resource/%s/history/' % resource.pk
    # version1 not found
    resp = app.get(
        '/manage/resource/%s/history/compare/?application=foobar&version1=40.0&version2=42.0' % resource.pk
    )
    assert resp.location == '/manage/resource/%s/history/' % resource.pk
    # version2 not found
    resp = app.get(
        '/manage/resource/%s/history/compare/?application=foobar&version1=41.0&version2=43.0' % resource.pk
    )
    assert resp.location == '/manage/resource/%s/history/' % resource.pk
    # ok
    resp = app.get(
        '/manage/resource/%s/history/compare/?application=foobar&version1=41.0&version2=42.0' % resource.pk
    )
    assert 'Snapshot (%s) -  (Version 41.0)' % snapshot1.pk in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % snapshot2.pk in resp

    assert ResourceSnapshot.objects.update(user=admin_user)
    admin_user.delete()
    assert ResourceSnapshot.objects.count() == 2
    assert ResourceSnapshot.objects.filter(user__isnull=True).count() == 2


def test_unavailability_calendar_history(settings, app, admin_user):
    unavailability_calendar = UnavailabilityCalendar.objects.create(slug='foo', label='Foo')
    snapshot1 = unavailability_calendar.take_snapshot()
    unavailability_calendar.label = 'Bar'
    unavailability_calendar.save()
    snapshot2 = unavailability_calendar.take_snapshot()
    snapshot2.application_slug = 'foobar'
    snapshot2.application_version = '42.0'
    snapshot2.save()
    assert UnavailabilityCalendarSnapshot.objects.count() == 2

    app = login(app)
    resp = app.get('/manage/unavailability-calendar/%s/settings' % unavailability_calendar.pk)
    resp = resp.click('History')
    assert [x.attrib['class'] for x in resp.pyquery.find('.snapshots-list tr')] == [
        'new-day',
        'collapsed',
    ]
    assert '(Version 42.0)' in resp.pyquery('tr:nth-child(1)').text()

    for mode in ['json', 'inspect', '']:
        resp = app.get(
            '/manage/unavailability-calendar/%s/history/compare/?version1=%s&version2=%s&mode=%s'
            % (unavailability_calendar.pk, snapshot1.pk, snapshot2.pk, mode)
        )
        assert 'Snapshot (%s)' % (snapshot1.pk) in resp
        assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
        if mode == 'inspect':
            assert resp.text.count('<ins>') == 1
            assert resp.text.count('<del>') == 1
        else:
            assert resp.text.count('diff_sub') == 0
            assert resp.text.count('diff_add') == 0
            assert resp.text.count('diff_chg') == 2
    resp = app.get(
        '/manage/unavailability-calendar/%s/history/compare/?version1=%s&version2=%s'
        % (unavailability_calendar.pk, snapshot2.pk, snapshot1.pk)
    )
    assert 'Snapshot (%s)' % (snapshot1.pk) in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % (snapshot2.pk) in resp
    assert resp.text.count('diff_sub') == 0
    assert resp.text.count('diff_add') == 0
    assert resp.text.count('diff_chg') == 2

    # check compare on application version number
    snapshot1.application_slug = 'foobar'
    snapshot1.application_version = '41.0'
    snapshot1.save()
    # application not found
    resp = app.get(
        '/manage/unavailability-calendar/%s/history/compare/?application=foobaz&version1=41.0&version2=42.0'
        % unavailability_calendar.pk
    )
    assert resp.location == '/manage/unavailability-calendar/%s/history/' % unavailability_calendar.pk
    # version1 not found
    resp = app.get(
        '/manage/unavailability-calendar/%s/history/compare/?application=foobar&version1=40.0&version2=42.0'
        % unavailability_calendar.pk
    )
    assert resp.location == '/manage/unavailability-calendar/%s/history/' % unavailability_calendar.pk
    # version2 not found
    resp = app.get(
        '/manage/unavailability-calendar/%s/history/compare/?application=foobar&version1=41.0&version2=43.0'
        % unavailability_calendar.pk
    )
    assert resp.location == '/manage/unavailability-calendar/%s/history/' % unavailability_calendar.pk
    # ok
    resp = app.get(
        '/manage/unavailability-calendar/%s/history/compare/?application=foobar&version1=41.0&version2=42.0'
        % unavailability_calendar.pk
    )
    assert 'Snapshot (%s) -  (Version 41.0)' % snapshot1.pk in resp
    assert 'Snapshot (%s) -  (Version 42.0)' % snapshot2.pk in resp

    assert UnavailabilityCalendarSnapshot.objects.update(user=admin_user)
    admin_user.delete()
    assert UnavailabilityCalendarSnapshot.objects.count() == 2
    assert UnavailabilityCalendarSnapshot.objects.filter(user__isnull=True).count() == 2


def test_unavailability_calendar_history_as_manager(app, manager_user):
    unavailability_calendar = UnavailabilityCalendar.objects.create(slug='foo', label='Foo')
    snapshot1 = unavailability_calendar.take_snapshot()
    snapshot2 = unavailability_calendar.take_snapshot()

    app = login(app, username='manager', password='manager')
    unavailability_calendar.view_role = manager_user.groups.all()[0]
    unavailability_calendar.save()
    app.get('/manage/unavailability-calendar/%s/history/' % unavailability_calendar.pk, status=403)
    app.get(
        '/manage/unavailability-calendar/%s/history/compare/?version1=%s&version2=%s'
        % (unavailability_calendar.pk, snapshot2.pk, snapshot1.pk),
        status=403,
    )

    unavailability_calendar.edit_role = manager_user.groups.all()[0]
    unavailability_calendar.save()
    app.get('/manage/unavailability-calendar/%s/history/' % unavailability_calendar.pk, status=200)
    app.get(
        '/manage/unavailability-calendar/%s/history/compare/?version1=%s&version2=%s'
        % (unavailability_calendar.pk, snapshot2.pk, snapshot1.pk),
        status=200,
    )
