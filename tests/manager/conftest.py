import pytest
from django.contrib.auth.models import Group, User


@pytest.fixture
def simple_user():
    try:
        user = User.objects.get(username='user')
    except User.DoesNotExist:
        user = User.objects.create_user('user', password='user')
    return user


@pytest.fixture
def managers_group():
    group, _ = Group.objects.get_or_create(name='Managers')
    return group


@pytest.fixture
def manager_user(managers_group):
    try:
        user = User.objects.get(username='manager')
    except User.DoesNotExist:
        user = User.objects.create_user('manager', password='manager')
    user.groups.set([managers_group])
    return user


@pytest.fixture
def admin_user():
    try:
        user = User.objects.get(username='admin')
    except User.DoesNotExist:
        user = User.objects.create_superuser('admin', email=None, password='admin')
    return user


@pytest.fixture
def api_user():
    try:
        user = User.objects.get(username='api-user')
    except User.DoesNotExist:
        user = User.objects.create(
            username='john.doe', first_name='John', last_name='Doe', email='john.doe@example.net'
        )
        user.set_password('password')
        user.save()
    return user
