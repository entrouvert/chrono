import pytest

from chrono.agendas.models import Agenda, Desk, Resource

pytestmark = pytest.mark.django_db


def test_agendas_shared_resources_api(app):
    agenda = Agenda.objects.create(label='Foo bar', kind='meetings')
    resource1 = Resource.objects.create(label='Resource 1', description='Foo bar Resource 1')
    resource2 = Resource.objects.create(label='Resource 2')
    agenda.resources.add(resource1, resource2)
    resp = app.get('/api/agenda/%s/shared-resources/' % agenda.slug)
    assert resp.json == {
        'data': [
            {
                'text': 'Resource 1',
                'id': 'resource-1',
                'description': 'Foo bar Resource 1',
            },
            {
                'text': 'Resource 2',
                'id': 'resource-2',
                'description': '',
            },
        ]
    }

    legacy_resp = app.get('/api/agenda/%s/resources/' % agenda.slug)
    assert legacy_resp.json == resp.json

    # unknown
    resp = app.get('/api/agenda/xxxx/shared-resources/', status=404)

    # wrong kind
    agenda.kind = 'virtual'
    agenda.save()
    resp = app.get('/api/agenda/%s/shared-resources/' % agenda.slug, status=404)
    agenda.kind = 'events'
    agenda.save()
    resp = app.get('/api/agenda/%s/shared-resources/' % agenda.slug, status=404)


def test_agendas_partial_bookings_resources_api(app):
    agenda = Agenda.objects.create(label='Foo bar', kind='meetings', partial_bookings=True)
    Desk.objects.create(agenda=agenda, label='Big room')
    resource = Resource.objects.create(label='Projector')
    agenda.resources.add(resource)

    resp = app.get('/api/agenda/%s/resources/' % agenda.slug)
    assert resp.json == {
        'data': [
            {
                'text': 'Big room',
                'id': 'big-room',
            }
        ]
    }

    resp = app.get('/api/agenda/%s/shared-resources/' % agenda.slug)
    assert resp.json == {
        'data': [
            {
                'text': 'Projector',
                'id': 'projector',
                'description': '',
            },
        ]
    }

    # unknown
    resp = app.get('/api/agenda/xxxx/resources/', status=404)

    # wrong kind
    agenda.kind = 'virtual'
    agenda.save()
    resp = app.get('/api/agenda/%s/resources/' % agenda.slug, status=404)
    agenda.kind = 'events'
    agenda.save()
    resp = app.get('/api/agenda/%s/resources/' % agenda.slug, status=404)
