import pytest

from chrono.agendas.models import Agenda, Desk

pytestmark = pytest.mark.django_db


def test_agendas_desks_api(app):
    agenda = Agenda.objects.create(label='Foo bar', kind='meetings')
    Desk.objects.create(agenda=agenda, label='Desk 1')
    resp = app.get('/api/agenda/%s/desks/' % agenda.slug)
    assert resp.json == {
        'data': [
            {
                'text': 'Desk 1',
                'id': 'desk-1',
            }
        ]
    }

    # unknown
    resp = app.get('/api/agenda/xxxx/desks/', status=404)

    # wrong kind
    agenda.kind = 'virtual'
    agenda.save()
    resp = app.get('/api/agenda/%s/desks/' % agenda.slug, status=404)
    agenda.kind = 'events'
    agenda.save()
    resp = app.get('/api/agenda/%s/desks/' % agenda.slug, status=404)
    agenda.kind = 'meetings'
    agenda.partial_bookings = True
    agenda.save()
    resp = app.get('/api/agenda/%s/desks/' % agenda.slug, status=404)
