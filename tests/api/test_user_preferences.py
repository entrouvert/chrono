import json

import pytest
from django.urls import reverse

from chrono.apps.user_preferences.models import UserPreferences
from tests.utils import login

pytestmark = pytest.mark.django_db


def test_user_preferences_api_ok(app, admin_user):
    login(app)

    fake_id = 'fake-id-1'
    url = reverse('api-user-preferences')
    app.post_json(url, params={fake_id: True}, status=204)

    user_pref = UserPreferences.objects.get(user=admin_user)
    assert user_pref.preferences[fake_id] is True

    app.post_json(url, params={fake_id: False}, status=204)

    user_pref = UserPreferences.objects.get(user=admin_user)
    assert user_pref.preferences[fake_id] is False

    fake_id2 = 'fake-id-2'
    app.post_json(url, params={fake_id2: False}, status=204)

    user_pref = UserPreferences.objects.get(user=admin_user)
    assert user_pref.preferences[fake_id] is False
    assert user_pref.preferences[fake_id2] is False

    app.post_json(url, params={fake_id2: False}, status=204)

    user_pref = UserPreferences.objects.get(user=admin_user)
    assert user_pref.preferences[fake_id] is False
    assert user_pref.preferences[fake_id2] is False

    app.post_json(url, params={fake_id2: True}, status=204)

    user_pref = UserPreferences.objects.get(user=admin_user)
    assert user_pref.preferences[fake_id] is False
    assert user_pref.preferences[fake_id2] is True


@pytest.mark.parametrize(
    'bad_body',
    (
        json.dumps({'fake-id-1': True, 'fake-id-2': False}),
        '"not a dict"',
        '[1,2,3]',
        '{\'fake-id-1\': true',
    ),
)
def test_user_preferences_api_invalid(app, admin_user, bad_body):
    login(app)
    url = reverse('api-user-preferences')
    app.post(url, params=bad_body, status=400)


def test_user_preferences_api_large_payload(app, admin_user):
    login(app)
    url = reverse('api-user-preferences')
    app.post(url, params='a' * 1024, status=400)
    app.post_json(url, params={'b' * 1024: True}, status=400)


def test_user_preferences_api_unauthorized(app):
    url = reverse('api-user-preferences')
    app.post(url, params={'toto': True}, status=302)
