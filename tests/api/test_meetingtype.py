import pytest

from chrono.agendas.models import Agenda, MeetingType, VirtualMember

pytestmark = pytest.mark.django_db


def test_agendas_meetingtypes_api(app):
    agenda = Agenda.objects.create(label='Foo bar meeting', kind='meetings')
    MeetingType.objects.create(agenda=agenda, label='Blah', duration=30)
    MeetingType.objects.create(agenda=agenda, label='Blah 2', duration=20)
    MeetingType.objects.create(agenda=agenda, label='Blah 3', duration=10)
    resp = app.get('/api/agenda/%s/meetings/' % agenda.slug)
    expected_resp = {
        'data': [
            {
                'text': 'Blah',
                'id': 'blah',
                'duration': 30,
                'api': {
                    'datetimes_url': 'http://testserver/api/agenda/foo-bar-meeting/meetings/blah/datetimes/',
                },
            },
            {
                'text': 'Blah 2',
                'id': 'blah-2',
                'duration': 20,
                'api': {
                    'datetimes_url': 'http://testserver/api/agenda/foo-bar-meeting/meetings/blah-2/datetimes/',
                },
            },
            {
                'text': 'Blah 3',
                'id': 'blah-3',
                'duration': 10,
                'api': {
                    'datetimes_url': 'http://testserver/api/agenda/foo-bar-meeting/meetings/blah-3/datetimes/',
                },
            },
        ]
    }
    assert resp.json == expected_resp

    resp = app.get('/api/agenda/%s/meetings/' % agenda.slug, params={'exclude': 'blah-2 ,blah,unknown'})
    expected_resp2 = {
        'data': [
            {
                'text': 'Blah 3',
                'id': 'blah-3',
                'duration': 10,
                'api': {
                    'datetimes_url': 'http://testserver/api/agenda/foo-bar-meeting/meetings/blah-3/datetimes/',
                },
            },
        ]
    }
    assert resp.json == expected_resp2

    # deleted meeting type does not show up
    MeetingType.objects.create(agenda=agenda, slug='deleted-meeting-type', duration=43, deleted=True)
    resp = app.get('/api/agenda/%s/meetings/' % agenda.slug)
    assert resp.json == expected_resp

    # wrong kind
    agenda.kind = 'events'
    agenda.save()
    resp = app.get('/api/agenda/%s/meetings/' % agenda.slug, status=404)

    # unknown
    resp = app.get('/api/agenda/xxxx/meetings/', status=404)


def test_agendas_meetingtype_api(app):
    agenda = Agenda.objects.create(label='Foo bar meeting', kind='meetings')
    MeetingType.objects.create(agenda=agenda, label='Blah', duration=30)
    resp = app.get('/api/agenda/%s/meetings/blah/' % agenda.slug)
    assert resp.json == {
        'data': {
            'text': 'Blah',
            'id': 'blah',
            'duration': 30,
            'api': {
                'datetimes_url': 'http://testserver/api/agenda/foo-bar-meeting/meetings/blah/datetimes/',
            },
        }
    }

    # unknown
    resp = app.get('/api/agenda/xxxx/meetings/', status=404)
    resp = app.get('/api/agenda/%s/meetings/xxxx/' % agenda.slug, status=404)

    # wrong kind
    agenda.kind = 'events'
    agenda.save()
    resp = app.get('/api/agenda/%s/meetings/' % agenda.slug, status=404)


def test_virtual_agendas_meetingtypes_api(app):
    virt_agenda = Agenda.objects.create(label='Virtual agenda', kind='virtual')

    # No meetings because no real agenda
    resp = app.get('/api/agenda/%s/meetings/' % virt_agenda.slug)
    assert resp.json == {'data': []}

    # One real agenda : every meetings exposed
    foo_agenda = Agenda.objects.create(label='Foo', kind='meetings')
    MeetingType.objects.create(agenda=foo_agenda, label='Meeting1', duration=30)
    MeetingType.objects.create(agenda=foo_agenda, label='Meeting2', duration=15)
    VirtualMember.objects.create(virtual_agenda=virt_agenda, real_agenda=foo_agenda)
    resp = app.get('/api/agenda/%s/meetings/' % virt_agenda.slug)
    assert resp.json == {
        'data': [
            {
                'text': 'Meeting1',
                'id': 'meeting1',
                'duration': 30,
                'api': {
                    'datetimes_url': 'http://testserver/api/agenda/virtual-agenda/meetings/meeting1/datetimes/',
                },
            },
            {
                'text': 'Meeting2',
                'id': 'meeting2',
                'duration': 15,
                'api': {
                    'datetimes_url': 'http://testserver/api/agenda/virtual-agenda/meetings/meeting2/datetimes/',
                },
            },
        ]
    }

    resp = app.get('/api/agenda/%s/meetings/' % virt_agenda.slug, params={'exclude': 'unknown, meeting2'})
    assert resp.json == {
        'data': [
            {
                'text': 'Meeting1',
                'id': 'meeting1',
                'duration': 30,
                'api': {
                    'datetimes_url': 'http://testserver/api/agenda/virtual-agenda/meetings/meeting1/datetimes/',
                },
            },
        ]
    }

    # Several real agendas

    bar_agenda = Agenda.objects.create(label='Bar', kind='meetings')
    MeetingType.objects.create(agenda=bar_agenda, label='Meeting Bar', duration=30)
    VirtualMember.objects.create(virtual_agenda=virt_agenda, real_agenda=bar_agenda)

    # Bar agenda has no meeting type: no meetings exposed
    resp = app.get('/api/agenda/%s/meetings/' % virt_agenda.slug)
    assert resp.json == {'data': []}

    # Bar agenda has a meetings wih different label, slug, duration: no meetings exposed
    mt = MeetingType.objects.create(agenda=bar_agenda, label='Meeting Type Bar', duration=15)
    resp = app.get('/api/agenda/%s/meetings/' % virt_agenda.slug)
    assert resp.json == {'data': []}

    # Bar agenda has a meetings wih same label, but different slug and duration: no meetings exposed
    mt.label = 'Meeting1'
    mt.save()
    resp = app.get('/api/agenda/%s/meetings/' % virt_agenda.slug)
    assert resp.json == {'data': []}

    # Bar agenda has a meetings wih same label and slug, but different duration: no meetings exposed
    mt.slug = 'meeting1'
    mt.save()
    resp = app.get('/api/agenda/%s/meetings/' % virt_agenda.slug)
    assert resp.json == {'data': []}

    # Bar agenda has a meetings wih same label, slug and duration: only this meeting exposed
    mt.duration = 30
    mt.save()
    resp = app.get('/api/agenda/%s/meetings/' % virt_agenda.slug)
    assert resp.json == {
        'data': [
            {
                'text': 'Meeting1',
                'id': 'meeting1',
                'duration': 30,
                'api': {
                    'datetimes_url': 'http://testserver/api/agenda/virtual-agenda/meetings/meeting1/datetimes/',
                },
            },
        ]
    }
