from contextlib import contextmanager

import pytest
from rest_framework import permissions

from chrono.agendas.models import Agenda
from chrono.api.views import AgendaAPI, Duplicate
from chrono.utils.permissions import ReadOnly

pytestmark = pytest.mark.django_db


@pytest.fixture
def mock_view_perm(monkeypatch):
    @contextmanager
    def _mock_view_perm(views, permitted, or_readonly):
        class FakePerm(permissions.BasePermission):
            def has_permission(self, request, view):
                return permitted

        for view in views:
            perm = (FakePerm | ReadOnly) if or_readonly else FakePerm
            monkeypatch.setattr(view, 'permission_classes', (perm,))
        yield None

    return _mock_view_perm


@pytest.fixture
def test_agenda():
    agenda = Agenda.objects.create(
        label='Foo bar Meeting', kind='meetings', minimal_booking_delay=1, maximal_booking_delay=56
    )
    agenda.save()
    return agenda


@pytest.mark.parametrize('permitted,write_status', ([True, 200], [False, 401]))
def test_apiclient_permission_or_readonly(app, test_agenda, mock_view_perm, permitted, write_status):
    agenda_slug = test_agenda.slug

    with mock_view_perm((AgendaAPI, Duplicate), permitted, True):
        # GET is always permitted with or_readonly
        app.get('/api/agenda/%s/' % agenda_slug, status=200)
        app.post('/api/agenda/%s/duplicate/' % agenda_slug, status=write_status)
        app.delete('/api/agenda/%s/' % agenda_slug, status=write_status)


@pytest.mark.parametrize('permitted,status', ([True, 200], [False, 401]))
def test_apiclient_permission_no_readonly(app, test_agenda, mock_view_perm, permitted, status):
    agenda_slug = test_agenda.slug

    with mock_view_perm((AgendaAPI, Duplicate), permitted, False):
        app.get('/api/agenda/%s/' % agenda_slug, status=status)
        app.post('/api/agenda/%s/duplicate/' % agenda_slug, status=status)
        app.delete('/api/agenda/%s/' % agenda_slug, status=status)
