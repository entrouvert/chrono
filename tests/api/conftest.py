import datetime

import pytest
from django.contrib.auth import get_user_model

from chrono.agendas.models import Agenda, Desk, Event, MeetingType, TimePeriod, VirtualMember
from chrono.utils.timezone import localtime, make_aware, now

User = get_user_model()


@pytest.fixture
def user():
    user = User.objects.create(
        username='john.doe', first_name='John', last_name='Doe', email='john.doe@example.net'
    )
    user.set_password('password')
    user.save()
    return user


@pytest.fixture
def admin_user():
    try:
        user = User.objects.get(username='admin')
    except User.DoesNotExist:
        user = User.objects.create_superuser('admin', email=None, password='admin')
    return user


@pytest.fixture(params=['Europe/Brussels', 'Asia/Kolkata', 'America/Sao_Paulo'])
def time_zone(request, settings):
    settings.TIME_ZONE = request.param


# 2017-05-20 -> saturday
@pytest.fixture(
    params=[
        datetime.datetime(year=2017, month=5, day=20, hour=1, minute=12),
        datetime.datetime(year=2017, month=5, day=20, hour=11, minute=42),
        datetime.datetime(year=2017, month=5, day=20, hour=23, minute=17),
    ]
)
def mock_now(request, freezer, time_zone):
    aware_datetime = make_aware(request.param)
    freezer.move_to(aware_datetime)
    return aware_datetime


@pytest.fixture
def some_data(mock_now):
    agenda = Agenda(label='Foo bar')
    agenda.save()
    first_date = localtime(now()).replace(hour=17, minute=0, second=0, microsecond=0)
    first_date += datetime.timedelta(days=1)
    for i in range(3):
        event = Event(start_datetime=first_date + datetime.timedelta(days=i), places=20, agenda=agenda)
        event.save()

    agenda2 = Agenda(label='Foo bar 2')
    agenda2.save()
    first_date = localtime(now()).replace(hour=20, minute=0, second=0, microsecond=0)
    first_date += datetime.timedelta(days=1)
    for i in range(2):
        event = Event(start_datetime=first_date + datetime.timedelta(days=i), places=20, agenda=agenda2)
        event.save()

    # a date in the past
    event = Event(start_datetime=first_date - datetime.timedelta(days=10), places=10, agenda=agenda)
    event.save()


@pytest.fixture
def meetings_agenda(mock_now):
    agenda = Agenda(
        label='Foo bar Meeting', kind='meetings', minimal_booking_delay=1, maximal_booking_delay=56
    )
    agenda.save()
    meeting_type = MeetingType(agenda=agenda, label='Blah', duration=30)
    meeting_type.save()

    test_1st_weekday = (localtime(now()).weekday() + 2) % 7
    test_2nd_weekday = (localtime(now()).weekday() + 3) % 7

    default_desk, _ = Desk.objects.get_or_create(agenda=agenda, label='Desk 1')

    time_period = TimePeriod(
        weekday=test_1st_weekday,
        start_time=datetime.time(10, 0),
        end_time=datetime.time(12, 0),
        desk=default_desk,
    )
    time_period.save()
    time_period = TimePeriod(
        weekday=test_2nd_weekday,
        start_time=datetime.time(10, 0),
        end_time=datetime.time(17, 0),
        desk=default_desk,
    )
    time_period.save()
    return agenda


@pytest.fixture
def virtual_meetings_agenda(meetings_agenda):
    agenda = Agenda.objects.create(label='Virtual Agenda', kind='virtual')
    VirtualMember.objects.create(virtual_agenda=agenda, real_agenda=meetings_agenda)
    return agenda
