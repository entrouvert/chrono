import datetime

import pytest
from django.core.files.base import ContentFile

from chrono.agendas.models import Person, SharedCustodyAgenda, SharedCustodySettings, UnavailabilityCalendar
from chrono.utils.timezone import now

pytestmark = pytest.mark.django_db


with open('tests/data/holidays.ics') as f:
    ICS_HOLIDAYS = f.read()


def test_add_shared_custody_agenda(app, user, settings):
    app.authorization = ('Basic', ('john.doe', 'password'))

    params = {
        'guardian_first_name': 'John',
        'guardian_last_name': 'Doe',
        'guardian_id': 'xxx',
        'other_guardian_first_name': 'Jane',
        'other_guardian_last_name': 'Doe',
        'other_guardian_id': 'yyy',
        'child_first_name': 'James',
        'child_last_name': 'Doe',
        'child_id': 'zzz',
        'date_start': '2020-10-20',
    }
    resp = app.post_json('/api/shared-custody/', params=params)

    first_guardian = Person.objects.get(user_external_id='xxx', first_name='John', last_name='Doe')
    second_guardian = Person.objects.get(user_external_id='yyy', first_name='Jane', last_name='Doe')
    child = Person.objects.get(user_external_id='zzz', first_name='James', last_name='Doe')

    agenda = SharedCustodyAgenda.objects.get()
    assert agenda.first_guardian == first_guardian
    assert agenda.second_guardian == second_guardian
    assert agenda.child == child

    assert resp.json['data'] == {
        'id': agenda.pk,
        'settings_url': 'http://testserver/manage/shared-custody/%s/settings/' % agenda.pk,
        'backoffice_url': 'http://testserver/manage/shared-custody/%s/' % agenda.pk,
    }

    # cannot add agenda without date_end
    params['date_start'] = '2019-01-01'
    resp = app.post_json('/api/shared-custody/', params=params, status=400)
    assert resp.json['errors']['non_field_errors'][0] == (
        'Invalid date_start/date_end, agenda would overlap with http://testserver/manage/shared-custody/%s/.'
        % agenda.pk
    )

    # cannot add agenda with date_end after existing agenda date_start
    params['date_start'] = '2019-01-01'
    params['date_end'] = '2020-11-01'
    resp = app.post_json('/api/shared-custody/', params=params, status=400)
    assert resp.json['errors']['non_field_errors'][0] == (
        'Invalid date_start/date_end, agenda would overlap with http://testserver/manage/shared-custody/%s/.'
        % agenda.pk
    )

    # can add agenda with date_end before existing agenda date_start
    params['date_start'] = '2019-01-01'
    params['date_end'] = '2020-01-01'
    resp = app.post_json('/api/shared-custody/', params=params)
    assert resp.json['err'] == 0
    new_agenda_id = resp.json['data']['id']

    # cannot add agenda that would overlap previous one
    params['date_start'] = '2018-01-01'
    params['date_end'] = '2019-01-01'
    resp = app.post_json('/api/shared-custody/', params=params, status=400)
    assert resp.json['errors']['non_field_errors'][0] == (
        'Invalid date_start/date_end, agenda would overlap with http://testserver/manage/shared-custody/%s/.'
        % new_agenda_id
    )

    params['date_start'] = '2020-01-01'
    params['date_end'] = '2020-02-01'
    resp = app.post_json('/api/shared-custody/', params=params, status=400)
    assert resp.json['errors']['non_field_errors'][0] == (
        'Invalid date_start/date_end, agenda would overlap with http://testserver/manage/shared-custody/%s/.'
        % new_agenda_id
    )

    params['date_start'] = '2021-02-01'
    params['date_end'] = '2021-01-01'
    resp = app.post_json('/api/shared-custody/', params=params, status=400)
    assert resp.json['errors']['non_field_errors'][0] == 'date_start must be before date_end'

    # different child, no overlap check
    params = {
        'guardian_first_name': 'John',
        'guardian_last_name': 'Doe',
        'guardian_id': 'xxx',
        'other_guardian_first_name': 'Other',
        'other_guardian_last_name': 'Doe',
        'other_guardian_id': 'other',
        'child_first_name': 'Bruce',
        'child_last_name': 'Doe',
        'child_id': 'bruce',
        'date_start': '2020-10-20',
    }
    resp = app.post_json('/api/shared-custody/', params=params)
    assert resp.json['data']['id'] != agenda.pk
    assert SharedCustodyAgenda.objects.filter(first_guardian=first_guardian).count() == 3


def test_add_shared_custody_agenda_with_rules(app, user, settings):
    app.authorization = ('Basic', ('john.doe', 'password'))

    # configure holidays
    unavailability_calendar = UnavailabilityCalendar.objects.create(label='Calendar')
    SharedCustodySettings.objects.create(holidays_calendar=unavailability_calendar)
    source = unavailability_calendar.timeperiodexceptionsource_set.create(
        ics_filename='holidays.ics', ics_file=ContentFile(ICS_HOLIDAYS, name='holidays.ics')
    )
    source.refresh_timeperiod_exceptions_from_ics()

    params = {
        'guardian_first_name': 'John',
        'guardian_last_name': 'Doe',
        'guardian_id': 'xxx',
        'other_guardian_first_name': 'Jane',
        'other_guardian_last_name': 'Doe',
        'other_guardian_id': 'yyy',
        'child_first_name': 'James',
        'child_last_name': 'Doe',
        'child_id': 'zzz',
        'date_start': '2020-10-20',
    }

    resp = app.post_json('/api/shared-custody/', params={'weeks': '', **params})
    agenda = SharedCustodyAgenda.objects.get(pk=resp.json['data']['id'])
    assert not agenda.is_complete()
    assert not agenda.rules.exists()
    agenda.delete()

    resp = app.post_json('/api/shared-custody/', params={'weeks': 'even', **params})
    agenda = SharedCustodyAgenda.objects.get(pk=resp.json['data']['id'])
    assert agenda.is_complete()
    assert agenda.rules.filter(guardian__first_name='John', weeks='even').exists()

    jane_rule = agenda.rules.get(guardian__first_name='Jane', weeks='odd')
    assert jane_rule.days == list(range(1, 8))

    agenda.delete()

    resp = app.post_json('/api/shared-custody/', params={'weeks': 'odd', **params})
    agenda = SharedCustodyAgenda.objects.get(pk=resp.json['data']['id'])
    assert agenda.is_complete()
    assert agenda.rules.filter(guardian__first_name='John', weeks='odd').exists()
    assert agenda.rules.filter(guardian__first_name='Jane', weeks='even').exists()
    agenda.delete()

    resp = app.post_json(
        '/api/shared-custody/', params={'christmas_holidays:periodicity': 'first-half', **params}
    )
    agenda = SharedCustodyAgenda.objects.get(pk=resp.json['data']['id'])
    assert agenda.holiday_rules.filter(
        guardian__first_name='John', holiday__slug='christmas_holidays', periodicity='first-half', years=''
    ).exists()
    assert agenda.holiday_rules.filter(
        guardian__first_name='Jane', holiday__slug='christmas_holidays', periodicity='second-half', years=''
    ).exists()
    assert agenda.periods.count() == 12
    agenda.delete()

    resp = app.post_json(
        '/api/shared-custody/',
        params={
            'summer_holidays:periodicity': 'first-and-third-quarters',
            'summer_holidays:years': 'odd',
            **params,
        },
    )
    agenda = SharedCustodyAgenda.objects.get(pk=resp.json['data']['id'])
    assert agenda.holiday_rules.filter(
        guardian__first_name='John',
        holiday__slug='summer_holidays',
        periodicity='first-and-third-quarters',
        years='odd',
    ).exists()
    assert agenda.holiday_rules.filter(
        guardian__first_name='John',
        holiday__slug='summer_holidays',
        periodicity='second-and-fourth-quarters',
        years='even',
    ).exists()
    assert agenda.holiday_rules.filter(
        guardian__first_name='Jane',
        holiday__slug='summer_holidays',
        periodicity='first-and-third-quarters',
        years='even',
    ).exists()
    assert agenda.holiday_rules.filter(
        guardian__first_name='Jane',
        holiday__slug='summer_holidays',
        periodicity='second-and-fourth-quarters',
        years='odd',
    ).exists()
    assert agenda.periods.count() == 20
    agenda.delete()

    # unknown holiday
    resp = app.post_json(
        '/api/shared-custody/', params={'unknown:periodicity': 'first-half', **params}, status=400
    )
    assert resp.json['errors']['non_field_errors'][0] == 'Unknown holiday: unknown'

    # unknown holiday param
    resp = app.post_json(
        '/api/shared-custody/', params={'summer_holidays:unknown': 'first-half', **params}, status=400
    )
    assert (
        resp.json['errors']['non_field_errors'][0] == 'Unknown parameter for holiday summer_holidays: unknown'
    )

    # years without periodicity
    resp = app.post_json(
        '/api/shared-custody/', params={'summer_holidays:years': 'even', **params}, status=400
    )
    assert resp.json['errors']['non_field_errors'][0] == 'Missing periodicity for holiday: summer_holidays'

    # quarters with short holiday
    resp = app.post_json(
        '/api/shared-custody/',
        params={'christmas_holidays:periodicity': 'first-and-third-quarters', **params},
        status=400,
    )
    assert resp.json['errors']['non_field_errors'][0] == 'Short holidays cannot be cut into quarters.'


def test_shared_custody_agenda_update_dates(app, user, settings):
    father = Person.objects.create(user_external_id='father_id', first_name='John', last_name='Doe')
    mother = Person.objects.create(user_external_id='mother_id', first_name='Jane', last_name='Doe')
    child = Person.objects.create(user_external_id='child_id', first_name='James', last_name='Doe')
    agenda = SharedCustodyAgenda.objects.create(
        first_guardian=father, second_guardian=mother, child=child, date_start=now()
    )

    app.authorization = ('Basic', ('john.doe', 'password'))
    resp = app.patch_json('/api/shared-custody/%s/' % agenda.pk, params={'date_start': '2020-10-20'})
    assert resp.json['err'] == 0

    agenda.refresh_from_db()
    assert agenda.date_start == datetime.date(year=2020, month=10, day=20)
    assert agenda.date_end is None

    resp = app.patch_json(
        '/api/shared-custody/%s/' % agenda.pk, params={'date_start': '2020-10-20', 'date_end': '2021-01-01'}
    )
    assert resp.json['err'] == 0

    agenda.refresh_from_db()
    assert agenda.date_start == datetime.date(year=2020, month=10, day=20)
    assert agenda.date_end == datetime.date(year=2021, month=1, day=1)

    resp = app.patch_json(
        '/api/shared-custody/%s/' % agenda.pk, params={'date_start': '2021-01-01', 'date_end': '2022-01-01'}
    )
    assert resp.json['err'] == 0

    agenda.refresh_from_db()
    assert agenda.date_start == datetime.date(year=2021, month=1, day=1)
    assert agenda.date_end == datetime.date(year=2022, month=1, day=1)

    resp = app.patch_json(
        '/api/shared-custody/%s/' % agenda.pk, params={'date_start': '2021-01-01', 'date_end': None}
    )
    assert resp.json['err'] == 0

    agenda.refresh_from_db()
    assert agenda.date_start == datetime.date(year=2021, month=1, day=1)
    assert agenda.date_end is None

    SharedCustodyAgenda.objects.create(
        first_guardian=father,
        second_guardian=mother,
        child=child,
        date_start=datetime.date(year=2020, month=1, day=1),
        date_end=datetime.date(year=2020, month=6, day=1),
    )

    resp = app.patch_json(
        '/api/shared-custody/%s/' % agenda.pk, params={'date_start': '2020-01-01'}, status=400
    )
    assert 'overlap' in resp.json['errors']['non_field_errors'][0]

    resp = app.patch_json(
        '/api/shared-custody/%s/' % agenda.pk,
        params={'date_start': '2020-10-20', 'date_end': '2019-10-20'},
        status=400,
    )
    assert resp.json['errors']['non_field_errors'][0] == 'date_start must be before date_end'

    resp = app.patch_json('/api/shared-custody/%s/' % agenda.pk, params={'first_guardian': 'xxx'}, status=400)
    app.patch_json('/api/shared-custody/%s/' % agenda.pk, params={}, status=400)

    agenda.delete()
    app.patch_json('/api/shared-custody/1/', params={'date_start': '2020-10-20'}, status=404)
