import datetime

import pytest

from chrono.agendas.models import Agenda, Subscription
from chrono.api.serializers import AgendaOrSubscribedSlugsSerializer
from chrono.utils.timezone import now

pytestmark = pytest.mark.django_db


def test_subscribed_with_dates():
    agenda = Agenda.objects.create(label='foobar', kind='events')
    Subscription.objects.create(
        agenda=agenda,
        user_external_id='xxx',
        date_start=now().date(),
        date_end=now().date() + datetime.timedelta(days=30),
    )

    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]

    # start: today
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': now().date().isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': now().isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]

    # start: before subscription
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() - datetime.timedelta(days=1)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]

    # start: included in subscription's period
    for days in [1, 29]:
        serializer = AgendaOrSubscribedSlugsSerializer(
            data={
                'user_external_id': 'xxx',
                'subscribed': 'all',
                'date_start': (now().date() + datetime.timedelta(days=days)).isoformat(),
            }
        )
        assert serializer.is_valid()
        assert list(serializer.validated_data['agendas']) == [agenda]

    # start: after subscription
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() + datetime.timedelta(days=30)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == []

    # end: today
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_end': now().date().isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_end': now().isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]

    # end: before subscription
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_end': (now().date() - datetime.timedelta(days=1)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == []

    # end: included in subscription's period
    for days in [1, 30]:
        serializer = AgendaOrSubscribedSlugsSerializer(
            data={
                'user_external_id': 'xxx',
                'subscribed': 'all',
                'date_end': (now().date() + datetime.timedelta(days=days)).isoformat(),
            }
        )
        assert serializer.is_valid()
        assert list(serializer.validated_data['agendas']) == [agenda]

    # end: after subscription
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_end': (now().date() + datetime.timedelta(days=31)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]

    # mix: start and end included in subscription's period
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': now().date().isoformat(),
            'date_end': (now().date() + datetime.timedelta(days=30)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() + datetime.timedelta(days=10)).isoformat(),
            'date_end': (now().date() + datetime.timedelta(days=20)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]

    # mix: start and end overlaps subscription's period
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() - datetime.timedelta(days=10)).isoformat(),
            'date_end': (now().date() + datetime.timedelta(days=40)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() - datetime.timedelta(days=10)).isoformat(),
            'date_end': (now().date() + datetime.timedelta(days=10)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() - datetime.timedelta(days=10)).isoformat(),
            'date_end': (now().date() + datetime.timedelta(days=0)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() + datetime.timedelta(days=20)).isoformat(),
            'date_end': (now().date() + datetime.timedelta(days=40)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == [agenda]
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() + datetime.timedelta(days=30)).isoformat(),
            'date_end': (now().date() + datetime.timedelta(days=40)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == []

    # mix: start and end out of subscription's period
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() - datetime.timedelta(days=10)).isoformat(),
            'date_end': (now().date() - datetime.timedelta(days=1)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == []
    serializer = AgendaOrSubscribedSlugsSerializer(
        data={
            'user_external_id': 'xxx',
            'subscribed': 'all',
            'date_start': (now().date() + datetime.timedelta(days=31)).isoformat(),
            'date_end': (now().date() + datetime.timedelta(days=40)).isoformat(),
        }
    )
    assert serializer.is_valid()
    assert list(serializer.validated_data['agendas']) == []
