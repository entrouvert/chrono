import os

TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'en-us'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': ['rest_framework.authentication.BasicAuthentication'],
    'EXCEPTION_HANDLER': 'chrono.api.utils.exception_handler',
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'TEST': {
            'NAME': 'chrono-test-%s' % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:45],
        },
    }
}

KNOWN_SERVICES = {
    'wcs': {
        'default': {
            'title': 'test',
            'url': 'http://example.org',
            'secret': 'chrono',
            'orig': 'chrono',
            'backoffice-menu-url': 'http://example.org/manage/',
        }
    },
    'lingo': {
        'default': {
            'title': 'test',
            'url': 'http://lingo.example.org',
            'secret': 'chrono',
            'orig': 'chrono',
            'backoffice-menu-url': 'http://example.org/manage/',
        }
    },
}

LEGACY_URLS_MAPPING = {'old.org': 'new.org'}

EXCEPTIONS_SOURCES = {}

SITE_BASE_URL = 'https://example.com'

SHARED_CUSTODY_ENABLED = True

PASSWORD_HASHERS = ['django.contrib.auth.hashers.MD5PasswordHasher']
