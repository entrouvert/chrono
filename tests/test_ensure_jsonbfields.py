import pytest
from django.core.management import call_command
from django.db import connection

pytestmark = pytest.mark.django_db


def test_ensure_jsonb_fields():
    json_fields = (
        'extra_data',
        'booking_errors',
    )

    with connection.cursor() as cursor:
        query = '''SELECT table_name, column_name, data_type
                     FROM information_schema.columns
                    WHERE column_name IN %(json_fields)s'''
        cursor.execute(query, {'json_fields': json_fields})

        # make sure the data_type is correct
        for line in cursor.fetchall():
            assert line[2] == 'jsonb'

        # alter columns
        cursor.execute(
            '''ALTER TABLE agendas_booking
              ALTER COLUMN extra_data TYPE text USING extra_data::text'''
        )
        cursor.execute(
            '''ALTER TABLE agendas_eventcancellationreport
              ALTER COLUMN booking_errors TYPE text USING booking_errors::text'''
        )

    call_command('ensure_jsonb')

    with connection.cursor() as cursor:
        query = '''SELECT table_name, column_name, data_type
                     FROM information_schema.columns
                    WHERE column_name IN %(json_fields)s'''
        cursor.execute(query, {'json_fields': json_fields})

        # check the data_type is correct
        for line in cursor.fetchall():
            assert line[2] == 'jsonb'
